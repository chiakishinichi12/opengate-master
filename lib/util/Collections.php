<?php

namespace lib\util;

use ArrayAccess;
use ArrayIterator;
use Closure;
use Countable;
use InvalidArgumentException;
use IteratorAggregate;
use lib\iterators\Cursor;
use lib\util\collections\BinarySearching;
use lib\util\collections\CollectionItem;
use lib\util\exceptions\CollectionTypeException;
use lib\util\exceptions\CursorIndexOutOfBoundsException;
use lib\util\paginators\CollectionsPaginator;
use lib\inner\App;
use lib\util\paginators\Paginateable;

/**
 * 
 * @author Jiya Sama
 *
 */
class Collections implements IteratorAggregate, Countable, Cursor, ArrayAccess, Paginateable {
    
    /**
     * 
     * 
     * @var array|Collections
     */
    protected $items;
    
    /**
     * 
     * @var string
     */
    protected $onlyAccepts = "";
    
    /**
     * 
     * @var string
     */
    protected $wasAccepting = "";
    
    /**
     * 
     * @var boolean
     */
    protected $readable = false;
    
    /**
     * 
     * @param array $items
     * @param string $onlyAccepts
     * 
     */
    protected function __construct(array $items, string $onlyAccepts = null){  
        $this->onlyAccepts = $onlyAccepts;
        
        $this->items = $items;
    }
    
    /**
     * 
     * @throws CollectionTypeException
     */
    protected function strictlyCheckTypes(){
        if(is_blank($this->onlyAccepts)){
            return;
        }
        
        foreach($this->items as $item){
            $this->checkItemType($item);
        }
    }
    
    /**
     * 
     * suspends filtering value types
     * 
     * when returned false, the reason could be
     * 
     * (1) The list still accepts various types
     * (2) No 'single type' was defined
     * 
     */
    public function liftTypeRestriction(){
        if(is_blank($this->onlyAccepts)){
            if(!is_blank($this->wasAccepting)){
                // "The list still accepts various types"
                return false;
            }else{
                // No 'single type' was defined
                return false;
            }
        }
        
        $this->wasAccepting = $this->onlyAccepts;
        
        $this->onlyAccepts = "";
        
        return true;
    }
    
    /**
     * 
     * @throws CollectionTypeException
     * 
     * when returned false, the reason could be
     * 
     * (1) The list still restricts various types other than the specified single type
     * (2) No 'single type' was defined
     * 
     */
    public function enableTypeRestriction(){
        if(is_blank($this->wasAccepting)){
            if(!is_blank($this->onlyAccepts)){
                // The list still restricts various types other than the specified single type
                return false;
            }else{
                // No 'single type' was defined
                return false;
            }
        }
        
        // reverting the value back to onlyAccepts
        $this->onlyAccepts = $this->wasAccepting;
        $this->wasAccepting = "";
        
        return true;
    }
    
    /**
     * 
     * @var integer
     */
    protected $cursorPointer = -1;
    
    /**
     * 
     * @param mixed $item
     * 
     * @throws CollectionTypeException
     */
    protected function checkItemType(mixed $item){
        if(is_object($item) && class_exists($this->onlyAccepts)){            
            if(strcmp($class = class_name($item), $this->onlyAccepts) !== 0){
                if(!is_subclass_of($item, $this->onlyAccepts)){
                    throw new CollectionTypeException("an element is not an instanceof '{$this->onlyAccepts}' [{$class} found]");
                }
            }
        }else{
            if(strcasecmp($type = gettype($item), $this->onlyAccepts) !== 0){
                throw new CollectionTypeException("an element which is not a type of '{$this->onlyAccepts}' found. [{$type}]");
            }
        }
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see ArrayAccess::offsetGet()
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset){        
        return $this->itemAt($offset);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see ArrayAccess::offsetSet()
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value){        
        if(!is_blank($this->onlyAccepts)){
            $this->checkItemType($value);
        }
        
        $this->items[$offset] = $value;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see ArrayAccess::offsetUnset()
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset){
        $this->remove($offset);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see ArrayAccess::offsetExists()
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset){
        return $this->hasKey($offset);
    }
    
    /**
     * 
     * it simplay has a same function with add but the index can be any other type
     * 
     * 
     * @param string $index
     * @param mixed $item
     * 
     * @return \lib\util\Collections
     */
    public function put(mixed $index, mixed $item){
        if(!is_blank($this->onlyAccepts)){
            $this->checkItemType($item);
        }
        
        $this->items[$index] = $item;
        
        return $this;
    }
    
    /**
     * 
     * @param mixed $item
     * @return \lib\util\Collections
     */
    public function add(mixed $item){
        if(!is_blank($this->onlyAccepts)){
            $this->checkItemType($item);
        }
        
        $this->items[] = $item; 
        
        return $this;
    }
    
    /**
     * 
     * prepends data on the collection stack
     * 
     * @param array|mixed $mutableArgs
     */
    public function prepend(...$mutableArgs){
        $this->warpMutableArgs($mutableArgs);
        
        $this->items = array_merge($mutableArgs, $this->items);
        
        return $this;
    }
    
    /**
     * 
     * this works like a merge but it strictly expects array or collection on its 1st argument
     * 
     * @param array|Collections $items
     * 
     * @return Collections
     */
    public function addAll($items){
        if(!(is_array($items) || ($items instanceof Collections))){
            throw new InvalidArgumentException("Argument #1: value must be an array or an instance of Collections. "
                .class_name($items)." found");
        }
        
        $this->merge($items);
        
        return $this;
    }
    
    /**
     * 
     * @param string $arg
     * @return mixed[]|unknown[]|array[]
     */
    public function valueContains(string $arg){
        $indices = [];
        
        foreach($this->items as $index => $item){
            if(is_string($item) && strpos($item, $arg) !== false){
                $indices[] = $index;
            }
        }
        
        return $indices;
    }
    
    /**
     * 
     * simply checks if the collection contains specified type
     * 
     * @param string $type
     * @throws CollectionTypeException
     * @return boolean
     */
    protected function hasAType($type){
        foreach($this->values() as $item){
            switch($type){
                case "string":
                    return is_string($item);
                case "object":
                    return is_object($item);
                case "integer":
                    return is_integer($item);
                case "double":
                    return is_double($item);
                case "float":
                    return is_float($item);
                case "array":
                    return is_array($item);
                case "callable":
                    return is_callable($item);
                case "countable":
                    return is_countable($item);
                default:
                    throw new CollectionTypeException("Unknown type: {$type}");
            }
        }
        
        return false;
    }
    
    /**
     * 
     * Retrieve the key of a given value (needle) from the items array.
     *
     * This method searches for the first occurrence of the specified value in the
     * $items array and returns the corresponding key. If the value is not found,
     * the method will return false.
     * 
     * @param mixed $needle
     * @return number|string|boolean
     */
    public function getKey(mixed $needle){
        return array_search($needle, $this->items);
    }
    
    /**
     * 
     * determines if the collection stack has a string
     * 
     * @return boolean
     */
    public function hasString(){
        $typeName = strtolower(substr(__FUNCTION__, 3));
        
        return $this->hasAType($typeName);
    }
    
    /**
     * 
     * determines if the collection stack has an object
     * 
     * @return boolean
     */
    public function hasObject(){
        $typeName = strtolower(substr(__FUNCTION__, 3));
        
        return $this->hasAType($typeName);
    }
    
    /**
     *
     * determines if the collection stack has an integer
     *
     * @return boolean
     */
    public function hasInteger(){
        $typeName = strtolower(substr(__FUNCTION__, 3));
        
        return $this->hasAType($typeName);
    }
    
    /**
     *
     * determines if the collection stack has a double
     *
     * @return boolean
     */
    public function hasDouble(){
        $typeName = strtolower(substr(__FUNCTION__, 3));
        
        return $this->hasAType($typeName);
    }
    
    /**
     * 
     * determines if the collection stack has a float
     * 
     * @return boolean
     */
    public function hasFloat(){
        $typeName = strtolower(substr(__FUNCTION__, 3));
        
        return $this->hasAType($typeName);
    }
    
    /**
     * 
     * determines if the collection stack has an array
     * 
     * @return boolean
     */
    public function hasArray(){
        $typeName = strtolower(substr(__FUNCTION__, 3));
        
        return $this->hasAType($typeName);
    }
    
    /**
     *
     * determines if the collection stack has a callable
     *
     * @return boolean
     */
    public function hasCallable(){
        $typeName = strtolower(substr(__FUNCTION__, 3));
        
        return $this->hasAType($typeName);
    }
    
    /**
     *
     * determines if the collection stack has a countable
     * 
     * @return boolean
     */
    public function hasCountable(){
        $typeName = strtolower(substr(__FUNCTION__, 3));
        
        return $this->hasAType($typeName);
    }
    
    /**
     * 
     * @param array $searchableItems
     * @param Closure $mappingcallback
     * @return boolean
     */
    public function hasTheFollowing(Array $searchableItems, $mappingcallback = null){
        foreach($searchableItems as $item){
            if($this->has($item, $mappingcallback)){
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * 
     * @param mixed $value
     * @param callable $mappingCallback
     * @return boolean
     */
    public function has($value, $mappingCallback = null){
        if(!is_null($mappingCallback)){
            $stack = $this->map($mappingCallback);
        }else{
            $stack = $this->items;
        }
        
        return in_array($value, $stack, true);
    }
    
    /**
     * 
     * checks if the key exists within the stack
     * 
     * @param mixed $key
     * @return boolean
     */
    public function hasKey($key){
        return array_key_exists($key, $this->items);
    }
    
    /**
     * 
     * @return number
     */
    #[\ReturnTypeWillChange]
    public function count(){
        return count($this->items);
    }
    
    /**
     * 
     * alias of count()
     * 
     * @return number
     */
    public function length(){
        return $this->count();
    }
    
    /**
     * 
     * @param mixed $key
     * @return null|mixed
     */
    public function get($key){
        if(isset($this->items[$key])){
            return $this->items[$key];
        }

        return null;
    }
    
    /**
     * 
     * @param string|mixed $key
     * @return \lib\util\collections\CollectionItem|NULL
     */
    public function itemAt($key){
        if($this->hasKey($key)){
            return new CollectionItem($this->get($key), $key, $this);
        }
        
        return null;
    }
    
    /**
     * 
     * removes a stack element by key
     * 
     * @param mixed $key
     * 
     * @return boolean
     */
    public function remove($key){
        if(isset($this->items[$key])){
            unset($this->items[$key]);
            return true;
        }
        
        return false;
    }
    
    /**
     * 
     * remove all the elements of array collection
     * 
     */
    public function clear(){
        $itemKeys = array_keys($this->items);
                
        foreach($itemKeys as $key){
            unset($this->items[$key]);
        }
        
        $this->items = array_values($this->items);
        
        $this->cursorPointer = -1;
    }
    
    /**
     * Resets the stack of routes and inserts a new set of items.
     *
     * @param array $items The new set of items to be inserted into the route stack.
     */
    public function reset($items){
        $this->clear();
        $this->addAll($items);
    }
    
    /**
     * 
     * @param string ...$types
     * 
     * @throws CollectionTypeException
     * @return \lib\util\Collections
     */
    public function filterType(string ...$types){
        $extracted = collect();
        
        if(count($types) === 0){
            goto ends;
        }
        
        $defaultTypes = [
            "string", "object", "integer", "double", 
            "float", "array", "countable", "numeric"
        ];
        
        // validate the specified type
        foreach($types as $type){
            if(!in_array($type, $defaultTypes)){
                throw new CollectionTypeException("Not a valid type: {$type}");
            }
        }
        
        $this->each(function($item, $index) use ($types, &$extracted){
            foreach($types as $type){
                $func = "is_{$type}";
                
                if($func($item)){                    
                    $extracted[$index] = $item;
                }
            }
        });
        
        ends:
        return $extracted;
    }
    
    /**
     * 
     * filters the stack according to the condition returned by the callback
     * 
     * @param Closure $callback
     * @return \lib\util\Collections
     */
    public function filter(Closure $callback = null){
        if($callback){
            return new static(array_filter($this->items, $callback, ARRAY_FILTER_USE_BOTH));
        }
        
        return new static(array_filter($this->items));
    }
    
    /**
     * implodes the collection stack data to a string.
     * 
     * @param string $glue
     * @param closure $mappingCallback
     * 
     * @return string
     */
    public function implode($glue = " ", $mappingCallback = null){
        if($glue instanceof Closure){
            $mappingCallback = $glue;
            $glue = " ";
        }
        
        if(!is_null($mappingCallback)){
            $toImplode = $this->map($mappingCallback);
        }else{
            $toImplode = $this->items;
        }
        
        return implode($glue, $toImplode);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see IteratorAggregate::getIterator()
     */
    #[\ReturnTypeWillChange]
    public function getIterator() {
        return new ArrayIterator($this->items);
    }
    
    /**
     * 
     * @param Closure $mappingFunc
     * @return array
     */
    public function map(Closure $mappingFunc){
        return array_map($mappingFunc, $this->items);
    }
    
    /**
     * 
     * @param String $search
     * @return \lib\util\collections\BinarySearching
     */
    public function binarySearch(mixed $search){
        return new BinarySearching($this, $search);
    }
    
    /**
     * 
     * @param \Closure $callable
     */
    public function each(Closure $callable){
        if(!is_array($this->items)){
            return;
        }
        
        foreach($this->items as $index => $item){
            $callable($item, $index);
        }
    }
    
    /**
     * 
     * gets all the associative keys
     * 
     * @return mixed[]
     */
    public function assocKeys(){
        $keys = $this->keySet();
        
        $extracted = [];
        
        foreach($keys as $key){
            if(!is_integer($key)){
                $extracted[] = $key;
            }
        }
        
        return $extracted;
    }
    
    /**
     * 
     * gets all the numeric keys
     * 
     * @return int[]
     */
    public function numericKeys(){
        $keys = $this->keySet();
        
        $extracted = [];
        
        foreach($keys as $key){
            if(is_integer($key)){
                $extracted[] = $key;
            }
        }
        
        sort($extracted);
        
        return $extracted;
    }
    
    /**
     * 
     * @return array
     */
    public function keySet(){
        return array_keys($this->items);
    }
    
    /**
     * 
     * @return array
     */
    public function values(){
        return array_values($this->items);
    }
    
    /**
     * 
     * sorts the stack that must only contain numeric values.
     * strictly expects integers
     * 
     * @return Collections
     */
    public function nsort(){
        /*
         * 
         * checks if there's non-numeric element within the collection stack
         * 
         */
        $this->each(function($num){
            if(!is_integer($num)){
                throw new CollectionTypeException("nsort only sorts stack of numeric values. "
                    .class_name($num)."({$num}) found");
            }
        });

        sort($this->items, SORT_NUMERIC);
        
        return $this;
    }
    
    /**
     * 
     * Flattens a multidimensional array into a single-level collection.
     * 
     * @param Collections $stack
     * @param array $arr
     * @param int $index
     */
    protected function recursiveFlat(Collections &$stack, array $arr, int $index){
        if($index === count($arr)){
            return;
        }
        
        $keys = array_keys($arr);
        
        if(is_array($arr[$keys[$index]])){
            $this->recursiveFlat($stack, $arr[$keys[$index]], 0);
        }else{
            // preserving keys
            $stack[$keys[$index]] = $arr[$keys[$index]];
        }
        
        $this->recursiveFlat($stack, $arr, $index + 1);
    }
    
    /**
     * Flattens a multidimensional array into a single-level collection.
     * 
     * @return Collections
     */
    public function flattened(){
        $stack = collect();
        
        foreach($this->items as $key => $item){
            if(is_array($item)){
                $this->recursiveFlat($stack, $item, 0);
            }else{
                // preserving keys
                $stack[$key] = $item;
            }
        }
        
        return $stack;
    }
    
    /**
     * 
     * determines if array is Multi-Dimensional
     * 
     * @return boolean
     */
    public function isMultidimensional(){
        if(!is_array($this->items)){
            goto nothing;
        }
        
        foreach($this->items as $item){
            if(is_array($item)){
                return true;
            }
        }
        
        nothing:
        return false;
    }
    
    /**
     * 
     * Sorts an array in ascending order and maintain index association
     * 
     * @param Closure $toCompare
     * 
     * @return \lib\util\Collections
     */
    public function asort(Closure $mapping = null){
        if(!is_null($mapping)){
            $temp = [];
            
            $this->each(function($item, $key) use (&$temp, $mapping){
                $temp[$key] = $mapping($item);
            });
            
            asort($temp);
            
            $keys = array_keys($temp);
            
            for($index = 0; $index < count($temp); $index++){
                $temp[$keys[$index]] = $this->items[$keys[$index]];
            }
            
            $this->clear();
            
            // refining indices and values
            foreach($temp as $index => $item){
                $this->items[$index] = $item;
            }
        }else{
            asort($this->items);
        }
        
        return $this;
    }
    
    /**
     * 
     * reverses the content of an array
     * 
     * @return \lib\util\Collections
     */
    public function reverse(){
        $this->items = array_reverse($this->items);
        
        return $this;
    }
    
    /**
     * 
     * sorts an array by values using a user-defined comparison function.
     * 
     * @param \Closure $valCompareFunc
     * 
     * @return Collections
     */
    public function usort($valCompareFunc){
        usort($this->items, $valCompareFunc);
        
        return $this;
    }
    
    /**
     * 
     * simply shuffles elements of the array
     * 
     * @return Collections
     */
    public function shuffle(){
        shuffle($this->items);
        
        return $this;
    }
    
    /**
     * This function compares the values of two (or more) arrays, 
     * and return an array that contains the entries from array1 
     * that are not present in array2 or array3, etc
     * 
     * @param array|Collections $stacks
     * 
     * @return Collections
     */
    public function diff(...$stacks){
        foreach($stacks as &$stack){
            if(!is_array($stack) && !($stack instanceof Collections)){
                throw new InvalidArgumentException(
                    "Argument#1: \$stack must be an array or instance of Collections: "
                    .class_name($stack));
            }
            
            if($stack instanceof Collections){
                $stack = $stack->toArray();
            }
        }
        
        // intercepting never stops to any data entries folks
        // as long as the collection stack only accepts a single type
        if(!is_blank($this->onlyAccepts)){
            foreach($stack as $data){
                $this->checkItemType($data);
            }
        }
        
        /*
         * 
         * returns new instance of Collections
         *  
         */
        return new Collections(
            array_diff($this->items, ...$stacks), 
            $this->onlyAccepts);
    }
    
    /**
     * 
     * checks if the collection stack is sorted numerically
     * 
     * @throws CollectionTypeException
     * @return boolean
     */
    public function isNumericallySorted(){
        for($index = 0; $index < $this->count() - 1; $index++){
            $curr = $this->get($index);
            $next = $this->get($index + 1);
            
            if(!is_numeric($curr) || !is_numeric($next)){
                throw new CollectionTypeException(
                    "The data type of a compared item isn't a number. [{$curr}|{$next}]");
            }
            
            if($curr > $next){
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * 
     * Checks if all elements in the collection are integers.
     * 
     * @return boolean
     */
    public function areAllIntegers(){
        return $this->areAllElementsOfType("number");
    }
    
    /**
     *
     * Checks if all elements in the collection are strings.
     *
     * @return boolean
     */
    public function areAllStrings(){
        return $this->areAllElementsOfType("string");
    }
    
    /**
     * 
     * Checks if all elements in the collection are objects.
     * 
     * @return boolean
     */
    public function areAllObjects(){
        return $this->areAllElementsOfType("object");
    }
    
    /**
     * Checks if all elements in the collection are arrays.
     *
     * @return boolean
     */
    public function areAllArrays(){
        return $this->areAllElementsOfType("array");
    }
    
    /**
     *
     * Checks if all elements in the collection are resources.
     *
     * @return boolean
     */
    public function areAllResources(){
        return $this->areAllElementsOfType("resource");
    }
    
    /**
     * 
     * Evaluates if all elements in the items array are of the specified type.
     * 
     * This method checks if each element in the $this->items array matches the provided 
     * type strictly. For the "number" type, it ensures that the element is either an 
     * integer or a float, and not a numeric string. It supports the following types:
     * - "number" (strictly int or float, not numeric strings)
     * - "string" (strictly string)
     * - "object" (strictly object)
     * - "array" (strictly array)
     * - "resource" (strictly resource)
     * 
     * @param string $type
     * @return boolean
     */
    protected function areAllElementsOfType(string $type){
        foreach($this->items as $item){
            switch($type){
                case "number":
                    // Ensure the item is either an int or float, not a numeric string
                    if(!(is_int($item) || is_float($item))){
                        return false;
                    }
                    break;
                case "string":
                    // Ensure the item is strictly a string
                    if(!is_string($item)){
                        return false;
                    }
                    break;
                case "object":
                    // Ensure the item is strictly an object
                    if(!is_object($item)){
                        return false;
                    }
                    break;
                case "array":
                    // Ensure the item is strictly an array
                    if(!is_array($item)){
                        return false;
                    }
                    break;
                case "resource":
                    // Ensure the item is strictly a resource
                    if(!is_resource($item)){
                        return false;
                    }
                    break;
            }
        }
        
        return true; // All elements are of the specified type
    }
    
    /**
     *
     * checks if the collection stack is sorted alphabetically
     *
     * @throws CollectionTypeException
     * @return boolean
     */
    public function isAlphabeticallySorted(Closure $mappingFunc = null){
        $list = $this->values();
        
        if(!is_null($mappingFunc)){
            $list = array_map($mappingFunc, $list);
        }
        
        for($index = 0; $index < $this->count() - 1; $index++){
            $curr = $list[$index];
            $next = $list[$index + 1];
            
            if(!is_string($curr) || !is_string($next)){
                throw new CollectionTypeException(
                    "The data type of a compared item isn't a string. [{$curr}|{$next}]");
            }
            
            if(strcasecmp($curr, $next) > 0){
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * 
     * pulls out the last element of a stack and returns its value.
     * 
     * will return null if the stack is empty
     * 
     * @return mixed
     */
    public function pop(){
        return array_pop($this->items);
    }
    
    /**
     * 
     * returns the last element of the stack and sets the internal pointer to it
     * 
     * @return mixed
     */
    public function end(){
        return end($this->items);
    }
    
    /**
     * 
     * gets the first index of the collection
     * 
     * @return mixed
     */
    public function getFirstIndex(){
        $indices = array_keys($this->items);
        
        return $indices[0];
    }
    
    /**
     * 
     * retrieves the first item of the collection
     * 
     * @return mixed|null
     */
    public function getFirstItem(){
        return $this->get($this->getFirstIndex());
    }
    
    /**
     * 
     * gets the last index of the collection
     * 
     * @return mixed
     */
    public function getLastIndex(){
        $indices = array_keys($this->items);
        
        return $indices[count($indices) - 1];
    }
    
    /**
     * merges an array or Collections data (appending data)
     * 
     * @param array|Collections $stack
     * 
     * @return Collections
     */
    public function merge(...$mutableArgs){
        $this->warpMutableArgs($mutableArgs);
        
        $this->items = array_merge($this->items, $mutableArgs);
        
        return $this;
    }
    
    /**
     * 
     * alias of merge
     * 
     * @param mixed ...$mutableArgs
     * 
     * @return Collections
     */
    public function append(...$mutableArgs){
        $this->merge($mutableArgs);
        
        return $this;
    }
    
    /**
     * modifications and intercepts with mutable arguments
     * 
     * @param array $mutableArgs
     * @return \lib\util\Collections|mixed|\lib\util\unknown|array
     */
    protected function warpMutableArgs(Array &$mutableArgs){
        if(count($mutableArgs) === 1){
            if($mutableArgs[0] instanceof Collections){
                $mutableArgs = $mutableArgs[0]->toArray();
            }else if(is_array($mutableArgs[0])){
                $mutableArgs = $mutableArgs[0];
            }
        }
        
        // intercepting invalid data types
        if(!is_blank($this->onlyAccepts)){
            foreach($mutableArgs as $item){
                $this->checkItemType($item);
            }
        }
    }
    
    /**
     * checks if the array is a subset of a stack
     * 
     * @param array $subset
     * 
     * @return boolean
     */
    public function isSubset(array $subset){
        return array_intersect_key($this->items, $subset) === $subset;
    }
    
    protected $fragmentLocatingError;
    
    /**
     * 
     * checks if a fragment is part of the stack
     * 
     * @param array $fragment
     * 
     * @return bool
     */
    public function isFragment(array $fragment){
        foreach($fragment as $key => $value) {
            $found = $this->findKeyValue($this->items, $key, $value);
           
            if(!$found){
                $this->fragmentLocatingError = "JSON doesn't contain the fragment [{$key} => {$value}]";
                
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * 
     * Retrieves the error message or details related to fragment locating.
     *
     * This method returns any error that occurred during the process of locating a fragment.
     * The error details can be used for debugging or logging purposes to identify issues
     * with fragment location in the application workflow.
     * 
     * @return string|null
     */
    public function getFragmentLocatingError(){
        return $this->fragmentLocatingError;
    }
    
    /**
     * locates the key-value pair recursively
     *
     * @param array $array
     * @param int|string $key
     * @param mixed $value
     * 
     * @return boolean
     */
    protected function findKeyValue(array $array, int|string $key, mixed $value) {
        foreach($array as $k => $v){
            if($k === $key && $v === $value) {
                return true; // Found the key-value pair
            }
            
            // If the value is an array, recurse
            if(is_array($v)){
                if($this->findKeyValue($v, $key, $value)) {
                    return true;
                }
            }
        }
        
        return false; // Key-value pair not found
    }
    
    /**
     * 
     * determines if the collection stack contains data (or can be looped)
     * 
     * @return boolean
     */
    public function isLoopable(){
        return $this->count() > 0;
    }
    
    /**
     * 
     * @param array $arrvar
     * @return mixed|unknown|array
     */
    public function toArray(array $custom = null){
        if(!is_blank($custom)){
            $key = a($custom, "key");
            $val = a($custom, "val");
            
            $enhanced = [];
            
            foreach($this->items as $item){
                if(is_object($item)){
                    $enhanced[$item->$key] = is_callable($val) 
                    ? $val($item) 
                    : $item;
                }
            }
            
            return $enhanced;
        }
        
        return $this->items;
    }
    
    /**
     * 
     * @return StdClass
     */
    public function toObject(){
        return (object) $this->items;
    }
    
    /**
     * 
     * @return string
     */
    public function toJSONString(){
        return json_encode($this->items, JSON_PRETTY_PRINT);
    }
    
    /**
     * 
     * picks an item from the stacklist randomly
     * 
     * @return CollectionItem
     */
    public function random(){
        if(!$this->count()){            
            return null;
        }

        $keys = $this->keySet();
        
        return $this[$keys[mt_rand(0, $this->count() - 1)]];
    }
    
    /**
     * 
     * Same thing with map() but this one returns a collection stack
     * 
     * @param Closure $mappingFunc
     * 
     * @return Collections
     */
    public function cmap(Closure $mappingFunc){
        $mapped = $this->map($mappingFunc);
        
        return collect($mapped);
    }
    
    /**
     * 
     * Same thing with keySet() but this one returns a collection stack
     * 
     * @return Collections
     */
    public function ckeySet(){
        return collect($this->keySet());
    }
    
    /**
     *
     * Same thing with values() but this one returns a collection stack
     *
     * @return Collections
     */
    public function cvalues(){
        return collect($this->values());
    }
    
    /**
     * 
     * @param int $rowsPerPage
     * @param int $page
     * @return \lib\util\paginators\CollectionsPaginator
     */
    public function paginate(int $rowsPerPage, int $page = 1) : Paginator {
        return App::make(CollectionsPaginator::class)->stack($this)
            ->itemCount($this->length())
            ->rowsPerPage($rowsPerPage)
            ->fetchPageItems($page);
    }
    
    /**
     * 
     * @param mixed $groupBy
     * @param bool $preservedKeys
     * 
     * @return \lib\util\Collections
     */
    public function groupBy(mixed $groupBy, bool $preservedKeys = false){
        $result = [];
        
        foreach($this->items as $key => $value){
            $groupKey = is_callable($groupBy) ? $groupBy($value, $key) : a($value, $groupBy);
            
            if(!array_key_exists($groupKey, $result)){
                $result[$groupKey] = [];
            }
            
            if($preservedKeys){
                $result[$groupKey][$key] = $value;
            }else{
                $result[$groupKey][] = $value;
            }
        }
        
        return new static($result);
    }
    
    /**
     * 
     * @param array|Collections $items
     * @param string $class
     * 
     * @return \lib\util\Collections
     */
    public static function make(mixed $items, string $class = null){
        if($items instanceof self){
            return $items;
        }
        
        $collection = new static($items, $class);
        
        $collection->strictlyCheckTypes();
        
        return $collection;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\iterators\Cursor::hasBegan()
     */
    public function hasBegan(): bool {
        return $this->cursorPointer >= 0;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\iterators\Cursor::isFirst()
     */
    public function isFirst(): bool {
        return $this->cursorPointer === 0;
    }

    /**
     * 
     * {@inheritDoc}
     * @see \lib\iterators\Cursor::isLast()
     */
    public function isLast(): bool {
        return $this->cursorPointer === $this->count() - 1;
    }

    /**
     * 
     * {@inheritDoc}
     * @see \lib\iterators\Cursor::moveToNext()
     */
    public function moveToNext(): bool {
        if($this->count() === 0){
            goto ends;
        }
        
        if(($this->cursorPointer + 1) !== $this->count()){
            $this->cursorPointer++;
            
            return true;
        }
        
        ends:
        return false;
    }

    /**
     * 
     * {@inheritDoc}
     * @see \lib\iterators\Cursor::moveToPosition()
     */
    public function moveToPosition(int $index): bool {
        if(!$this->isNull($index)){
            $this->cursorPointer = $index;
            
            return true;
        }
        
        return false;
    }

    /**
     * 
     * {@inheritDoc}
     * @see \lib\iterators\Cursor::moveToLast()
     */
    public function moveToLast() : bool {
        if($this->count()){
            $this->cursorPointer = $this->count() - 1;
            
            return true;
        }
        
        return false;
    }

    /**
     * 
     * {@inheritDoc}
     * @see \lib\iterators\Cursor::moveToPrevious()
     */
    public function moveToPrevious() : bool {
        if(($this->cursorPointer - 1) > 0){
            $this->cursorPointer--;
            
            return true;
        }
        
        return false;
    }

    /**
     * 
     * {@inheritDoc}
     * @see \lib\iterators\Cursor::moveToFirst()
     */
    public function moveToFirst() : bool {
        if($this->count()){
            $this->cursorPointer = 0;
            
            return true;
        }
        
        return false;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\iterators\Cursor::isNull()
     */
    public function isNull($index) : bool {
        return is_null($this->get($index)); 
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\iterators\Cursor::getCursoredItem()
     */
    public function getCursoredItem(){
        if($this->cursorPointer < 0){
            throw new CursorIndexOutOfBoundsException("Cursor index is negative.");
        }
        
        if($this->cursorPointer >= $this->count()){
            throw new CursorIndexOutOfBoundsException("Cursor index is greater than the number of elements in the collection");
        }
        
        $keys = $this->keySet();
        
        return $this->get($keys[$this->cursorPointer]);
    }
    
    /**
     * recursively converts ancient array() notation into []
     * 
     * @param array $array
     * @param number $level
     * 
     * @return string
     */
    protected function exportHelper(array $array, $level = 0){
        $indent = str_repeat('    ', $level);
        
        $exportedString = "[\n";
        
        foreach($array as $key => $value) {
            $exportedString .= "{$indent}    ";
            
            if(!is_integer($key)){
                $exportedString .= "\"".addslashes($key)."\" => ";
            }
            
            if(is_array($value)){
                $exportedString .= $this->exportHelper($value, $level + 1);
            }else if(is_bool($value)){
                $exportedString .= ($value ? "true" : "false");
            }else if(is_null($value)){
                $exportedString .= "null";
            }else if(is_numeric($value)){
                $exportedString .= $value;
            }else{
                $exportedString .= "\"".addslashes($value)."\"";
            }
            
            $exportedString .= ",\n";
        }
        
        $exportedString .= "{$indent}]";
        
        return $exportedString;
    }
    
    /**
     * 
     * sets the __toString value as readable formatted string
     * 
     * @param bool $readable
     * 
     * @return Collections
     */
    public function readable(bool $readable){
        $this->readable = $readable;
        
        return $this;
    }
    
    /**
     *
     * Outputs or returns a parsable string representation of this list
     *
     * @return string
     */
    public function export(){
        return $this->exportHelper($this->items);
    }
    
    /**
     * 
     * @return string
     */
    public function __toString(){
        if($this->readable){
            return print_r($this->items, true);
        }
        
        return $this->toJSONString();
    }
}