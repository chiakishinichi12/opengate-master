<?php

return [
    "classes" => [
        app\providers\RouteServiceProvider::class,
        app\providers\DetailServiceProvider::class,
        app\providers\ControllerAnnotationServiceProvider::class,
        app\providers\CommandAnnotationServiceProvider::class,
        app\providers\HttpInstanceAnnotationServiceProvider::class,
        app\providers\HttpLocaleServiceProvider::class,
        
        lib\commands\InternalCommandServiceProvider::class,
        lib\mocks\MockeryServiceProvider::class,
        lib\util\nebula\ViewMorphServiceProvider::class,
        lib\mail\MailerServiceProvider::class
    ]  
];