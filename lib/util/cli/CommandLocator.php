<?php

namespace lib\util\cli;

use lib\inner\App;
use lib\inner\Context;
use lib\instances\CommandInstance;
use lib\util\Collections;
use lib\util\Reader;
use lib\util\controller\Controller;
use lib\util\exceptions\ClassNotFoundException;
use lib\util\exceptions\ControllerException;

/**
 * 
 * @author Antonius Aurelius
 *
 */
class CommandLocator {
    
    /**
     * 
     * @var boolean
     */
    protected $located = false;
    
    /**
     * 
     * @var string
     */
    protected $inputted;
    
    /**
     * 
     * @var Collections
     */
    protected $arguments;
    
    /**
     * 
     * @param WarpArgv $warpArgv
     */
    public function __construct(WarpArgv $warpArgv){
        $this->inputted = $warpArgv->getInputtedCommand();
                
        $this->arguments = $warpArgv->obtainArguments();
    }
    
    /**
     * 
     * @param CommandBean $bean
     */
    public function locateCommand(){
        $bean = null;
                
        App::make(CommandContainer::class)->getCommands()->each(function($cmdbean, $index) use (&$bean){            
            if(is_string($cmdbean->command())){
                if(strcmp($this->inputted, $cmdbean->command()) === 0){
                    $bean = $cmdbean;
                }
            }else if(is_array($cmdbean->command())){
                if(in_array($this->inputted, $cmdbean->command())){
                    $bean = $cmdbean;
                }
            }
        });
        
        if(!is_blank($bean)){
            $this->located = true;
            $this->determineQueryAction($bean);
        }
        
        return $this;
    }
    
    /**
     * 
     * @return boolean
     */
    public function isLocated(){
        return $this->located;
    }
    
    /**
     * 
     * @param CommandBean $bean
     */
    protected function determineQueryAction(CommandBean $commandBean){
        if(is_array($commandBean->query()) && count($commandBean->query()) === 2){
            $commandBean->query(implode("@", $commandBean->query()));
        }
        
        if(is_string($commandBean->query())){
            if($this->isControllerExpression($commandBean->query())){
                $commandBean->query($this->controllerAlignment($commandBean));
            }
        }
        
        $this->renderReaderResponse($commandBean);
    }
    
    /**
     *
     * @param string $expression
     */
    protected function isControllerExpression(string $expression){
        return preg_match("/([a-zA-Z0-9]+)@([a-zA-Z0-9]+)/", $expression);
    }
    
    /**
     * 
     * @param CommandBean $bean
     * 
     * @throws ControllerException
     */
    protected function controllerAlignment(CommandBean $commandBean){
        $action = explode("@", $commandBean->query());
        
        if(count($action) == 2){
            $class = $action[0];
            $method = $action[1];
            
            if(!class_exists($class)){
                throw new ClassNotFoundException("Controller class '{$class}' doesn't exist", $class, $method);
            }
            
            if(!App::checkInheritance($class, Controller::class)){
                throw new ControllerException(class_name($class)." is not an instance of ".Controller::class);
            }
            
            if(!method_exists($class, $method)){
                throw new ControllerException("Controller method {{$method}} doesn't exist.", $class, $method);
            }
            
            return [$class, $method];
        }
    }
    
    /**
     *
     * @param CommandBean $commandBean
     */
    protected function renderReaderResponse(CommandBean $commandBean){
        $locatedCommandBeanQuery = $commandBean->query();
        
        Context::setCurrentContextualItem($locatedCommandBeanQuery);
        
        if(is_string($locatedCommandBeanQuery) && class_exists($locatedCommandBeanQuery)
            && App::checkInheritance($locatedCommandBeanQuery, CommandInstance::class)){
            Reader::readInstance($locatedCommandBeanQuery);
        }else{
            Reader::readCommand($commandBean);
        }
    }
}