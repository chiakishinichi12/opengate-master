<?php
use lib\util\route\Route;
use app\instances\page\InstanceTest;
use app\instances\api\InstanceAPI;

/**
 * this file isn't actually necessary but you can use this if you want uniformity 
 * of defining a route for response instances.
 * 
 * You can consider that response instances are the counterpart of Java Servlet
 * with more flexibility
 * 
 */