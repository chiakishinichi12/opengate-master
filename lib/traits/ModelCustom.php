<?php
namespace lib\traits;

use ReflectionClass;
use ReflectionMethod;
use lib\inner\App;
use lib\util\ContentValues;
use lib\util\ReflectionUtil;
use lib\util\builders\ModelQueryBuilder;
use lib\util\collections\CollectionItem;
use lib\util\datagate\DB;
use lib\util\datagate\Prepare;
use lib\util\exceptions\ModelException;
use lib\util\exceptions\SQLException;
use lib\util\model\SoftDeletes;
use lib\util\model\TableInfo;
use lib\util\model\TableInfoContainer;
use lib\util\resolver\MethodInvocationResolver;

trait ModelCustom {
    
    /**
     * 
     * @var string
     */
    protected $table_name;
    
    /**
     * 
     * @var string
     */
    protected $primary_key;
    
    /**
     * 
     * @var string
     */
    protected $dbms = "mysql";
    
    /**
     * 
     * @var array
     */
    protected $readonly = [];
    
    /**
     * 
     * @var array
     */
    protected $default_values = [];
    
    /**
     * 
     * @var array
     */
    protected $original = [];
    
    /**
     * 
     * @var boolean
     */
    protected $update_time = true;
    
    /**
     * 
     * @var ModelQueryBuilder
     */
    protected $builder;
    
    /**
     *
     * @var array
     */
    protected $overwrite = [];
    
    /**
     * 
     * model custom init
     * 
     */
    protected function initComponents(array $outprops = null){
        $this->connectionToTheDatabase();
        $this->readTableInfo();
        $this->checkReadonlyColsValidity();
        $this->loadPropertyDefaults($outprops);
    }
    
   /**
    * 
    * @param string $propname
    * @return mixed|null
    */
    protected function retrievePropertyValue(string $propname){
        $value = null;
        
        if(array_key_exists($propname, $this->outprops)){
            $value = $this->outprops[$propname];
        }
        
        if(!in_array($propname, $this->overwrite) 
            && !is_null($warped = $this->warpedMethod($propname))){
            $value = $warped->invoke($this);
        }
        
        return $value;
    }
    
    /**
     * 
     * @param string $method
     * @return mixed|null
     */
    public function warpedMethod(string $method){
        $methods = ReflectionUtil::getDeclaredMethods(new ReflectionClass($this), ReflectionMethod::IS_PROTECTED);
        
        $warped = $methods->binarySearch($method)
        ->toCompare(fn(ReflectionMethod $m) => $m->getName())
        ->get();
        
        if(!is_null($warped)){
            return $warped->value();
        }
        
        return null;
    }
    
    /**
     * 
     * sets default property values into empty string at first
     * 
     * @param array $properties
     */
    protected function loadPropertyDefaults(array $outprops = null){
        $this->builder = $this->newQueryBuilderInst();
        
        if(!is_blank($outprops)){
            $this->outprops = $outprops;
        }
    }
    
    /**
     * 
     * @param string $method
     * @return boolean
     */
    protected function isAllowedProtectedMethod(string $method){
        return !is_null($this->warpedMethod($method));
    }
    
    /**
     * 
     * establishing database connection
     * 
     * @throws SQLException
     */
    protected function connectionToTheDatabase(){
        if(!DB::isConnected()){
            throw new SQLException("[DB_ERR:{$this->dbms}] No Connection to the Database was established");
        }
    }
    
    /**
     * 
     * @param string $method
     * @param array $params
     * 
     * @throws ModelException
     * 
     * @return mixed
     */
    protected function builderInv(string $method, array $params){
        return App::make(MethodInvocationResolver::class)
        ->parent($this)
        ->instances([$this->builder])
        ->toCall($method)
        ->params($params)
        ->allowMagicCall()
        ->resolve()
        ->invoke();
    }
    
    /**
     * 
     * 
     * checks if the readonly columns exists
     */
    protected function checkReadonlyColsValidity(){        
        if(!is_array($this->readonly)){
            return;
        }
        
        $cols = $this->columns->map(function($elem){
            return strtolower($elem->field);
        });
            
        foreach($this->readonly as $ro){
            if(!in_array($ro, $cols)){
                throw new ModelException("Unknown column `{$this->table_name}`.`{$ro}`", get_class($this));
            }
        }
    }
    
    /**
     * 
     * @return \lib\util\builders\ModelQueryBuilder
     */
    protected function newQueryBuilderInst(){
        return new ModelQueryBuilder($this, $this->dbms);
    }
    
    /**
     * 
     * 
     * reads table info
     */
    protected function readTableInfo(){
        if(is_blank($this->table_name)){
            throw new ModelException("Table name is not defined.", get_class($this));
        }
        
        $container = App::make(TableInfoContainer::class);
        
        $tableInfo = $container->getTableInfos()->get(get_class($this));
        
        if(is_null($tableInfo)){  
            $tableInfo = new TableInfo($this->table_name, $this->dbms);
            $container->appendTableInfo(get_class($this), $tableInfo);
        }
        
        $this->is_view = strcasecmp($tableInfo->getTableType(), "view") === 0;
        
        $this->columns = $tableInfo->getColumns();
            
        $this->primary_key = $tableInfo->getPrimaryKey($this->primary_key);
    }
    
    /**
     * 
     * @throws ModelException
     */
    protected function checkMandatories(){
        if(is_array($this->readonly)){
            collect($this->readonly)->each(function(string $not){
                if($this->outprops[$not] !== $this->original[$not]){
                    $message = "Column `{$this->table_name}`.`{$not}` is readonly";
                    
                    throw new ModelException($message, get_class($this),
                        ModelException::COLUMN_CANNOT_BE_MODIFIED);
                }
            });
        }
        
        if(is_blank($this->primary_key)){
            throw new ModelException("No PRIMARY KEY found in the table", get_class($this));
        }
    }
    
    /**
     * 
     * @throws ModelException
     */
    protected function checkIfPrimaryKeyDefined(){
        if(is_null($this->getPrimaryKeyValue())){            
            throw new ModelException("PRIMARY KEY {{$this->primary_key}} isn't defined", 
                get_class($this));
        }
    }
    
    /**
     * 
     * checks if a column exists
     * 
     * @param String $colName
     * @return boolean
     */
    public function isColumnExist(string $colName){
        $colNames = $this->columns->map(function($col){
            return strtolower($col->field);
        });
        
        return in_array($colName, $colNames);
    }
    
    /**
     * 
     * original values
     * 
     * @return array
     */
    public function getDefinedColumnVars(){
        return $this->outprops;
    } 
    
    /**
     * 
     * @param array $original
     */
    public function appendOriginal(array $original){
        $this->original = $original;
    }
    
    /**
     * 
     * @return boolean
     */
    public function save(){        
        $cval = new ContentValues($this->table_name);
        
        foreach($this->outprops as $column => $value){
            if($value instanceof CollectionItem){
                $value = $value->value();
            }
            
            $cval->put($column, $value);
        }
            
        if(!$this->isStored()){
            return $this->inserting($cval);
        }
        
        return $this->updating($cval);
    }
    
    /**
     * 
     * @param ContentValues $cval
     * 
     * @throws ModelException
     * 
     * @return mixed
     */
    protected function inserting(ContentValues $cval){
        if($this->isView()){
            throw new ModelException("View ({$this->table_name}) cannot perform data insertion");
        }
                
        if(is_array($this->default_values)){    
            collect($this->default_values)->each(function($value, string $column) use ($cval){
                if(!$this->isColumnExist($column)){
                    throw new ModelException(
                        "Column doesn't exist: `{$this->table_name}`.`{$column}`",
                        get_class($this),
                        ModelException::NONEXISTENT_COLUMN);
                }
                
                if(!isset($this->outprops[$column])){
                    $cval->put($column, $value);
                }
            });
        }
        
        return Prepare::connection($this->dbms)->insert($cval);
    }
    
    /**
     * 
     * @param ContentValues $cval
     * 
     * @return boolean|mixed
     */
    protected function updating(ContentValues $cval){
        $this->checkMandatories();
        $this->checkIfPrimaryKeyDefined();
        
        $pkvalue = $this->escape($this->getPrimaryKeyValue());
        
        if(!$this->isReasonableUpdate()){
            return false;
        }
        
        // ommit clauses that sets the same column value
        foreach($cval as $column => $value){
            if($value == $this->original[$column]){
                $cval->pop($column);
            }
        }
        
        if($this->update_time){
            if($this->isColumnExist("update_time")){
                $cval->put("update_time", ContentValues::NOW);
            }
        }
        
        $where = "`{$this->primary_key}` = \"{$pkvalue}\"";
        
        return Prepare::connection($this->dbms)->update($cval, $where);
    }
    
    /**
     * 
     * checks if there's a new update within the columns
     * 
     * @return boolean
     */
    protected function isReasonableUpdate(){
        $same = 0;
        
        $outprops = collect($this->outprops);
        
        $outprops->each(function($value, $column) use (&$same){
            if($value == $this->original[$column]){
                $same++;
            }
        });
        
        return ($same !== $outprops->length());
    }
    
    /**
     *
     * @return boolean
     */
    public function isSoftDeletesInherited(){
        return class_uses_traits($this, SoftDeletes::class)
            && $this->isColumnExist("delete_time");
    }
    
    /**
     * 
     * @return mixed|boolean
     */
    public function delete(){
        if($this->isStored()){
            $this->checkMandatories();
            $this->checkIfPrimaryKeyDefined();
            
            if(class_uses_traits($this, SoftDeletes::class)){
                if(!$this->isColumnExist("delete_time")){
                    throw new ModelException("SoftDeletes Error: Column 'delete_time' must be defined");
                }
                
                $this->delete_time = ContentValues::NOW;
                
                return $this->save();
            }else{
                $pkeyValue = $this->escape($this->getPrimaryKeyValue());
                $whereClause = "`{$this->primary_key}` = (0)";
                
                return Prepare::connection($this->dbms)->delete(
                    $this->table_name, $whereClause, [$pkeyValue]);
            }
        }
        
        return false;
    }
    
    /**
     * 
     * @return string
     */
    public function getTableName(){
        return $this->table_name;
    }
    
    /**
     * 
     * @return \lib\util\model\PrimaryKey
     */
    public function getPrimaryKey(){
        return $this->primary_key;
    }
    
    /**
     * 
     * @return mixed|null
     */
    public function getPrimaryKeyValue(){
        if(!is_null($primaryKey = $this->getPrimaryKey())){
            $primaryKey = $primaryKey->getName();
        }else{
            goto nothing;
        }
        
        if(isset($this->original[$primaryKey])){
            return $this->original[$primaryKey];
        }
        
        nothing:
        return null;
    }
    
    
    /**
     * 
     * @param bool $stored
     */
    public function setStored(bool $stored){
        $this->stored = $stored;
    }
    
    /**
     * 
     * @return string
     */
    public function getDBMS(){
        return $this->dbms;
    }
    
    /**
     * 
     * @return boolean
     */
    public function isStored(){
        return $this->stored;
    }
    
    /**
     * 
     * @return boolean
     */
    public function isView(){
        return $this->is_view;
    }
    
    /**
     * 
     * @return \lib\util\Collections
     */
    public function getColumns(){
        return $this->columns;
    }
}