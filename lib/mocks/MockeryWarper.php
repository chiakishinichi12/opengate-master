<?php
namespace lib\mocks;

use lib\util\Collections;
use lib\inner\App;

class MockeryWarper {
    
    /**
     * 
     * @var Collections
     */
    protected $mockeries;
    
    /**
     * 
     * constructor
     * 
     */
    public function __construct(){
        $this->mockeries = collect();
    }
    
    /**
     * 
     * @param string $modelclass
     * @return Mockery|null
     */
    public function locateMockery(string $modelclass){
        if(!is_blank($mockery = $this->mockeries->get($modelclass))){
            return $mockery;
        }
        
        return null;
    }
    
    /**
     * 
     * @param string|object $class
     * @param callable $closure
     */
    public function define($class){
        if(!class_exists($class)){
            throw new MockeryException("Unknown class: {$class}");
        }
        
        if(!App::checkInheritance($class, Mockery::class)){
            throw new MockeryException("{$class} is not an instance of ".Mockery::class);
        }
        
        if(is_object($class)){
            $class = class_name($class);
        }
        
        $mockery = App::make($class);
        
        $this->mockeries->put($mockery->model(), $mockery);
    }
}