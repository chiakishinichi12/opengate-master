<?php
namespace lib\util\model;

class PrimaryKey{
    
    /**
     * 
     * 
     * @var string
     */
    protected $name;
    
    /**
     * 
     * 
     * @var string
     */
    protected $type;
    
    /**
     * 
     * let's see
     * 
     * @var mixed
     */
    protected $value;
    
    public function __construct(string $name, string $type, $value = null){
        $this->name = $name;
        $this->type = $type;
        $this->value = $value;
    }
    
    /**
     * 
     * @return string
     */
    public function getName(){
        return $this->name;
    }
    
    /**
     * 
     * @return string
     */
    public function getType(){
        return $this->type;
    }
    
    /**
     * 
     * @return mixed|string
     */
    public function getValue(){
        return $this->value;
    }
    
    /**
     * 
     * @return mixed|string
     */
    public function __toString(){
        return $this->getName();
    }
}

