<?php
namespace lib\util\exceptions;

use lib\exceptions\OpenGateException;

class MethodNotFoundException extends OpenGateException{
    
}