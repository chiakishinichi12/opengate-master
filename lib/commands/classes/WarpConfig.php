<?php
namespace lib\commands\classes;

use lib\instances\CommandInstance;
use lib\util\file\File;

/**
 *
 * @Commands(["gen-pc", "grpc"])
 *
 */
class WarpConfig extends CommandInstance{
    
    protected $name = "gen-pc";
    
    protected $desc = "Generates the project config file";
    
    public function listen(){
        $materialized = new File(base_path("config.php"));
        
        if(!$materialized->exists()){
            $blueprint = new File(base_path("config.php.example"));
            
            $materialized->write($blueprint->getContents(), "w");
            
            $blueprint->close();
            
            out()->println("<info>config.php has been generated</info>");
        }else{
            out()->println("<warning>config.php already generated.</warning>");
        }
        
        $materialized->close();
    }
}