<?php

use lib\util\nebula\interpretations\Directive;
use lib\util\nebula\interpretations\DirectiveThrowers;
use lib\util\nebula\interpretations\Keyword;

return new class implements Directive, Keyword {
    
    use DirectiveThrowers;
    
    /**
     *
     * @var string
     */
    protected $name;
    
    /**
     *
     * @var string
     */
    protected $exp;
    
    public function __construct(){
        $base = basename(__FILE__);
        
        $this->name = substr($base, 0, strrpos($base, ".php"));
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\nebula\interpretations\Directive::obtains()
     */
    public function obtains($file, $interpreter){
        
        return $this;
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\nebula\interpretations\Directive::parenthesis()
     */
    public function parenthesis($exp){
        $this->exp = $exp;
        
        return $this;
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\nebula\interpretations\Directive::interpretation()
     */
    public function interpretation(){
        $this->requireParenthesis();
        
        $params = $this->parameters(1);        
        
        $this->expectsPhpExpression($params->get(0), "Argument#1");
        
        $message = $params->get(0);
        $viewstr = $params->get(1);
        
        $display = "{$message}";
        
        if(!is_blank($viewstr)){
            $display = "morph({$viewstr}, [\"message\" => {$message}])";
        }
        
        return "<?php out()->print(isset({$message}) ? {$display} : \"\"); ?>\n";
    }
};