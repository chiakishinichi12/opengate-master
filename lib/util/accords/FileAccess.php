<?php
namespace lib\util\accords;

use lib\util\Collections;
use lib\exceptions\OpenGateException;

interface FileAccess{
    
    /**
     * 
     * returns the filepath/URL string provided to the resource
     * 
     * @return string
     */
    function getName() : string;
    
    /**
     * 
     * returns the relative file path string of the resource. (returns null if URL)
     * 
     * @return string|null
     */
    function getRelativePath() : string;
    
    /**
     * 
     * retrieves the content of the file/URL 
     * 
     * @return string
     */
    function getContents() : string;
    
    /**
     * 
     * retrieves the content of the file/URL as a collection
     * 
     * @return Collections
     */
    function getContentsAsCollection() : Collections;
    
    /**
     * 
     * checks if the file exists
     * 
     * @return boolean
     */
    function exists() : bool;
    
    /**
     * 
     * writes/updates a content to a file
     * 
     * @param String $content
     * @param String $mode
     * 
     * @return bool|void
     */
    function write(String $content, String $mode);
    
    /**
     * 
     * closes the file handling
     * 
     */
    function close() : void;
    
    /**
     * 
     * checks if the file handling is already closed
     * 
     * @return boolean
     */
    function isClosed() : bool;
    
    /**
     * 
     * checks if the string is URL formatted.
     * 
     */
    function isURL() : bool;
    
    /**
     * 
     * move the existing file to an existing destination dir
     * 
     * @param String $destination
     * @param String $newName
     * @param bool $createDir
     * 
     * @return bool
     */
    function moveTo(String $destination, String $newName = null, bool $createDir = false) : bool;
    
    /**
     * 
     * copy and paste the file to an existing destination dir
     * 
     * @param String $destination
     * @param bool $createDir
     * 
     * @return bool
     */
    function copyTo(String $destination, bool $createDir = false) : bool;
    
    /**
     * 
     * returns the basename of the filepath
     * 
     * @return string
     */
    function getBasename() : string;
    
    /**
     * 
     * retrieves the extension name of the file
     * 
     * @return string
     */
    function getExtensionName() : string;
    
    /**
     * 
     * retrieves the size of the file
     * 
     * @param string $metric
     * @return float
     */
    function getSize($metric) : float;
    
    /**
     * 
     * retrieves the MIME type of the file
     * 
     * @return string
     */
    function getMimeType() : string;
    
    /**
     * 
     * retrieves the owner of the file
     * 
     * @return string
     */
    function getOwner() : string;
    
    /**
     * 
     * retrieves the last accessed time of the file
     * 
     * @return string
     */
    function getLastAccessedTime() : string;
    
    /**
     *
     * retrieves the last modified time of the file
     *
     * @return string
     */
    function getLastModifiedTime() : string;
    
    /**
     * 
     * deletes an existing file. and it also automatically closes the file handler
     * 
     * @return bool
     */
    function delete() : bool;
    
    /**
     *
     * setting a new filename
     *
     * @return bool
     */
    function renameTo(String $newFileName) : bool;
    
    /**
     * 
     * checks if the file is a directory
     * 
     * @return bool
     */
    function isDirectory() : bool;
    
    /**
     * 
     * checks whether the file denoted by this abstract pathname is a normal file.
     * 
     * @return bool
     */
    function isFile() : bool;
    
    /**
     * 
     * gets all the files of the directory
     * 
     * @param bool $recursive
     * when true, it also retrieves the files of the subdirectories
     * 
     * @return Collections
     */
    function listFiles(bool $recursive = false) : Collections;
    
    /**
     * creates a new file if not exists
     * 
     * @return bool
     */
    function createNewFile() : bool;
    
    /**
     * 
     * creates a new directory if not exists
     * 
     * @param bool $recursive
     * @param int $permission
     * @return bool
     */
    function makeDir(bool $recursive = true, int $permission = 0777) : bool;
    
    /**
     * 
     * determines if the file is writeable
     * 
     * @return bool
     */
    function canWrite(): bool;
    
    /**
     * 
     * archiving the file if it's a directory
     * 
     * @throws OpenGateException
     * @return bool|null
     */
    function archived();
    
    /**
     * 
     * extract all the files of an archived directory
     * 
     * @throws OpenGateException
     * @return bool|null|int
     */
    function extract($to = null);
    
    /**
     * 
     * checks if a file is an archived
     * 
     */
    function isArchived() : bool;
    
    /**
     * 
     * @param boolean $downloadable
     * @param array $headers
     * 
     * @return mixed
     */
    function read($downloadable = false, $headers = []);
    
    /**
     * requires and returns the value contained by the file.
     * 
     * @return mixed
     */
    function require() : mixed;
}