<?php
namespace lib\mail;

use lib\inner\Accessor;

/**
 * 
 * The shorthanded version of Attachment class
 * 
 * @author Jiya Sama
 * 
 * @method static \lib\mail\Attachment as(string $as = null)
 * @method static \lib\mail\Attachment disk(string $path = null)
 * @method static \lib\mail\Attachment path(string $path = null)
 * @method static \lib\mail\Attachment mime(string $mime = null)
 * @method static \lib\mail\Attachment encoding(string $encoding = null)
 */
class Attach extends Accessor{
    
    public function warpedInstance(){
        return Attachment::class;
    }
    
}