<?php
namespace lib\mail;

use lib\inner\Accessor;

/**
 * 
 * @author antonio
 *
 * @method static \lib\mail\WarpSender dispatchable($dispatchable)
 * @method static \lib\mail\WarpSender attachments($attachments)
 * @method static \lib\mail\WarpSender subject($subject)
 * @method static \lib\mail\WarpSender recipient($recipient)
 * @method static \lib\mail\WarpSender recipients(array $recipients)
 */
class Mail extends Accessor{
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\inner\Accessor::warpedInstance()
     */
    public function warpedInstance(){
        return WarpSender::class;
    }
}