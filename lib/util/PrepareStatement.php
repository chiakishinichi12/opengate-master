<?php

namespace lib\util;

interface PrepareStatement {
    /**
     * 
     * @param ContentValues $values
     */
    function insert(ContentValues $values);
    
    /**
     * 
     * @param ContentValues $values
     * @param string $where
     */
    function update(ContentValues $values, $where = "");
    
    /**
     * 
     * @param QuerySpecification $specifications
     */
    function query(QuerySpecification $specifications);
    
    /**
     * 
     * @param String $tableName
     * @param String $whereClause
     * @param array $whereArgs
     */
    function delete(String $tableName, String $whereClause, Array $whereArgs = []);
}