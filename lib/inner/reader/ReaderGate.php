<?php
namespace lib\inner\reader;

use Closure;
use lib\exceptions\OpenGateException;
use lib\inner\App;
use lib\inner\WebAppCore;
use lib\instances\StandardInstance;
use lib\util\Middleware;
use lib\util\route\RouteBean;
use lib\util\cli\CommandBean;

class ReaderGate {
        
    /**
     * 
     * @var WebAppCore
     */
    protected $instance;
    
    /**
     * 
     * @var Closure
     */
    protected $initInstanceCallback;
    
    /**
     * 
     * @var array
     */
    protected $middlewares = [];
    
    public function __construct(){
        $this->applyConfiguredTimezone();
    }
    
    /**
     * 
     * applies the configured system timezone by setting it as 
     * the default timezone for the application. This function 
     * ensures that the application's date and time operations 
     * are performed in the correct timezone as specified in 
     * the system configuration.
     * 
     */
    protected function applyConfiguredTimezone(){
        $systemDateAndTimeZone = preferences("setup")->get("system_date_timezone");
        
        if($systemDateAndTimeZone){
            date_default_timezone_set($systemDateAndTimeZone);
        }
    }
    
    /**
     * 
     * @param string $instance
     * @return \lib\inner\reader\ReaderGate
     */
    public function instance(string $instance){
        $this->instance = $instance;
        
        return $this;
    }
    
    /**
     * 
     * @param string $type
     * @return \lib\inner\reader\ReaderGate
     */
    protected function initInstanceClass($type = "page"){        
        if(!class_exists($this->instance)){
            throw new OpenGateException("Class {$this->instance} isn't declared");
        }
        
        if(!App::checkInheritance($this->instance, WebAppCore::class)){
            throw new OpenGateException("[{$this->instance}] The Object must be an instance of ".WebAppCore::class);
        }
        
        // string to object
        $this->instance = App::make($this->instance);
        
        App::instance(WebAppCore::class, $this->instance);
        
        if(App::checkInheritance($this->instance, StandardInstance::class)){
            $this->instance->validateType($type);
        }else{
            $this->instance->inspect();
        }
        
        if(!is_blank($this->initInstanceCallback)){
            App::call($this->initInstanceCallback);
        }
        
        return $this;
    }
    
    /**
     * 
     * rendering the result out of rendering core instance
     * 
     */
    protected function result(){
        $this->resolvingMiddlewares($this->instance);
        
        if(App::isRunningUnitTest() && response()->isErrResponseCode()){
            return;
        }
        
        App::make(ReaderRenderer::class)->readerGate($this)->render();
    }
    
    /**
     * 
     * @param WebAppCore $instance
     */
    protected function resolvingMiddlewares(WebAppCore $instance){
        if(!is_array($instance->middlewares())){            
            throw new OpenGateException("[".class_name($instance)."] 'middlewares' must be an array type");
        }
        
        foreach($instance->middlewares() as $midclass){
            if(!App::checkInheritance($midclass, Middleware::class)){
                throw new OpenGateException("ERROR: {$midclass} is not an instance of ".Middleware::class);
            }
            
            $this->middlewares[] = App::make($midclass);
        }
    }
    
    /**
     * 
     * @return \lib\inner\WebAppCore
     */
    public function resolvedCore(){
        return $this->instance;
    }
    
    /**
     * 
     * @return array
     */
    public function resolvedMiddlewares(){
        return $this->middlewares;
    }
    
    /**
     *
     * @param Closure $initInstanceCallback
     */
    public function setInitInstanceCallback(Closure $initInstanceCallback){
        $this->initInstanceCallback = $initInstanceCallback;
    }
    
    /**
     * 
     * @param callable $callable
     */
    public function readCommand(CommandBean $commandBean){
        $this->instance(StandardInstance::class)->initInstanceClass("cli");
        
        if(!is_blank($commandBean->query())){
            $this->instance->invokable($commandBean->query());
        }
        
        if(!is_blank($commandBean->middlewares())){
            $this->instance->middlewares($commandBean->middlewares());
        }
        
        $this->result();
    }
    
    /**
     * 
     * @param string $instance
     */
    public function readInstance(string $instance){        
        $this->instance($instance)->initInstanceClass()->result();
    }
    
    /**
     * 
     * @param RouteBean $routeBean
     */
    public function readAccess(RouteBean $routeBean){       
        $this->instance(StandardInstance::class)->initInstanceClass($routeBean->type());
        
        $this->instance->responseConfig($routeBean->responseConfig());
        
        if(!is_blank($routeBean->query())){
            $this->instance->invokable($routeBean->query());
        }
        
        if(!is_blank($routeBean->middlewares())){
            $this->instance->middlewares($routeBean->middlewares());
        }
        
        $this->result();
    }
}