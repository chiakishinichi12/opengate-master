<?php
namespace lib\util\configurables;

use OutOfBoundsException;
use lib\traits\ValueLocatorHelper;
use lib\util\Collections;
use lib\util\file\FileUtil;

class AppConfig {
    
    use ValueLocatorHelper;
    
    /**
     * 
     * @var string
     */
    protected $configpath = __DIR__."/../../../config.php";
    
    /**
     * 
     * @var Collections
     */
    protected $config;
    
    /**
     * 
     * @var string
     */
    protected $key;
    
    /**
     * 
     * @var mixed
     */
    protected $value = "";
    
    /**
     * 
     * @var string
     */
    protected $prefix;
    
    /**
     * 
     * constructor
     * 
     */
    public function __construct(){
        $this->config = collect();
        
        if($value = FileUtil::requireIfExists($this->configpath)){
            $this->config->addAll($value);
        }
    }
    
    /**
     * 
     * @param string $prefix
     * @return \lib\util\configurables\AppConfig
     */
    public function prefix(string $prefix){
        $this->prefix = $prefix;
        
        return $this;
    }
    
    /**
     * 
     * @param string $key
     * @param mixed $defaultValue
     * 
     * @return \lib\util\configurables\AppConfig
     */
    public function itemAt(string $key, $defaultValue = null){
        $this->key = $key;
        
        $recursive = $this->recursiveLocate($this->config, explode(".", $key), 0);
        
        if(is_blank($this->value = $recursive)){
            $this->value = $defaultValue;
        }
        
        return $this;
    }
    
    /**
     * 
     * @param string $key
     * @param mixed $defaultValue
     * 
     * @return string|NULL|mixed
     */
    public function get(string $key, $defaultValue = null){
        if(!is_blank($this->prefix)){
            $key = "{$this->prefix}.{$key}";
        }
        
        return $this->itemAt($key, $defaultValue)->value();
    }
    
    /**
     * 
     * @param mixed $key
     * @param mixed $value
     */
    public function modify($key, $value = null){
        if(is_null($value)){
            $value = $key;
            $key = $this->key;
        }
        
        if(!is_blank($this->prefix)){
            $key = "{$this->prefix}.{$key}";
        }
        
        $explodedPath = explode(".", $key);
        
        $identifier = $explodedPath[0];
        
        unset($explodedPath[0]);
                
        $data = $this->config->get($identifier);
                        
        if(!is_null($data)){
            if(is_array($data)){
                $this->recursiveUpdate($data,
                    array_values($explodedPath),
                    $value, 0);
            }else{
                $data = $value;
            }
            
            $this->config->put($identifier, $data);

        }else{
            throw new OutOfBoundsException("Unknown config identifier: {$identifier}");
        }
        
        return $this;
    }
    
    /**
     * 
     * actual item value
     * 
     * @return mixed|string|NULL
     */
    public function value(){
        return $this->value;
    }
    
    /**
     * 
     * default output
     * 
     * @return mixed|string|NULL
     */
    public function __toString(){
        if(is_string($this->value())){
            return $this->value();
        }
        
        return print_r($this->value(), true);
    }
    
    /**
     * 
     * saves updated values to the config.php file
     * 
     */
    public function write(){        
        $code = "<?php \n\n return {$this->config->export()};";
        
        return FileUtil::write($this->configpath, $code, "w");
    }
}