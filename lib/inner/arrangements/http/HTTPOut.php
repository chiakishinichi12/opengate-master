<?php
namespace lib\inner\arrangements\http;

use lib\inner\ApplicationGate;
use lib\util\accords\Outputter;

class HTTPOut implements Outputter{
    
    /**
     * 
     * @var ApplicationGate
     */
    protected $gate;
    
    /**
     * 
     * @var boolean
     */
    protected $sanitize;
    
    /**
     * 
     * @param ApplicationGate $gate
     */
    public function __construct(ApplicationGate $gate){
        $this->gate = $gate;    
    }
    
    /**
     * 
     * @param string $arg
     * @return string
     */
    protected function sanitizedString($arg){
        return htmlspecialchars($arg);
    }
    
    /**
     * 
     * @param bool $stze
     * @return \lib\inner\arrangements\http\HTTPOut
     */
    public function sanitize(bool $stze){
        $this->sanitize = $stze;
        
        return $this;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\accords\Outputter::print()
     */
    public function print($arg) : void {
        if(is_object($arg)){
            if(!method_exists($arg, "__toString")){
                $arg = print_r($arg, true);
            }
        }else if(is_array($arg)){
            $arg = print_r($arg, true);
        }
        
        if($this->sanitize){
            $arg = $this->sanitizedString($arg);
        }
        
        echo $arg;
    }

    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\accords\Outputter::println()
     */
    public function println($arg = "") : void {
        $this->print($arg);
        echo "\n";
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\accords\Outputter::printf()
     */
    public function printf($str, ...$args) : void {
        $arg = sprintf($str, ...$args);
        $this->print($arg);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\accords\Outputter::printbr()
     */
    public function printbr($arg = "") : void {
        $this->print($arg);
        echo "<br/>";
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\accords\Outputter::halt()
     */
    public function halt($arg) : void {
        $this->print($arg);
        
        exit(0);
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\accords\Outputter::halt()
     */
    public function haltln($arg = "") : void {
        $this->println($arg);
        
        exit(0);
    }
}