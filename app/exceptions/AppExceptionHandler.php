<?php
namespace app\exceptions;

use Throwable;
use lib\handlers\ExceptionHandler;

class AppExceptionHandler extends ExceptionHandler {
    
    // List all exceptions that will be exempted from report logging
    protected $dontReport = [
        
    ];
    
    public function report(Throwable $e){
        parent::report($e);
    }
    
    public function render(Throwable $e){
        parent::render($e);
    }
}