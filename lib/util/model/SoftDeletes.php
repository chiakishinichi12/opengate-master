<?php
namespace lib\util\model;

use lib\util\datagate\Prepare;
use lib\util\exceptions\ModelException;


trait SoftDeletes{
    /**
     *
     * Forcefully delete a record from the database, bypassing soft delete mechanism.
     *
     * @return boolean
     */
    public function forceDelete(){
        if(!$this->isStored()){
            return false;
        }
        
        $this->checkMandatories();
        $this->checkIfPrimaryKeyDefined();
        
        $pkeyValue = $this->escape($this->getPrimaryKeyValue());
        $whereClause = "`{$this->primary_key}` = (0)";
        
        return Prepare::connection($this->dbms)->delete(
            $this->table_name, $whereClause,[$pkeyValue]);
    }
    
    /**
     * 
     * Restore a soft-deleted record by setting the "delete_time" column to null.
     * 
     * @throws ModelException if the "delete_time" column doesn't exist in the database table.
     * 
     * @return boolean
     */
    public function restore(){
        if(!$this->isColumnExist("delete_time")){
            throw new ModelException(
                "SoftDelete Error: 'delete_time' column doesn't exists", class_name($this));
        }
        
        if(!is_blank($this->delete_time)){
            $this->delete_time = null;
        
            return $this->save();
        }
        
        return false;
    }
}

