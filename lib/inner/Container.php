<?php
namespace lib\inner;

use ArrayIterator;
use Closure;
use Countable;
use IteratorAggregate;
use ReflectionClass;
use ReflectionException;
use ReflectionParameter;
use ReflectionProperty;
use TypeError;
use lib\exceptions\OpenGateException;
use lib\util\Collections;
use lib\util\Tool;
use lib\util\exceptions\BindingResolutionException;
use lib\util\exceptions\LogicException;
use lib\util\exceptions\NotInstantiableException;

/**
 * 
 * 
 * replicating laravel's biggest instantiator
 * 
 * using Array Utils made only for OpenGate
 * 
 * @author Taylor Otwell 
 * 
 * Interpreted by antonio
 */
class Container implements IteratorAggregate, Countable {
    
    protected static $instance = null;
    
    /**
     * 
     * @var Collections
     */
    private $binds;
    
    /**
     *
     * @var Collections
     */
    private $instances;
    
    /**
     *
     * @var Collections
     */
    private $buildStack;
    
    /**
     *
     * @var Collections
     */
    private $shared;
    
    /**
     * 
     * @var Collections
     */
    private $aliases;
    
    /**
     *
     * @var Collections
     */
    private $contextual;
    
    /**
     * 
     * @var Collections
     */
    private $abstractAliases;
    
    /**
     * 
     * @var Collections
     */
    private $with;
    
    /**
     *
     * @var Collections
     */
    private $extenders;
    
    /**
     *
     * @var Collections
     */
    private $reboundCallbacks;
    
    /**
     *
     * @var Collections
     */
    private $resolved;
    
    /**
     *
     * @var Collections
     */
    private $globalBeforeResolvingCallbacks;
    
    /**
     *
     * @var Collections
     */
    private $beforeResolvingCallbacks;
    
    /**
     *
     * @var Collections
     */
    private $globalResolvingCallbacks;
    
    /**
     * 
     * @var Collections
     */
    private $resolvingCallbacks;
    
    /**
     *
     * @var Collections
     */
    private $globalAfterResolvingCallbacks;
    
    /**
     *
     * @var Collections
     */
    private $afterResolvingCallbacks;
    
    /**
     * 
     * @var Collections
     */
    private $methodBindings;
    
    protected function reset(){
        $collectionProperties = (new ReflectionClass(Container::class))
                ->getProperties(ReflectionProperty::IS_PRIVATE);
        
        foreach($collectionProperties as $property){
            $propName = $property->getName();
            $this->$propName = collect();
        }
    }
    
    /**
     * 
     * @return \lib\inner\Container
     */
    public static function getInstance(){
        if(self::$instance === null){
            self::$instance = new Container();
        }
        
        return self::$instance;
    }
    
    /**
     * 
     * @param Container $instance
     */
    public static function setInstance(Container $instance){
        self::$instance = $instance;
    }
    
    /**
     * 
     * checks if the given concrete is buildable
     * 
     * @param mixed $concrete
     * @param string $abstract
     * @return boolean
     */
    protected function isBuildable($concrete, $abstract){
        return $concrete === $abstract || $concrete instanceof Closure;
    }
    
    /**
     * Find the concrete binding for the given abstract in the contextual binding array.
     * 
     * @param string $abstract
     * @return NULL
     */
    protected function findInContextualBindings($abstract){
        $binding = $this->contextual->get($this->buildStack->end());
        
        return $binding && is_array($binding) ? $binding[$abstract] : null;
    }
    
    /**
     * Get the contextual concrete binding for the given abstract
     * 
     * @param string $abstract
     * @return void|NULL
     */
    protected function getContextualConcrete($abstract){
        if(!is_null($binding = $this->findInContextualBindings($abstract))){
            return $binding;
        }
        
        if(empty($this->abstractAliases->get($abstract))){
            return;
        }
        
        foreach($this->abstractAliases->get($abstract) as $alias){
            if(!is_null($binding = $this->findInContextualBindings($alias))){
                return $binding;
            }
        }
    }
    
    /**
     * 
     * Fire all of the before resolving callbacks
     * 
     * @param unknown $abstract
     * @param array $parameters
     */
    protected function fireBeforeResolvingCallbacks($abstract, $parameters = []){
        $this->fireBeforeCallbackArray($abstract, $parameters, $this->globalBeforeResolvingCallbacks);
        
        foreach($this->beforeResolvingCallbacks as $type => $callbacks){
            if($type === $abstract || is_subclass_of($abstract, $type)){
                $this->fireBeforeCallbackArray($abstract, $parameters, $callbacks);
            }
        }
    }
    
    /**
     * 
     * fire an array of callbacks with an object
     * 
     * @param string $abstract
     * @param string $parameters
     * @param array $callbacks
     */
    protected function fireBeforeCallbackArray($abstract, $parameters, $callbacks){
        foreach($callbacks as $callback){
            $callback($abstract, $parameters, $this);
        }
    }
    
    /**
     * 
     * Fire all of the resolving callbacks
     * 
     * @param string $abstract
     * @param mixed $object
     */
    protected function fireResolvingCallbacks($abstract, $object){
        $this->fireCallbackArray($object, $this->globalResolvingCallbacks);
        
        $this->fireCallbackArray($object, 
            $this->getCallbacksForType($abstract, $object, $this->resolvingCallbacks));
        
        $this->fireAfterResolvingCallbacks($abstract, $object);
    }
    
    /**
     * 
     * @param string $abstract
     * @param mixed $object
     */
    protected function fireAfterResolvingCallbacks($abstract, $object){
        $this->fireCallbackArray($object, $this->globalAfterResolvingCallbacks);
        
        $this->fireCallbackArray($object,
            $this->getCallbacksForType($abstract, $object, $this->afterResolvingCallbacks));
    }
    
    /**
     * 
     * Fire an array of callbacks with an object
     * 
     * @param mixed $object
     * @param array $callbacks
     */
    protected function fireCallbackArray($object, $callbacks){
        foreach($callbacks as $callback){
            $callback($object, $this);
        }
    }
    
    /**
     * 
     * Get all callbacks for a given type
     * 
     * @param string $abstract
     * @param object $object
     * @param array $callbacksPerType
     */
    protected function getCallbacksForType($abstract, $object, $callbacksPerType){
        $results = collect();
        
        foreach($callbacksPerType as $type => $callbacks){
            if($type === $abstract || $object instanceof $type){
                $results->merge($callbacks);
            }
        }
        
        return $results;
    }
    
    /**
     * 
     * Remove an alias from contextual binding alias cache
     * 
     * @param string $searched
     */
    protected function removeAbstractAlias($searched){
        if(!$this->aliases->get($searched)){
            return;
        }
        
        foreach($this->abstractAliases as $abstract => $aliases){
            foreach($aliases as $index => $alias){
                if($alias === $searched){
                    $this->abstractAliases->get($abstract)->remove($index);
                }
            }
        }
    }
    
    /**
     * Drop all of the stale instances and aliases
     *
     * @param string $abstract
     */
    protected function dropStaleInstances($abstract){
        $this->instances->remove($abstract);
        $this->aliases->remove($abstract);
    }
    
    /**
     * 
     * Get the concrete type for a given abstract
     * 
     * @param mixed $abstract
     * @return mixed
     */
    protected function getConcrete($abstract){   
        if($concrete = $this->binds->get($abstract)){
            return $concrete;
        }
        
        return $abstract;
    }
    
    /**
     * Get the Closure to be used when building a type.
     * 
     * @param string $abstract
     * @param string $concrete
     * @return Closure
     */
    protected function getClosure($abstract, $concrete){
        return function ($container, $parameters = []) use ($abstract, $concrete) {
            if($abstract == $concrete){
                return $container->build($concrete);
            }
            
            return $container->resolve($concrete, $parameters, false);
        };
    }
    
    /**
     * Get the extender callbacks for a given type.
     * 
     * @param string $abstract
     * @return \lib\util\Collections|boolean|mixed
     */
    protected function getExtenders($abstract){
        return $this->extenders->get($this->getAlias($abstract)) ?? collect();
    }
    
    /**
     * 
     * Remove all the extender callbacks for a given type
     * 
     * @param string $abstract
     */
    public function forgetExtenders($abstract){
        $this->extenders->remove($this->getAlias($abstract));
    }
    
    /**
     * 
     * @return \lib\util\Collections
     */
    public function getBindings(){
        return $this->binds;
    }
    
    /**
     * 
     * Resolves the given type from the container
     * 
     * @param unknown $abstract
     * @param array $parameters
     * @return boolean|mixed|unknown
     */
    protected function resolve($abstract, $parameters = [], $raiseEvents = true){
        $abstract = $this->getAlias($abstract);
        
        if($raiseEvents){
            $this->fireBeforeResolvingCallbacks($abstract, $parameters);
        }
        
        $concrete = $this->getContextualConcrete($abstract);
        
        $needsContextualBuild = !empty($parameters) || !is_null($concrete);
        
        if(($instance = $this->instances->get($abstract)) && !$needsContextualBuild){
            return $instance;
        }
        
        $this->with->add(collect($parameters));
        
        if(is_null($concrete)){
            $concrete = $this->getConcrete($abstract);
        }
        
        if($this->isBuildable($concrete, $abstract)){
            $object = $this->build($concrete);
        }else{
            $object = $this->make($concrete);
        }
        
        // if the extenders were defined, setting them must be considered to allow extension of services
        // such as changing configuration or decorating the object
        foreach($this->getExtenders($abstract) as $extender){
            $object = $extender($object, $this);
        }
        
        // if the requested type is defined as singleton, the instance will be cached-off in "memory" 
        // so that we don't have to define/create a new instance of an object for the requested abstract type
        if($this->isShared($abstract) && !$needsContextualBuild){
            $this->instances->put($abstract, $object);
        }
        
        if($raiseEvents){
            $this->fireResolvingCallbacks($abstract, $object);
        }
        
        $this->resolved->put($abstract, true);
        
        $this->with->pop();
        
        return $object;
    }
    
    /**
     * 
     * Fire the rebound callbacks for the fiven abstract types
     * 
     * @param string $abstract
     */
    protected function rebound($abstract){
        $instance = $this->make($abstract);
        
        foreach($this->getReboundCallbacks($abstract) as $callback){
            call_user_func($callback, $this, $instance);
        }
    }
    
    /**
     * Get the parameter override for a dependency
     * 
     * @param ReflectionParameter $dependency
     * @return boolean|mixed
     */
    protected function getParameterOverride($dependency){
        return $this->getLastParameterOverride()->get($dependency->name);
    }
    
    /**
     * 
     * @param ReflectionParameter $dependency
     * @return boolean
     */
    protected function hasParameterOverride($dependency){
        return $this->getLastParameterOverride()->hasKey($dependency->name);    
    }
    
    /**
     * 
     * Get the last parameter override
     * 
     * @return \lib\util\Collections|mixed
     */
    protected function getLastParameterOverride(){
        return $this->with->count() ? $this->with->end() : collect();
    }
    
    /**
     * get the "rebound" callbacks for the given abstract type
     * 
     * @param string $abstract
     * @return \lib\util\Collections|boolean|mixed
     */
    protected function getReboundCallbacks($abstract){
        return $this->reboundCallbacks->get($abstract) ?? collect();
    }
    
    /**
     * 
     * Resolve a non-class hinted primitive dependency
     * 
     * @param ReflectionParameter $parameter
     * @throws BindingResolutionException
     * @return \Closure|mixed
     */
    protected function resolvePrimitive(ReflectionParameter $parameter){
        if(!is_null($concrete = $this->getContextualConcrete("$".$parameter->getName()))){
            return $concrete instanceof Closure ? $concrete($this) : $concrete;
        }
        
        if($parameter->isDefaultValueAvailable()){
            return $parameter->getDefaultValue();
        }
        
        throw new BindingResolutionException("Unresolvable dependency resolving [{$parameter}] in class "
            ."{$parameter->getDeclaringClass()->getName()}");
    }
    
    /**
     * 
     * Resolve all the dependencies from the ReflectionParameters
     * 
     * @param array $dependencies
     * @return \lib\util\Collections
     */
    protected function resolveDependencies(Array $dependencies){
        $results = collect();
        
        foreach($dependencies as $dependency){
            
            if($this->hasParameterOverride($dependency)){
                $results->add($this->getParameterOverride($dependency));
                
                continue;
            }
            
            $result = is_null(Tool::getParameterClassName($dependency)) 
                        ? $this->resolvePrimitive($dependency)
                        : $this->resolveClass($dependency);
            
            if($dependency->isVariadic()){
                $results->merge($results, $result);
            }else
                $results->add($result);
        }
        
        return $results;
    }
    
    /**
     * 
     * Resolve a class based dependency from the container
     * 
     * @param ReflectionParameter $parameter
     * @throws BindingResolutionException
     * @return boolean|mixed|\lib\inner\unknown|NULL|mixed|array
     */
    protected function resolveClass(ReflectionParameter $parameter){
        try{
            return $parameter->isVariadic() 
                ? $this->resolveVariadicClass($parameter)
                : $this->make(Tool::getParameterClassName($parameter));
        }
        
        // if not resolve, will check other ways to find suitable value for the parameter as a part of the resolve.
        catch(BindingResolutionException $e){
            if($parameter->isDefaultValueAvailable()){
                return $parameter->getDefaultValue();
            }
            
            if($parameter->isVariadic()){
                return collect();
            }
            
            throw $e;
        }
    }
    
    /**
     * Resolve a class based dependency from the container
     * 
     * 
     * @param ReflectionParameter $parameter
     * @return boolean|mixed|\lib\inner\unknown|NULL|unknown
     */
    protected function resolveVariadicClass(ReflectionParameter $parameter){
        $className = Tool::getParameterClassName($parameter);
        
        $abstract = $this->getAlias($className);
        
        if(! ($concrete = $this->getContextualConcrete($abstract)) instanceof Collections){
            return $this->make($className);
        }
        
        return $concrete->map(function($abstract){
            return $this->resolve($abstract);
        });
    }
    
    /**
     * 
     * Register a new before resolving callback for all types
     * 
     * @param string $abstract
     * @param Closure $callback
     */
    public function beforeResolving($abstract, Closure $callback = null){
        if(is_string($abstract)){
            $abstract = $this->getAlias($abstract);
        }
        
        if($abstract instanceof Closure && is_null($callback)){
            $this->globalBeforeResolvingCallbacks->add($abstract);    
        }else{
            if(!($abstractBefore = $this->$this->beforeResolvingCallbacks->get($abstract))){
                $abstractBefore = collect();
                $this->beforeResolvingCallbacks->put($abstract, $abstractBefore);
            }
            
            $abstractBefore->add($callback);
        }
    }
    
    /**
     * Register a new resolving callback
     * 
     * @param string $abstract
     * @param Closure $callback
     */
    public function resolving($abstract, Closure $callback = null){
        if(is_string($abstract)){
            $abstract = $this->getAlias($abstract);
        }
        
        if(is_null($callback) && $abstract instanceof Closure){
            $this->globalResolvingCallbacks->add($abstract);
        }else{
            if(!($abstractResolving = $this->resolvingCallbacks->get($abstract))){
                $abstractResolving = collect();
                $this->resolvingCallbacks->put($abstract, $abstractResolving);
            }
            
            $abstractResolving->add($callback);
        }
    }
    
    /**
     * Register a new after resolving callback for all types
     * 
     * @param string $abstract
     * @param Closure $callback
     */
    public function afterResolving($abstract, Closure $callback = null){
        if(is_string($abstract)){
            $abstract = $this->getAlias($abstract);
        }
        
        if(is_null($callback) && $abstract instanceof Closure){
            $this->globalAfterResolvingCallbacks->add($abstract);
        }else{
            if(!($abstractAfter = $this->afterResolvingCallbacks->get($abstract))){
                $abstractAfter = collect();
                $this->afterResolvingCallbacks->put($abstract, $abstractAfter);
            }
            
            $abstractAfter->add($callback);
        }
    }
    
    /**
     * Alias a type to a different name
     * 
     * @param string $abstract
     * @param string $alias
     * @throws LogicException
     */
    public function alias($abstract, $alias){
        if($alias === $abstract){
            throw new LogicException("[{$abstract}] is aliased to itself");
        }
        
        $this->aliases->put($alias, $abstract);
        
        if(!($aliasCollection = $this->abstractAliases->get($abstract))){
            $aliasCollection = collect();        
            $this->abstractAliases->put($abstract, $aliasCollection);
        }
        
        $aliasCollection->add($alias);
    }
    
    /**
     * 
     * Get the alias for an abstract if available
     * 
     * @param string $abstract
     * @return string
     */
    public function getAlias($abstract){
        return $this->aliases->get($abstract) 
            ? $this->getAlias($this->aliases->get($abstract))
            : $abstract;
    }
    
    /**
     * 
     * "Extend" an abstract type in the container
     * 
     * @param unknown $abstract
     * @param Closure $closure
     */
    public function extend($abstract, Closure $closure){
        $abstract = $this->getAlias($abstract);
        
        if($instance = $this->instances->get($abstract)){
            $this->instances->put($abstract, $closure($instance, $this));
            
            $this->rebound($abstract);
        }else{
            if(!($absExtender = $this->extenders->get($abstract))){
                $absExtender = collect();
                $absExtender->add($abstract);   
                
                $this->extenders->put($abstract, $absExtender);
            }else{
                $absExtender->add($abstract);
            }
            
            if($this->resolved($abstract)){
                $this->rebound($abstract);
            }
        }
    }
    
    /**
     * Add contextual binding to the container
     * 
     * @param unknown $concrete
     * @param unknown $abstract
     * @param unknown $implementation
     */
    public function addContextualBinding($concrete, $abstract, $implementation){        
        $this->contextual->put($concrete, collect()->put($this->getAlias($abstract), $implementation));
    }
    
    /**
     * 
     * Instantiate a concrete instance of a given type
     * 
     * @param unknown $concrete
     * @throws BindingResolutionException
     * @throws NotInstantiableException
     * @return unknown
     */
    public function build($concrete){
        if($concrete instanceof Closure){
            return $concrete($this, $this->getLastParameterOverride());
        }
        
        try{
            $reflector = new ReflectionClass($concrete);
        }catch(ReflectionException $e){
            throw new BindingResolutionException("Target class [{$concrete}] doesn't exist", 0, $e);
        }
        
        if(!$reflector->isInstantiable()){
            throw new NotInstantiableException("Target class [{$concrete}] cannot be instantiated");
        }
        
        $this->buildStack->add($concrete);
        
        $constructor = $reflector->getConstructor();
        
        
        // no constructor defined means there's no dependencies to find for parameters
        if(is_null($constructor)){
            $this->buildStack->pop();
            
            return new $concrete;
        }
        
        $dependencies = $constructor->getParameters();
        
        try{
            $instances = $this->resolveDependencies($dependencies);
        }catch(BindingResolutionException $e){
            $this->buildStack->pop();
            
            throw $e;
        }
        
        $this->buildStack->pop();
                
        return $reflector->newInstanceArgs($instances->toArray());
    }
    
    /**
     * 
     * @param string $abstract
     * @return boolean
     */
    public function bound($abstract){
        return $this->binds->get($abstract) ||
                $this->instances->get($abstract) ||
                $this->aliases->get($abstract);
    }
    
    /**
     * 
     * checks if the given abstract type is already resolved
     * 
     * @param string $abstract
     * @return boolean
     */
    public function resolved($abstract){
        if($this->aliases->get($abstract)){
            $abstract = $this->getAlias($abstract);
        }
        
        return $this->resolved->get($abstract) || 
                $this->instances->get($abstract);
    }
    
    /**
     * 
     * register a binding with a container
     * 
     * @param string $abstract
     * @param mixed|string $concrete
     * @param boolean $shared
     * @throws OpenGateException
     */
    public function bind($abstract, $concrete = null, $shared = false){
        $this->dropStaleInstances($abstract);
        
        if(is_null($concrete)){
            $concrete = $abstract;
        }
        
        if(!($concrete instanceof Closure)){
            if(!is_string($concrete)){
                throw new TypeError(self::class."::bind(): 2nd Param (concrete) must be a type of Closure|string|null");
            }
            
            $concrete = $this->getClosure($abstract, $concrete);
        }
        
        $this->binds->put($abstract, $concrete);
        
        if($shared){
            $this->shared->add($abstract);
        }
        
        // if the abstract type was already resolved in this container we'll fire the
        // rebound listener so that any objects which have already gotten resolved 
        // can have their copy of the object updated via the listener callbacks
        if($this->resolved($abstract)){
            $this->rebound($abstract);
        }
    }
    
    /**
     * 
     * bind a new callback to an abstract's rebind event
     * 
     * @param string $abstract
     * @param Closure $callback
     * @return boolean|mixed|\lib\inner\unknown|NULL
     */
    public function rebinding($abstract, Closure $callback){
        $abstract = $this->getAlias($abstract);
        
        if(!($rebindCallback = $this->reboundCallbacks->get($abstract))){
            $rebindCallback = collect();
            $rebindCallback->add($callback);
            
            $this->reboundCallbacks->put($abstract, $rebindCallback);
        }else
            $rebindCallback->add($callback);
        
        $this->reboundCallbacks->add($abstract = $this->getAlias($abstract));
        
        if($this->bound($abstract)){
            return $this->make($abstract);
        }
    }
    
    /**
     * 
     * Refresh an instance on the given target and method
     * 
     * @param string $abstract
     * @param mixed $target
     * @param string $method
     * @return boolean|mixed|\lib\inner\unknown|NULL
     */
    public function refresh($abstract, $target, $method){
        return $this->rebinding($abstract, function($app, $instance) use($target, $method){
            $target->{$method}($instance);
        });
    }
    
    /**
     * 
     * Register an existing instance as shared in the container
     * 
     * @param string $abstract
     * @param mixed $instance
     * @return unknown
     */
    public function instance($abstract, $instance){
        $this->removeAbstractAlias($abstract);
        
        $isBound = $this->bound($abstract);
        
        $this->aliases->remove($abstract);
        
        // We'll check to determine if this type has been bound before. and if it has
        // we'll fire the rebound callbacks registered with the container and it
        // can be updated with consuming classes that have gotten resolve here.
        $this->instances->put($abstract, $instance);
        
        if($isBound){
            $this->rebound($abstract);
        }
        
        return $instance;
    }
    
    /**
     *
     * @param string $abstract
     * @param mixed|string $concrete
     */
    public function singleton($abstract, $concrete = null){
        $this->bind($abstract, $concrete, true);
    }
    
    /**
     *
     * @param string $abstract
     * @return boolean
     */
    public function isShared($abstract){
        return $this->shared->has($abstract);
    }
    
    /**
     * Get a closure to resolve the given type from the container
     * 
     * @param string $abstract
     * @return Closure
     */
    public function factory($abstract){
        return function () use ($abstract) {
            return $this->make($abstract);
        };
    }
    
    /**
     * Resolve the given type from the container
     * 
     * @param string $abstract
     * @param array $parameters
     * @return boolean|mixed|unknown|NULL
     */
    public function make($abstract, Array $parameters = []){
        return $this->resolve($abstract, $parameters);
    }
    
    /**
     * Call the given Closure / class@method and inject its dependencies
     * 
     * @param callable|string $callback
     * @param array $parameters
     * @param string|null $defaultMethod
     * @return mixed|\Closure
     */
    public function call($callback, $parameters = [], $defaultMethod = null){
        return BoundMethod::call($this, $callback, $parameters, $defaultMethod);
    }
    
    /**
     * 
     * Bind a callback to resolve with Container::call
     * 
     * @param array|string $method
     * @param Closure $callback
     */
    public function bindMethod($method, $callback){
        $this->methodBindings->put($this->parseBindMethod($method), $callback);
    }
    
    /**
     * Get the method to be bound in class@method format
     * 
     * @param unknown $method
     * @return string|unknown
     */
    protected function parseBindMethod($method){
        if(is_array($method)){
            return "{$method[0]}@{$method[1]}";
        }
        
        return $method;
    }
    /**
     * 
     * Get the method binding for the event
     * 
     * @param string $method
     * @param string $instance
     * @return mixed
     */
    public function callMethodBinding($method, $instance){
        return call_user_func($this->methodBindings->get($method), $instance, $this);
    }
    
    /**
     * Determine if the container has a method binding.
     * 
     * @param string $method
     * @return boolean
     */
    public function hasMethodBinding($method){
        return $this->methodBindings->has($method);
    }
    
    #[\ReturnTypeWillChange]
    public function getIterator(){
        return new ArrayIterator($this->binds);
    }
    
    #[\ReturnTypeWillChange]
    public function count(){
        return count($this->binds);    
    }
}