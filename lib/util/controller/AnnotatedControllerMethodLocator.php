<?php
namespace lib\util\controller;

use ReflectionClass;
use ReflectionMethod;
use lib\inner\App;
use lib\inner\ApplicationGate;
use lib\util\Collections;
use lib\util\annotation\Annotation;
use lib\util\annotation\AnnotationParser;
use lib\util\exceptions\ControllerException;

class AnnotatedControllerMethodLocator {
    
    /**
     * 
     * @var ApplicationGate
     */
    protected $gate;
    
    /**
     * 
     * @var array
     */
    protected $annotations;
    
    /**
     *
     * @var Collections
     */
    protected $controllers;
    
    /**
     * 
     * @var Collections
     */
    protected $queries;
    
    /**
     * 
     * @var Collections
     */
    protected $reusables;
    
    /**
     * 
     * @param ApplicationGate $gate
     */
    public function __construct(ApplicationGate $gate){
        $this->gate = $gate;
        
        $this->controllers = collect();
        $this->queries = collect();
        
        $this->registerAnnotations();
    }
    
    /**
     * 
     * defining necessary annotations for controller methods
     * 
     */
    protected function registerAnnotations(){
        $this->annotations = [
            (new Annotation("Command"))
                ->setProperty(["keywords", ["array", "string"], true])
                ->setProperty(["name", "string"]),
            (new Annotation("CommandDesc"))
                ->setProperty(["desc", "string", true]),
            (new Annotation("Route"))
                ->setProperty(["method", "string"])
                ->setProperty(["type", "string"])
                ->setProperty(["uri", ["string", "array"], true]),
            (new Annotation("Primary"))
                ->setProperty(["uri", "string", true]),
            (new Annotation("RouteConcat"))
                ->setProperty(["uri", "string", true])
                ->setProperty(["position", "string"])
                ->setReusable(true),
            (new Annotation("RoutePush"))
                ->setProperty(["uri", "string", true])
                ->setReusable(true),
            (new Annotation("ResponseConfig"))
                ->setProperty(["contentType", "string"])
                ->setProperty(["readonly", "boolean"]),
            (new Annotation("MidClassNS"))
                ->setProperty(["base", "string", true]),
            (new Annotation("Middleware"))
                ->setProperty(["baseNS", "string"])
                ->setProperty(["classes", ["array", "string"], true])
                ->setReusable(true),
            (new Annotation("Subdomain"))
                ->setProperty(["hosts", "array", true]),
            (new Annotation("Localized"))
        ];
    }
    
    /**
     * 
     * retrieves annotations used on a controller
     * 
     * @return Collections
     */
    public function registeredAnnotations(){
        return collect($this->annotations);
    }
    
    /**
     * 
     * @param string $controller
     * @throws ControllerException
     * @return \lib\util\controller\AnnotatedControllerMethodLocator
     */
    public function addController($controller){        
        if(!App::checkInheritance($controller, Controller::class)){
            throw new ControllerException("'{$controller}' is not a subclass of ".Controller::class);
        }
        
        $this->controllers->add($controller);
        
        return $this;
    }
    
    /**
     * 
     * querying and evaluated available annotation within doc comment
     * 
     */
    public function scanQualifiedQueries(){
        $this->controllers->each(function($contcls){
            $rc = new ReflectionClass($contcls);
            $methods = collect($rc->getMethods(ReflectionMethod::IS_PUBLIC));
            
            $topAnnotationData = $this->evaluateAnnotations($rc)->getParsedValues();
                        
            // will be evaluated even if there's no annotation found
            // @ the top of class name
            $this->queries->put($contcls, $topAnnotationData);
            
            $methods->each(function(ReflectionMethod $method) use ($rc, $contcls){
                if($method->getDeclaringClass()->getName() === $rc->getName()){
                    $parser = $this->evaluateAnnotations($method);
                                            
                    $parsedAnnotationData = $parser->getParsedValues();
                    
                    if(!$this->gate->isRunningUnitTest()){
                        if($this->gate->checkCLI()){
                            if($parsedAnnotationData->isNull("@Command")){
                                return;
                            }
                        }else{
                            if($parsedAnnotationData->isNull("@Route")){
                                return;
                            }
                        }
                    }else{
                        // validation for unit testing http request simulation
                        if($parsedAnnotationData->isNull("@Route")){
                            return;
                        }
                    }
                    
                    if($parsedAnnotationData->count()){
                        $this->queries->put("{$contcls}@{$method->getName()}", $parsedAnnotationData);
                    }
                }
            });
        });
    }
    
    /**
     * 
     * @return \lib\util\Collections
     */
    public function getQueries(){
        return $this->queries;
    }
    
    /**
     * 
     * @param ReflectionMethod $method
     * @return \lib\util\annotation\AnnotationParser
     */
    protected function evaluateAnnotations($reflection){
        return App::make(AnnotationParser::class)
            ->reference($reflection)
            ->annotations($this->annotations)
            ->parse();
    }
}