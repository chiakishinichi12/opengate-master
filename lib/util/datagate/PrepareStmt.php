<?php
namespace lib\util\datagate;

use lib\traits\Sanitation;
use lib\util\ContentValues;
use lib\util\QuerySpecification;
use lib\util\SequelDump;
use lib\util\StringUtil;
use lib\util\exceptions\SQLException;
use lib\util\vroller\RollerGate;

class PrepareStmt {
    
    use Sanitation;
    
    /**
     * 
     * @var string
     */
    protected $dbms = "mysql";
    
    /**
     * 
     * @param string $dbms
     * 
     * @return \lib\util\datagate\PrepareStmt
     */
    public function connection(string $dbms){
        $this->dbms = $dbms;
        
        return $this;
    }
    
    /**
     *
     * @return string
     */
    protected function getSQLTimezone(){
        if(is_blank($timezone = config("db_conn.{$this->dbms}.timezone"))){
            $timezone = "+08:00";
        }
        
        return $timezone;
    }
    
    /**
     *
     * {@inheritDoc}
     * @see PrepareStatement::insert()
     */
    public function insert(ContentValues $values){
        $contents = $values->getContents();
        
        if(!count($contents)){
            throw new SQLException("Content Values array has no elements.");
        }
        
        $sql = "INSERT INTO {$values->getTableName()} (";
        $sql .= implode(", ", array_keys($contents));
        
        $arrVals = array_values($contents);
        
        foreach($arrVals as &$value){
            $value = $this->psval($value);
        }
        
        $sql .= ") VALUES (".implode(", ", $arrVals).")";
        
        if(SequelDump::insertAndUpdate()){
            var_dump($sql);
        }
        
        return DB::connection($this->dbms)->execute($sql);
    }
    
    /**
     *
     * {@inheritDoc}
     * @see PrepareStatement::update()
     */
    public function update(ContentValues $values, $where = ""){
        $contents = $values->getContents();
        
        if(!count($contents)){
            throw new SQLException("Content Values array has no elements.");
        }
        
        $sql = "UPDATE {$values->getTableName()} SET ";
        
        $k = 0;
        
        $updatePair = [];
        foreach($contents as $key => &$value){
            $value = $this->psval($value);
            
            $updatePair[] = "`{$key}`={$value}";
            
            $where = str_replace("k({$k})", "`{$key}`", $where);
            $where = str_replace("v({$k})", $value, $where);
            
            $k++;
        }
        
        $sql .= implode(", ", $updatePair)." ";
        
        if(strlen(trim($where)) !== 0){
            $sql .= "WHERE {$where}";
        }
        
        if(SequelDump::insertAndUpdate()){
            var_dump($sql);
        }
        
        return DB::connection($this->dbms)->execute($sql);
    }
    
    /**
     *
     * @param mixed $value
     * @return string
     */
    protected function psval($value){
        if(!is_null($value)){
            if(strcasecmp($value, ContentValues::NOW) === 0){
                $sqlTimezone = $this->getSQLTimezone();
                $value = "CONVERT_TZ(NOW(), @@session.time_zone, '{$sqlTimezone}')";
            }else if(strcasecmp($value, ContentValues::UUID) === 0){
                $value = "\"".StringUtil::warpUuid()."\"";
            }else if(is_string($value)){
                if(class_exists($value)){
                    $value = RollerGate::valueOf($value);
                    
                    if(is_string($value) || (is_object($value) && method_exists($value, "__toString"))){
                        $value = "\"{$this->escape($value)}\"";
                    }else if(is_null($value)){
                        $value = "NULL";
                    }
                }else{
                    $value = "\"{$this->escape($value)}\"";
                }
            }
        }else{
            $value = "NULL";
        }
        
        ends:
        return $value;
    }
    
    /**
     *
     * {@inheritDoc}
     * @see PrepareStatement::query()
     */
    public function query(QuerySpecification $specification){
        $props = $specification->getFilledProperties();
        
        if(is_array($props["fields"])){
            foreach($props["fields"] as $key => &$field){
                $field = "`{$field}`";
                $props["where"] = str_replace("k({$key})", $field, $props["where"]);
            }
            
            foreach($props["values"] as $key => &$value){
                $value = $this->escape($value);
                $props["where"] = str_replace("v({$key})", "\"{$value}\"", $props["where"]);
            }
            
            if(count($props["fields"])){
                $props["fields"] = implode(", ", $props["fields"]);
            }else{
                $props["fields"] = "*";
            }
            
            $sql = "SELECT {$props["fields"]} FROM `{$props["tableName"]}` ";
            
            if(strlen(trim($props["where"])) !== 0){
                $sql .= "WHERE {$props["where"]} ";
            }
            
            if(strlen(trim($props["orderBy"])) !== 0){
                $sql .= "{$props["orderBy"]} ";
            }
            
            if(strlen(trim($props["limit"])) !== 0){
                $sql .= "{$props["limit"]} ";
            }
        }else{
            throw new SQLException("'field' property is not an array");
        }
        
        return DB::connection($this->dbms)->raw($sql);
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\PrepareStatement::delete()
     */
    public function delete(string $tableName, string $whereClause = "", array $whereArgs = []){
        $tableName = $this->escape($tableName);
        
        if(is_blank($whereClause)){
            $whereClause = 1;
        }
        
        foreach($whereArgs as $key => $arg){
            $whereClause = str_replace("({$key})", "\"{$arg}\"", $whereClause);
        }
        
        $sql = "DELETE FROM `{$tableName}` WHERE {$whereClause}";
        
        if(SequelDump::deletion()){
            var_dump($sql);
        }
        
        return DB::connection($this->dbms)->execute($sql);
    }
}