<?php
namespace lib\util\datagate;

use lib\util\builders\SelectBuilder;

class DatagateHelper {
    
    /**
     * 
     * @var string
     */
    protected $dbms = "mysql";
    
    /**
     * 
     * @var WarpRawSQL
     */
    protected $warper;
    
    /**
     * 
     * initiates WarpRawSQL if null
     * 
     * @return boolean
     */
    protected function initWarperIfNull(){
        if(is_null($this->warper)){
            $this->warper = new WarpRawSQL($this->dbms);
            
            return true;
        }
        
        return false;
    }
    
    /**
     * 
     * @param unknown $dbms
     * @return \lib\util\datagate\DatagateHelper
     */
    public function connection(string $dbms){
        $this->dbms = $dbms;
        
        if(!$this->initWarperIfNull()){
            $this->warper->changeConnection($dbms);
        }

        return $this;
    }
    
    /**
     * 
     * returns 'true' if connected to a selected dbms
     * 
     * @return boolean
     */
    public function isConnected(){
        $this->initWarperIfNull();
        
        return $this->warper->isConnected();
    }
    
    /**
     * 
     * prints the error message thrown by an SQL conflict/error
     * 
     * @return \lib\util\datagate\DatagateHelper
     */
    public function errorDump(){
        $this->warper->errorDump();
        
        return $this;
    }
    
    /**
     * 
     * @param string|array $tablename
     * 
     * @return \lib\util\builders\SelectBuilder
     */
    public function table($tablename){
        return new SelectBuilder($tablename, $this->dbms);
    }
    
    /**
     * 
     * @param string $sql
     * @param array $args
     * 
     * @return boolean|mixed
     */
    public function raw(string $sql, array $args = []){
        $this->initWarperIfNull();
        
        return $this->warper->rawQuery($sql, $args);
    }
    
    /**
     * 
     * @param string $sql
     * @param array $args
     * 
     * @return boolean|mixed
     */
    public function execute(string $sql, array $args = []){
        $this->initWarperIfNull();
        
        return $this->warper->rawModify($sql, $args);
    }
}