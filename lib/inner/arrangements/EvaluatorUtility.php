<?php
namespace lib\inner\arrangements;

use lib\exceptions\OpenGateException;
use lib\util\StringUtil;

/**
 * 
 * @author Antonius Aurelius
 *
 */
trait EvaluatorUtility {
    
    /**
     * 
     * Dynamically evaluates a string of PHP code and returns the result.
     *
     * This method allows for runtime execution of PHP code passed as a string.
     * It ensures that single-line code strings are properly terminated with a semicolon,
     * and handles errors gracefully to prevent runtime crashes.
     * 
     * @param string $code
     * 
     * @return mixed|string
     */
    public function evaluate(string $code){
        try{
            $lined = StringUtil::getLinedString($code);
            
            // if the expression is a single line, append a semicolon at the end of it.
            if($lined->length() === 1){
                $code = "{$code};";
            }
            
            return eval($code);
        }catch(OpenGateException $ex){
            // Handle errors gracefully
            return "Error in eval: {$ex->getMessage()}";
        }
    }
}

