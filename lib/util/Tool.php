<?php
namespace lib\util;

use Closure;
use ReflectionNamedType;
use ReflectionParameter;

class Tool {
    /**
     * Return the default value of the given value
     * 
     * @param mixed $value
     * @return mixed
     */
    public static function unwrapIfClosure($value){
        return $value instanceof Closure ? $value() : $value;
    }
    
    /**
     * 
     * @param ReflectionParameter $parameter
     */
    public static function getParameterClassName($parameter){
       
        $type = $parameter->getType();
        
        if(! $type instanceof ReflectionNamedType || $type->isBuiltin()){
            return null;
        }
        
        $name = $type->getName();
        
        if(!is_null($class = $parameter->getDeclaringClass())){
            if($name === "self"){
                return $class->getName();
            }
            
            if($name === "parent" && $parent = $class->getParentClass()){
                return $parent->getName();
            }
        }
        
        return $name;
    }
}