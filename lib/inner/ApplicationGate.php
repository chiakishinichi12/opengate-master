<?php
namespace lib\inner;

use Closure;
use lib\inner\arrangements\EvaluatorUtility;
use lib\traits\Checker;
use lib\util\Collections;
use lib\util\Reader;
use lib\util\cli\unittest\GateTestCore;

/**
 * 
 * @author Jiya Sama
 *
 */
class ApplicationGate extends Container {
   
    use Checker, EvaluatorUtility;
    
    /**
     * 
     * @var Closure
     */
    protected $warpAfterCoreInstantiationCallback;
    
    /**
     * 
     * @var boolean
     */
    protected $hasBeenBootstrapped = false;
    
    /**
     * 
     * @var Collections
     */
    protected $terminatingCallbacks;
    
    /**
     * 
     * @var mixed
     */
    protected $context;
    
    public function __construct(){
        $this->initCollectionComponents();
        $this->registerBaseBindings();
        $this->registerCoreContainerAliases();
    }
    
    /**
     * 
     * this is where put all definitions of properties that has 'Collections' instance
     * 
     */
    protected function initCollectionComponents(){
        $this->terminatingCallbacks = collect();
    }
    
    /**
     * 
     * aligning base bindings
     * 
     */
    protected function registerBaseBindings(){
        self::setInstance($this);
        
        $this->reset();
        
        $this->instance("gate", $this);
        
        $this->instance(Container::class, $this);
    }
    
    /**
     * 
     * @param mixed $context
     */
    public function context(mixed $context){
        $this->context = $context;
        
        return $this;
    }
    
    /**
     * 
     * aligning alias(es) to the main classes. 
     * 
     */
    protected function registerCoreContainerAliases(){
        // TODO to add more core classes in the future
        
        $aliases = collect([
            "gate" => collect(self::class, Container::class)
        ]);
        
        foreach($aliases as $index => $stack){
            foreach($stack as $alias){
                $this->alias($index, $alias);
            }
        }
    }
    
    /**
     * 
     * @param Closure $callback
     * @return \lib\inner\ApplicationGate
     */
    public function terminating(Closure $callback){
        $this->terminatingCallbacks->add($callback);
        
        return $this;
    }
    
    /**
     * 
     * invokes all registered terminating callback closures
     * 
     */
    public function terminate(){
        $this->terminatingCallbacks->each(function($callback, $index){
           $this->call($this->terminatingCallbacks->get($index)); 
        });
    }
    
    /**
     * determines if running unit tests
     * 
     * @return boolean
     */
    public function isRunningUnitTest(){
        return !is_null($this->context) && $this->context instanceof GateTestCore;
    } 
    
    /**
     * determine if the web app is running on live/production mode
     * 
     * @return mixed|boolean
     */
    public function isRunningOnLiveMode(){
        return config("live_mode");
    }
    
    /**
     * run the given array of bootstrap classes.
     * 
     * 
     * @param array $bootstrappers
     */
    public function bootstrapWith(array $bootstrappers){
        Collections::make($bootstrappers)->each(function($bootstrapper, $index){
            $this->make($bootstrapper)->bootstrap($this);
        });
        
        $this->hasBeenBootstrapped = true;
    }
    
    /**
     * Determine if the application has been bootstrapped before.
     * 
     * @return boolean
     */
    public function hasBeenBootstrapped(){
        return $this->hasBeenBootstrapped;
    }
    
    /**
     * 
     * @param Closure $warpAfterCoreInstantiationCallback
     */
    public function warpForCore(Closure $warpAfterCoreInstantiationCallback){
        $this->warpAfterCoreInstantiationCallback = $warpAfterCoreInstantiationCallback;
    }
    
    /**
     * 
     * Initializes app reader
     * 
     */
    public function initAppReader(){ 
        if(!is_null($this->warpAfterCoreInstantiationCallback)){
            Reader::setInitInstanceCallback($this->warpAfterCoreInstantiationCallback);
        }
    }
}