<?php
namespace lib\inner;


/**
 * 
 * @author antonio
 *
 * @method static mixed make($abstract, array $parameters = [])
 * @method static mixed call($callback, $parameters = [], $defaultMethod = null)
 * @method static mixed singleton($abstract, $concrete = null)
 * @method static mixed instance($abstract, $instance)
 * @method static boolean isRunningUnitTest()
 * @method static boolean checkInheritance(mixed $object, array|string $instanceof, string $bop = "or")
 * @method static boolean checkJSONString(string $string)
 * @method static boolean checkCLI()
 * @method static boolean checkAJAX()
 * @method static boolean checkSSLPresenceOnURL()
 * @method static void alias(string $class, string $alias)
 * @method static mixed evaluate(string $code)
 */
class App extends Accessor {
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\inner\Accessor::warpedInstance()
     */
    public function warpedInstance(){
       return ApplicationGate::class; 
    }
}