<?php
namespace lib\startup;

use lib\inner\ApplicationGate;
use lib\inner\arrangements\http\HTTPOut;
use lib\inner\arrangements\http\Request;
use lib\inner\arrangements\http\Response;
use lib\inner\arrangements\http\url\UrlHandler;
use lib\util\CookieDataWarper;
use lib\util\route\RouteArgs;
use lib\util\route\RouteContainer;
use lib\util\route\RouteGate;

class BootHttpApplicationUtil {
    
    use DefineCsrfToken;
    
    /**
     * 
     * @var ApplicationGate
     */
    protected $gate;
    
    /**
     * 
     * @param ApplicationGate $gate
     */
    public function bootstrap(ApplicationGate $gate){
        $this->gate = $gate;
        
        $gate->singleton(CookieDataWarper::class);
        $gate->singleton(RouteContainer::class);
        $gate->singleton(RouteGate::class);
        $gate->singleton(UrlHandler::class);
        
        /*
         * 
         * request handler
         * 
         */
        $this->bindUtil(Request::class, "request");
        
        /*
         * 
         * response handler
         * 
         */
        $this->bindUtil(Response::class, "response");
        
        /*
         *
         * HTTP's outputter object
         *
         */
        $this->bindUtil(HTTPOut::class, "out");
        
        /**
         * 
         * HTTP route parametric argument handler
         * 
         */
        $this->bindUtil(RouteArgs::class, "route_args");
        
        /*
         * 
         * preparing csrf-token string value 
         * 
         */
        $this->defineCsrfTokenString();
        
        /*
         *
         * This header tells the client (browser) and intermediaries (like proxy servers)
         * not to store a cached copy of the response. It also indicates that the response
         * must be revalidated with the server before use, ensuring that the client always
         * gets the latest version
         *
         */
        response()->setHeaders([
            "Cache-Control" => "no-store, no-cache, must-revalidate, max-age=0"
        ]);
    }
    
    /**
     *
     * @param string $classname
     * @param string $alias
     */
    protected function bindUtil($classname, $alias){
        $this->gate->singleton($classname);
        $this->gate->alias($classname, $alias);
    }
}