<?php
namespace lib\mail\warper;

use lib\util\cli\WarpArgv;
use lib\mail\Mail;
use lib\inner\App;
use lib\mail\Dispatchable;
use lib\util\cli\LongOption;

/**
 * 
 * @author Jiya Sama
 *
 * This can be utilized to send email using defined dispatchable classes.
 * but aimed to make email sending tests much easier.
 *
 */
class MailSender {
    
    /**
     * 
     * @var LongOption
     */
    protected $recipient;
    
    /**
     * 
     * Dispatchable class
     * 
     * @var string|LongOption
     */
    protected $dispatchable;
    
    /**
     * 
     * @var string
     */
    protected $mailers;
    
    /**
     * 
     * @param WarpArgv $argv
     */
    public function __construct(WarpArgv $argv){
        $this->recipient = $argv->findLongOption("--recipient");
        
        $this->dispatchable = $argv->findLongOption("--dispatchable");
        
        $this->mailers = preferences("structure")->get("mailers");
    }
    
    protected function validate(){
        if(is_blank($this->recipient)){
            out()->haltln("[ERROR] <error>Recipient must not be defined</error>");
        }
        
        if(is_blank($this->dispatchable)){
            out()->haltln("[ERROR]: <error>Dispatchable class must not be defined</error>");
        }
        
        $this->dispatchable = str_replace("/", "\\", relative_path($this->mailers))
            ."\\{$this->dispatchable->value()}";
        
        if(!class_exists($this->dispatchable)){
            out()->haltln("[ERROR]: <error>Class '{$this->dispatchable}' doesn't exist</error>");
        }
        
        if(!App::checkInheritance($this->dispatchable, Dispatchable::class)){
            out()->haltln("[ERROR]: <error>Class '{$this->dispatchable}' is not an instance of ".Dispatchable::class."</error>");
        }
    }
    
    public function dispatch(){
        $this->validate(); 
        
        out()->println("Sending email ... ");
        
        $email_sent = Mail::recipient($this->recipient->value())
        ->subject("OG Mail Sender") // subject is a fallback value
        ->dispatchable(App::make($this->dispatchable))
        ->send();
        
        if($email_sent){
            out()->println("[Dispatchable: {$this->dispatchable}] <primary>Email Sent Successfully!</primary>");
            
            return 0;
        }else{
            out()->println("[Dispatchable: {$this->dispatchable}] <error>Email wasn't sent. Email content saved to logs</error>");
        }
        
        return 1;
    }
}