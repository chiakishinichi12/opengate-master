<?php

namespace lib\util\exceptions;

use lib\exceptions\OpenGateException;

class ModelException extends OpenGateException {
    
    /**
     * 
     * @var string
     */
    protected $modelClass;
    
    /**
     * 
     * @var int
     */
    protected $exceptionFlag;
    
    /**
     * 
     * @var integer
     */
    public const COLUMN_CANNOT_BE_MODIFIED = 1;
    
    /**
     * 
     * @var integer
     */
    public const NONEXISTENT_RECORD = 2;
    
    /**
     *
     * @var integer
     */
    public const NONEXISTENT_COLUMN = 3;
    
    /**
     * 
     * @param string $message
     * @param string $modelClass
     * @param int $exceptionFlag
     */
    public function __construct(string $message, string $modelClass = null, int $exceptionFlag = -1){
        parent::__construct("[Model={$modelClass}] {$message}");
        
        $this->modelClass = $modelClass;
        $this->exceptionFlag = $exceptionFlag;
    }
    
    /**
     * 
     * @return number
     */
    public function getExceptionFlag(){
        return $this->exceptionFlag;
    }
    
    /**
     * 
     * @return string
     */
    public function getModelClass(){
        return $this->modelClass;
    }
}