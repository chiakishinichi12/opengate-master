<?php
namespace lib\mocks;

use lib\util\Collections;
use lib\util\model\Model;

/**
 * 
 * @author Antonius Aurelius
 *
 */
class Mockery {

    /**
     * 
     * @var string
     */
    protected $model;
    
    /**
     * 
     * @var integer
     */
    protected $length = 10;
    
    /**
     * 
     * @var callable
     */
    protected $sequence;
    
    /**
     * 
     * @var Collections
     */
    protected $made;
    
    /**
     *
     * @var callable
     */
    protected $postmake;
    
    /**
     * 
     * @var callable
     */
    protected $postcreate;
    
    /**
     * Mockery constructor.
     * 
     * Initializes the mockery class and calls the configure method, which can be overridden in subclasses
     * to set up specific configurations.
     * 
     */
    public function __construct(){
        $this->configure();
    }
    
    /**
     * 
     * Set the number of instances to create.
     * 
     * @param int $length
     * @return \lib\mocks\Mockery
     */
    public function length(int $length){
        $this->length = $length;
        $this->made = collect();
        
        return $this;
    }
    
    /**
     * 
     * Create and persist mock instances based on the defined attributes and modifications.
     * 
     * @param array $mod
     */
    public function create(array $mod = []){
        if($this->made->isLoopable()){
            $this->made->each(function($model){
                if($model->save() && is_callable($this->postcreate)){
                    ($this->postcreate)($model);
                }
            });
        }else{
            $filling = collect($this->fills($mod));
            
            $filling->each(function($definition){
                $model = $this->newInstance($definition);
                
                if($model->save() && is_callable($this->postcreate)){
                    ($this->postcreate)($model);
                }
            });
        }
    }
    
    /**
     * 
     * Generate an array of attribute sets for the mock instances.
     * 
     * @param array $mod
     * @return array[]
     */
    protected function fills(array $mod = []){
        $sequence = new Sequence();
        $sequence->length = $this->length;
        
        $makes = [];
        
        for($sequence->index = 0; $sequence->index < $sequence->length; $sequence->index++){
            $mockdef = $this->definition();
            
            if(!is_array($mockdef)){
                $type = class_name($mockdef);
                
                throw new MockeryException("overriden 'definition()' method must return an array. {$type} found");
            }
            
            if(!is_blank($mod)){
                $mockdef = array_merge($mockdef, $mod);
            }
            
            if(is_blank($this->sequence)){
                goto making;
            }
            
            $seqclosinv = ($this->sequence)($sequence);
            
            if(is_array($seqclosinv) && !is_blank($seqclosinv)){
                $mockdef = array_merge($mockdef, $seqclosinv);
            }
            
            making:
            $makes[] = $mockdef;
        }
        
        return $makes;
    }
    
    /**
     * Configure the mock settings.
     *
     * This method is meant to be overridden in subclasses to set up
     * specific configuration for the mock instance.
     */
    
    protected function configure(){
        // Subclasses should override this method to provide specific configurations.
    }
    
    /**
     * 
     * Set a callback to be executed after making each mock instance.
     * 
     * @param callable $postmake
     * 
     * @return \lib\mocks\Mockery
     */
    public function afterMaking($postmake){
        $this->postmake = $postmake;
        
        return $this;
    }
    
    /**
     * 
     * Set a callback to be executed after creating each mock instance.
     * 
     * @param callable $postcreate
     * 
     * @return \lib\mocks\Mockery
     */
    public function afterCreating($postcreate){
        $this->postcreate = $postcreate;
        
        return $this;
    }
    
    /**
     * 
     * @param array $mod
     * 
     * @return \lib\mocks\Mockery
     */
    public function make(array $mod = []){
        foreach($this->fills($mod) as $fill){
            $model = $this->newInstance($fill);
            
            $this->made->add($model);
            
            if(is_callable($this->postmake)){
                ($this->postmake)($model);
            }
        }
        
        return $this;
    }
    
    public function list(){
        return $this->made;
    }
    
    /**
     * 
     * @param array $values
     * 
     * @return Model
     */
    protected function newInstance(array $values){
        $model = $this->model;
        
        return new $model($values);
    }
    
    /**
     * 
     * @param mixed $sequence
     */
    public function sequence($sequence){
        $this->sequence = $sequence;
        
        return $this;
    }
    
    /**
     * 
     * this is where all the fake data will be defined.
     * and method must return an array
     * 
     */
    public function definition(){
        throw new MockeryException("Nothing was inherited by Mockery");
    }
    
    /**
     * 
     * @return string
     */
    public function model(){
        return $this->model;
    }
}