<?php
namespace lib\mail\warper;

use lib\inner\ApplicationGate;
use lib\mail\Dispatchable;
use lib\traits\WarperCommons;

class DispatchableList{
    
    use WarperCommons;
    
    /**
     *
     * @var ApplicationGate
     */
    protected $gate;
    
    public function __construct(ApplicationGate $gate){
        $this->gate = $gate;
    }
    
    public function showList(){
        echo "Established Mailer Classes:\n\n";
        
        $failedCallback = function(){
            out()->println("<error>No mailer class found.</error>");
            die();
        };
        
        $this->classList(preferences("structure")->get("mailers"), 
            Dispatchable::class,
            $failedCallback);
    }
}

