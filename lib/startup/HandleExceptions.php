<?php
namespace lib\startup;

use ErrorException;
use Throwable;
use lib\handlers\ExceptionHandler;
use lib\inner\ApplicationGate;
use lib\util\accords\Logger;
use lib\util\exceptions\OutOfMemoryError;

class HandleExceptions {
    
    /**
     * 
     * @var ApplicationGate
     */
    protected $gate;
    
    /**
     * Reserved memory so that errors can be displayed properly on memory exhaustion.
     *
     * @var string
     */
    public static $reservedMemory;
    
    public function bootstrap(ApplicationGate $gate){
        self::$reservedMemory = str_repeat('x', 32768);
        
        $this->gate = $gate;
        
        error_reporting(-1);
        
        set_error_handler([$this, "handleError"]);
        
        set_exception_handler([$this, "handleException"]);
        
        register_shutdown_function([$this, "handleShutdown"]);
        
        ini_set("display_errors", !$gate->isRunningOnLiveMode());
    }
    
    /**
     * 
     * @param int $level
     * @param string $message
     * @param string $file
     * @param number $line
     * @param array $context
     */
    public function handleError($level, $message, $file = "", $line = 0, $context = []){
        if($this->isDeprecation($level)){
            $logger = $this->gate->make(Logger::class);
            $logger->log($message, LOG_WARNING);
        }
        
        if(error_reporting() & $level){
            throw new ErrorException($message, 0, $level, $file, $line);
        }
    }
    
    /**
     * Handle an uncaught exception from the application.
     *
     * Note: Most exceptions can be handled via the try / catch block in
     * the HTTP and Console kernels. But, fatal error exceptions must
     * be handled differently since they are not normal exceptions.
     * 
     * @param Throwable $e
     */
    public function handleException(Throwable $e){
        self::$reservedMemory = null;
        
        $handler = $this->getDefaultExceptionHandler();
        
        $handler->report($e);
        
        $handler->render($e);
    }
    
    /**
     * Handle the PHP shutdown event.
     * 
     */
    public function handleShutdown(){
        self::$reservedMemory = null;
        
        if (!is_null($error = error_get_last()) && $this->isFatal($error['type'])) {
            $this->handleException($this->warpFatalError($error));
        }
    }
    
    protected function warpFatalError($error){
        if(strpos(strtolower($error["message"]), "allowed memory size of") !== false){
            return new OutOfMemoryError($error);
        }else{
            return new ErrorException($error["message"], 1, $error["type"], $error["file"], $error["line"]);
        }
    }
    
    /**
     * Determine if the error level is deprecation
     * 
     * @param number $level
     * @return boolean
     */
    protected function isDeprecation($level){
        return in_array($level, [E_DEPRECATED, E_USER_DEPRECATED]);
    }
    
    /**
     * Determine if the error type is fatal.
     *
     * @param  int  $type
     * @return boolean
     */
    protected function isFatal($type){
        return in_array($type, [E_COMPILE_ERROR, E_CORE_ERROR, E_ERROR, E_PARSE]);
    }
    
    /**
     * 
     * @return \lib\handlers\ExceptionHandler
     */
    private function getDefaultExceptionHandler(){
        return $this->gate->make(ExceptionHandler::class);
    }
}