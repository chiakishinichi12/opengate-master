<?php

namespace lib\traits;

trait Checker {
    /**
     *
     * checks if a string is JSON-formatted one
     * @param unknown $string
     * @return boolean
     */
    public function checkJSONString($string){
        return is_string($string)
            && is_array(json_decode($string, true))
            && (json_last_error() == JSON_ERROR_NONE) ? true : false;
    }
    
    /**
     *
     *
     * checks if SSL is being used on the URL
     * @return boolean
     */
    public function checkSSLPresenceOnURL(){
        return (
            request()->server("HTTPS") ||
            request()->server("HTTP_X_FORWARDED_PROTO") == "https"
        );
    }
    
    /**
     *
     * checks if a request is an AJAX request
     * @return boolean
     */
    public function checkAJAX(){
        if(request()->server("HTTP_X_REQUESTED_WITH")){
            $ajaxIndication = strtolower(request()->server("HTTP_X_REQUESTED_WITH"));
            
            if($ajaxIndication == "xmlhttprequest"){
                return true;
            }
        }
        
        return false;
    }
    
    /**
     *
     * checks if a script was called via CLI
     * @return boolean
     */
    public function checkCLI(){
        return php_sapi_name() === "cli";
    }
    
    /**
     * 
     * @param mixed $class
     * @param array|string $parentClasses
     * @param string $bop
     * @return boolean
     */
    public function checkInheritance($class, array|string $parentClasses, string $bop = "or"){
        $instc = 0;
        
        if(is_string($parentClasses)){
            $parentClasses = [$parentClasses];
        }
        
        foreach($parentClasses as $parentClass){
            if(!is_string($class)){
                $class = class_name($class);
            }
            
            if($class === $parentClass || is_subclass_of($class, $parentClass)){
                $instc++;
            }
        }
        
        switch($bop){
            case "or":
            default:
                $bexp = $instc > 0;
                break;
            case "and":
                $bexp = $instc === count($parentClasses);
                break;
        }
        
        return $bexp;
    }
}
