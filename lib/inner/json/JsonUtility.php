<?php
namespace lib\inner\json;

use lib\inner\App;
use lib\inner\OpenObject;

/**
 * Utility class for JSON encoding and decoding operations.  
 * 
 * This class provides alternative methods to the native `json_encode()` and `json_decode()` functions,
 * offering additional features or integration with custom classes like `JsonEncoder` and `OpenObject`.  
 * 
 * Its primary goal is to standardize JSON handling across the application and add extensibility where needed.  
 * 
 * @author Antonius Aurelius
 */
class JsonUtility{
    
    /**
     * 
     * Converts an array or object into a JSON-encoded string.
     * 
     * This method serves as an alternative to PHP's `json_encode()` function by using a custom `JsonEncoder` class.
     * It allows for additional customization or pre-processing of the data before encoding.
     * 
     * @param mixed $object
     * @param int $depth
     * 
     * @return JsonEncoder
     */
    public static function toEncode(array|object $object, int $depth = 512){
        return App::make(JsonEncoder::class)->material($object)->encode();
    }
    
    /**
     * 
     * Decodes a JSON string into an `OpenObject` instance.
     * 
     * This method acts as an alternative to PHP's `json_decode()` function, returning an `OpenObject` instead of 
     * a standard object or array. This provides more control and flexibility in handling decoded JSON data.
     * 
     * @param string $json
     * @param int $depth
     * 
     * @return OpenObject
     */
    public static function parse(string $json, int $depth = 512){
       return open_object($json, $depth);
    }
}

