<?php
namespace lib\commands\classes;

use lib\instances\CommandInstance;
use lib\commands\CommonMethods;
use lib\util\cli\WarpArgv;
use lib\inner\App;
use lib\util\controller\warper\ControllerClassCreation;
use lib\util\controller\warper\ControllerList;
use lib\util\controller\warper\ControllerLookup;

/**
 *
 * @Commands(["ctr", "controller", "control"])
 *
 */
class WarpController extends CommandInstance{
    
    use CommonMethods;
    
    protected $name = "controller";
    
    protected $desc = "controller management feature";
    
    /**
     *
     * @Argument("creates controller class. [option(s): { --primary=route(O) }]")
     *
     */
    public function create(){
        App::make(ControllerClassCreation::class)->write();
    }
    
    /**
     *
     * @Argument("show controller class list.")
     *
     */
    public function list(){
        App::make(ControllerList::class)->showList();
    }
    
    /**
     *
     * @Argument("lookup controller class.")
     *
     */
    public function lookup(){
        App::make(ControllerLookup::class)->lookupController();
    }
    
    /**
     * 
     * @param WarpArgv $argv
     */
    public function listen(WarpArgv $argv){
        $this->execution($argv);
    }
}

