<?php

namespace app\controller;

use lib\inner\Locale;
use lib\inner\WebAppCore;
use lib\inner\arrangements\http\Request;
use lib\inner\arrangements\http\Response;
use lib\traits\GateKeeperValue;
use lib\util\cli\IO;
use lib\util\controller\Controller;
use lib\util\route\RouteArgs;

class IndexController extends Controller {
    
    use GateKeeperValue;
    
    public function index(RouteArgs $routeArgs){
        Locale::set("ja");
        
        /**
         * 
         * view name
         * 
         */
        return "startpage";    
    }
}