<?php
namespace app\instances\page;

use app\middlewares\IndexMiddleware;
use lib\inner\Locale;
use lib\instances\PageInstance;

/**
 * 
 * @Route("/")
 *
 */
class IndexPage extends PageInstance{
    
    /**
     * 
     * @var array
     */
    protected $middlewares = [
    ];
    
    public function configure(){
        $this->responseConfig([
            "method" => "any",
            "readonly" => true
        ]);
    }
    
    public function listen(){        
        return morph("startpage");
    }
}