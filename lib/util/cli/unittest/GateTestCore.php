<?php
namespace lib\util\cli\unittest;

use PHPUnit\Framework\TestCase;
use app\providers\ControllerAnnotationServiceProvider;
use app\providers\HttpInstanceAnnotationServiceProvider;
use app\providers\RouteServiceProvider;
use lib\inner\App;
use lib\inner\Locale;
use lib\inner\arrangements\ServiceProvider;
use lib\startup\DefineCsrfToken;
use lib\util\accords\SessionUtil;
use lib\util\cli\unittest\tool\InternalHTTPRequestMockery;
use lib\util\cli\unittest\tool\UnitSessionUtil;
use lib\util\resolver\MethodInvocationResolver;
use lib\util\route\RouteArgs;
use lib\util\route\RouteContainer;
use lib\util\route\RouteGate;

/**
 * 
 * @author Jiya Sama
 *
 * @method InternalHTTPRequestMockery get(string $uri, array $temporary = null)
 * @method InternalHTTPRequestMockery post(string $uri, array $temporary = null)
 * @method InternalHTTPRequestMockery put(string $uri, array $temporary = null)
 * @method InternalHTTPRequestMockery patch(string $uri, array $temporary = null)
 * @method InternalHTTPRequestMockery delete(string $uri, array $temporary = null)
 * @method InternalHTTPRequestMockery acceptAny(string $uri, array $temporary = null)
 * @method InternalHTTPRequestMockery withSession(array $session)
 * @method InternalHTTPRequestMockery params(array $data)
 * @method InternalHTTPRequestMockery input(string $data)
 * @method InternalHTTPRequestMockery headers(array $headers)
 * @method InternalHTTPRequestMockery provideCsrfToken()
 */
class GateTestCore extends TestCase {
    
    use DefineCsrfToken;
    
    /**
     * 
     * bootstrapping objects needed for HTTP request simulation
     * 
     */
    protected function bootstrap(){
        App::singleton(RouteGate::class);
        
        App::singleton(RouteContainer::class);
        
        App::singleton(SessionUtil::class, UnitSessionUtil::class);
        
        App::make(SessionUtil::class)->initSessionLibrary();
        
        $this->obtainRoutes();
        
        $this->bindUtil(RouteArgs::class, "route_args");
        
        $this->defineCsrfTokenString();
        
        // sets (or resets) currently referenced locale
        Locale::set(preferences("setup")->get("locale"));
    }
    
    /**
     *
     * @param string $classname
     * @param string $alias
     */
    protected function bindUtil($classname, $alias){
        App::singleton($classname);
        App::alias($classname, $alias);
    }
    
    /**
     *
     * booting registered http routes
     *
     */
    protected function obtainRoutes(){
        $providers = [
            RouteServiceProvider::class,
            HttpInstanceAnnotationServiceProvider::class,
            ControllerAnnotationServiceProvider::class
        ];
        
        foreach($providers as $provider){
            if(App::checkInheritance($provider, ServiceProvider::class)){
                App::make($provider)->boot(gate());
            }
        }
    }
    
    /**
     * 
     * @param string $method
     * @param array $params
     */
    public function __call(string $method, array $params){        
        return App::make(MethodInvocationResolver::class)
        ->parent($this)
        ->instances([App::make(InternalHTTPRequestMockery::class)])
        ->aliases(["sessions" => "withSession"])
        ->toCall($method)
        ->params($params)
        ->allowMagicCall()
        ->resolve()
        ->invoke();
    }
}

