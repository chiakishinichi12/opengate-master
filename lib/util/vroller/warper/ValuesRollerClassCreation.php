<?php
namespace lib\util\vroller\warper;

use lib\traits\WarperCommons;
use lib\util\cli\WarpArgv;
use lib\util\file\FileUtil;

class ValuesRollerClassCreation {
    
    use WarperCommons;
    
    /**
     *
     * @var string
     */
    protected $warping = "ValuesRoller";
    
    /**
     *
     * @var Collections
     */
    protected $args;
    
    /**
     * 
     * @var string
     */
    protected $rollers;
    
    /**
     * 
     * @var string
     */
    protected $prototypes;
    
    /**
     * 
     * @param WarpArgv $argv
     */
    public function __construct(WarpArgv $argv){
        $this->args = $argv->obtainArguments();
        
        $this->rollers = preferences("structure")->get("rollers");
        
        $this->prototypes = base_path("lib/util/vroller/prototype");
    }
    
    public function write(){
        if($this->args->count() < 2){
            return;
        }
        
        $template = FileUtil::getContents("{$this->prototypes}/ValuesRoller.tmp", true);
        
        $roller_name = $this->args->get(1);
        
        $this->alterAndCreate($roller_name, $this->rollers);
        
        $toFind = collect([
            [
                "find" => "[VRoller_Class]",
                "replace" => $roller_name
            ],
            [
                "find" => "[VRoller_Namespace]",
                "replace" => str_replace("/", "\\", relative_path($this->rollers))
            ]
        ]);
        
        $this->writeWarpedScript($template, 
            $toFind, 
            $this->rollers, 
            $roller_name);
    }
}

