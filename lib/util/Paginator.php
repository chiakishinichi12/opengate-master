<?php

namespace lib\util;

use ArrayIterator;
use Countable;
use IteratorAggregate;
use lib\inner\App;
use lib\util\cli\WarpArgv;
use lib\util\html\HtmlExpression;
use lib\util\model\Model;
use lib\util\nebula\View;
use lib\util\resolver\MethodInvocationResolver;
use lib\util\route\Route;
use lib\util\route\RouteArgs;
use lib\inner\json\JsonUtility;
use lib\inner\json\JsonEncoder;

abstract class Paginator implements IteratorAggregate, Countable {
    
    /**
     * 
     * @param int $page
     */
    abstract function fetchPageItems(int $page);
    
    /**
     * 
     * @var Collections
     */
    protected $items;
    
    /**
     * 
     * @var int
     */
    protected $itemCount;
    
    /**
     * 
     * @var int
     */
    protected $rowsPerPage;
    
    /**
     * 
     * @var int
     */
    protected $pageCount;
    
    /**
     * 
     * @var int
     */
    protected $currentPage = 0;
    
    /**
     * 
     * @var EncapsulationHelper
     */
    protected $encapsulation;
    
    /**
     * 
     * @param int $itemCount
     * @param int $rowsPerPage
     */
    public function __construct(){
        $this->encapsulation = App::make(EncapsulationHelper::class)->instance($this);
    }
    
    /**
     * 
     * calculates total page count for the entire stack pagination
     * 
     */
    public function calculatePageCount(){
        $this->pageCount = $this->itemCount / $this->rowsPerPage;
        
        if(floor($this->pageCount) < $this->pageCount){
            $this->pageCount = intval($this->pageCount) + 1;
        }
        
        return $this;
    }
    
    /**
     * 
     * @param int $currentPage
     * @return mixed|object
     */
    public function currentPage(int $currentPage = null){
        return $this->encapsulation->propertyDefinition($this->{__FUNCTION__}, $currentPage);
    }
    
    /**
     * 
     * @param int $itemCount
     * @return mixed|object
     */
    public function itemCount(int $itemCount = null){
        return $this->encapsulation->propertyDefinition($this->{__FUNCTION__}, $itemCount);
    }
    
    /**
     * 
     * @param int $rowsPerPage
     * @return mixed|object
     */
    public function rowsPerPage(int $rowsPerPage = null){
        return $this->encapsulation->propertyDefinition($this->{__FUNCTION__}, $rowsPerPage);
    }
    
    /**
     * 
     * @param string $uri
     * @param string $method
     * 
     * @return \lib\util\Paginator
     */
    public function withRoute(string $uri, string $method = "any"){
        return Route::$method($uri, $this)->responseConfig([
            "readonly" => true
        ]);
    }
    
    /**
     * 
     * @return string
     */
    protected function httpQueryString(){
        $httpQueries = request()->get();
        
        if($httpQueries->hasKey("page")){
            $httpQueries->remove("page");
        }
        
        $buildquerystr = collect();
        
        foreach($httpQueries as $key => $value){
            $value = str_replace(" ", "+", $value);
            
            $buildquerystr->add("{$key}={$value}");
        }
        
        return $buildquerystr->implode("&");
    }
    
    /**
     * 
     * Generates pagination links based on the current state and view.
     * 
     * This function creates an array of pagination variables and renders them using
     * a specified view template. It supports a flexible range for the number of pages
     * displayed around the current page and defaults to a pre-defined view if none is provided.
     * 
     * @param string $view
     * @param number $range
     * 
     * @return string
     */
    public function links($view = "", $range = 5){
        // Default view template if none is specified
        $defaultView = preferences("predefined")->get("paginator-link");
        
        // If the view parameter is an integer, use it as the range and set the view to default
        if(is_int($view)){
            $range = $view;
            $view = $defaultView;
        }
        
        // If the view is blank, use the default view template
        if(is_blank($view)){
            $view = $defaultView;
        }
        
        // Create an instance of RouteArgs to handle route parameters
        $routeArgs = App::make(RouteArgs::class);
        
        $httpQueries = $this->httpQueryString();
        
        $baseUrl = base_url().$routeArgs->getCurrentURIString();
        
        $pagePhrase = "?page";
        
        if(!is_blank($httpQueries)){
            $baseUrl .= "?{$httpQueries}";
            $pagePhrase = "&page";
        }
        
        // Prepare pagination variables for the view template
        $linkVars = [
            "baseUrl" => $baseUrl,
            "pagePhrase" => $pagePhrase,
            "currentPage" => $this->currentPage(),
            "pageCount" => $this->getPageCount(),
            "start" => max(1, $this->currentPage() - $range),
            "end" => min($this->getPageCount(), $this->currentPage() + $range)
        ];
        
        if(!View::exists($view)){
            return "";
        }
        
        // Render the pagination links using the specified view and variables
        return new HtmlExpression(morph($view, $linkVars)->buffered());
    }
    
    /**
     * 
     * @return number
     */
    public function getPageCount(){
        return $this->pageCount;
    }
    
    /**
     *
     * {@inheritDoc}
     * @see IteratorAggregate::getIterator()
     */
    #[\ReturnTypeWillChange]
    public function getIterator(){
        return new ArrayIterator($this->items->toArray());
    }
    
    /**
     *
     * @return number
     */
    #[\ReturnTypeWillChange]
    public function count(){
        return $this->items->length();
    }
    
    /**
     *
     * @param string $method
     * @param mixed ...$params
     *
     * @return mixed
     */
    public function __call(string $method, array $params){
        return App::make(MethodInvocationResolver::class)
            ->parent($this)
            ->instances([
                $this->items
            ])
            ->toCall($method)
            ->params($params)
            ->resolve()
            ->invoke();
    }
    
    /**
     * Retrieves the current page number either from CLI arguments or HTTP request.
     *
     * This function checks if the application is running in a CLI environment.
     * If it is, it fetches the page number from the command line arguments using
     * the `WarpArgv` class. If not running in CLI, it retrieves the page number
     * from the HTTP request parameters.
     *
     * @return mixed
     */
    public function scannedPageNumber(){
        if(App::checkCLI()){            
            if(!is_blank($longOption = App::make(WarpArgv::class)->findLongOption("--page"))){
                return $longOption->value();
            }
        }
        
        return request()->get("page");
    }
    
    /**
     * Converts the current object data to a JSON string with pagination details.
     *
     * This function prepares an associative array with pagination information and
     * data items, and then encodes it to a JSON string. It includes details such as
     * total page count, rows per page, current page, and total items. If a base URL
     * is available and the total page count is not zero, it also generates pagination URLs
     * for the last, next, and previous pages.
     *
     * @return JsonEncoder
     */
    public function toJSONString(){
        $routeArgs = App::make(RouteArgs::class);
        
        $toEncode = [
            "totalPageCount" => $this->getPageCount(),
            "rowsPerPage" => $this->rowsPerPage(),
            "currentPage" => $this->currentPage(),
            "total" => $this->itemCount(),
            "data" => $this->items->map(function($item){
                if($item instanceof Model){
                    return $item->getDefinedColumnVars();    
                }
            
                return $item;
            })
        ];
        
        if(!is_blank($baseUrl = base_url()) && $this->getPageCount() !== 0){
            $httpQueries = $this->httpQueryString();
                        
            $pagePhrase = "?page";
            
            $paginationBaseUrl = "{$baseUrl}{$routeArgs->getCurrentURIString()}";
            
            if(!is_blank($httpQueries)){
                $paginationBaseUrl .= "?{$httpQueries}";
                $pagePhrase = "&page";
            }
            
            $paginationUrls = [
                "last_page_url" => "{$paginationBaseUrl}{$pagePhrase}={$this->getPageCount()}",
            ];
            
            $nextPage = $this->currentPage() + 1;
            
            if($nextPage <= $this->getPageCount()){
                $paginationUrls["next_page_url"] = "{$paginationBaseUrl}{$pagePhrase}={$nextPage}";
            }
            
            $prevPage = $this->currentPage() - 1;
            
            if($prevPage >= 1){
                $paginationUrls["prev_page_url"] = "{$paginationBaseUrl}{$pagePhrase}={$prevPage}";
            }
            
            $toEncode = array_merge($toEncode, $paginationUrls);
        }
        
        return JsonUtility::toEncode($toEncode);
    }
    
    /**
     * 
     * @return string
     */
    public function __toString(){
        return $this->toJSONString();
    }
}