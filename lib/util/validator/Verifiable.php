<?php
namespace lib\util\validator;


/**
 * Interface Verifiable
 *
 * This interface defines a contract for classes that can be verified.
 * Any class implementing this interface should provide an implementation
 * for the verify() method, which will handle the logic for verification.
 */
interface Verifiable {
    
    /**
     * Method to verify the object implementing this interface.
     *
     * @return mixed The return type can vary based on the implementation needs.
     */
    function verify(mixed $arg);
}

