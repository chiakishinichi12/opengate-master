<?php

return [
    /**
     * 
     * application timezone setting
     * 
     */
    "system_date_timezone" => config("system_date_timezone", date_default_timezone_get()),
    
    /**
     * 
     * this is where the views are located
     * 
     */
    "view_base_path" => base_path("nebula/views"),
    
    /**
     * 
     * Defines the path to the cache directory for storing morphed views.
     * 
     */
    "view_morph_cache" => base_path("stockade/views"),
    
    /**
     * 
     * Specifies the marker file used for tracking morphed views.
     * 
     */
    "view_morph_marker" => base_path("stockade/marker"),
    
    /**
     * 
     * sets to strictly evaluating view morph syntax
     * 
     */
    "view_morph_strict" => false,
    
    /**
     * 
     * sets to strictly evaluating cli output syntax
     * 
     */
    "cli_outputter_strict" => false,
    
    /**
     * 
     * enforced strict api response (no sessions nor redirection)
     * 
     */
    "api_response_strict" => true,
    
    /**
     * 
     * here's where the localization base path is defined.
     * 
     */
    "locale_base_path" => base_path("nebula/lang"),
    
    /**
     * 
     * sets the default locale for the application.
     * 
     */
    "locale" => "en",
    
    /**
     * 
     * when the translation of the specified element is missing, 
     * the fallback_locale setting will take its place to resolve
     * the missing translation link. 
     * 
     */
    "fallback_locale" => "en",
    
    /**
     *
     * enables/disables the specification of country codes after language code
     *
     */
    "include_country_code_in_dynamic_locale" => false,
    
    /**
     * 
     * used as a reference to evaluate and set the default locale dynamically
     * 
     * browser|ipLoc only
     * 
     */
    "dynamic_locale" => "browser",
    
    /**
     *
     * Specifies the directory path where various values are stored,
     * such as strings, colors, and dimensions
     *
     */
    "values" => base_path("nebula/values"),
    
    /**
     * 
     * mailer engine setter for the application. 
     * 
     */
    "mailer_engine" => config("mailer_engine", "phpmailer")
];