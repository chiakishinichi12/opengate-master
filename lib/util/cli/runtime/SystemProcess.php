<?php
namespace lib\util\cli\runtime;

use lib\inner\Accessor;

/**
 * 
 * @author Antonius Aurelius
 *
 * @method static ReactiveProcess command(string $command)
 * @method static ReactiveProcess mode(string $mode)
 * @method static ReactiveProcess during(Closure $callback)
 * @method static ReactiveProcess complete(Closure $callback)
 * @method static ReactiveProcess exec()
 *
 */
class SystemProcess extends Accessor{
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\inner\Accessor::warpedInstance()
     */
    public function warpedInstance(){
        return ReactiveProcess::class;
    }
}

