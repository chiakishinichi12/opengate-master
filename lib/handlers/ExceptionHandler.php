<?php
namespace lib\handlers;

use Throwable;
use lib\inner\App;
use lib\inner\arrangements\http\Request;
use lib\inner\arrangements\http\Response;
use lib\util\accords\Logger;

class ExceptionHandler {
    
    use ExceptionHandlerCallbacks;
    
    /**
     * container for Exceptions that don't have to be reported
     * 
     * @var array
     */
    protected $dontReport = [];
    
    /**
     * 
     * @param Request $request
     * @param Response $response
     */
    public function __construct(protected Request $request, protected Response $response){
        $this->setDefaults();
    }
    
    /**
     * 
     * default settings
     * 
     */
    protected function setDefaults(){        
        if(!App::checkCLI()){
            $this->response->statusCode(500);
        }
    }
    
    /**
     *
     *  writes the error report to a log file
     *  
     *  @param Throwable $e
     *
     */
    public function report(Throwable $e){
        if($this->shouldNotReport($e)){
            return;
        }
                
        $logger = App::make(Logger::class);
        
        $logger->log($this->reportingContent($e), LOG_ERR);
    }
    
    /**
     *
     * renders the error message into HTTP response
     * 
     * @param Throwable $e
     *
     */
    public function render(Throwable $e){
        if(App::checkCLI()){
            $this->cliExceptionCallback($e);   
        }else{
            $this->webExceptionCallback($e);
        }
    }
    
    /**
     * 
     * checks if the exception class is exempted from report logging
     * 
     * @param Throwable $e
     * 
     * @return boolean
     */
    public function shouldNotReport(Throwable $e){
        $class = class_name($e);
                
        return collect($this->dontReport)->has($class);
    }
}