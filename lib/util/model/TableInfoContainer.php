<?php

namespace lib\util\model;

use lib\util\Collections;


/**
 * 
 * for web performance
 *
 */
class TableInfoContainer {
    
    /**
     *
     * @var TableInfoContainer
     */
    protected static $instance = null;
    
    /**
     *
     * @var Collections
     */
    protected $tableInfos;
    
    public function __construct(){
        $this->tableInfos = Collections::make([], TableInfo::class);
    }
    
    /**
     * 
     * @param string $hostModelClass
     * @param TableInfo $tableInfo
     */
    public function appendTableInfo(string $hostModelClass, TableInfo $tableInfo){
        $this->tableInfos->put($hostModelClass, $tableInfo);
    }
    
    /**
     * 
     * @return \lib\util\Collections
     */
    public function getTableInfos(){
        return $this->tableInfos;
    }
}