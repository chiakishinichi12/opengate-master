<?php
namespace lib\util\array;

use Exception;
use lib\traits\ValueLocatorHelper;
use lib\util\Collections;

/**
 * 
 * The ArrayValueLocator class provides a way to retrieve and modify values in an 
 * array or Collections object using a dot-notated path syntax.
 *
 * This class is designed for ease of use, allowing you to access and update nested 
 * values without the need for verbose code. It leverages the ValueLocatorHelper trait 
 * for recursive operations.
 *
 */
class ArrayValueLocator {
    
    use ValueLocatorHelper;
    
    /**
     * 
     * @var array|Collections
     */
    protected $data;
    
    /**
     * 
     * @var boolean
     */
    protected $isCollection = false;
    
    /**
     * 
     * @param array|Collections $array
     */
    public function __construct(&$data){
        $this->validate($data);
        
        if($data instanceof Collections){
            $this->isCollection = true;
            $data = $data->toArray();
        }
        
        $this->data = &$data;
    }
    
    /**
     * 
     * @param array|Collections $array
     */
    protected function validate($data){
        if(! $data instanceof Collections && !is_array($data)){
            throw new Exception("''data' must be an array or ".Collections::class." instance");
        }
    }
    
    /**
     * 
     * @param string $path
     * @return mixed|NULL
     */
    public function retrieve(string $path){
        return $this->recursiveLocate($this->data, explode(".", $path), 0);
    }
    
    /**
     * 
     * @param string $path
     * @param mixed $newValue
     */
    public function fill(string $path, mixed $newValue){
        $this->recursiveUpdate($this->data, explode(".", $path), $newValue, 0);
    }
    
    /**
     * 
     * @return \lib\util\array\ArrayValueLocator
     */
    public function dump(){
        var_dump($this->data);
        
        return $this;
    }
    
    /**
     * 
     * destructing
     * 
     */
    public function __destruct(){
        if($this->isCollection){
            $this->data = collect($this->data);
        }
    }
}