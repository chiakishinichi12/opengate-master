<?php
namespace lib\util\migration;

use lib\util\exceptions\MigrationException;

class ColumnIndexer {
    
    /**
     * 
     * @var array
     */
    protected $indexingAloorithms = [
        "BTREE", 
        "HASH", 
        "FULLTEXT", 
        "SPATIAL",
        "RTREE"
    ];
    
    /**
     * 
     * @var string
     */
    protected $name;
    
    /**
     * 
     * @var array
     */
    protected $columns;
    
    /**
     * 
     * expects BTREE
     * 
     * @var string 
     */
    protected $type;
    
    /**
     * 
     * @param string $name
     * @param array $columns
     * @param string $type
     */
    public function __construct($name, $columns, $type = null){
        $this->name = $name;
        $this->columns = $columns;
        $this->type = strtoupper($type);
        
        $this->validate();
    }
    
    private function validate(){        
        if(is_array($this->columns)){
            if(!count($this->columns)){
                throw new MigrationException("There are no columns to be indexed");
            }
        }else{
            throw new MigrationException("Argument#2: 'columns' is not an array");
        }
        
        if(!is_null($this->type) && !in_array($this->type, $this->indexingAloorithms)){
            throw new MigrationException("Invalid indexing algoritm: '{$this->type}'");
        }
    }
    
    public function __toString(){
        $idx_name = !is_null($this->name) && !is_array($this->name) ? " {$this->name}" : "";
        $columns = implode(", ", $this->columns);
        $pattern = "INDEX{$idx_name} ({$columns})";
        
        switch($this->type){
            case "BTREE":
                $pattern .= " USING BTREE";
                break;
            case "HASH":
                $pattern .= " USING HASH";
                break;
            case "FULLTEXT":
                $pattern = "FULLTEXT {$this->name} ({$columns})";
                break;
            case "RTREE":
            case "SPATIAL":
                $pattern = "SPATIAL INDEX {$this->name} ({$columns})";
                break;
        }
        
        return $pattern;
    }
}