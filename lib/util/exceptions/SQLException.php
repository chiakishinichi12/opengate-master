<?php

namespace lib\util\exceptions;

use lib\exceptions\OpenGateException;

class SQLException extends OpenGateException {
    
    const CONN_ERROR = 1;
    const SQL_ERROR = 2;
    
    protected $errorCode;
    
    public function __construct(String $message, int $errorCode = 0){
        parent::__construct($message);
        $this->errorCode = $errorCode;
    }
    
    public function getErrorCode(){
        return $this->errorCode;
    }
}