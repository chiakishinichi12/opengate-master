<?php
namespace lib\util;

class StringUtil {
    
    /**
     * 
     * Custom explode method that splits a string by a given separator and returns an array of substrings.
     * 
     * This method extends the functionality of PHP's standard explode() function by adding a check for empty strings.
     * If the input string (haystack) is empty or consists only of whitespace, this method returns an empty array instead
     * of an array with a single empty string element, which is the default behavior of the standard explode() function.
     * 
     * @param string $separator
     * @param string $haystack
     * 
     * @return array
     */
    public static function explode(string $separator, string $haystack){
        if(is_blank($haystack)){
            return [];
        }
        
        return explode($separator, $haystack);
    }
    
    /**
     * 
     * @param String $filepath
     */
    public static function extractExtensionName(String $filepath){
        $lastIndexOf = strrpos($filepath, ".");
        
        /*
         * 
         * gets the part of the string where the last '..' occurrence is there
         * 
         */
        $doubleDots = substr($filepath, $lastIndexOf - 1);
                
        if($lastIndexOf !== false && !self::contains($doubleDots, "..")){
            return substr($filepath, $lastIndexOf);
        }
        
        return null;
    }
    
    
    /**
     *
     * https://stackoverflow.com/questions/834303/startswith-and-endswith-functions-in-php
     *
     * @param string $haystack
     * @param string $needle
     * @return boolean
     */
    public static function startsWith($haystack, $needle) {
        $length = strlen($needle);
        
        return substr($haystack, 0, $length) === $needle;
    }
    
    /**
     *
     * https://stackoverflow.com/questions/834303/startswith-and-endswith-functions-in-php
     *
     * @param string $haystack
     * @param string $needle
     * @return boolean
     */
    public static function endsWith($haystack, $needle) {
        $length = strlen($needle);
        
        if(!$length){
            return true;
        }
        
        return substr($haystack, -$length) === $needle;
    }
    
    /**
     * 
     * @param string $haystack
     * @param string|array $needle
     * @return boolean
     */
    public static function contains(string $haystack, string|array $needle){
        if(is_array($needle)){
            if(is_blank($needle)){
                return false;    
            }
            
            foreach($needle as $find){
                if(strpos($haystack, $find) !== false){
                    return true;
                }
            }
        }else{
            return strpos($haystack, $needle) !== false;
        }
    }
    
    /**
     *
     * @param number $lim
     * @param boolean $numericOnly
     * @return string
     */
    public static function getRandomizedString($lim = 10, $numericOnly = false){
        $str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        
        if($numericOnly){
            $str = "0123456789";
        }
            
        $randomized = "";
        
        for($i = 0; $i < $lim; $i++){
            $rand = mt_rand(0, strlen($str) - 1);
            $randomized .= $str[$rand];
        }
        
        return $randomized;
    }
    
    /**
     * 
     * gets the string content with their line numbers
     * 
     * @param String $str
     * @return Collections
     */
    public static function getLinedString(String $str){
        $lined = explode("\n", $str);
        
        $temp1 = "";
        $temp2 = "";
        
        foreach($lined as $key => $line){
            if($key === 0){
                if(isset($lined[$key + 1])){
                    $temp1 = $lined[$key + 1];
                }
                
                $lined[$key + 1] = "{$line}\n";
            }else{
                if(isset($lined[$key + 1])){
                    $temp2 = $lined[$key + 1];
                }
                
                $lined[$key + 1] = "{$temp1}\n";
                
                if(isset($lined[$key + 1])){
                    $temp1 = $temp2;
                }
            }
        }
        
        unset($lined[0]);
        
        $lines = collect($lined);
        
        return $lines;
    }
    
    /**
     * 
     * checks whether the string is blank or not
     * 
     * @param String $string
     * @return boolean
     */
    public static function isBlank(String $string){
        if(is_string($string) && strlen(trim($string)) === 0){
            return true;
        }
        
        if(is_null($string)){
            return true;
        }
        
        return false;
    }
    
    /**
     *
     * reference: https://stackoverflow.com/questions/2040240/php-function-to-generate-v4-uuid#comment72492505_15875555
     *
     * @return string
     */
    public static function warpUuid(){
        $data = openssl_random_pseudo_bytes(16);
        
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10
        
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }
    
    /**
     * 
     * splits string by capital letters
     * 
     * @param string $phrase
     * @return string[]|mixed[]
     */
    public static function splitByCapital(String $phrase){
        $phraseLength = strlen($phrase);
        $splitArrays = [];
        
        $currentArray = "";
        for($i = 0; $i < $phraseLength; $i++){
            $char = $phrase[$i];
            
            if(ctype_upper($char)){
                if(!empty($currentArray)){
                    $splitArrays[] = $currentArray;
                }
                
                $currentArray = "";
            }
            
            $currentArray .= $char;
        }
        
        if(!empty($currentArray)){
            $splitArrays[] = $currentArray;
        }
        
        return $splitArrays;
    }
    
    /**
     * 
     * @param string $htmlstr
     * @return boolean
     */
    public static function hasHTMLExpression($htmlstr){
        $pattern = '/<(\w+)([^>]*)>(.*?)<\/\1>/s';
        
        return preg_match($pattern, $htmlstr);
    }
    
    /**
     * 
     * @param string $str
     * @return boolean
     */
    public static function hasWhitespaces(string $str){
        return (strpos($str, ' ') !== false || 
            strpos($str, "\t") !== false || 
            strpos($str, "\n") !== false || 
            strpos($str, "\r") !== false);
    }
    
    /**
     * 
     * determine if the string is a base64 encoded.
     * 
     * @param unknown $string
     * @return boolean
     */
    public static function isBase64Encoded($string) {
        // Decoding the input string
        $decoded = base64_decode($string, true);
        
        // Checking if decoding was successful and the decoded data matches the original string
        if($decoded !== false && base64_encode($decoded) === $string) {
            return true;
        }
        
        return false;
    }
    
    
    /**
     * 
     * Checks if the given subject matches any of the provided regular expressions.
     *
     * This method accepts either a single regular expression string or an array of 
     * regular expression strings. It then checks if the given subject matches any 
     * of the provided regular expressions.
     * 
     * @param string|array $regex
     * @param string $subject
     * @param array &$matches
     * 
     * @return boolean
     */
    public static function matches(string|array $regex, string $subject, array &$matches = null){
        if(is_array($regex)){
            foreach($regex as $pattern){
                if(preg_match($pattern, $subject, $matches)){
                    return true;
                }
            }
        }else if(preg_match($regex, $subject, $matches)){
            return true;
        }
        
        return false;
    }
}