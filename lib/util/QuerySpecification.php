<?php

namespace lib\util;

class QuerySpecification {
    
    /**
     *
     * @var string
     */
    private $tableName;
    
    /**
     *
     * @var array
     */
    private $fields = [];
    
    /**
     *
     * @var string
     */
    private $where;
    
    /**
     *
     * @var array
     */
    private $values = [];
    
    /**
     *
     * @var string
     */
    private $orderBy;
    
    /**
     *
     * @var string
     */
    private $limit;
    
    public function __construct($tableName){
        $this->tableName = $tableName;
    }
    
    /**
     *
     * @param array $fields
     * @return QuerySpecification
     */
    public function setFields(Array $fields){
        $this->fields = $fields;
        return $this;
    }
    
    /**
     *
     * @param string $pattern
     * @param array $values
     * @return QuerySpecification
     */
    public function setWhere($pattern, $values = []){
        $this->where = $pattern;
        $this->values = $values;
        
        return $this;
    }
    
    /**
     *
     * @param string $orderBy
     * @param string $direction
     */
    public function setOrderBy($orderBy, $direction = ""){
        $this->orderBy = "ORDER BY {$orderBy} {$direction}";
    }
    
    /**
     *
     * @param unknown $offset
     * @param unknown $limit
     */
    public function setLimit($offset, $limit){
        if($offset != -1)
            $this->limit = "LIMIT {$offset}, {$limit}";
        else
            $this->limit = "LIMIT {$limit}";
    }
    
    /**
     *
     * @return string[]|array[]
     */
    public function getFilledProperties(){
        return [
            "tableName" => $this->tableName,
            "fields" => $this->fields,
            "where" => $this->where,
            "values" => $this->values,
            "orderBy" => $this->orderBy,
            "limit" => $this->limit
        ];
    }
}