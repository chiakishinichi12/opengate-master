<?php
namespace lib\inner\arrangements;

use lib\inner\Accessor;
use lib\inner\arrangements\http\UploadedFile;

/**
 * 
 * @author antonio
 *
 * @method static \lib\inner\arrangements\http\UploadedFile get(string $param)
 * @method static \lib\inner\arrangements\http\UploadedFile strict(bool $strict)
 * @method static \lib\inner\arrangements\http\UploadedFile onlyAccepts(array $extensions)
 */
class LocalUpload extends Accessor{
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\inner\Accessor::warpedInstance()
     */
    public function warpedInstance(){
        return UploadedFile::class;
    }
}