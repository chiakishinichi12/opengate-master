<?php
namespace lib\mail;

use lib\util\EncapsulationHelper;
use lib\inner\App;

class Attachment implements Attachable {
    
    /**
     * 
     * @var string
     */
    protected $path;
    
    /**
     * 
     * @var string
     */
    protected $as = "";
       
    /**
     * 
     * @var string
     */
    protected $mime = "";
    
    /**
     * 
     * @var string
     */
    protected $encoding = "base64";
    
    /**
     * 
     * @var EncapsulationHelper
     */
    protected $encapsulation;
    
    /**
     * 
     * constructor
     * 
     */
    public function __construct(){
        $this->encapsulation = App::make(EncapsulationHelper::class)->instance($this);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\mail\Attachable::path()
     */
    public function path(string $path = null){
        if(!is_null($path)){
            $path = base_path($path);
        }
        
        return $this->encapsulation->propertyDefinition($this->{__FUNCTION__}, $path);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\mail\Attachable::disk()
     */
    public function disk(string $path){
        $this->path = $path;
        
        return $this;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\mail\Attachable::as()
     */
    public function as(string $as = null){
        return $this->encapsulation->propertyDefinition($this->{__FUNCTION__}, $as);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\mail\Attachable::mime()
     */
    public function mime(string $mime = null){
        return $this->encapsulation->propertyDefinition($this->{__FUNCTION__}, $mime);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\mail\Attachable::encoding()
     */
    public function encoding(string $encoding = null){
        return $this->encapsulation->propertyDefinition($this->{__FUNCTION__}, $encoding);
    }
}