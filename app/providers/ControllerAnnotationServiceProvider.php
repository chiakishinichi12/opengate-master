<?php
namespace app\providers;

use lib\inner\ApplicationGate;
use lib\inner\Locale;
use lib\inner\arrangements\ServiceProvider;
use lib\util\Collections;
use lib\util\GateFinder;
use lib\util\StringUtil;
use lib\util\controller\AnnotatedControllerMethodLocator;
use lib\util\exceptions\AnnotationException;
use lib\util\exceptions\RoutingException;
use lib\util\route\Route;
use lib\util\route\RouteBean;

/**
 * 
 * @author Antonius Aurelius
 *
 */
class ControllerAnnotationServiceProvider extends ServiceProvider{
    
    /**
     * 
     * @var boolean
     */
    protected $forCLI = false;
    
    /**
     * 
     * @var AnnotatedControllerMethodLocator
     */
    protected $locator;
    
    
    /**
     * 
     * appends the alignment of the controller's method to the RouteBean container
     * 
     * @param ApplicationGate $gate
     * @throws RoutingException
     */
    public function boot(ApplicationGate $gate){        
        $this->locator = $gate->make(AnnotatedControllerMethodLocator::class);
       
        // GateFinder object will help you to resolve the class names that extends the Controller class.
        (new GateFinder(relative_path(preferences("structure")->get("controllers")), true))->load(function($class){
            $this->locator->addController($class);
        });
        
        $this->locator->scanQualifiedQueries();
        
        $this->locator->getQueries()->each(function(Collections $annotations, $query){            
            $this->readRoutingAnnotations($annotations, $query);
        }); 
    }
    
    /**
     * 
     * this holds the value of base namespace for middlewares globally
     *
     * @var string
     */
    protected $globalNamespace;
    
    /**
     * 
     * this contains the subdomain value on a global scope
     * 
     * @var string
     */
    protected $globalSubdomain;
    
    /**
     * 
     * this contains a string to be concatenated within each routes on
     * 
     * @var string
     */
    protected $globalPrimary;
    
    /**
     * 
     * appends a new RouteBean object (backed by Annotations) to the Routing Container
     * 
     * @param Collections $annotations
     * @param string $query
     * @throws RoutingException
     */
    protected function readRoutingAnnotations($annotations, $query){
        if(collect(explode("@", $query))->count() < 2){
            $topAnnotations = $this->locator->getQueries()->get($query);
            
            $this->globalNamespace = $topAnnotations->get("@MidClassNS");
            $this->globalSubdomain = $topAnnotations->get("@Subdomain");
            $this->globalPrimary = $topAnnotations->get("@Primary");
                        
            return;
        }
        
        $route = $annotations->get("@Route");
        $config = $annotations->get("@ResponseConfig");
        $middleware = $annotations->get("@Middleware");
        $routePush = $annotations->get("@RoutePush");
        $routeConcat = $annotations->get("@RouteConcat");
        $subdomain = $annotations->get("@Subdomain") ?: $this->globalSubdomain;
        $mdcbasensp = $annotations->get("@MidClassNS") ?: $this->globalNamespace;
        $localized = $annotations->get("@Localized");
        
        // subdomain availability checking
        if($subdomain){            
            $host = request()->server("HTTP_HOST");
             
            if(!in_array($host, $subdomain->get("hosts"))){
                return;
            }
        }
        
        // initalizing the base namespace value for middlewares
        $mdcbasensp = $mdcbasensp ? $mdcbasensp->get("base") : false;
        
        $method = $route?->get("method") ?: "GET";
        $method = strtolower($method);
        
        if(!Route::isValidMethod($method)){
            throw new RoutingException("Invalid request method found in {$query}: {$method}");
        }
        
        $uri = $route?->get("uri");
        
        $type = $route?->get("type") ?: "page";
        
        if(!is_blank($routePush)){
            $this->routePushing($routePush, $uri);
        }
        
        if(!is_blank($routeConcat)){
            $this->routeConcatenation($routeConcat, $uri);
        }
        
        if(!is_array($uri) && !is_blank($this->globalPrimary)){
            $uri = "{$this->globalPrimary->get("uri")}{$uri}";
        }
        
        if(!is_blank($localized)){
            $this->localizedRouting($uri);
        }
        
        if(!is_blank($uri)){
            $this->purifyUri($uri);
            
            $routeBean = Route::$method($uri, $query, $type)->type($type);
            
            if(!is_null($config)){
                $config->put("method", $method);
                
                $default = collect($routeBean->responseConfig());
                
                foreach($config as $key => $value){
                    if(!isset($value)){
                        $config->put($key, $default->get($key));
                    }
                }
                
                $routeBean->responseConfig($config->toArray());
            }
            
            if(!is_blank($middleware)){
                $this->alignMiddlewares($middleware, $mdcbasensp, $routeBean);
            }
        }
    }
    
    /**
     * 
     * @param string|array $uri
     */
    protected function purifyUri(string|array &$uri){
        if(is_array($uri)){
            foreach($uri as $key => $slug){
                if(strcmp($slug, "/") !== 0 && StringUtil::endsWith($slug, "/")){
                    $uri[$key] = substr($slug, 0, strlen($slug) - 1);
                }
            }
        }else if(is_string($uri)){
            if(strcmp($uri, "/") !== 0 && StringUtil::endsWith($uri, "/")){
                $uri = substr($uri, 0, strlen($uri) - 1);
            }
        }
    }
    
    /**
     * 
     * 
     * @param Collections $routePush
     * @param mixed $uri
     */
    protected function routePushing(Collections $routePush, &$uri){
        $extras = [];
        
        foreach($routePush as $push){
            $added = $push->get("uri");
            
            if(is_blank($added)){
                continue;
            }
            
            if(!is_blank($this->globalPrimary)){
                $added = "{$this->globalPrimary->get("uri")}{$added}";
            }
            
            $extras[] = $added;
        }
        
        if(is_array($uri)){
            $uri = array_merge($extras, $uri);
        }else{
            if(!is_blank($this->globalPrimary)){
                $uri = "{$this->globalPrimary->get("uri")}{$uri}";
            }
            
            $extras[] = $uri;
            
            $uri = $extras;
        }
    }
    
    /**
     * 
     * @param mixed $middleware
     * @param string $mdcbasensp
     * @param mixed $rb
     */
    protected function alignMiddlewares($middleware, $mdcbasensp, RouteBean $routeBean){
        $middlewares = [];
        
        foreach($middleware as $reusedmid){
            $baseNamespace = $reusedmid->get("baseNS");
            $classes = $reusedmid->get("classes");
            
            // if a classname was found as a string type (not an array)
            if(is_string($classes)){
                $stringed = $this->middlewareNamespacing($classes,
                    $mdcbasensp,
                    $baseNamespace);
                
                $this->appendIfNotStored($middlewares, $stringed);
                
                continue;
            }
            
            // default
            foreach($classes as $class){
                $class = $this->middlewareNamespacing($class,
                    $mdcbasensp,
                    $baseNamespace);
                
                $this->appendIfNotStored($middlewares, $class);
            }
        }
        
        $routeBean->middlewares($middlewares);
    }
    
    /**
     * 
     * prepends available language codes on the configured route.
     * 
     * @param string|array $uri
     */
    protected function localizedRouting(string|array &$uri){
        $mixed = [];
        
        // inserts the string or merges the array to the $mixed[] stack
        if(is_string($uri)){
            $mixed[] = $uri;
        }else if(is_array($uri)){
            $mixed = array_merge($mixed, $uri);
        }
        
        foreach(Locale::languages() as $lang){
            if(is_array($uri)){
                foreach($uri as $slug){
                    $mixed[] = "/{$lang}{$slug}";
                }
            }else{
                $mixed[] = "/{$lang}{$uri}";
            }
        }
        
        $uri = $mixed;
    }
    
    /**
     * 
     * @param Collections $routeConcat
     * @param mixed $uri
     * @throws AnnotationException
     */
    protected function routeConcatenation(Collections $routeConcat, &$uri){
        $mixed = [];
        
        if(is_array($uri)){
            foreach($uri as $u){
                $this->concatSub($routeConcat, $u, $mixed);
            }
            
            $uri = array_merge($mixed, $uri);
        }else{
            $this->concatSub($routeConcat, $uri, $mixed);
            
            if(!is_blank($this->globalPrimary)){
                $uri = "{$this->globalPrimary->get("uri")}{$uri}";
            }
            
            $mixed[] = $uri;
            $uri = $mixed;
        }
    }
    
    /**
     * 
     * @param Collections $routeConcat
     * @param string $uri
     * @param array $mixed
     * 
     * @throws RoutingException
     * @throws AnnotationException
     */
    protected function concatSub(Collections $routeConcat, string $uri, array &$mixed){
        foreach($routeConcat as $concat){
            $position = $concat->get("position");
            $concuri = $concat->get("uri");
            
            if(is_blank($concuri)){
                continue;
            }
            
            if(!StringUtil::startsWith($concuri, "/")){
                throw new RoutingException("Route string must start with /. [{$concuri}]");
            }
            
            $uristring = "";
            
            if(!is_blank($position)){
                // concatenation
                switch(strtolower($position)){
                    case "suffix":
                    case "last":
                        $uristring = "{$uri}{$concuri}";
                        break;
                    case "first":
                    case "prefix":
                        $uristring = "{$concuri}{$uri}";
                        break;
                    default:
                        throw new AnnotationException("Unknown position: {$position} [@RouteConcat]");
                        break;
                }
            }else{
                // by default, the route will be concatenated @
                // the beginning of URI string (prefix)
                $uristring = "{$concuri}{$uri}";
            }
            
            if(!is_blank($this->globalPrimary)){
                $uristring = "{$this->globalPrimary->get("uri")}{$uristring}";
            }
            
            $mixed[] = $uristring;
        }
    }
    
    /**
     * eliminates duplicate values within strings
     * 
     * @param array $data
     * @param mixed $toAppend
     */
    protected function appendIfNotStored(array &$data, $toAppend){
        if(!in_array($toAppend, $data)){
            $data[] = $toAppend;
        }
    }
    
    /**
     * 
     * @param string $class
     * @param string $midbasensp
     * @param string $baseNamespace
     * 
     * @return string|mixed
     */
    protected function middlewareNamespacing($class, $midbasensp, $baseNamespace){
        $definite = $class;
        
        // if defined, this will be the default base namespace
        // for middleware classes
        if($midbasensp){
            $definite = "{$midbasensp}\\{$class}";
        }
        
        // this explicitly sets its own base namespace for the set of
        // middleware objects within the '@Middleware' annotation
        if($baseNamespace){
            $definite = "{$baseNamespace}\\{$class}";
        }
        
        return $definite;
    }
}