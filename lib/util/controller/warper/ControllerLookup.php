<?php
namespace lib\util\controller\warper;

use ReflectionClass;
use ReflectionMethod;
use lib\inner\App;
use lib\util\Collections;
use lib\util\ReflectionUtil;
use lib\util\cli\WarpArgv;
use lib\util\controller\Controller;
use lib\util\annotation\AnnotationUtil;
use lib\util\annotation\Annotation;
use lib\util\controller\AnnotatedControllerMethodLocator;

class ControllerLookup {
    
    /**
     * 
     * @var Collections
     */
    protected $args;
    
    /**
     * 
     * @var string
     */
    protected $controllers;
    
    /**
     * 
     * @var Collections
     */
    protected $annotations;
    
    /**
     * 
     * @param WarpArgv $argv
     */
    public function __construct(WarpArgv $argv){
        $this->args = $argv->obtainArguments();
        
        $this->controllers = preferences("structure")->get("controllers");
        
        $locator = App::make(AnnotatedControllerMethodLocator::class);
        
        $this->annotations = $locator->registeredAnnotations();
    }
    
    public function lookupController(){
        if($this->args->count() < 2){
            return;
        }
        
        $controller = str_replace("/", "\\", relative_path($this->controllers)."/{$this->args->get(1)}");
        
        if(!is_null($errmsg = $this->evaluateLookup($controller))){
            out()->println("<error>{$errmsg}</error>");
            return;
        }
        
        $reflection = new ReflectionClass($controller);
        
        out()->println("\n Class: <primary>{$controller}</primary>");
        
        $this->annotations->each(function(Annotation $annotation) use ($reflection){
            $properties = collect();
            
            if(AnnotationUtil::hasAnnotation($reflection, $annotation, $properties)){
                out()->println(" <info>{$annotation}</info><warning>".$this->reveal($properties)."</warning>");
            }
        });
        
        out()->println();
        
        $methods = ReflectionUtil::getDeclaredMethods($reflection, ReflectionMethod::IS_PUBLIC); 
        
        $methods->each(function(ReflectionMethod $method) use ($controller){
            if(strcmp($method->getName(), "__construct") === 0){
                return;
            }
            
            $annotations = [];
            
            $this->annotations->each(function(Annotation $annotation) use ($method, &$annotations){
                $properties = collect();
                
                if(AnnotationUtil::hasAnnotation($method, $annotation, $properties)){
                    $annotations[$method->getName()][$annotation->getName()] = $properties;
                }
            });
            
            out()->println(" <magenta>".class_basename($controller)."::{$method->getName()}()</magenta>");
            
            if(isset($annotations[$method->getName()])){
                foreach($annotations[$method->getName()] as $annotation => $value){
                    out()->print(" {$annotation}");
                    out()->println("<warning>{$this->reveal($value)}</warning>");
                }
            }
            
            out()->println();
        });
    }
    
    /**
     * 
     * @param Collections $value
     * @return string
     */
    protected function reveal(Collections $value){
        $builder = [];
        
        foreach($value->toArray() as $k => $v){
            if(is_array($v)){
                $builder[] = "{$k}=[".implode(", ", $v)."]";
            }else if(is_string($v) || is_numeric($v)){
                $builder[] = "{$k}={$v}";
            }else if(is_bool($v)){
                $builder[] = "{$k}=".($v ? "true" : "false");
            }
        }
        
        return !is_blank($builder) ? "(".implode(", ", $builder).")" : "";
    }
    
    /**
     * 
     * @param string $controller
     * @return string|NULL
     */
    protected function evaluateLookup(string $controller){        
        if(!class_exists($controller)){
            return "Class '{$controller}' doesn't exist.";    
        }
        
        if(!App::checkInheritance($controller, Controller::class)){
            return "Class '{$controller}' is not an instance of ".Controller::class;
        }
        
        return null;
    }
}

