<?php

namespace lib\traits;

use lib\inner\App;
use lib\util\QueryUtil;
use lib\util\exceptions\SQLException;
use lib\util\mysql\MySQLInstance;
use lib\util\mysql\MySQLQueryUtil;

trait SQLFeatures {
    
    /**
     * 
     * @param String $dbms
     * @return QueryUtil
     */
    public function getQueryUtil(String $dbms = "mysql"){
        $qu = null;
        
        switch($dbms){
            case "mysql":{
                $conn = MySQLInstance::getInstance()->getConnection();
                
                if(App::checkInheritance(MySQLQueryUtil::class, [QueryUtil::class])){
                    if($conn === null){
                        goto end;
                    }
                    
                    $qu = MySQLQueryUtil::getInstance($conn); 
                }
                break;
            }
            default:
                throw new SQLException("Unknown DBMS: {{$dbms}}");
        }
        
        end:
        return $qu;
    }
}