<?php

namespace lib\inner;

use lib\instances\HasCsrfToken;
use lib\traits\Security;
use lib\util\EncapsulationHelper;
use lib\traits\GateKeeperValue;

class WebAppCore{
    
    use Security, HasCsrfToken, GateKeeperValue, VarBinder, SessionVolatileBinder, ValidationBinder;
    
    /**
     *
     * @var Array
     */
    protected $middlewares = [];
    
    /**
     *
     * @var string
     */
    protected $instanceType = "";
    
    /**
     *
     * @var array
     */
    protected $responseConfig = [
        "contentType" => "text/json",
        "method" => "GET",
        "readonly" => false
    ];
    
    /**
     * 
     * @var EncapsulationHelper
     */
    protected $encapsulation;
    
    public function __construct(){
        $this->encapsulation = App::make(EncapsulationHelper::class)->instance($this);
    }
    
    /**
     * 
     * @param array $responseConfig
     * @return array|object
     */
    public function responseConfig($responseConfig = null){
        return $this->encapsulation->propertyArrayMerge($this->{__FUNCTION__}, $responseConfig);
    }
    
    /**
     * 
     * @param array $responseConfig
     */
    public function loadResponseContentType(){
        if(App::checkCLI() && !App::isRunningUnitTest()){
            return;
        }
        
        response()->setHeaders([
            "Content-Type" => $this->responseConfig["contentType"]
        ], true);
    }
    
    /**
     * 
     * @param array $middlewares
     * @return mixed|object
     */
    public function middlewares(array $middlewares = null){
        return $this->encapsulation->propertyDefinition($this->{__FUNCTION__}, $middlewares);
    }
    
    /**
     * 
     * @param string $instanceType
     * @return mixed|object
     */
    public function instanceType(string $instanceType = null){
        return $this->encapsulation->propertyDefinition($this->{__FUNCTION__}, $instanceType);
    }
}