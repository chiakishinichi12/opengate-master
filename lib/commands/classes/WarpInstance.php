<?php
namespace lib\commands\classes;

use lib\instances\CommandInstance;
use lib\util\cli\WarpArgv;
use lib\commands\CommonMethods;
use lib\inner\App;
use lib\instances\warper\InstanceClassCreation;
use lib\instances\warper\InstanceList;

/**
 * 
 * @Commands(["instance", "inst"])
 *
 */
class WarpInstance extends CommandInstance {
    
    use CommonMethods;
    
    /**
     * 
     * @var string
     */
    protected $name = "instance";
    
    /**
     * 
     * @var string
     */
    protected $desc = "core instance tool";
    
    /**
     * 
     * @Argument("creates new instance object. { --cli=(class) | --page=(class) | --api=(class) }\n { --identifier=(string) }")
     * 
     */
    public function create(){
        App::make(InstanceClassCreation::class)->write();
    }
    
    /**
     * 
     * @Argument("shows core instance class list")
     * 
     */
    public function list(){
        App::make(InstanceList::class)->showList();
    }
    
    /**
     * 
     * @param WarpArgv $argv
     */
    public function listen(WarpArgv $argv){
        $this->execution($argv);
    }
}

