<?php
namespace lib\inner\client;

use Closure;

class Http {
    
    /**
     * 
     * @param string $url
     * @param Closure $with
     * 
     * @return ClientResponse
     */
    public static function get(string $url, Closure $with = null){
        $client = fetch($url)->method("GET");
        
        $retrievedResponse = null;
        
        $callback = function(ClientResponse $response) use (&$retrievedResponse){
            $retrievedResponse = $response;
        };
        
        if(!is_null($with)){
            $with($client);
        }
        
        $client->responseSuccess($callback)->responseError($callback);
                
        return $retrievedResponse;
    }
    
    /**
     * 
     * @param string $url
     * @param array $data
     * @param Closure $with
     * 
     * @return ClientResponse
     */
    public static function post(string $url, array $data, Closure $with = null){
        $client = fetch($url)
            ->method("POST")
            ->data($data);
        
        $retrievedResponse = null;
        
        $callback = function(ClientResponse $response) use (&$retrievedResponse){
            $retrievedResponse = $response;
        };
        
        if(!is_null($with)){
            $with($client);
        }
        
        $client->responseSuccess($callback)->responseError($callback);
        
        return $retrievedResponse;
    }
}

