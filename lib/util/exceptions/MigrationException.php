<?php

namespace lib\util\exceptions;

use lib\exceptions\OpenGateException;

class MigrationException extends OpenGateException {
    
}