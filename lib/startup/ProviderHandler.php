<?php
namespace lib\startup;

use lib\inner\ApplicationGate;
use lib\inner\App;

class ProviderHandler {
    
    /**
     * 
     * @param ApplicationGate $gate
     */
    public function bootstrap(ApplicationGate $gate){
        $providerConf = preferences("providers");
        
        if(is_null($providerConf)){
            goto ends;
        }
        
        $classes = $providerConf->get("classes");
        
        if(!@is_array($classes)){
            goto ends;            
        }
        
        foreach($classes as $class){
            $provider = $gate->make($class);
                        
            if($provider->isForAll()){
                goto boot;
            }
            
            if(App::checkCLI()){
                if(!$provider->isForCLI()){
                    continue;
                }
            }else{
                if($provider->isForCLI()){
                    continue;
                }
            }
            
            boot:
            if(method_exists($provider, "boot")){
                $provider->boot($gate);
            }
        }
        
        ends:
        return;
    }
}