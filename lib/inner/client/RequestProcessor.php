<?php

namespace lib\inner\client;

use CURLFile;
use lib\exceptions\OpenGateException;
use lib\inner\App;
use lib\util\Collections;
use lib\util\ElapsedTime;
use lib\util\EncapsulationHelper;
use lib\util\accords\Logger;
use lib\util\file\File;
use lib\inner\json\JsonUtility;

/**
 * 
 * Handles HTTP request processing with various configurations and settings.
 * Provides methods to set request parameters, send the request, and handle responses.
 * 
 * Commonly used for making API calls.
 * 
 * @author Antonius Aurelius
 *
 */
class RequestProcessor {
    
    // most commonly used mime types in API
    public const CT_APPLICATION_X_WWW_FORM_URLENCODED   = "application/x-www-form-urlencoded";
    public const CT_APPLICATION_JSON                    = "application/json";
    
    /**
     * 
     * @var \CurlHandle
     */
    protected $init;
    
    /**
     * 
     * @var string
     */
    protected $url = "";
    
    /**
     * 
     * @var boolean
     */
    protected $enableHeader = true;
    
    /**
     * 
     * @var boolean
     */
    protected $nobody = false;
    
    /**
     * 
     * @var string
     */
    protected $method = "POST";
    
    /**
     * 
     * @var string
     */
    protected $data = "";
    
    /**
     * 
     * @var string
     */
    protected $encodeAs;
    
    /**
     * 
     * @var string
     */
    protected $xmlroot = "root";
    
    /**
     * 
     * @var string
     */
    protected $characterEncoding = "utf8";
    
    /**
     * 
     * @var array
     */
    protected $headers = [];
    
    /**
     * 
     * @var boolean
     */
    protected $returnTransfer = true;
    
    /**
     * 
     * @var boolean
     */
    protected $followLocation = true;
    
    /**
     * 
     * @var boolean
     */
    protected $sslVerifyPeer = true;
    
    /**
     * 
     * @var boolean
     */
    protected $sslVerifyHost = true;
    
    /**
     * 
     * @var boolean
     */
    protected $logResponse = false;
    
    /**
     * 
     * @var boolean
     */
    protected $strict = true;
    
    /**
     * 
     * @var ClientResponse
     */
    protected $response;
    
    /**
     * 
     * @var int
     */
    protected $responseCode = -1;
    
    /**
     * 
     * @var string
     */
    protected $responseHeaders = "";
    
    /**
     * 
     * @var string
     */
    protected $responseBody = "";
    
    /**
     * 
     * @var boolean
     */
    protected $successfullyLoaded = false;
    
    /**
     * 
     * @var callable
     */
    protected $closure;
    
    
    /**
     * 
     * @var EncapsulationHelper
     */
    protected $encapsulation;
    
    /**
     * 
     * @var ElapsedTime
     */
    protected $elapsedTime;
    
    /**
     * 
     * @var boolean
     */
    protected $curlErrorHappened = false;
    
    /**
     * 
     * @param string $url
     * @param boolean $enableHeader
     * @param boolean $nobody
     */
    public function __construct(string $url = "", bool $enableHeader = true, bool $nobody = false){
        $this->url = $url;
        $this->enableHeader = $enableHeader;
        $this->nobody = $nobody;
        
        $this->elapsedTime = App::make(ElapsedTime::class);
        
        $this->encapsulation = App::make(EncapsulationHelper::class)->instance($this);
    }
    
    /**
     * 
     * Sets or gets the URL for the request.
     * 
     * @param string $url
     * @return mixed|RequestProcessor
     */
    public function url(string $url = null){
        return $this->encapsulation->propertyDefinition($this->{__FUNCTION__}, $url);
    }
    
    /**
     * 
     * Sets or gets whether to enable headers in the response.
     * 
     * @param bool $enableHeader
     * @return mixed|RequestProcessor
     */
    public function enableHeader(bool $enableHeader = null){
        return $this->encapsulation->propertyDefinition($this->{__FUNCTION__}, $enableHeader);
    }
    
    /**
     * 
     * Sets or gets whether to omit the body in the response.
     * 
     * @param bool $nobody
     * @return mixed|RequestProcessor
     */
    public function nobody(bool $nobody = null){
        return $this->encapsulation->propertyDefinition($this->{__FUNCTION__}, $nobody);
    }
    
    /**
     * 
     * Sets or gets the encoding type for the data.
     * 
     * @param string $encodeAs
     * @return mixed|RequestProcessor
     */
    public function encodeAs(string $encodeAs = null){
        return $this->encapsulation->propertyDefinition($this->{__FUNCTION__}, $encodeAs);
    }
    
    /**
     * 
     * sets or gets the root of the generated xml document
     * 
     * @param string $xmlroot
     * @return mixed|RequestProcessor
     */
    public function xmlroot(string $xmlroot = null){
        return $this->encapsulation->propertyDefinition($this->{__FUNCTION__}, $xmlroot);
    }
    
    /**
     * 
     * Sets the data to be sent in the request.
     * 
     * @return mixed|RequestProcessor
     */
    public function data(array $data = null){
        return $this->encapsulation->propertyDefinition($this->{__FUNCTION__}, $data);
    }
    
    /**
     * 
     * enable exception thrower when something wrong happens during response fetching
     * 
     * @param bool $strict
     * @return mixed|object
     */
    public function strict(bool $strict = null){
        return $this->encapsulation->propertyDefinition($this->{__FUNCTION__}, $strict);
    }
    
    /**
     * 
     * Sets or gets the HTTP method for the request.
     * 
     * @param String $method
     * @return RequestProcessor
     * 
     * <br/><br/>
     * request method
     */
    public function method(string $method = ""){        
        return $this->encapsulation->propertyDefinition($this->{__FUNCTION__}, $method);
    }
    
    /**
     * 
     * Sets or merges the headers for the request.
     * 
     * @param array $headers
     * @return RequestProcessor
     * 
     * <br/><br/>
     * setting request headers
     */
    public function headers(array $headers = []){
        return $this->encapsulation->propertyArrayMerge($this->{__FUNCTION__}, $headers);
    }
    
    /**
     * 
     * Sets or gets the character encoding for the request.
     * 
     * @param string $characterEncoding
     * @return RequestProcessor
     */
    public function characterEncoding(string $characterEncoding = ""){
        return $this->encapsulation->propertyDefinition($this->{__FUNCTION__}, $characterEncoding);
    }
    
    /**
     * 
     * Sets or gets whether to return the transfer as a string.
     * 
     * @param bool $returnTransfer
     * @return RequestProcessor
     */
    public function returnTransfer(bool $returnTransfer = null){
        return $this->encapsulation->propertyDefinition($this->{__FUNCTION__}, $returnTransfer);
    }
    
    /**
     * 
     * Sets or gets whether to follow redirects.
     * 
     * @param bool $followLocation
     * @return RequestProcessor
     */
    public function followLocation(bool $followLocation = null){
        return $this->encapsulation->propertyDefinition($this->{__FUNCTION__}, $followLocation);
    }
    
    /**
     * 
     * Sets or gets whether to verify the SSL peer.
     * 
     * @param bool $sslVerifyPeer
     * @return RequestProcessor
     */
    public function sslVerifyPeer(bool $sslVerifyPeer = null){
        return $this->encapsulation->propertyDefinition($this->{__FUNCTION__}, $sslVerifyPeer);
    }
    
    /**
     * 
     * Sets or gets whether to verify the SSL host.
     * 
     * @param bool $sslVerifyHost
     * @return RequestProcessor
     */
    public function sslVerifyHost(bool $sslVerifyHost = null){
        return $this->encapsulation->propertyDefinition($this->{__FUNCTION__}, $sslVerifyHost);
    }
    
    /**
     * 
     * Adds Basic Authentication token to the headers.
     * 
     * pushing a Basic Authentication token to the header side
     * 
     * @param string $user
     * @param string $pass
     * 
     */
    public function basicAuth(string $user, string $pass){
        $hashedCredential = base64_encode("{$user}:{$pass}");
        
        array_push($this->headers, "Authorization: Basic {$hashedCredential}");
        
        return $this;
    }
    
    /**
     *
     * Ensures that each header in the $headers array is properly formatted as a string.
     * Iterates through the $headers array and formats each header as "HeaderName: HeaderValue"
     * if the header name is a string. This method modifies the $headers array in place.
     *
     */
    protected function purifyHeaders(){
        foreach($this->headers as $headerName => $headerValue){
            if(is_string($headerName)){
                $this->headers[$headerName] = "{$headerName}: {$headerValue}";
            }
        }
    }
    
    /**
     * 
     * Encodes the request data based on the specified encoding type.
     * 
     * This method checks the value of the $encodeAs property to determine how to encode the data.
     * It supports two encoding types: "httpQuery" and "json". 
     * - If $encodeAs is "httpQuery", the data is encoded as a URL-encoded query string.
     * - If $encodeAs is "json", the data is encoded as a JSON string.
     * If an unsupported encoding type is specified, the method throws an OpenGateException 
     * and provides a warning message.
     * 
     * @throws OpenGateException If an unsupported encoding type is specified
     * 
     */
    protected function encodeData(){
        if(is_null($this->encodeAs)){
            return;
        }
        
        switch(strtolower($this->encodeAs)){
            case "httpquery":
                $this->data = http_build_query($this->data);
                break;
            case "json":
                $this->data = JsonUtility::toEncode($this->data)->encoded();
                break;
            case "xml":
                $this->data = xml_encode($this->data, $this->xmlroot);
                break;
            default:
                throw new HttpClientException("\encodeAs() only accepts 2 values ['httpQuery' | 'json']. '{$this->encodedAs()}' found");
        }
    }
    
    /**
     * 
     * Purify data by processing file objects for cURL file uploads.
     * 
     * For each value that is an object, it checks whether it is an instance of the `File` class
     * or any class that `File` is derived from. If the value is not an instance of `File`,
     * it removes the corresponding key from the `$data` array. If it is an instance of `File`,
     * it converts the value to a `CURLFile` object using the file's name and MIME type.
     * 
     */
    protected function purifyData(){
        foreach($this->data as $param => $value){
            if(!is_object($value)){
                continue;
            }
            
            if($value instanceof CURLFile || method_exists($value, "__toString")){                
                continue;
            }
            
            if($value instanceof File){            
                $this->data[$param] = new CURLFile($value->getName(), $value->getMimeType());
            }else{
                unset($this->data[$param]);
            }
        }
    }
    
    /**
     * 
     * throws exception if the url string is invalid
     * 
     */
    protected function throwIfInvalidURL(){
        if(filter_var($this->url(), FILTER_VALIDATE_URL) === false){
            throw new HttpClientException("Invalid URL: {$this->url()}");
        }
    }
    
    /**
     * returns true if a response code is successful. otherwise, false if failed.
     * A successful response code is in the range of 200-299.
     *
     * @param int $responseCode
     * 
     * @return bool
     */
    public function isSuccessfulResponse() {
        return $this->responseCode >= 200 && $this->responseCode <= 299;
    }
    
    /**
     * 
     * Handles the response callback.
     * 
     * @param closure $closure
     */
    protected function handleResponseCallback($closure){
        if(is_blank($this->closure) && is_callable($closure)){
            $this->closure = $closure;
            $closure($this->response());
        }
    }
    
    /**
     * 
     * Executes the provided callback if the response is successful.
     * A response is considered successful if the isSuccessfulResponse method returns true.
     * 
     * @param callable $closure
     * @return RequestProcessor
     */
    public function responseSuccess($closure){ 
        if(!$this->isSuccessfullyLoaded()){
            $this->send();
        }
        
        if($this->isSuccessfulResponse()){
            $this->handleResponseCallback($closure);
        }
        
        return $this;
    }
    
    /**
     *
     * * Executes the provided callback if the response is an error.
     * A response is considered an error if the isSuccessfulResponse() method returns false.
     *
     * @param callable $closure
     * 
     * @return RequestProcessor
     */
    public function responseError($closure){
        if(!$this->isSuccessfullyLoaded()){
            $this->send();
        }
        
        if(!$this->isSuccessfulResponse()){
            $this->handleResponseCallback($closure);
        }
        
        return $this;
    }
    
    /**
     * 
     * @return \lib\inner\client\ClientResponse
     */
    public function response(){
        return $this->response;
    }
    
    /**
     * 
     * enables the response logger
     * 
     * @return RequestProcessor
     */
    public function logResponse(){        
        $this->logResponse = true;        

        return $this;
    }
    
    /**
     * 
     * returns true if error was found during cURL execution
     * 
     * @return boolean
     */
    public function curlErrorHappened(){
        return $this->curlErrorHappened;
    }
    
    /**
     * 
     * returns a User-Agent header value for this ClientURL tool
     * 
     * @return string
     */
    protected function getUserAgent(){
        $agent = "OpenGate ".class_basename(RequestProcessor::class)."/".OPENGATE_VERSION;
        
        if(function_exists("curl_version")){
            $version = curl_version();
            
            $agent .= "(curl[{$version["version"]}])";
        }
        
        return $agent;
    }
    
    /**
     * 
     * Sends the HTTP request and processes the response.
     * Initializes the cURL session, sets the options, and executes the request.
     * Stores the response code, headers, and body for further processing.
     * 
     * @return RequestProcessor
     */
    public function send(){
        $this->headers["User-Agent"] = $this->getUserAgent();
        
        if($this->isSuccessfullyLoaded()){
            goto ends;
        }
        
        $this->throwIfInvalidURL();
        
        $this->init = @curl_init($this->url);
        
        @curl_setopt($this->init, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        @curl_setopt($this->init, CURLOPT_HEADER, $this->enableHeader);
        @curl_setopt($this->init, CURLOPT_NOBODY, $this->nobody);
        
        @curl_setopt($this->init, CURLOPT_CUSTOMREQUEST, $this->method);
        @curl_setopt($this->init, CURLOPT_ENCODING, $this->characterEncoding);
        @curl_setopt($this->init, CURLOPT_RETURNTRANSFER, $this->returnTransfer);
        @curl_setopt($this->init, CURLOPT_FOLLOWLOCATION, $this->followLocation);
        @curl_setopt($this->init, CURLOPT_SSL_VERIFYPEER, $this->sslVerifyPeer);
        @curl_setopt($this->init, CURLOPT_SSL_VERIFYHOST, $this->sslVerifyHost);
        
        if(!is_blank($this->data)){
            $this->purifyData();
            $this->encodeData();
            @curl_setopt($this->init, CURLOPT_POSTFIELDS, $this->data);
        }
        
        if(!is_blank($this->headers)){
            $this->purifyHeaders();
            @curl_setopt($this->init, CURLOPT_HTTPHEADER, $this->headers);
        }
        
        $this->elapsedTime->start();
        
        $output = @curl_exec($this->init);
        
        if($err = @curl_error($this->init)){
            $this->curlErrorHappened = true;
            $this->responseBody = $err;
            
            if($this->strict()){
                throw new HttpClientException("cURL Communication Error: {$err}");
            }
            
            return;
        }
        
        $this->successfullyLoaded = true;
        
        $headerSize = curl_getinfo($this->init, CURLINFO_HEADER_SIZE);
        
        $this->responseCode = curl_getinfo($this->init, CURLINFO_HTTP_CODE);
        $this->responseHeaders = substr($output, 0, $headerSize);
        $this->responseBody = substr($output, $headerSize);
        
        @curl_close($this->init);
        
        $this->elapsedTime = $this->elapsedTime->formatElapsed();
        
        $this->response = new ClientResponse($this);
        
        $this->loggingResponse();
        
        ends:
        return $this;
    }
    
    /**
     * 
     * logs the response obtained after request processing
     * 
     */
    protected function loggingResponse(){
        if($this->logResponse){
            $logtype = $this->isSuccessfulResponse() ? LOG_INFO : LOG_ERR;
            
            App::make(Logger::class)->log(
                $this->response()->details(true), $logtype);
        }
    }
    
    /**
     * 
     * @return string
     */
    public function details(){
        if($this->curlErrorHappened()){
            return "cURL Error: {$this->getResponseBody()}";
        }
        
        $details = [
            "client_url" => $this->url(),
            "date_requested" => date('D, d M Y H:i:s'),
            "elapsed_time" => $this->elapsedTime,
            "is_success_response" => $this->isSuccessfulResponse() ? "true" : "false",
            "response_code" => $this->getResponseCode()
        ];
        
        $output = "[\n";
        
        foreach($details as $key => $detail){
            $output .= "  {$key}: {$detail}\n";
        }
        
        $output .= "]";
        
        return $output;
    }
    
    /**
     * 
     * @return string
     */
    public function __toString(){
        return $this->details();
    }
    
    /**
     * 
     * Checks if the request received a successful response.
     * It won't return false even if the response code indicates some kind of error.
     * 
     * @return bool
     */
    public function isSuccessfullyLoaded() : bool {
        return $this->successfullyLoaded;
    }
    
    /**
     * 
     * Gets the HTTP response code from the request.
     * 
     * @return number|mixed
     */
    public function getResponseCode(){
        return $this->responseCode;
    }
    
    /**
     * 
     * Gets the response body from the request.
     * 
     * @return string|unknown
     */
    public function getResponseBody(){
        return $this->responseBody;
    }
    
    /**
     * 
     * Gets the response headers from the request.
     * 
     * @return Collections
     */
    public function getResponseHeaders(){
        $headers = collect();
        
        if(!is_blank($this->responseHeaders)){
            foreach(explode("\n", $this->responseHeaders) as $header){
                if(!is_blank($header)){
                    $headers->add($header);
                }
            }
        }
        
        return $headers;
    }
}