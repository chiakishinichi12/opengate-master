<?php
namespace lib\util\nebula;

use lib\inner\Accessor;

/**
 * 
 * @author Jiya Sama
 *
 * @method static \lib\util\nebula\ViewRenderer setBindings(array|mixed $data)
 * @method static \lib\util\nebula\ViewRenderer setView(string $view)
 * @method static boolean exists(string $view)
 *
 */
class View extends Accessor {
    
    /**
     * 
     * @var array
     */
    protected $alias = [
        "set" => "setView"
    ];
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\inner\Accessor::warpedInstance()
     */
    public function warpedInstance(){
        return ViewRenderer::class;
    }
}