<?php
namespace lib\util;

class PlainEncryptor{
    
    /**
     * 
     * @param string|mixed $data
     * @param string $password
     * @param mixed $salt
     * 
     * @return string
     */
    public static function encryptData($data, $password, $salt) {
        // Generate a key from the password
        $key = hash_pbkdf2('sha256', $password, $salt, 1000, 32); 
        
        // Generate an initialization vector
        $iv = openssl_random_pseudo_bytes(16); 
        
        // Encrypt the data
        $encrypted = openssl_encrypt($data, 'AES-256-CBC', $key, 0, $iv); 
        
        // Return as base64-encoded string
        return base64_encode("{$iv}{$encrypted}"); 
    }
    
    /**
     * 
     * @param string|mixed $data
     * @param string $password
     * @param mixed $salt
     * 
     * @return string
     */
    public static function decryptData($data, $password, $salt) {
        // Generate the key from the password
        $key = hash_pbkdf2('sha256', $password, $salt, 1000, 32);
        
        // Decode base64-encoded data
        $data = base64_decode($data);
        
        // Extract the initialization vector
        $iv = substr($data, 0, 16); 
        
        // Remove the initialization vector
        $data = substr($data, 16); 
        
        // Decrypt the data
        $decrypted = openssl_decrypt($data, 'AES-256-CBC', $key, 0, $iv); 

        return $decrypted;
    }
}