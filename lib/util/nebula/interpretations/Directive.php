<?php
namespace lib\util\nebula\interpretations;

use lib\util\nebula\ViewMorphInterpreter;

interface Directive {
    
    /**
     * 
     * sets the morph file subject for interpretation
     * 
     * @param string|mixed $file
     * @param ViewMorphInterpreter $interpreter
     */
    function obtains($file, $interpreter);
    
    /**
     * 
     * provides parenthesis data if necessary for a directive
     * 
     * @param string $exp
     */
    function parenthesis($exp);
    
    /**
     * 
     * returns the interpretation of a directive
     * 
     */
    function interpretation();
}