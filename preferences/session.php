<?php
return [
    /**
     * 
     * sets the driver for the session that will be used on the request
     * 
     */
    "driver" => "file",
    
    /**
     * 
     * sets the lifetime for the session id. by default, it will last for 2 hours.
     * if null, session id will last for 30 days. (only applicable for file driver)
     * 
     */
    "session_id_lifetime" => null,
    
    /**
     * 
     * sets longetivity for each registered session
     * 
     */
    "duration" => -1,
    
    /**
     * 
     * this is where the session data are stored when session file driver is in use
     * 
     */
    "storage_path" => base_path("stockade/session"),
    
    /**
     * Controls whether the session cache should be automatically deleted when it expires.
     *
     * - If set to `true`, the session cache will be removed once it reaches its expiration time.
     * - If set to `false`, the session cache may persist even after it expires.
     */
    "delete_cache_when_expired" => true,
    
    /**
     * Indicates whether the cookie is set as HTTP Only, restricting access to JavaScript.
     * 
     * - If set to `true`, the cookie is not accessible via client-side JavaScript.
     * - If set to `false`, the cookie can be accessed and manipulated by JavaScript.
     * 
     */
    "http_only" => true,
    
    /**
     * 
     * Specifies the SameSite attribute for the cookie to control cross-site requests.
     * 
     * - Set to "Lax" to allow the cookie in safe cross-site top-level navigations.
     * - Set to "Strict" to only allow the cookie in same-site requests.
     * - Leave empty or set to "None" for no SameSite attribute.
     * 
     */
    "same_site" => "lax"
];