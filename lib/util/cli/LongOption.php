<?php
namespace lib\util\cli;


class LongOption{
    
    /**
     * 
     * @var string
     */
    protected $name;
    
    /**
     * 
     * @var string
     */
    protected $value;
    
    /**
     * 
     * @param string $name
     * @param string $value
     */
    public function __construct(string $name, string $value = null){
        $this->name = $name;
        $this->value = $value;
    }
    
    /**
     * 
     * @return string
     */
    public function name(){
        return $this->{__FUNCTION__};
    }
    
    /**
     * 
     * @return string|null
     */
    public function value(){
        return $this->{__FUNCTION__};
    }
    
    /**
     * 
     * @return string
     */
    public function __toString(){
        $value = "";
        
        if(!is_blank($this->value)){
            $value = "=\"{$this->value}\"";
        }
        
        return "{$this->name}{$value}";
    }
}

