<?php
namespace app\providers;

use lib\inner\arrangements\ServiceProvider;
use lib\inner\ApplicationGate;
use lib\util\route\Route;
use lib\util\cli\Console;
use lib\inner\App;
use lib\util\route\RouteContainer;
use lib\util\route\RouteBean;

/**
 * 
 * registers a route and command for phpinfo() and framework info.<br/>
 * IMPORTANT NOTE: live-mode must be disabled 
 * 
 * this is a not-so-special service provider
 * you can remove or modify this whenever you want to
 * 
 * @author Antonius Aurelius
 *
 */
class DetailServiceProvider extends ServiceProvider {
    
    /**
     * 
     * @var boolean
     */
    protected $forAll = true;
    
    /**
     * 
     * @param ApplicationGate $gate
     */
    public function boot(ApplicationGate $gate){
        $liveMode = config("live_mode");
                
        // if live-mode is enabled, this cannot be utilized for debugging
        if($liveMode === true){
            return;
        }
        
        if(App::checkCLI()){
            Console::command("ogphpinfo", $this->closurephpinfo())->details([
                "name" => "ogphpinfo",
                "desc" => "Echoes PHP Info"
            ]);
        }else{
            Route::get("/ogphpinfo", $this->closurephpinfo());
            Route::get("/oginfo", $this->frameworkinfo());
        }
    }
    
    /**
     * 
     * @return callable
     */
    protected function frameworkinfo(){
        return function(){
            out()->printbr("<pre>OpenGate (v.".OPENGATE_VERSION.") <br/>");
            
            $routes = App::make(RouteContainer::class)->getRoutes();
            
            out()->printbr("<strong>Existing Routes ({$routes->length()}):</strong>");
            
            out()->printbr("<ol>");
            
            $routes->each(function(RouteBean $routeBean){
                out()->print("<li>");
                out()->print("[{$this->revealQuery($routeBean->query())}]<br/> ".print_r($routeBean->uri(), true));
                out()->printbr("</li>");
            });
                
            out()->printbr("</ol>");
        };
    }
    
    /**
     * 
     * @return callable
     */
    protected function closurephpinfo(){
        return function(){
            phpinfo();
        };
    }
    
    /**
     * 
     * @param mixed $query
     * @return mixed|string
     */
    protected function revealQuery(mixed $query){
        $type = class_name($query);
        
        switch($type){
            case "string":
                return $query;
            default:
                return $type;
        }
    }
}

