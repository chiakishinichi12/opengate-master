<?php

namespace lib\util\paginators;

use lib\traits\Sanitation;
use lib\util\Paginator;
use lib\util\builders\SelectBuilder;
use lib\util\datagate\SQLBuilderHelper;
use lib\util\collections\ModelCollections;

class QueryPaginator extends Paginator{
    
    use Sanitation, SQLBuilderHelper;
        
    /**
     * 
     * @var SelectBuilder
     */
    protected $builder;
       
    /**
     *
     * @var int
     */
    protected $currentPage = 0;
    
    /**
     * 
     * @param SelectBuilder $builder
     * @return \lib\util\paginators\QueryPaginator
     */
    public function builder(SelectBuilder $builder){
        $this->builder = $builder;
        
        return $this;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\Paginator::fetchPageItems()
     */
    public function fetchPageItems(int $page){      
        $this->itemCount($this->builder->count());
        
        $this->calculatePageCount();
        
        if(!is_blank($this->scannedPageNumber())){
            $page = $this->scannedPageNumber();
        }
        
        // if the page is zero/negative or page exceeds the last page num
        if($page <= 0){
            $page = 1;
        }else if($page > $this->getPageCount()){
            $page = $this->getPageCount();
        }
        
        $rowsPerPage = $this->rowsPerPage();
        
        $this->items = collect();
        
        if($this->getPageCount() !== 0){
            $start = ($page - 1) * $rowsPerPage;
            
            $this->currentPage = $page;
            
            $this->items->addAll($this->builder->limit($start, $rowsPerPage)->get());
        }
        
        if($this->builder->hasModelClass()){
            $this->items = ModelCollections::storeStack($this->items->toArray());
        }
                
        return $this;
    }
}