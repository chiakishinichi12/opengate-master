<?php

use lib\util\nebula\interpretations\Directive;
use lib\util\nebula\interpretations\DirectiveThrowers;
use lib\util\nebula\interpretations\Keyword;

return new class implements Directive, Keyword {
    
    use DirectiveThrowers;
    
    /**
     *
     * @var string
     */
    protected $name;
    
    /**
     *
     * @var string
     */
    protected $exp;
    
    public function __construct(){
        $base = basename(__FILE__);
        
        $this->name = substr($base, 0, strrpos($base, ".php"));
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\nebula\interpretations\Directive::obtains()
     */
    public function obtains($file, $interpreter){
        
        return $this;
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\nebula\interpretations\Directive::parenthesis()
     */
    public function parenthesis($exp){
        $this->exp = $exp;
        
        return $this;
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\nebula\interpretations\Directive::interpretation()
     */
    public function interpretation(){
        $this->requireParenthesis();
        
        $params = $this->parameters(1);
        
        $stack = $params->get(0);
        $selected = $params->get(1);
        
        $chosen = "";
        
        if(!is_blank($selected)){
            $chosen .= "if(isset({$selected}) && strcmp({$selected}, \$key) === 0){\n";
            $chosen .= "\$selected = \"selected\";\n";
            $chosen .= "}\n";
        }
        
        $phpcode =<<<SCRIPT
        <?php
            if(isset({$stack}) && is_array({$stack})){
                foreach({$stack} as \$key => \$value){
                    \$selected = "";
                    {$chosen}
                    echo "<option value=\\"{\$key}\\" {\$selected}>{\$value}</option>\n";
                }
            }
        ?>
SCRIPT;
        
        return $phpcode;
    }
    
};