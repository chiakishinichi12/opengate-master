<?php

namespace lib\util\migration;

use lib\traits\MigrationInspector;
use lib\traits\Sanitation;
use lib\util\exceptions\MigrationException;

/**
 * 
 * Migration Column Builder
 *
 */
class Column {
    
    use MigrationInspector, Sanitation;
    
    /**
     * 
     * mandatory
     * 
     * @var string
     */
    protected $name;
    
    /**
     *
     * mandatory
     * 
     * @var string
     */
    protected $type;
    
    /**
     * 
     * @var boolean
     */
    protected $primaryKey = false;
    
    /**
     * 
     * @var boolean
     */
    protected $unique = false;
    
    /**
     * 
     * @var string
     */
    protected $default;
    
    /**
     * 
     * @var boolean
     */
    protected $autoIncrement = false;
    
    /**
     * 
     * @var boolean
     */
    protected $notNull = false;
    
    /**
     * 
     * @var integer
     */
    protected $length = 10;
    
    /**
     * 
     * @var array
     */
    protected $doubleParams = null;
    
    /**
     * 
     * @var string
     */
    protected $after;
    
    /**
     * 
     * @var boolean
     */
    protected $first = false;
    
    /**
     * 
     * @var boolean
     */
    protected $modify = false;
    
    /**
     * 
     * @var string
     */
    protected $rename = "";
    
    /**
     *
     * @var boolean
     */
    protected $change = false;
    
    /**
     * can be a 'p' value for double if 'doubleParams' isn't defined
     * 
     * @var unknown
     */
    protected $determinator;
    
    /**
     * 
     * @var array
     */
    protected $enumerations = [];
    
    public function __construct($name){
        $this->name = $name;
    }
    
    /**
     * 
     * @param String $type
     * @return \lib\util\migration\Column
     */
    public function setType(string $type, int $length = 0){
        if(!$this->checkDataType($type)){
            throw new MigrationException("Unknown Column Data Type '{$type}'");
        }
        
        $this->type = $type;
        $this->length = $length;
        
        return $this;
    }
    
    /**
     * 
     * @param string $after
     * 
     * @return string
     */
    public function after(string $after = null){
        if(!is_null($after)){
            $this->after = $after;
            
            return $this;
        }
        
        return $this->after;
    }
    
    /**
     * 
     * @param string $rename
     * 
     * @return \lib\util\migration\Column
     */
    public function renameTo(string $rename = null){
        if(!is_null($rename)){
            $this->rename = $rename;
            
            return $this;
        }
        
        return $this->rename;
    }
    
    /**
     * 
     * @return boolean
     */
    public function isRenaming(){
        return !is_blank($this->rename);
    }
    
    /**
     *
     * @return \lib\util\migration\Column
     */
    public function change(){
        $this->change = true;
        
        return $this;
    }
    
    /**
     *
     * @return boolean
     */
    public function isChange(){
        return $this->change;
    }
    
    /**
     * 
     * @return \lib\util\migration\Column
     */
    public function modify(){
        $this->modify = true;
        
        return $this;
    }
    
    /**
     * 
     * @return boolean
     */
    public function isModify(){
        return $this->modify;
    }
    
    /**
     *
     *
     * @return \lib\util\migration\Column
     */
    public function first(){
        $this->first = true;
        
        return $this;
    }
    
    /**
     * 
     * @return boolean
     */
    public function isFirst(){
        return $this->first;   
    }
    
    /**
     * 
     * @return \lib\util\migration\Column
     */
    public function primaryKey(){
        $this->primaryKey = true;
        
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getName(){
        return $this->name;
    }
    
    /**
     * 
     * @return string
     */
    public function getType(){
        return $this->type;
    }
    
    /**
     *  
     * @return boolean
     */
    public function isPrimaryKey(){
        return $this->primaryKey;
    }
    
    /**
     * 
     * @return boolean
     */
    public function isUnique(){
        return $this->unique;
    }
    
    /**
     * 
     * @return boolean
     */
    public function isAutoIncrement(){
        return $this->autoIncrement;
    }
    
    /**
     * 
     * @return boolean
     */
    public function isNotNull(){
        return $this->notNull;
    }
    
    /**
     * 
     * @return \lib\util\migration\Column
     */
    public function unique(){
        $this->unique = true;
        
        return $this;
    }
    
    
    /**
     * 
     * @param mixed $default
     * @return \lib\util\migration\Column
     */
    public function default($default){
        $this->default = $default;
        
        return $this;
    }
    
    /**
     * 
     * @return \lib\util\migration\Column
     */
    public function autoIncrement(){
        $this->autoIncrement = true;
        
        return $this;
    }
    
    /**
     * 
     * @param boolean $notNull
     * @return \lib\util\migration\Column
     */
    public function notNull(){
        $this->notNull = true;
        
        return $this;
    }
    
    /**
     * 
     * @param int $length
     * @return \lib\util\migration\Column
     */
    public function setLength(int $length){
        $this->length = $length;
        
        return $this;
    }
    
    /**
     * 
     * @param int $size
     * @param int $decimals
     * @return \lib\util\migration\Column
     */
    public function setDoubleParams(int $size, int $decimals = 0){
        $this->doubleParams = [$size, $decimals];
        
        return $this;
    }
    
    /**
     * 
     * @param mixed $determinator
     * @return \lib\util\migration\Column
     */
    public function setFloatDeterminator($determinator){
        $this->determinator = $determinator;
        
        return $this;
    }
    
    /**
     * 
     * @param array $enumerations
     * @return \lib\util\migration\Column
     */
    public function values(Array $enumerations){
        // enclosing with double quotations if string is represented by each element
        for($index = 0; $index < count($enumerations); $index++){
            if(is_string($enumerations[$index]))
                $enumerations[$index] = sprintf("'%s'", $this->escape($enumerations[$index]));
        }
        
        $this->enumerations = $enumerations;
        
        return $this;
    }
    
    private function finalizeTypeString(){
        $finalType = "";
        
        switch(strtolower($this->type)){
            case "bit":
            case "int":
            case "tinyint":
            case "mediumint":
            case "smallint":
            case "bigint":
            case "integer":
            case "char":
            case "varchar":
            case "binary":
            case "varbinary":
            case "text":
                $finalLength = $this->length > 0 ? "({$this->length})" : ""; 
                $finalType = "{$this->type}{$finalLength}";
                break;
            case "double":
            case "double precision":
            case "decimal":
                if($this->doubleParams !== null){
                    $params = implode(", ", $this->doubleParams);
                }else
                    $params = $this->length;
                    
                $finalType = "{$this->type}({$params})";
                break;
            case "float":
                $finalType = "{$this->type}({$this->determinator})";
                break;
            case "enum":
            case "set":
                $finalType = "{$this->type}(".implode(", ", $this->enumerations).")";
                break;
            default:
                $finalType = $this->type;
                break;
        }
        
        return $finalType;
    }
    
    /**
     * 
     * returns 'true' if the string is a MySQL function
     * 
     * @param string $str
     * 
     * @return boolean
     */
    protected function isMySQLFunction($str) {
        // Define an array of commonly used MySQL functions
        $mysqlFunctions = array(
            'ABS', 'AVG', 'COUNT', 'MAX', 'MIN', 'SUM', 'UCASE', 'LCASE',
            'NOW', 'CURDATE', 'CURTIME', 'DATE_ADD', 'DATE_SUB', 'DATEDIFF',
            'DAYNAME', 'MONTHNAME', 'DATEDIFF', 'YEAR', 'MONTH', 'DAY', 'HOUR',
            'MINUTE', 'SECOND', 'CONCAT', 'CONCAT_WS', 'IFNULL', 'LEAST', 'GREATEST'
            // Add more functions as needed
        );
        
        // Check if the string starts with any of the MySQL function names followed by opening parenthesis
        foreach($mysqlFunctions as $func) {
            if(strpos($str, $func . '(') === 0){
                return true;
            }
        }
        
        return false;
    }
    
    
    public function build(){
        $dataType = $this->finalizeTypeString();
        
        if($this->isRenaming()){
            $this->rename = "{$this->rename} ";
        }
        
        $column = "`{$this->name}` {$this->rename}{$dataType}"; 
        
        if($this->isUnique()){
            $column .= " UNIQUE";
        }
        
        if($this->isAutoIncrement()){
            $column .= " AUTO_INCREMENT";
        }
        
        if($this->isNotNull()){
            $column .= " NOT NULL";
        }else{
            if(is_null($this->default) && (!$this->isPrimaryKey() && !$this->isUnique())){
                $column .= " NULL";
            }
        }
        
        if(!is_null($this->default)){
            $defval = $this->escape($this->default);
            
            if(!$this->isMySQLFunction($defval)){
                $defval = "'{$defval}'";
            }
            
            $column .= " DEFAULT {$defval}";
        }
        
        return $column;
    }
    
    public function __toString(){
        return $this->build();
    }
}