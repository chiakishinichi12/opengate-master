<?php
namespace lib\util\migration\warper;

use Closure;
use Exception;
use lib\inner\ApplicationGate;
use lib\util\Collections;
use lib\util\GateFinder;
use lib\util\cli\WarpArgv;
use lib\util\exceptions\ClassNotFoundException;
use lib\util\exceptions\MigrationException;
use lib\util\file\FileUtil;
use lib\util\migration\Migration;
use lib\inner\App;

class MigrationRefresh {
    
    use MigrationClassValidation;
    
    /**
     *
     * @var Collections
     */
    protected $args;
    
    /**
     *
     * @var ApplicationGate
     */
    protected $gate;
    
    /**
     * 
     * @var Collections
     */
    protected $migrations;
    
    public function __construct(ApplicationGate $gate, WarpArgv $argv){
        $this->gate = $gate;
        
        $this->migrations = collect();
        
        $this->args = $argv->obtainArguments();
        
        $this->inputClass = $argv->findLongOption("--class");
        
        $this->validateAndStore();
    }
    
    protected function validateAndStore(){
        $failedCallback = function(){
            die("No migration class found.\n");
        };
        
        $migpath = preferences("structure")->get("migrations");
        
        if(!FileUtil::exists($migpath)){
            $this->gate->call($failedCallback);
        }
        
        $inputtedClassname = $this->inputClass?->value();    
            
        (new GateFinder(relative_path($migpath), true))->failedCallback($failedCallback)->load(function($class)
            use ($inputtedClassname){
                
                if(!App::checkInheritance($class, Migration::class)){
                    throw new MigrationException("Class {{$class}} is not an instance of ".Migration::class);
                }
                
                $migration = $this->gate->make($class);
                
                if(!method_exists($migration, "up")){
                    throw new MigrationException("Method up() is not defined in {$class}");
                }
                
                if(!method_exists($migration, "down")){
                    throw new MigrationException("Method down() is not defined in {$class}");
                }
               
                $this->filtering($migration, $class, $inputtedClassname);
        });
        
        if(!is_null($inputtedClassname) && !$this->migrations->count()){
            throw new ClassNotFoundException("Migration class '{$inputtedClassname}' doesn't exist");
        }
    }
    
    /**
     * 
     * this calls migration method with necessary exception handling
     * 
     * @param Migration $migration
     * @param string $method
     * @return mixed|Closure|boolean
     */
    protected function migrationMethod($migration, $method){
        try{
            $call = $this->gate->call([$migration, $method]);
            
            if(!is_bool($call)){
                goto nothing;
            }
            
            return $call;
        }catch(Exception $ex){
            out()->println("<warning>Migration Exception</warning>: <error>{$ex->getMessage()}</error>");
        }
        
        nothing:
        return false;
    }
    
    public function refresh(){
        $this->migrations->each(function($migration){
            $classbase = basename(str_replace("\\", "/", class_name($migration)));
            
            // reversing the migration first
            $this->migrationMethod($migration, "down");
            
            // then putting back what has to be migrated as a new
            $this->migrationMethod($migration, "up");
            
            out()->println("Migration Refreshed [{$classbase}]");
        });
    }
}