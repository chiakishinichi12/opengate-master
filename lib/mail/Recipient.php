<?php
namespace lib\mail;

class Recipient {
    
    /**
     * 
     * @var string
     */
    protected $email;
    
    /**
     * 
     * @var string
     */
    protected $name;
    
    /**
     * 
     * @param string $email
     * @param string $name
     */
    public function __construct(string $email, string $name = ""){
        $this->email = $email;
        $this->name = $name;
    }
    
    /**
     * 
     * @return string
     */
    public function email(){
        return $this->email;
    }
    
    /**
     * 
     * @return string
     */
    public function name(){
        return $this->name;
    }
}