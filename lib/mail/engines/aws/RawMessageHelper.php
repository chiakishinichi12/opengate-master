<?php
namespace lib\mail\engines\aws;

use lib\mail\Attachment;
use lib\mail\Recipient;
use lib\util\file\File;

class RawMessageHelper {
    
    /**
     * 
     * @var string
     */
    protected $altName = "";
    
    /**
     * 
     * @var string
     */
    protected $senderMail;
    
    /**
     * 
     * @var array
     */
    protected $recipients;
    
    /**
     * 
     * @var string
     */
    protected $subject;
    
    /**
     * 
     * @var string
     */
    protected $charset;
    
    /**
     * 
     * @var string
     */
    protected $content;
    
    /**
     * 
     * @var boolean
     */
    protected $isHtml = true;
       
    /**
     * 
     * @var array
     */
    protected $attachments = [];
    
    /**
     * 
     * @param string $senderMail
     * @param array $recipients
     */
    public function __construct($senderMail, $recipients){
        $this->senderMail = $senderMail;
        $this->recipients = $recipients;
    }
    
    /**
     * 
     * @param string $altName
     * @return string|\lib\mail\engines\aws\RawMessageHelper
     */
    public function altName($altName = ""){
        if(is_blank($altName)){
            return $this->altName;
        }
        
        $this->altName = $altName;
        
        return $this;
    }
    
    /**
     * 
     * @param string $subject
     * @return string|\lib\mail\engines\aws\RawMessageHelper
     */
    public function subject($subject = ""){
        if(is_blank($subject)){
            return $this->$subject;
        }
        
        $this->subject = $subject;
        
        return $this;
    }
    
    /**
     * 
     * @param string $charset
     * @return string|\lib\mail\engines\aws\RawMessageHelper
     */
    public function charset($charset = ""){
        if(is_blank($charset)){
            return $this->charset;
        }
        
        $this->charset = $charset;
        
        return $this;
    }
    
    /**
     * 
     * @param string $content
     * @return string|\lib\mail\engines\aws\RawMessageHelper
     */
    public function content($content = ""){
        if(is_blank($content)){
            return $this->$content;
        }
        
        $this->content = $content;
        
        return $this;
    }
    
    /**
     * 
     * @param boolean $isHtml
     * @return boolean|\lib\mail\engines\aws\RawMessageHelper
     */
    public function isHtml($isHtml = null){
        if(is_blank($isHtml)){
            return $this->isHtml;
        }
        
        $this->isHtml = $isHtml;
        
        return $this;
    }
    
    /**
     * 
     * @param array $attachments
     * @return array|mixed|\lib\mail\engines\aws\RawMessageHelper
     */
    public function attachments($attachments = []){
        if(is_blank($attachments)){
            return $this->attachments;
        }
        
        $this->attachments = $attachments;
        
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function build(){
        $senderformat = $this->senderMail;
        
        if(!is_blank($this->altName)){
            $senderformat = "{$this->altName} <{$this->senderMail}>";
        }
        
        $recipients = [];
        
        collect($this->recipients)->each(function($recipient) use (&$recipients){
            if($recipient instanceof Recipient){
                $recipients[] = $recipient->email();
            }
        });
        
        $recipients = implode(",", $recipients);
        
        $builder = "From: {$senderformat}\r\n";
        $builder .= "To: {$recipients}\r\n";
        $builder .= "Subject: {$this->subject}\r\n";
        $builder .= "MIME-Version: 1.0\r\n";
        $builder .= "Content-Type: multipart/mixed; boundary=\"boundary\"\r\n";
        $builder .= "\r\n";
        $builder .= "--boundary\r\n";
        
        $bodyType = $this->isHtml ? "text/html" : "text/plain";
        $docuEnco = $this->isHtml ? "quoted-printable" : "7bit";
        
        $builder .= "Content-Type: {$bodyType}; charset={$this->charset}\r\n";
        $builder .= "Content-Transfer-Encoding: {$docuEnco}\r\n";
        $builder .= "\r\n";
        $builder .= "{$this->content}\r\n";
        $builder .= "--boundary\r\n";
        
        collect($this->attachments)->each(function(Attachment $attachment) use (&$builder){
            $attachFile = new File($attachment->path());
            
            if(!$attachFile->exists()){
                return;
            }
            
            if(is_blank($as = $attachment->as())){
                $as = $attachFile->getBasename();
            }
            
            $as = " ; name=\"{$as}\"";
            
            $builder .= "Content-Type: {$attachFile->getMimeType()}{$as}\r\n";
            $builder .= "Content-Disposition: attachment{$as}\r\n";
            $builder .= "Content-Transfer-Encoding: base64\r\n\r\n";
            $builder .= base64_encode($attachFile->getContents())."\r\n";
            $builder .= "--boundary\r\n";
        });
        
        return $builder;
    }
}