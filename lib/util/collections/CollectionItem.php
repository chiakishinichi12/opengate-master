<?php
namespace lib\util\collections;

use ArrayAccess;
use ArrayIterator;
use Countable;
use InvalidArgumentException;
use IteratorAggregate;
use OutOfBoundsException;
use lib\exceptions\OpenGateException;
use lib\util\Collections;
use lib\util\exceptions\CollectionTypeException;
use lib\util\exceptions\MethodNotFoundException;

class CollectionItem implements ArrayAccess, Countable, IteratorAggregate{
    
    /**
     * 
     * @var mixed
     */
    protected $key;
    
    /**
     * 
     * @var mixed
     */
    protected $value;
    
    /**
     * 
     * @var Collections
     */
    protected $list;
    
    /**
     * 
     * @param mixed $value
     * @param mixed $key
     * @param Collections $list
     */
    public function __construct($value, $key, Collections &$list){
        $this->value = $value;
        $this->list = $list;
        $this->key = $key;
    }
    
    /**
     * 
     * @param string $method
     * @throws OpenGateException
     */
    protected function throwExceptionIfNotArray($method){
        if(!(is_array($this->value) || ($this->value instanceof Collections))){
            $message = "{$method}() issue: item is not an array or instance of Collections";
            
            throw new CollectionTypeException($message);
        }
    }
    
    protected function throwExceptionIfNotObject(){
        if(!is_object($this->value)){
            throw new CollectionTypeException("item is not an object: {$this->type()}");
        }
    }
    
    /**
     *
     * @param string $what
     * @param string $lookingFor
     * @throws OpenGateException
     */
    protected function throwNotFoundException($what, $lookingFor){
        throw new OpenGateException("{$what} Not Found: {$lookingFor}");
    }
    
    /**
     * 
     * @param mixed $newValue
     * @return \lib\util\collections\CollectionItem
     */
    public function update($newValue){
        $this->list[$this->key] = $newValue;
        
        $this->value = $newValue;
        
        return $this;
    }
    
    /**
     * simply swaps the item to other existing key
     * 
     * @param mixed $key
     * @throws OutOfBoundsException
     * @throws InvalidArgumentException
     * 
     * @return CollectionItem
     */
    public function swap($withKey){
        if(!$this->list->hasKey($withKey)){
            throw new OutOfBoundsException("Undefined offset '{$withKey}'");
        }
        
        if($withKey === $this->key){
            throw new InvalidArgumentException("Swapping cannot be done: same key found ({$withKey}).");
        }
        
        // securing swapped value
        $newValue = $this->list->get($withKey);
        
        /*
         * 
         * modifying list
         * 
         */
        $this->list->put($withKey, $this->value());
        $this->list->put($this->key, $newValue);
        
        // assigning a new value to the collection item object
        $this->value = $newValue;
        
        return $this;
    }
    
    /**
     * 
     * @return mixed
     */
    public function value(){
        return $this->value;
    }
    
    /**
     * 
     * @return mixed
     */
    public function key(){
        return $this->key;
    }
    
    /**
     * 
     * @return string
     */
    public function type(){
        return class_name($this->value);
    }
    
    /**
     * 
     * @param string $name
     * @return mixed
     */
    public function __get($name){
        if(is_object($this->value)){
            if(!property_exists($this->value, $name)){
                $this->throwNotFoundException("Property", sprintf("%s->%s",
                    $this->type(),
                    $name));
            }
            
            return $this->value->$name;
        }
        
        $this->throwExceptionIfNotObject();
    }
    
    /**
     * 
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value){
        if(is_object($this->value)){
            if(!property_exists($this->value, $name)){
                $this->throwNotFoundException("Property", sprintf("%s->%s",
                    $this->type(),
                    $name));
            }
            
            $this->value->$name = $value;
        }else{
            $this->throwExceptionIfNotObject();
        }
    }
    
    /**
     * 
     * @param string $method
     * @param array $args
     * @throws MethodNotFoundException
     * @return mixed
     */
    public function __call($method, $args){
        if(is_object($this->value)){
            if(!method_exists($this->value, $method)){
                throw new MethodNotFoundException("{$this->type()}: {$method}() not found");
            }
            
            return call_user_func([$this->value, $method], ...$args);
        }else{
            $this->throwExceptionIfNotObject();
        }
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see ArrayAccess::offsetGet()
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset){
        $this->throwExceptionIfNotArray(__METHOD__);
        
        return $this->value[$offset];
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see ArrayAccess::offsetSet()
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value){
        $this->throwExceptionIfNotArray(__METHOD__);
        
        $this->value[$offset] = $value;
    }
        
    /**
     * 
     * {@inheritDoc}
     * @see ArrayAccess::offsetUnset()
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset){
        $this->throwExceptionIfNotArray(__METHOD__);
        
        unset($this->value[$offset]);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see ArrayAccess::offsetExists()
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset){
        $this->throwExceptionIfNotArray(__METHOD__);
        
        return isset($this->value[$offset]);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see Countable::count()
     */
    #[\ReturnTypeWillChange]
    public function count(){
        $this->throwExceptionIfNotArray(__METHOD__);
        
        return count($this->value);
    }
    
    /**
     *
     * {@inheritDoc}
     * @see IteratorAggregate::getIterator()
     */
    #[\ReturnTypeWillChange]
    public function getIterator() {
        $this->throwExceptionIfNotArray(__METHOD__);
        
        return new ArrayIterator($this->value);
    }
    
    /**
     * 
     * @return string
     */
    public function __toString(){
        if(!is_string($this->value)){
            return print_r($this->value, true);
        }
        
        return $this->value;
    }
}