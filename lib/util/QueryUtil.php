<?php

namespace lib\util;

interface QueryUtil {
    
    /**
     * 
     * the standard database record fetcher
     * 
     * @param String $sql
     * @param array $args
     */
    function executeQuery(string $sql, array $args = []);
    
    /**
     * 
     * updates database record(s)
     * 
     * @param String $sql
     * @param array $args
     */
    function executeUpdate(string $sql, array $args);
}