<?php
namespace lib\util\nebula;

use lib\util\Collections;
use lib\util\file\FileUtil;

/**
 * 
 * @author Jiya Sama
 *
 */
class ViewMorphMarker {
    
    /**
     * 
     * @var string
     */
    protected $marker;
    
    /**
     * 
     * @var Collections
     */
    protected $morphs;
    
    /**
     * Constructor
     * 
     * Initializes the marker file path and loads existing morphs from the file.
     */
    public function __construct(){
        // Retrieve the file path from application preferences
        $this->marker = preferences("setup")->get("view_morph_marker");
        
        // Load serialized data from the marker file if it exists
        if(!is_null($ser = FileUtil::getContents($this->marker))){
            $this->morphs = unserialize($ser);
        }else{
            $this->morphs = collect(); // Initialize as an empty collection if file is absent
        }
    }
    
    /**
     * 
     * Get the collection of morphs.
     * 
     * @return \lib\util\nebula\Collections
     */
    public function morphs(){
        return $this->morphs;
    }
    
    /**
     * 
     * Save the current morphs collection to the marker file.
     * 
     */
    public function push(){
        // Serialize the morphs collection and write it to the marker file
        FileUtil::write($this->marker, serialize($this->morphs), "w+");
    }
}

