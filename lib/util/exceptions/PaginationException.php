<?php

namespace lib\util\exceptions;

use lib\exceptions\OpenGateException;

class PaginationException extends OpenGateException{
    
}