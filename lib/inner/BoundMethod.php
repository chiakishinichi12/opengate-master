<?php
namespace lib\inner;

use Closure;
use InvalidArgumentException;
use ReflectionFunction;
use ReflectionMethod;
use lib\util\Tool;
use lib\util\exceptions\BindingResolutionException;

/**
 * 
 * @author Taylor Otwell
 *
 */
class BoundMethod {
    
    /**
     * Call the given Closure / class@method and inject its dependencies
     * 
     * @param Container $container
     * @param callable|string $callback
     * @param array $parameters
     * @param string|null $defaultMethod
     * @return mixed|\Closure
     */
    public static function call($container, $callback, $parameters = [], $defaultMethod = null){
        if(is_string($callback) && !$defaultMethod && method_exists($callback, "__invoke")){
            $defaultMethod = "__invoke";
        }
        
        if(self::isCallableWithAtSign($callback) || $defaultMethod){
            return self::callClass($container, $callback, $parameters, $defaultMethod);
        }
        
        return self::callBoundMethod($container, $callback, function () use ($container, $callback, $parameters){
            return $callback(...array_values(self::getMethodDependencies($container, $callback, $parameters)));
        });
    }
    
    /**
     * Call a string reference to a class using class@method syntax
     * 
     * @param Container $container
     * @param string $target
     * @param array $parameters
     * @param string|null $defaultMethod
     * @throws InvalidArgumentException
     * @return mixed|Closure
     */
    protected static function callClass($container, $target, $parameters = [], $defaultMethod = null){
        $segments = explode("@", $target);
        
        $method = count($segments) === 2 ? $segments[1] : $defaultMethod;
        
        if(is_null($method)){
            throw new InvalidArgumentException("Method not provided");
        }
        
        return self::call($container, [$container->make($segments[0], $method)], $parameters);
    }
    
    /**
     * Call a method that has been vound to the container
     * 
     * @param Container $container
     * @param callable $callback
     * @param mixed $default
     * @return mixed|\Closure|unknown
     */
    protected static function callBoundMethod($container, $callback, $default){
        if(!is_array($callback)){
            return Tool::unwrapIfClosure($default);
        }
        
        $method = self::normalizeMethod($callback);
        
        if($container->hasMethodBinding($method)){
            return $container->callMethodBinding($method, $callback[0]);
        }
        
        return Tool::unwrapIfClosure($default);
    }
    
    /**
     * Normalize the given callback into Class@method syntax
     * 
     * @param callable $callback
     * @return string
     */
    protected static function normalizeMethod($callback){
        $class = is_string($callback[0]) ? $callback[0] : get_class($callback[0]);
        
        return "{$class}@{$callback[1]}";
    }
    
    /**
     * Get all dependencies for a given method
     * 
     * @param Container $container
     * @param callable $callback
     * @param array $parameters
     * @return array
     */
    protected static function getMethodDependencies($container, $callback, $parameters = []){
        $dependencies = [];
        
        foreach(self::getCallReflector($callback)->getParameters() as $parameter){
            self::addDependencyForCallParameter($container, $parameter, $parameters, $dependencies);
        }
        
        return array_merge($dependencies, array_values($parameters));
    }
    
    /**
     * Get the proper reflection instance for the given callback
     * 
     * @param mixed $callback
     * @return \lib\inner\ReflectionMethod|\lib\inner\ReflectionFunction
     */
    protected static function getCallReflector($callback){
        if(is_string($callback) && strpos($callback, "::") !== false){
            $callback = explode("::", $callback);
        }else if(is_object($callback) && !($callback instanceof Closure)){
            $callback = [$callback, "__invoke"];
        }
        
        return is_array($callback) 
            ? new ReflectionMethod($callback[0], $callback[1]) 
            : new ReflectionFunction($callback);
    }
    
    /**
     * 
     * @param Container $container
     * @param \ReflectionParameter $parameter
     * @param array $parameters
     * @param array $dependencies
     * @throws BindingResolutionException
     */
    protected static function addDependencyForCallParameter($container, $parameter, &$parameters, &$dependencies){
        if(array_key_exists($paramName = $parameter->getName(), $parameters)){
            $dependencies[] = $parameters[$paramName];
            
            unset($parameters[$paramName]);
        }else if(!is_null($className = Tool::getParameterClassName($parameter))){
            if(array_key_exists($className, $parameters)){
                $dependencies[] = $parameters[$className];
                
                unset($parameters[$className]);
            }else {
                if($parameter->isVariadic()){
                    $variadicDependencies = $container->make($className);
                    
                    $dependencies = array_merge($dependencies, is_array($variadicDependencies) 
                        ? $variadicDependencies 
                        : [$variadicDependencies]);
                }else{
                    $dependencies[] = $container->make($className);
                }
            }
        }else if($parameter->isDefaultValueAvailable()){
            $dependencies[] = $parameter->getDefaultValue();
        }else if(!$parameter->isOptional() && !array_key_exists($paramName, $parameters)){
            throw new BindingResolutionException("Unable to resolve dependency [{$parameter}] in class "
                ."{$parameter->getDeclaringClass()->getName()}");
        }
    }
    
    /**
     * Determine if the given string is in Class@method syntax
     * 
     * @param mixed $callback
     * @return boolean
     */
    protected static function isCallableWithAtSign($callback){
        return is_string($callback) && strpos($callback, "@") !== false;
    }
}