<nav>
    <ul class="uk-pagination uk-flex-center"">
        @if($currentPage > 1)
            <li><a href="{{ $baseUrl }}{{ $pagePhrase }}={{ $currentPage - 1 }}"><span uk-pagination-previous></span></a></li>
        @endif

        @if($start > 1)
            <li class="uk-disabled">..</li>
        @endif

        @for($i = $start; $i <= $end; $i++)
            @if($i == $currentPage)
                <li class="uk-active"><a href="#"><span aria-current="page">{{ $i }}</span></a></li>
            @else
                <li><a href="{{ $baseUrl }}{{ $pagePhrase }}={{ $i }}">{{ $i }}</a></li>
            @endif
        @endfor

        @if($end < $pageCount)
            <li class="uk-disabled">..</li>
        @endif

        @if($currentPage < $pageCount)
            <li><a href="{{ $baseUrl }}{{ $pagePhrase }}={{ $currentPage + 1 }}"><span uk-pagination-next></span></a></li>
        @endif
    </ul>
</nav>