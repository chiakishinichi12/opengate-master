<?php
namespace lib\util\exceptions;

use ErrorException;

class OutOfMemoryError extends ErrorException {
    
    public function __construct($error){        
        parent::__construct($error["message"], 1, $error["type"], $error["file"], $error["line"]);
    }
}