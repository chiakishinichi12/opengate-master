<?php

namespace lib\util;

interface SQLInstance {
    /**
     * 
     * return SQLInstance
     * 
     */
    function getConnection();
    
    /**
     * 
     * void method
     * 
     */
    function closeConnection();
}