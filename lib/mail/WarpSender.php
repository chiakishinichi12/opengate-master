<?php
namespace lib\mail;

class WarpSender{
    
    /**
     * 
     * @var Dispatchable
     */
    protected $dispatchable;
    
    /**
     * 
     * @var array
     */
    protected $recipients = [];
    
    /**
     * 
     * @var Engine
     */
    protected $engine;
    
    /**
     * 
     * @param Engine $engine
     */
    public function __construct(Engine $engine){
        $this->engine = $engine;
    }
    
    /**
     * 
     * @param Dispatchable $dispatchable
     * 
     * @return \lib\mail\WarpSender
     */
    public function dispatchable($dispatchable){
        $this->dispatchable = $dispatchable;
        
        return $this;
    }
    
    /**
     * 
     * @param array $attachments
     * 
     * @return \lib\mail\WarpSender
     */
    public function attachments($attachments){
        $this->engine->attachments($attachments);
        
        return $this;
    }
    
    /**
     * optional. subject can be defined on the dispatchable class
     * 
     * @param string $subject
     * 
     * @return \lib\mail\WarpSender
     */
    public function subject($subject){
        $this->engine->subject($subject);
        
        return $this;
    }
    
    /**
     * 
     * @param string $mail
     * @param string $name
     * 
     * @return \lib\mail\WarpSender
     */
    public function recipient($mail, $name = ""){
        $this->recipients[] = new Recipient($mail, $name);
        
        return $this;
    }
    
    /**
     * 
     * @param Recipient[] $recipients
     * 
     * @return \lib\mail\WarpSender
     */
    public function recipients($recipients){
        $this->recipients = array_merge($this->recipients, $recipients);
        
        return $this;
    }
    
    /**
     * 
     * @return boolean
     */
    public function send(){        
        return $this->engine
        ->recipients($this->recipients)
        ->triggerSend($this->dispatchable);
    }
}