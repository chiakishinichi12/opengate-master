<?php
namespace lib\inner\arrangements\http\session\file;

use lib\exceptions\OpenGateException;
use lib\inner\ApplicationGate;
use lib\inner\arrangements\http\ApiResponseThrower;
use lib\inner\arrangements\http\session\SessionLifetime;
use lib\util\Collections;
use lib\util\GateFinder;
use lib\util\accords\SessionUtil;
use lib\util\builders\SessionValue;
use lib\util\exceptions\SessionException;
use lib\util\file\File;
use lib\util\file\FileUtil;

/**
 *
 * A class that implements the SessionUtil interface for file-based session management.
 *
 * This class provides methods and functionality for managing sessions using files as the storage medium.
 * It implements the SessionUtil interface to ensure compatibility with session handling in the application.
 * 
 * @author Jiya Sama
 * 
 */
class FileSessionUtil implements SessionUtil{
    
    use SessionLifetime, ApiResponseThrower;
    
    /**
     * 
     * @var ApplicationGate
     */
    protected $gate;
    
    /**
     * 
     * @var string
     */
    protected $sessionId;
    
    /**
     * 
     * @var SessionDataContainer
     */
    protected $container;
    
    /**
     * 
     * @var string
     */
    protected $path;
    
    /**
     * 
     * @var Collections
     */
    protected $preferences;
    
    /**
     * 
     * @param ApplicationGate $gate
     */
    public function __construct(ApplicationGate $gate){
        $this->gate = $gate;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\accords\SessionUtil::initSessionLibrary()
     */
    public function initSessionLibrary(){
        $this->preferences = preferences("session");
        
        $this->path = $this->preferences->get("storage_path");
        
        if(!is_null($cookie = SessionCookie::obtain())){
            $this->sessionId = $cookie->sessionId;
            
            $data = FileUtil::getContents("{$this->path}/{$cookie->sessionId}");
            
            $this->container = !is_null($data) 
                ? unserialize($data) 
                : new SessionDataContainer();
                            
            if(is_null($data)){
                $this->container->expiration = $cookie->toExpire;      
            }
        }else{
            $this->container = new SessionDataContainer();
        }
        
        $this->evalSessionExpiration();
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\accords\SessionUtil::modify()
     */
    public function modify($name, $value){
        $this->throwIfOnApiResponse(__FUNCTION__);
        
        $this->monitorSession($name);
        
        $this->container->sessions->put($name, $value);
        
        $urls = $this->container->urls->get($name);
        $urls["update"] = $this->getRequestURL();
        
        $this->container->urls->put($name, $urls);
        
        $this->save();
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\accords\SessionUtil::expires()
     */
    public function expires($duration){
        $this->duration = $duration;
    }

    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\accords\SessionUtil::set()
     */
    public function set($name, $value){
        $this->throwIfOnApiResponse(__FUNCTION__);
        
        if(!$this->active($name)){
            /**
             *
             * when there's expiration defined.
             *
             */
            $this->limitSession($name);
            
            $this->container->sessions->put($name, $value);
            
            $this->container->urls->put($name, [
                "stored" => $this->getRequestURL(),
                "update" => $this->getRequestURL()
            ]);
        
            $this->save();
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\accords\SessionUtil::get()
     */
    public function get($name){
        $this->monitorSession($name);
                
        return new SessionValue($name, $this->container->sessions->get($name));
    }

    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\accords\SessionUtil::active()
     */
    public function active($name){
        return $this->container->sessions->hasKey($name);
    }

    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\accords\SessionUtil::purgeAll()
     */
    public function purgeAll(){
        $this->throwIfOnApiResponse(__FUNCTION__);
        
        SessionCookie::destroy();
        
        $sessfile = new File("{$this->path}/{$this->sessionId}");
        
        if($sessfile->exists()){
            $sessfile->delete();
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\accords\SessionUtil::unset()
     */
    public function unset($name){
        $this->throwIfOnApiResponse(__FUNCTION__);
        
        $this->monitorSession($name);
                
        $this->container->sessions->remove($name);
        $this->container->urls->remove($name);
        
        $this->save();
    }
    
    /**
     *
     * feature that allows you to store temporary data in the session
     * that is available only for the next request.
     *
     * {@inheritDoc}
     * @see \lib\util\accords\SessionUtil::temporary()
     */
    public function temporary($name, $value = null){
        if(is_null($value)){
            if($this->active($this->tempsname)){
                $temps = $this->container->sessions->get($this->tempsname);
                                
                return collect($temps)->has($name);
            }
            
            return false;
        }
        
        $tempdata = [$name];
        
        if($this->active($this->tempsname)){
            $oldset = $this->get($this->tempsname)->value();
            
            if(collect($oldset)->has($name)){
                return;
            }
            
            $tempdata = array_merge($tempdata, $oldset);
            $this->modify($this->tempsname, $tempdata);
        }else{
            $this->set($this->tempsname, $tempdata);
        }
        
        if(!$this->active($name)){
            $this->set($name, $value);
        }
    }
    
    
    
    /**
     * Observe and manage the garbage collection of session data.
     *
     * This method checks the session cache for expired data and deletes it.
     * 
     * - It loads session data from the configured session cache directory.
     * - If the session data has expired (current time > expiration time), it is deleted.
     *
     * @return void
     */
    protected function observeGC(){        
        (new GateFinder())
        ->absolutePath($this->path)
        ->load(function(File $file){
            $container = unserialize($file->getContents());
                        
            if(time() > strtotime($container->expiration)){
                $file->delete();
            }
        });
    }
    
    /**
     * 
     * gets the full request URL
     * 
     * @return string
     */
    protected function getRequestURL(){
        return sprintf("%s%s%s",
            request()->getProtocolUsed(),
            request()->server("HTTP_HOST"),
            request()->server("REQUEST_URI")
        );
    }
    
    /**
     * 
     * saves session data to the framework-defined session cache directory
     * 
     */
    protected function save(){
        if($payload = SessionCookie::create()){
            $this->sessionId = $payload["sessionId"];
            $this->container->expiration = $payload["toExpire"];
        }
        
        FileUtil::write(
            "{$this->path}/{$this->sessionId}", 
            serialize($this->container), "w+");
    }
    
    /**
     *
     * @param string $name
     * @throws OpenGateException
     */
    protected function monitorSession($name){
        if(!$this->active($name)){
            throw new SessionException("Session '{$name}' is not defined");
        }
    }
    
    /**
     * 
     * Destructor method that triggers the garbage collection observation.
     * 
     */
    public function __destruct(){
        $this->rmvtemp();
        
        $isDeletionEnabled = $this->preferences->get("delete_cache_when_expired");
        
        if($isDeletionEnabled){
            $this->observeGC();
        }
    }
}