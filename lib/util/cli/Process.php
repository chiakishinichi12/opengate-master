<?php
namespace lib\util\cli;

use Closure;

class Process {
    
    /**
     * 
     * @var string
     */
    protected $command;
    
    /**
     * 
     * @var Closure
     */
    protected $successCallback;
    
    /**
     *
     * @var Closure
     */
    protected $cliErrorCallback;
    
    /**
     * 
     * @var int
     */
    protected $exitCode = null;
    
    /**
     * 
     * @var resource
     */
    protected $process;
    
    /**
     * 
     * @param string $command
     */
    public function __construct($command){
        $this->command = $command;
    }
    
    /**
     * 
     * @param \Closure $successCallback
     */
    public function setSuccessCallback($successCallback){
        $this->successCallback = $successCallback;
        
        return $this;
    }
    
    /**
     * 
     * @param \Closure $cliErrorCallback
     */
    public function setCliErrorCallback($cliErrorCallback){
        $this->cliErrorCallback = $cliErrorCallback;
        
        return $this;
    }
    
    /**
     * 
     * @return number
     */
    public function getExitCode(){
        return proc_close($this->process);
    }
    
    /**
     * 
     * executes the process
     * 
     */
    public function startProcess(){
        $descriptors = array(
            0 => array('pipe', 'r'), // stdin
            1 => array('pipe', 'w'), // stdout
            2 => array('pipe', 'w'), // stderr
        );
        
        $pipes = [];
        
        $this->process = proc_open($this->command, $descriptors, $pipes);
        
        if (is_resource($this->process)) {
            // Close stdin since we're not writing to it
            fclose($pipes[0]);
            
            $successLine = 1;
            $failureLine = 1;
            
            // Read and process the output line by line from stdout and stderr
            while (!feof($pipes[1]) || !feof($pipes[2])) {
                $stdoutLine = fgets($pipes[1]);
                if ($stdoutLine !== false) {
                    if(!is_null($this->successCallback) && $this->successCallback instanceof Closure){
                        call_user_func($this->successCallback, $stdoutLine, $successLine);
                    }
                    
                    $successLine++;
                }
                
                $stderrLine = fgets($pipes[2]);
                if ($stderrLine !== false) {
                    if(!is_null($this->cliErrorCallback) && $this->cliErrorCallback instanceof Closure){
                        call_user_func($this->cliErrorCallback, $stderrLine, $failureLine);
                    }
                    
                    $failureLine++;
                }
            }
            
            // Close the pipes and wait for the process to finish
            fclose($pipes[1]);
            fclose($pipes[2]);
        }
        
        return $this;
    }
}