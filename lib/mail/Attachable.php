<?php
namespace lib\mail;

interface Attachable {
    /**
     * 
     * sets the relative path of an attached file 
     * with an app base path.
     * 
     * @param string $path
     */
    function path(string $path = null);
    
    /**
     * 
     * overrides $path value without relying on the base_path() function.
     * 
     * @param string $disk
     */
    function disk(string $path);
    
    /**
     * sets a new name for an attached file.
     * 
     * @param string $as
     */
    function as(string $as = null);
    
    /**
     * 
     * sets a content type for an attached file.
     * 
     * @param string $mime
     */
    function mime(string $mime = null);
    
    /**
     * sets the encoding for an attached file.
     * 
     * @param string $encoding
     */
    function encoding(string $encoding = null);
}