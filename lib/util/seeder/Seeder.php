<?php
namespace lib\util\seeder;

use InvalidArgumentException;
use lib\inner\ApplicationGate;
use lib\util\exceptions\SeederException;

abstract class Seeder{
    
    /**
     * 
     * @var ApplicationGate
     */
    protected $gate;
    
    public function __construct(ApplicationGate $gate){
        $this->gate = $gate;
    }
    
    public function call(Array $seeders){
        $seeders = collect($seeders);
        
        $seeders->each(function($classes, $index){
            $seeder = $this->gate->make($classes);
            
            if(! $seeder instanceof Seeder){
                throw new SeederException(class_basename($seeder)." is not an instance of ".Seeder::class." class");
            }
            
            $seeder->invoke();
        });
    }
    
    public function invoke(){
        if(!method_exists($this, "run")){
            throw new InvalidArgumentException("Method {run} isn't declared within the ".class_name($this)." class");
        }
        
        $this->gate->call([$this, "run"]);
    }
}