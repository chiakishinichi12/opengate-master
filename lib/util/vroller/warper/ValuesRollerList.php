<?php
namespace lib\util\vroller\warper;

use lib\inner\ApplicationGate;
use lib\traits\WarperCommons;
use lib\util\ValuesRoller;

class ValuesRollerList {
    
    use WarperCommons;
    
    /**
     *
     * @var ApplicationGate
     */
    protected $gate;
    
    public function __construct(ApplicationGate $gate){
        $this->gate = $gate;
    }
    
    public function showList(){
        out()->println("Established ValuesRoller Classes:\n");
        
        $this->classList(preferences("structure")->get("rollers"), ValuesRoller::class,
            fn() => out()->haltln("<error>No ValuesRoller class found.</error>"));
    }
}

