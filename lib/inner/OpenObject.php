<?php
namespace lib\inner;

use ArrayAccess;
use ArrayIterator;
use IteratorAggregate;
use OutOfBoundsException;
use Traversable;
use lib\exceptions\OpenGateException;
use lib\inner\json\JsonUtility;
use lib\util\Collections;
use lib\util\EncapsulationHelper;
use lib\util\resolver\MethodInvocationResolver;

/**
 * 
 * @author Antonius Aurelius
 * 
 * Represents a flexible object with dynamic properties and capabilities for property manipulation.
 * 
 * This class allows for dynamic property management, including checking for property existence,
 * enforcing read-only states, and merging new properties into the object.
 * 
 * It also supports iteration, array-like access, and JSON representation of the object properties
 *
 * @method array properties()
 *
 */
class OpenObject implements IteratorAggregate, ArrayAccess {
    
    /**
     * 
     * Holds the dynamic properties of the object.
     * 
     * @var array
     */
    protected $properties = [];
    
    /**
     * 
     * @var EncapsulationHelper
     */
    protected $encapsulation;
    
    /**
     * 
     * constructor
     * 
     */
    public function __construct(){
        $this->encapsulation = App::make(EncapsulationHelper::class)->instance($this);
    }
    
    /**
     * 
     * Stores properties into the object.
     * 
     * This method accepts an array, JSON string, or Collections object and merges it into the current properties.
     * 
     * 
     * @param array|Collections $stack
     * 
     * @throws OpenGateException
     * 
     * @return \lib\inner\OpenObject
     */
    public function store(array|string|Collections $stack, int $depth = 512){
        $this->throwIfReadonly();
        
        if(App::checkInheritance($stack, Collections::class)){
            $stack = $stack->toArray();
        }else if(is_string($stack)){
            if(!App::checkJSONString($stack)){
                throw new OpenGateException("Argument#1(string): string must represent JSON formatted data");
            }
            
            $stack = json_decode($stack, true, $depth);
        }
        
        return $this->encapsulation->propertyArrayMerge($this->properties, $stack);
    }
    
    /**
     * 
     * Sets or gets the read-only state of the object.
     * 
     * @param bool $readonly
     * @return boolean|OpenObject
     */
    public function readonly(bool $readonly = null){
        return $this->encapsulation->appendProperty(__FUNCTION__, $readonly);
    }
    
    /**
     * 
     * Checks if a property exists in the object.
     * 
     * @param string $propname
     * @return boolean
     */
    public function exists(string $propname){
        return array_key_exists($propname, $this->properties);
    }
    
    /**
     * 
     * throws if the object is readonly
     * 
     */
    protected function throwIfReadonly(){
        if($this->readonly()){
            throw new OpenGateException("Cannot define new or existing property. readonly mode is enabled");
        }
    }
    
    /**
     *
     * @param string $propname
     */
    public function __get($propname){
        if(!$this->exists($propname)){
            return "";
        }
        
        return $this->properties[$propname];
    }
    
    /**
     * 
     * @param string $propname
     * @param mixed $propvalue
     * @throws OpenGateException
     */
    public function __set(string $propname, mixed $propvalue){
        $this->throwIfReadonly();
        
        $this->properties[$propname] = $propvalue;
    }
    
    /**
     *
     * @param mixed $propname
     *
     * @return bool
     */
    public function __isset($propname){
        return isset($this->properties[$propname]);
    }
    
    /**
     * 
     * @param string $method
     * @param array $args
     * 
     * @return mixed
     */
    public function __call(string $method, array $args){
        if(!method_exists($this, $method)){
            if(!property_exists($this, $method)){
                return App::make(MethodInvocationResolver::class)
                ->instances([collect($this->properties)])
                ->parent($this)
                ->toCall($method)
                ->params($args)
                ->resolve()
                ->invoke();
            }else{                
                return $this->$method;
            }
        }
    }
    
    /**
     * 
     * @return string|boolean
     */
    public function __toString(){
        return "OpenObject = ".JsonUtility::toEncode($this->properties);
    }
    
    /**
     * @inheritdoc
     * 
     * @return Traversable
     */
    public function getIterator(): Traversable {
        return new ArrayIterator($this->properties);
    }
    
    /**
     * @inheritdoc
     * 
     * @param mixed $offset
     * @return mixed
     */
    public function offsetGet(mixed $offset): mixed {
        if(!isset($this->properties[$offset])){
            throw new OutOfBoundsException("Undefined array index: {$offset}");
        }
        
        return $this->properties[$offset];
    }

    /**
     * @inheritdoc
     * 
     * @param mixed $offset
     * @return bool
     */
    public function offsetExists(mixed $offset): bool {
        return isset($this->properties[$offset]);
    }

    /**
     * @inheritdoc
     * 
     * @throws OpenGateException
     * 
     * @param mixed $offset
     */
    public function offsetUnset(mixed $offset): void {
        $this->throwIfReadonly();
        
        unset($this->properties[$offset]);
    }

    /**
     * @inheritdoc
     * 
     * @param mixed $offset
     * @param mixed $value
     */
    public function offsetSet(mixed $offset, mixed $value): void {
        $this->throwIfReadonly();
        
        $this->properties[$offset] = $value;
    }
}
