<?php
namespace lib\util\migration;

use lib\traits\SQLFeatures;
use lib\util\Collections;
use lib\util\datagate\DB;
use lib\util\exceptions\MigrationException;

class ShowTables {
    
    use SQLFeatures;
    
    /**
     * 
     * singleton object
     * 
     * @var ShowTables
     */
    protected static $instance = null;
    
    /**
     * 
     * @var string
     */
    protected $dbms;
    
    /**
     * 
     * @var Collections
     */
    protected $tables;
    
    /**
     * 
     * constructor
     * 
     * @param string $dbms
     */
    protected function __construct($dbms){
        $this->dbms = $dbms;
        $this->tables = collect();
        
        $this->fetchTables();
    }
    
    /**
     * 
     * fetching tables and their info
     * 
     */
    protected function fetchTables(){
        $result = DB::connection($this->dbms)->raw("show tables");
        
        if($result instanceof Collections){
            $result->each(function($item, $index){
                $this->tables->add(array_values($item)[0]);
            });
        }
    }
    
    /**
     * 
     * @param string $tblname
     */
    public function addTable($tblname, $builder){
        if(!($builder instanceof SchemaBuilder)){
            throw new MigrationException("Cannot add new table name on the ShowTables container");
        }
        
        $this->tables->add($tblname);
    }
    
    /**
     * 
     * @param string $index
     */
    public function removeTable($index, $builder){
        if(!($builder instanceof SchemaBuilder)){
            throw new MigrationException("removal of a table denied");
        }
        
        $this->tables->remove($index);
    }
    
    /**
     * 
     * @return \lib\util\Collections
     */
    public function getTables(){
        return $this->tables;
    }
    
    /**
     * 
     * @param string $dbms
     * @return \lib\util\migration\ShowTables
     */
    public static function getInstance($dbms = "mysql"){
        if(self::$instance === null){
            self::$instance = new ShowTables($dbms);
        }
        
        return self::$instance;
    }
}