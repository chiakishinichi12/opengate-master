<?php
namespace lib\util\migration;

use lib\traits\MigrationInspector;
use lib\util\exceptions\MigrationException;
use lib\util\Collections;

/**
 * 
 * @author antonio
 *
 * @method \lib\util\migration\Column varchar(string $name, int $length = null)
 * @method \lib\util\migration\Column integer(string $name, int $length = null)
 * @method \lib\util\migration\Column tinyint(string $name, int $length = null)
 * @method \lib\util\migration\Column double(string $name, int $length, int $decimals)
 * @method \lib\util\migration\Column date(string $name, int $length = null)
 * @method \lib\util\migration\Column timestamp(string $name, int $length = null)
 * @method \lib\util\migration\Column datetime(string $name, int $length = null)
 * @method \lib\util\migration\Column enum(string $name, int $length = null)
 * @method \lib\util\migration\Column set(string $name, int $length = null)
 * @method \lib\util\migration\Column text(string $name)
 * @method \lib\util\migration\Column longtext(string $name)
 * 
 */
class Table{
    
    use MigrationInspector;
    /**
     * 
     * @var string
     */
    protected $name;
    
    /**
     * 
     * @var array
     */
    protected $cols;
    
    /**
     * 
     * @var array
     */
    protected $indexes = [];
    
    /**
     * 
     * @var string
     */
    protected $engine;
    
    /**
     * 
     * @var string
     */
    protected $charset;
    
    /**
     * 
     * @var Collections
     */
    protected $todrop;
    
    /**
     * 
     * @param String $name
     * @param array $cols
     */
    public function __construct(String $name, Array $cols = []){
        $this->name = $name;
        $this->cols = $cols;
        $this->todrop = [];
    }
    
    /**
     * 
     * @param array $cols
     * @return \lib\util\migration\Table
     */
    public function setColumns(Array $cols){
        $this->cols = $cols;
        
        return $this;
    }
    
    /**
     * 
     * @param string $engine
     * @return \lib\util\migration\Table
     */
    public function setEngine($engine){
        $this->engine = $engine;
        
        return $this;
    }
    
    /**
     * 
     * @param string $charset
     * @return \lib\util\migration\Table
     */
    public function setCharset($charset){
        $this->charset = $charset;
        
        return $this;
    }
    
    /**
     * 
     * @return string|\lib\util\Collections
     */
    public function alter(){ 
        $alters = collect();
        
        $altstart = "ALTER TABLE `{$this->name}`";
        
        foreach($this->cols as $col){
            if($col->isModify()){
                if($col->isRenaming()){
                    $altercmd = "{$altstart} CHANGE {$col->build()}";
                }else{
                    $altercmd = "{$altstart} MODIFY {$col->build()}";
                }
            }else{
                $altercmd = "{$altstart} ADD {$col->build()}";
            }
            
            if($col->isPrimaryKey()){
                $altercmd .= " PRIMARY KEY";
            }
            
            if(!is_blank($col->after())){
                $altercmd .= " AFTER `{$col->after()}`";
            }
            
            if($col->isFirst()){
                $altercmd .= " FIRST";
            }
            
            $altercmd .= ";";
            
            $alters->put($col->getName(), $altercmd);
        }
        
        foreach($this->todrop as $col => $dropCommaned){
            $altercmd = "{$altstart} {$dropCommaned}";
            
            $alters->put($col, $altercmd);
        }
                
        return $alters;
    }
    
    /**
     * 
     * @param string $colname
     * @return \lib\util\Collections
     */
    public function dropColumn($colname){
        $this->todrop[$colname] = "DROP COLUMN `{$colname}`"; 
    }
   
    /**
     * 
     * @param string $colname
     */
    public function dropDefault($colname){
        $this->todrop[$colname] = "ALTER COLUMN `{$colname}` DROP DEFAULT";
    }
    
    /**
     * 
     * @throws MigrationException
     * @return string
     */
    public function build(){
        if(count($this->cols) === 0){
            return "";
        }
        
        $create = "CREATE TABLE IF NOT EXISTS `{$this->name}`";
        
        $primaries = [];
        
        foreach($this->cols as $col){
            if(!($col instanceof Column)){
                throw new MigrationException(sprintf("An element which is not an instance of %s found", Column::class));
            }
            
            if($col->isAutoIncrement()){
                if(!$this->isNumericDataType($col->getType()))
                    throw new MigrationException("A non-numeric column cannot have AUTO_INCREMENT constraint {{$col->getName()}}");
            }
            
            if($col->isPrimaryKey()){
                $primaries[] = sprintf("`%s`", $col->getName());
            }
        }
        
        $primaries  = count($primaries) ? ", PRIMARY KEY(".implode(",",$primaries).")" : "";
        $indexes    = count($this->indexes) ? ", ".implode(",", $this->indexes) : "";
        
        $create .= "(".implode(", ", $this->cols)."{$primaries}{$indexes})";
        
        if($this->engine !== null){
            $create .= " ENGINE={$this->engine}";
        }
        
        if($this->charset !== null){
            $create .= " CHARSET={$this->charset}";
        }
        
        return $create;
    }
    
    /**
     * 
     * @param string $name
     * @param array $columns
     * @param string $type
     */
    public function addIndex($name, $columns, $type = null){
        if(is_string($columns)){
            $type = $columns;
        }
        
        if(is_array($name)){
            $columns = $name;
        }
        
        $this->indexes[] = new ColumnIndexer($name, $columns, $type);
        
        return $this;
    }
    
    /**
     * 
     * adding default timestamps to the table
     * 
     */
    public function timestamps(){        
        $this->timestamp("create_time")->default("NOW()");
        $this->timestamp("update_time")->default("NOW()");
    }
    
    /**
     * 
     * adding soft delete column
     * 
     */
    public function softDelete(){
        $this->timestamp("delete_time");
    }
    
    /**
     * magic method
     * 
     * @param string $method_name
     * @param array $args
     * @throws MigrationException
     * @return mixed
     */
    public function __call($method_name, $args){
        if(!method_exists($this, $method_name)){
            $typeAsFunctions = ["varchar", "integer", "tinyint", "double", "date", "timestamp",
                "datetime", "enum", "set", "text", "longtext"];
            
            if(in_array($method_name, $typeAsFunctions)){
                if(is_blank($args)){
                    throw new MigrationException("function {$method_name}: no arguments found.");
                }
                
                $identifier = $args[0];
                
                unset($args[0]);
                
                $args = array_values($args);
                
                $column = (new Column($identifier))->setType($method_name, ...$args);
                
                if(strcmp($method_name, "double") === 0 && count($args) === 2){
                    $column->setDoubleParams(...$args);
                }
                
                $this->cols[] = $column;
                
                return $column;
            }
        }
        
        throwing:
        throw new MigrationException("Method '{$method_name}' is not defined.");
    }
}