<?php
namespace lib\util\annotation;

class AnnotationProperty {
    
    /**
     * 
     * @var string
     */
    protected $name;
    
    /**
     * 
     * @var string
     */
    protected $type;
    
    /**
     * 
     * @var boolean
     */
    protected $default;
    
    /**
     * 
     * @param String $name
     * @param mixed $type
     * @param bool $default
     */
    public function __construct(string $name, $type, bool $default = false){
        $this->name = $name;
        $this->type = $type;
        $this->default = $default;
    }
    
    /**
     * checks if the annotation property is a default
     * 
     * @return boolean
     */
    public function isDefault(){
        return $this->default;
    }
    
    /**
     * returns the annotation property name
     * 
     * @return string
     */
    public function getName(){
        return $this->name;
    }
    
    /**
     * returns the expected type of value for a defined annotation property
     * 
     * @return mixed
     */
    public function getType(){
        return $this->type;    
    }
}