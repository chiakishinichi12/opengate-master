<?php

namespace lib\traits;

use lib\util\Collections;

trait MigrationInspector {
    
    private function getDataTypes(){
        return [
            "numeric" => [
                "bit",
                "bool",
                "boolean",
                "int",
                "integer",
                "tinyint",
                "smallint",
                "mediumint",
                "bigint",
                "tinyint",
                "float",
                "double",
                "double precision",
                "decimal",
                "dec"
            ],
            "string" => [
                "char",
                "varchar",
                "binary",
                "varbinary",
                "tinyblob",
                "tinytext",
                "text",
                "blob",
                "mediumtext",
                "mediumblob",
                "longtext",
                "longblob",
                "enum",
                "set"
            ],
            "date_and_time" => [
                "date",
                "datetime",
                "timestamp",
                "time",
                "year"
            ]
        ];
    }
    
    /**
     * 
     * @param string $sqlDataType
     * @return boolean
     */
    private function isDateTimeDataType($sqlDataType){
        return in_array($sqlDataType, $this->getDataTypes()["date_and_time"]);
    }
    
    /**
     * 
     * @param string $sqlDataType
     * @return boolean
     */
    private function isStringDataType($sqlDataType){
        return in_array($sqlDataType, $this->getDataTypes()["string"]);
    }
    
    /**
     * 
     * @param string $sqlDataType
     * @return boolean
     */
    private function isNumericDataType($sqlDataType){
        return in_array($sqlDataType, $this->getDataTypes()["numeric"]);
    }
    
    /**
     * 
     * @param string $sqlDataType
     * @return bool
     */
    private function checkDataType($sqlDataType) : bool {
        $sqlDataType = strtolower($sqlDataType);
        
        $dataTypes = [];
        
        Collections::make($this->getDataTypes())->each(function($item, $index) use (&$dataTypes){
            if(is_array($item)){
                foreach($item as $i){
                    $dataTypes[] = $i;
                }
            }
        });
        
        return in_array($sqlDataType, $dataTypes);
    }
    
}