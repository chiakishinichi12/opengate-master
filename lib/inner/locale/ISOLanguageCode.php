<?php
namespace lib\inner\locale;

/**
 * 
 * @author Antonius Aurelius
 *
 */
class ISOLanguageCode {
    
    public const EN = 'en'; // English
    public const FR = 'fr'; // French
    public const DE = 'de'; // German
    public const ES = 'es'; // Spanish
    public const IT = 'it'; // Italian
    public const ZH = 'zh'; // Chinese
    public const JA = 'ja'; // Japanese
    public const RU = 'ru'; // Russian
    public const AR = 'ar'; // Arabic
    public const HI = 'hi'; // Hindi
    public const PT = 'pt'; // Portuguese
    public const KO = 'ko'; // Korean
    public const NL = 'nl'; // Dutch
    public const TR = 'tr'; // Turkish
    public const PL = 'pl'; // Polish
    public const SV = 'sv'; // Swedish
    public const NO = 'no'; // Norwegian
    public const DA = 'da'; // Danish
    public const FI = 'fi'; // Finnish
    public const EL = 'el'; // Greek
    public const HE = 'he'; // Hebrew
    public const TH = 'th'; // Thai
    public const CS = 'cs'; // Czech
    public const HU = 'hu'; // Hungarian
    public const RO = 'ro'; // Romanian
    public const VI = 'vi'; // Vietnamese
    public const ID = 'id'; // Indonesian
    public const MS = 'ms'; // Malay
    public const TL = 'tl'; // Tagalog (Filipino)
    public const BG = 'bg'; // Bulgarian
    public const UK = 'uk'; // Ukrainian
    public const HR = 'hr'; // Croatian
    public const SK = 'sk'; // Slovak
    public const SL = 'sl'; // Slovenian
    public const SR = 'sr'; // Serbian
    public const LV = 'lv'; // Latvian
    public const LT = 'lt'; // Lithuanian
    public const ET = 'et'; // Estonian
    public const MT = 'mt'; // Maltese
    public const GA = 'ga'; // Irish
    public const IS = 'is'; // Icelandic
    public const MK = 'mk'; // Macedonian
    
}

