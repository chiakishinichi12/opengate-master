<?php
namespace lib\commands\classes;

use lib\commands\CommonMethods;
use lib\instances\CommandInstance;
use lib\util\cli\WarpArgv;
use lib\inner\App;
use lib\util\mockeries\warper\MockeryClassCreation;
use lib\util\mockeries\warper\MockeryList;

/**
 *
 * @Commands(["mockery", "mock"])
 *
 */
class WarpMockery extends CommandInstance {
    
    use CommonMethods;
    
    protected $name = "mockery";
    
    protected $desc = "Database mockery feature";
    
    /**
     *
     * @Argument("creates mockery class. [option(s): { --model=model_class(O) }]")
     *
     */
    public function create(){
        App::make(MockeryClassCreation::class)->write();
    }
    
    /**
     *
     * @Argument("shows all created mockeries")
     *
     */
    public function list(){
        App::make(MockeryList::class)->showList();
    }
    
    /**
     * 
     * @param WarpArgv $argv
     */
    public function listen(WarpArgv $argv){
        $this->execution($argv);
    }
}