<?php
namespace lib\util;

use lib\inner\Accessor;
use lib\inner\arrangements\GateKeepDefiner;

/**
 * 
 * @author antonio
 *
 * @method static mixed fetchValue($class)
 * @method static mixed remove($class)
 * @method static mixed handleValue($class, $value, $strict = false)
 *
 */
class GateKeeper extends Accessor{

    /**
     * 
     * {@inheritDoc}
     * @see \lib\inner\Accessor::warpedInstance()
     */
    public function warpedInstance(){
        return GateKeepDefiner::class;    
    }
}