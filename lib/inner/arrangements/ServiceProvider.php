<?php
namespace lib\inner\arrangements;

use lib\inner\ApplicationGate;

class ServiceProvider {
    
    /**
     * 
     * @var ApplicationGate
     */
    protected $gate;
    
    /**
     * 
     * @var boolean
     */
    protected $forCLI = false;
    
    /**
     * 
     * @var boolean
     */
    protected $forAll = false;
    
    /**
     * 
     * @param ApplicationGate $gate
     */
    public function __construct(ApplicationGate $gate){
        $this->gate = $gate;    
    }
    
    /**
     * 
     * @return boolean
     */
    public function isForCLI(){
        return $this->forCLI;
    }
    
    /**
     * 
     * @return boolean
     */
    public function isForAll(){
        return $this->forAll;
    }
}