<?php
namespace lib\startup;

use lib\inner\WebAppCore;
use lib\util\GateKeeper;
use lib\util\StringUtil;

trait DefineCsrfToken {
    
    /**
     *
     * defining csrf-token string value and put it into GateKeeper stack
     *
     */
    protected function defineCsrfTokenString(){        
        $hashedString = hash_hmac("sha256",
            uniqid(StringUtil::getRandomizedString(20), true),
            date(STANDARD_TIMESTAMP_FORMAT));
        
        GateKeeper::handleValue(WebAppCore::class, $hashedString, true);
    }
}

