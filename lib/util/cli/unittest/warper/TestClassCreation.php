<?php
namespace lib\util\cli\unittest\warper;

use lib\traits\WarperCommons;
use lib\util\Collections;
use lib\util\cli\LongOption;
use lib\util\cli\WarpArgv;
use lib\util\file\FileUtil;

class TestClassCreation {
    
    use WarperCommons;
    
    /**
     *
     * @var string
     */
    protected $warping = "GateTestCase";
    
    /**
     *
     * @var Collections
     */
    protected $args;
    
    /**
     *
     * @var string
     */
    protected $prototype;
    
    /**
     * 
     * @var LongOption
     */
    protected $testType;
    
    /**
     * 
     * @var string
     */
    protected $testScripts;
    
    /**
     *
     * @param WarpArgv $argv
     */
    public function __construct(WarpArgv $argv){
        $this->args = $argv->obtainArguments();
        
        $this->testType = $argv->findLongOption("--type");
        
        $this->testScripts = preferences("structure")->get("tests");
        
        $this->prototype = base_path("lib/util/cli/unittest/prototype");
    }
    
    protected function resolveTestScriptType(){
        // default script type
        $testScriptType = "feature";
        
        if(!is_null($this->testType)){
            if(!is_blank($this->testType->value()) && 
                in_array($this->testType->value(), ["feature", "unit"])){
                $testScriptType = $this->testType->value();
            }
        }
        
        return $testScriptType;
    }
    
    public function write(){
        if($this->args->count() < 2){
            return;
        }
        
        $this->testScripts = "{$this->testScripts}/{$this->resolveTestScriptType()}";
        
        $template = FileUtil::getContents("{$this->prototype}/TestScriptBlueprint.tmp", true);
        
        $test_name = $this->args->get(1);
        
        $this->alterAndCreate($test_name, $this->testScripts);
        
        $toFind = collect([
            [
                "find" => "[Test_Class_Name]",
                "replace" => $test_name
            ],
            [
                "find" => "[GateTest_Namespace]",
                "replace" => str_replace("/", "\\", relative_path($this->testScripts))
            ]
        ]);
        
        $this->writeWarpedScript($template,
            $toFind,
            $this->testScripts,
            $test_name);
    }
}

