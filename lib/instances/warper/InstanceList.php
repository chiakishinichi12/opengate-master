<?php
namespace lib\instances\warper;

use lib\traits\WarperCommons;
use lib\inner\WebAppCore;

class InstanceList{
    
    use WarperCommons;
    
    public function showList(){
        out()->println("Established Core Instance Classes:\n");
        
        $this->classList(preferences("structure")->get("instances"), WebAppCore::class,
            fn() => out()->haltln("<error>No WebAppCore inherited class found.</error>"));
    }
}

