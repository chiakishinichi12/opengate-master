<?php
namespace lib\util\file;

/**
 *
 * @author Jiya Sama
 *
 */
trait FileTypeEvaluator {
    
    /**
     *
     * @var string
     */
    protected $invalidationRemark;
    
    /**
     *
     * Evaluates if the file's MIME type matches the expected category.
     *
     * This method checks if the MIME type of the file is included in the provided 
     * array of acceptable MIME types (`extensionStack`). It uses a collection 
     * for easy lookup and comparison. If the MIME type is found within the 
     * `extensionStack`, the method returns `true`, indicating a valid file type.
     * Otherwise, it sets an error message (`invalidationRemark`) stating that a file
     * of the expected type was required.
     *
     * 
     * @param array $extensionStack
     * @param string $expected
     *
     * @return boolean
     */
    protected function fileEvaluation(array $extensionStack, string $expected){
        $extensions = collect($extensionStack);
        
        // evaluates if the mime is on the list
        if($extensions->has($this->getMimeType())){
            return true;
        }else{
            $this->invalidationRemark = "{$expected} file expected";
        }
        
        return false;
    }
    
    /**
     * 
     * Checks if the file's MIME type matches the expected MIME type for its extension.
     * 
     * This method compares the MIME type retrieved from the file's extension with the actual MIME type
     * of the file to determine if there is a mismatch.
     * 
     * @return boolean
     */
    public function isMimeTypeMismatched(){
        $extensions = collect(array_merge(
            self::IMAGE_EXTENSIONS,
            self::VIDEO_EXTENSIONS,
            self::AUDIO_EXTENSIONS,
            self::DOCUMENT_EXTENSIONS,
            self::SPREADSHEET_EXTENSIONS,
            self::TEXT_EXTENSIONS
        ));
        
        // evaluates if the MIME type is matching on the file
        if(strcmp($mime = $extensions->get($this->getExtensionName()), 
            $expected = $this->getMimeType()) !== 0){
            $this->invalidationRemark = "MIME type mismatched {{$mime}}: '{$expected}' expected";
            
            return true;
        }
        
        return false;
    }
    
    /**
     * Retrieves the suitable extension name based on the MIME type of the file.
     * 
     * This method collects all file extensions from predefined constants (image, video, audio, document, 
     * spreadsheet, and text file types) and performs a binary search to find the corresponding file extension
     * based on the file's MIME type.
     * 
     * @return string|null
     */
    public function getSuitableExtensionName(){
        $extensions = collect(array_merge(
            self::IMAGE_EXTENSIONS,
            self::VIDEO_EXTENSIONS,
            self::AUDIO_EXTENSIONS,
            self::DOCUMENT_EXTENSIONS,
            self::SPREADSHEET_EXTENSIONS,
            self::TEXT_EXTENSIONS
        ));
        
        $find = $extensions->binarySearch($this->getMimeType())->get();
        
        if(!is_null($find)){
            return $find->key();
        }
        
        return null;
    }
    
    /**
     *
     * Retrieves the invalidation remark for the file.
     *
     * This method returns the remark or error message associated with the file validation process. 
     * It provides insight into why a file was considered invalid, based on checks such as its extension, MIME type, or other evaluation logic.
     * The remark is set during the file evaluation process when an issue is found, like a mismatched extension or MIME type. 
     * If no issues were encountered, it will return `null`.
     *
     * @return string|null
     */
    public function getInvalidationRemark(){
        return $this->invalidationRemark;
    }
    
    /**
     * Checks if the file MIME type corresponds to a recognized image format.
     *
     * @return boolean
     */
    public function isImageFile(){
        return $this->fileEvaluation(self::IMAGE_EXTENSIONS, "image");
    }
    
    /**
     * Checks if the file MIME type corresponds to a recognized video format.
     *
     * @return boolean
     */
    public function isVideoFile(){
        return $this->fileEvaluation(self::VIDEO_EXTENSIONS, "video");
    }
    
    /**
     * Checks if the file MIME type matches a known spreadsheet format.
     *
     * @return boolean
     */
    public function isSpreadsheetFile(){
        return $this->fileEvaluation(self::SPREADSHEET_EXTENSIONS, "spreadsheet");
    }
    
    /**
     * Checks if the file MIME type matches a known document format.
     *
     * @return boolean
     */
    public function isDocumentFile(){
        return $this->fileEvaluation(self::DOCUMENT_EXTENSIONS, "document");
    }
    
    /**
     * Checks if the file MIME type matches a known audio format.
     *
     * @return boolean
     */
    public function isAudioFile(){
        return $this->fileEvaluation(self::AUDIO_EXTENSIONS, "audio");
    }
    
    /**
     * Checks if the file MIME type matches a known text format.
     *
     * @return boolean
     */
    public function isTextFile(){
        return $this->fileEvaluation(self::TEXT_EXTENSIONS, "text");
    }
}