<?php

namespace lib\util\exceptions;

use lib\exceptions\OpenGateException;

class ControllerException extends OpenGateException {
    
    /**
     * 
     * @var string
     */
    protected $controller;
    
    /**
     * 
     * @var string
     */
    protected $method;
    
    /**
     * 
     * @param String $message
     * @param unknown $controller
     * @param unknown $method
     */
    public function __construct(String $message, String $controller = null, String $method = null){
        parent::__construct($message);
        
        $this->controller = $controller;
        $this->method = $method;
    }
}