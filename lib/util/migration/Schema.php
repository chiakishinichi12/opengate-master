<?php

namespace lib\util\migration;

use lib\inner\Accessor;

/**
 * 
 * @author antonio
 *
 * @method static mixed|boolean create(string $tblname, Closure $callback)
 * @method static boolean view(string $viewName, mixed $query)
 * @method static boolean table(string $tblname, Closure $callback)
 * @method static boolean hasTable(string $tblname)
 * @method static boolean dropIfExists(string $tblname, boolean $isView = false)
 */
class Schema extends Accessor {
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\inner\Accessor::warpedInstance()
     */
    public function warpedInstance(){
        return SchemaBuilder::class;
    }
    
}