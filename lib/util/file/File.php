<?php
namespace lib\util\file;

use ArrayIterator;
use Countable;
use IteratorAggregate;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use ZipArchive;
use lib\util\Collections;
use lib\util\StringUtil;
use lib\util\accords\FileAccess;
use lib\util\exceptions\FileNotFoundException;
use lib\util\exceptions\IOException;

/**
 *
 * @author antonio
 *
 */
class File implements FileAccess, IteratorAggregate, Countable {
    
    use FileTypeEvaluator;
    
    /**
     * @var array
     */
    protected const VIDEO_EXTENSIONS = [
        '.mp4'   => 'video/mp4',   // MPEG-4 Video
        '.mov'   => 'video/quicktime',   // QuickTime Movie
        '.avi'   => 'video/x-msvideo',   // Audio Video Interleave
        '.wmv'   => 'video/x-ms-wmv',   // Windows Media Video
        '.flv'   => 'video/x-flv',   // Flash Video
        '.mkv'   => 'video/x-matroska',   // Matroska Video
        '.webm'  => 'video/webm',   // WebM
        '.3gp'   => 'video/3gpp',   // 3GPP
        '.m4v'   => 'video/x-m4v',   // iTunes Video File
        '.mpeg'  => 'video/mpeg',   // MPEG
        '.mpg'   => 'video/mpeg',   // MPEG
        '.ts'    => 'video/mp2t',   // Transport Stream
        '.f4v'   => 'video/x-f4v',   // Flash MP4 Video
        '.rm'    => 'application/vnd.rn-realmedia',   // RealMedia
        '.rmvb'  => 'application/vnd.rn-realmedia-vbr',   // RealMedia Variable Bitrate
        '.ogv'   => 'video/ogg',   // Ogg Video
        '.vob'   => 'video/dvd'    // Video Object (DVD)
    ];
    
    /**
     * @var array
     */
    protected const IMAGE_EXTENSIONS = [
        '.jpg'   => 'image/jpeg',   // Joint Photographic Experts Group
        '.jpeg'  => 'image/jpeg',   // Joint Photographic Experts Group
        '.png'   => 'image/png',   // Portable Network Graphics
        '.gif'   => 'image/gif',   // Graphics Interchange Format
        '.bmp'   => 'image/bmp',   // Bitmap Image File
        '.tiff'  => 'image/tiff',   // Tagged Image File Format
        '.tif'   => 'image/tiff',   // Tagged Image File Format
        '.webp'  => 'image/webp',   // Web Picture format
        '.svg'   => 'image/svg+xml',   // Scalable Vector Graphics
        '.ico'   => 'image/vnd.microsoft.icon',   // Icon File
        '.heic'  => 'image/heic',   // High Efficiency Image Format
        '.heif'  => 'image/heif',   // High Efficiency Image File Format
        '.raw'   => 'image/x-raw',   // Raw Image Data
        '.psd'   => 'image/vnd.adobe.photoshop',   // Photoshop Document
        '.ai'    => 'application/postscript',   // Adobe Illustrator File
        '.eps'   => 'application/postscript',   // Encapsulated PostScript
        '.indd'  => 'application/x-indesign',   // Adobe InDesign Document
        '.cdr'   => 'application/cdr',   // CorelDRAW Image File
        '.pdf'   => 'application/pdf'    // Portable Document Format
    ];
    
    /**
     * @var array
     */
    protected const DOCUMENT_EXTENSIONS = [
        '.doc'   => 'application/msword',   // Microsoft Word Document (older format)
        '.docx'  => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',   // Microsoft Word Open XML
        '.docm'  => 'application/vnd.ms-word.document.macroenabled.12',   // Macro-Enabled Document
        '.dot'   => 'application/msword',   // Template (older format)
        '.dotx'  => 'application/vnd.openxmlformats-officedocument.wordprocessingml.template',   // Open XML Template
        '.dotm'  => 'application/vnd.ms-word.template.macroenabled.12',   // Macro-Enabled Template
        '.odt'   => 'application/vnd.oasis.opendocument.text',   // OpenDocument Text Document
        '.ott'   => 'application/vnd.oasis.opendocument.text-template',   // Text Template
        '.rtf'   => 'application/rtf',   // Rich Text Format
        '.txt'   => 'text/plain',   // Plain Text File
        '.pdf'   => 'application/pdf',   // Portable Document Format
        '.wps'   => 'application/vnd.ms-works',   // Works Document
        '.pages' => 'application/x-iwork-pages-sffpages'    // Apple Pages Document
    ];
    
    /**
     * @var array
     */
    protected const SPREADSHEET_EXTENSIONS = [
        '.xls'   => 'application/vnd.ms-excel',   // Microsoft Excel Spreadsheet
        '.xlsx'  => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',   // Excel Open XML
        '.xlsm'  => 'application/vnd.ms-excel.sheet.macroenabled.12',   // Macro-Enabled Spreadsheet
        '.xlt'   => 'application/vnd.ms-excel',   // Template
        '.xltx'  => 'application/vnd.openxmlformats-officedocument.spreadsheetml.template',   // Open XML Template
        '.xltm'  => 'application/vnd.ms-excel.template.macroenabled.12',   // Macro-Enabled Template
        '.csv'   => 'text/csv',   // Comma-Separated Values
        '.ods'   => 'application/vnd.oasis.opendocument.spreadsheet',   // OpenDocument Spreadsheet
        '.ots'   => 'application/vnd.oasis.opendocument.spreadsheet-template',   // Spreadsheet Template
        '.fods'  => 'application/vnd.oasis.opendocument.spreadsheet',   // Flat Spreadsheet
        '.sylk'  => 'text/vnd.sylk',   // Symbolic Link Format
        '.dif'   => 'application/x-dif',   // Data Interchange Format
        '.prn'   => 'text/plain',   // Formatted Text
        '.wk1'   => 'application/vnd.lotus-1-2-3',   // Lotus 1-2-3 Spreadsheet
        '.wk2'   => 'application/vnd.lotus-1-2-3',
        '.wk3'   => 'application/vnd.lotus-1-2-3',
        '.wk4'   => 'application/vnd.lotus-1-2-3',
        '.123'   => 'application/vnd.lotus-1-2-3',
        '.wb1'   => 'application/x-quattro-pro',   // Quattro Pro
        '.wb2'   => 'application/x-quattro-pro',
        '.numbers' => 'application/vnd.apple.numbers'    // Apple Numbers Spreadsheet
    ];
    
    /**
     * @var array
     */
    protected const AUDIO_EXTENSIONS = [
        '.mp3'   => 'audio/mpeg',   // MPEG Layer Audio 3
        '.wav'   => 'audio/wav',   // Waveform Audio
        '.flac'  => 'audio/flac',   // Free Lossless Audio Codec
        '.aac'   => 'audio/aac',   // Advanced Audio Coding
        '.ogg'   => 'audio/ogg',   // Ogg Vorbis
        '.m4a'   => 'audio/mp4',   // MPEG-4 Audio
        '.wma'   => 'audio/x-ms-wma',   // Windows Media Audio
        '.alac'  => 'audio/alac',   // Apple Lossless Audio Codec
        '.aiff'  => 'audio/aiff',   // Audio Interchange File Format
        '.dsd'   => 'audio/dsd',   // Direct Stream Digital
        '.opus'  => 'audio/opus',   // Opus
        '.amr'   => 'audio/amr',   // Adaptive Multi-Rate
        '.mpa'   => 'audio/mpeg',   // MPEG Audio
        '.ra'    => 'audio/vnd.rn-realaudio',   // RealAudio
        '.voc'   => 'audio/x-voc'    // Creative Voice File
    ];
    
    /**
     * @var array
     */
    protected const TEXT_EXTENSIONS = [
        '.txt'   => 'text/plain',   // Plain Text File
        '.rtf'   => 'application/rtf',   // Rich Text Format
        '.md'    => 'text/markdown',   // Markdown File
        '.html'  => 'text/html',   // HTML File
        '.htm'   => 'text/html',
        '.xml'   => 'application/xml',   // XML File
        '.json'  => 'application/json',   // JSON File
        '.csv'   => 'text/csv',   // CSV File
        '.log'   => 'text/plain',   // Log File
        '.yaml'  => 'application/x-yaml',   // YAML
        '.yml'   => 'application/x-yaml',
        '.ini'   => 'text/plain',   // INI File
        '.sql'   => 'application/sql',   // SQL File
        '.bat'   => 'application/x-bat',   // Batch File
        '.ps1'   => 'application/powershell',   // PowerShell Script
        '.cfg'   => 'text/plain',   // Configuration File
        '.conf'  => 'text/plain',
        '.tex'   => 'application/x-tex',   // LaTeX File
        '.srt'   => 'application/x-subrip',   // Subtitle File
        '.asc'   => 'text/plain'    // ASCII Text File
    ];
    
    /**
     *
     * @var string
     */
    protected $name;
    
    /**
     *
     * @var resource
     */
    protected $handle;
    
    /**
     *
     * @var boolean
     */
    protected $closed = false;
    
    /**
     *
     * @param string $name
     */
    public function __construct($name){
        $this->name = $name;
    }
    
    /**
     *
     * @param string $mode
     */
    protected function initOrUpdateHandler($mode){
        if(!$this->exists()){
            $this->evaluateDirectoryExistence();
            $this->handle = fopen($this->getName(), $mode);
            return;
        }
        
        if(is_null($this->handle)){
            if($this->isDirectory()){
                $this->handle = opendir($this->getName());
            }else{
                $this->handle = fopen($this->getName(), $mode);
            }
        }else{
            fclose($this->handle);
            $this->handle = fopen($this->getName(), $mode);
        }
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\accords\FileAccess::makeDir()
     */
    public function makeDir(bool $recursive = true, int $permissions = 0777) : bool {
        if(!$this->exists()){
            return @mkdir($this->name, $permissions, $recursive);
        }
        
        return false;
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\accords\FileAccess::createNewFile()
     */
    public function createNewFile() : bool {
        if(!$this->exists()){
            $this->initOrUpdateHandler("w+");
            return true;
        }
        
        return false;
    }
    
    /**
     *
     * evaluates if the directory where the file is gonna be stored exists.
     * will create one if not
     *
     */
    protected function evaluateDirectoryExistence(){
        $slashed_base = "/".basename($this->getName());
        
        $directory = substr($this->getName(), 0, strrpos($this->getName(), $slashed_base));
        
        if(!@file_exists($directory)){
            @mkdir($directory, 0777, true);
        }
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\accords\FileAccess::exists()
     */
    public function exists() : bool {
        return @file_exists($this->getName());
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\accords\FileAccess::getName()
     */
    public function getName() : string {
        return $this->name;
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\accords\FileAccess::getRelativePath()
     */
    public function getRelativePath() : string {
        if($this->isURL()){
            return null;
        }
        
        return relative_path($this->getName());
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\accords\FileAccess::getBasename()
     */
    public function getBasename() : string {
        return basename($this->getName());
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\accords\FileAccess::canWrite()
     */
    public function canWrite() : bool {
        return is_writable($this->getName());
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\accords\FileAccess::getContents()
     */
    public function getContents() : string {
        if($this->isDirectory()){
            throw new IOException("No contents could be retrieved from directory {{$this->getName()}}");
        }
        
        if(!$this->exists() && !$this->isURL()){
            throw new FileNotFoundException("File '{$this->getName()}' doesn't exist");
        }
        
        if($this->isClosed()){
            throw new IOException("Cannot retrieve contents when the file is already closed");
        }
        
        $content = "";
        
        $this->initOrUpdateHandler("r");
        
        if($this->handle){
            while(($line = fgets($this->handle)) !== false){
                $content .= $line;
            }
        }
        
        return $content;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\accords\FileAccess::read()
     */
    public function read($downloadable = false, $headers = []){
        $filename = "; filename=\"{$this->getBasename()}\"";
        
        $defaults = [
            "Content-Type" => $this->getMimeType(),
            "Content-Disposition" => "inline{$filename}",
            "Content-Length" => $this->getSize("Bytes")
        ];
        
        if($downloadable){
            // alters response headers that will be parsed by the browser
            // to make the file downloadable
            $defaults = array_merge($defaults, [
                "Cache-Control" => "public",
                "Content-Description" => "File Transfer",
                "Content-Transfer-Encoding" => "binary",
                "Content-Disposition" => "attachment{$filename}"
            ]);
        }
        
        // merges with custom headers
        $defaults = array_merge($defaults, $headers);
        
        response()->setHeaders($defaults, true);
        
        // echoes compressed binary
        return readfile($this->getName());
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\accords\FileAccess::getExtensionName()
     */
    public function getExtensionName() : string {
        $extension = StringUtil::extractExtensionName($this->getName());
        
        if(is_null($extension)){
            return "";
        }
        
        return $extension;
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\accords\FileAccess::getMimeType()
     */
    public function getMimeType() : string {
        if($this->isDirectory()){
            return null;
        }
        
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        
        if($finfo){
            $mimeType = finfo_file($finfo, $this->getName());
            finfo_close($finfo);
            
            return $mimeType;
        }
        
        return false;
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\accords\FileAccess::getSize()
     */
    public function getSize($metric = "MB") : float {
        if($this->exists()){
            $sizeInBytes = filesize($this->getName());
            
            $sizeInKB = $sizeInBytes / 1024; // Convert to KB
            $sizeInMB = $sizeInKB / 1024; // Convert to MB
            
            switch(strtolower($metric)){
                case "mb":
                    return $sizeInMB;
                case "kb":
                    return $sizeInKB;
                case "bytes":
                    return $sizeInBytes;
                default:
                    throw new IOException("Unsupported Metric for getting the size: {{$metric}}");
            }
        }
        
        return -1;
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\accords\FileAccess::getOwner()
     */
    public function getOwner() : string {
        if(!extension_loaded("posix")){
            return "";
        }
        
        if(!$this->exists()){
            throw new FileNotFoundException("File doesn't exist {{$this->getName()}}");
        }
        
        $ownerUid = fileowner($this->getName());
        
        $ownerInfo = posix_getpwuid($ownerUid);
        $ownerUsername = $ownerInfo["name"];
        
        return $ownerUsername;
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\accords\FileAccess::getLastAccessedTime()
     */
    public function getLastAccessedTime() : string {
        if(!$this->exists()){
            throw new FileNotFoundException("File doesn't exist {{$this->getName()}}");
        }
        
        $lastAccessedTime = fileatime($this->getName());
        $formattedTime = date(STANDARD_TIMESTAMP_FORMAT, $lastAccessedTime);
        
        return $formattedTime;
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\accords\FileAccess::getLastModifiedTime()
     */
    public function getLastModifiedTime() : string {
        if(!$this->exists()){
            throw new FileNotFoundException("File doesn't exist {{$this->getName()}}");
        }
        
        $lastModifiedTime = filemtime($this->getName());
        $formattedTime = date(STANDARD_TIMESTAMP_FORMAT, $lastModifiedTime);
        
        return $formattedTime;
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\accords\FileAccess::getContentsAsCollection()
     */
    public function getContentsAsCollection() : Collections {
        return StringUtil::getLinedString($this->getContents());
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\accords\FileAccess::write()
     */
    public function write($content, $mode = "a"){
        if($this->isDirectory()){
            throw new IOException("No contents could be saved to directory {{$this->getName()}}");
        }
        
        if($this->isURL()){
            throw new IOException("Cannot write/ammend a content to a provided URL");
        }
        
        if($this->isClosed()){
            throw new IOException("Cannot retrieve contents when the file is already closed");
        }
        
        if(!in_array($mode, ["x", "x+", "r+", "a", "a+", "w", "w+"])){
            throw new IOException("Invalid File Mode for writing: {{$mode}}");
        }
        
        $this->initOrUpdateHandler($mode);
        
        if($this->handle){
            if(is_string($content)){
                fwrite($this->handle, $content);
            }
            
            if(is_array($content)){
                foreach($content as $line){
                    fwrite($this->handle, $line . PHP_EOL);
                }
            }
            
            return true;
        }
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\accords\FileAccess::moveTo()
     */
    public function moveTo(String $destination, String $newName = null, bool $createDir = false) : bool {
        if($this->isClosed()){
            throw new IOException("Moving file cannot be done as the file handler is already closed");
        }
        
        if($this->isURL()){
            throw new IOException("Cannot move the file provided by the URL");
        }
        
        if(!$this->exists()){
            throw new FileNotFoundException("Moving file cannot be done as File doesn't exist. {{$this->getName()}}");
        }
        
        $slashed_basename = "/{$this->getBasename()}";
        
        if(!is_blank($newName)){
            $slashed_basename = "/{$newName}";
        }
        
        $destination_dir = StringUtil::endsWith($destination, $slashed_basename)
        ? substr($destination, 0, strpos($destination, $slashed_basename))
        : $destination;
        
        if($createDir && !@file_exists($destination_dir)){
            @mkdir($destination_dir, 0777, true);
        }
        
        // checks if the destination directory exists
        if(@file_exists($destination_dir)){
            if(!StringUtil::endsWith($destination, $slashed_basename)){
                $destination = "{$destination}{$slashed_basename}";
            }
        }else{
            throw new FileNotFoundException("The destination directory doesn't exist: {{$destination_dir}}");
        }
        
        /**
         *
         * even though this function has the validator for destination existence,
         * I still considered writing its own validation to display the error messages
         * with the series of exception types to be more specific.
         *
         */
        if(rename($this->getName(), $destination)){
            $this->name = $destination;
            return true;
        }
        
        return false;
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\accords\FileAccess::renameTo()
     */
    public function renameTo($newFileName) : bool {
        if($this->isClosed()){
            throw new IOException("Renaming a file cannot be done as the file handler is already closed");
        }
        
        if($this->isURL()){
            throw new IOException("Cannot rename the file provided by the URL");
        }
        
        if(!$this->exists()){
            throw new FileNotFoundException("Renaming a file cannot be done as File doesn't exist. {{$this->getName()}}");
        }
        
        $slashed_basename = "/{$this->getBasename()}";
        
        $directory = substr($this->getName(), 0, strpos($this->getName(), $slashed_basename));
        
        $newFileName = "{$directory}/{$newFileName}";
        
        if(rename($this->getName(), $newFileName)){
            $this->name = $newFileName;
            return true;
        }
        
        return false;
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\accords\FileAccess::copyTo()
     */
    public function copyTo($destination, $createDir = false) : bool {
        if($this->isClosed()){
            throw new IOException("Moving file cannot be done as the file handler is already closed");
        }
        
        if(!$this->isURL() && !$this->exists()){
            throw new FileNotFoundException("Copying file cannot be done as File doesn't exist. {{$this->getName()}}");
        }
        
        if($this->exists()){
            $slashed_basename = "/{$this->getBasename()}";
            
            $destination_dir = StringUtil::endsWith($destination, $slashed_basename)
            ? substr($destination, 0, strpos($destination, $slashed_basename))
            : $destination;
            
            if($createDir && !@file_exists($destination_dir)){
                @mkdir($destination_dir, 0777, true);
            }
            
            // checks if the destination directory exists
            if(@file_exists($destination_dir)){
                if(!StringUtil::endsWith($destination, $slashed_basename)){
                    $destination = "{$destination}{$slashed_basename}";
                }
            }else{
                throw new FileNotFoundException("The destination directory doesn't exist: {{$destination_dir}}");
            }
            
            return copy($this->getName(), $destination);
        }else if($this->isURL()){
            /*
             * keeps the original filename here so that we cannot lost its track
             * when writing a new file for the retrieved content provided by the URL
             *
             */
            $tempname = $this->getName();
            
            $urlContent = $this->getContents();
            
            /*
             *
             * the destination will be temporarily serving as the primary filename
             * where the retrieved contents from the URL will be stored. because we'll
             * just create a new file rather than copying it.
             *
             */
            $this->name = $destination;
            
            $this->write($urlContent);
            
            /*
             *
             * moving back to the original filename
             */
            $this->name = $tempname;
            
            return true;
        }
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\accords\FileAccess::delete()
     */
    public function delete() : bool {
        if($this->isClosed()){
            throw new IOException("File deletion cannot be done as the file handler is already closed");
        }
        
        if($this->isURL()){
            throw new IOException("File deletion cannot be made as the resource was provided by the URL");
        }
        
        if(!$this->exists()){
            throw new FileNotFoundException("File Deletion cannot be done as File doesn't exist. {{$this->getName()}}");
        }
        
        if(!$this->isDirectory()){
            if(!is_null($this->handle)){
                fclose($this->handle);
            }
            unlink($this->getName());
        }else{
            $this->deleteDirectory($this->getName());
        }
        
        $this->closed = true;
        
        return true;
    }
    
    /**
     * deletes the directory recursively
     *
     * @return boolean
     */
    private function deleteDirectory($dir) {
        if(!is_dir($dir)){
            return false;
        }
        
        // Open the directory and start looping through its contents
        $handle = opendir($dir);
        
        while(($file = readdir($handle)) !== false){
            if($file != '.' && $file != '..') {
                $filePath = "{$dir}/{$file}";
                
                // If the element is a file, delete it
                if(is_file($filePath)){
                    unlink($filePath);
                }else if(is_dir($filePath)){
                    // If the element is a directory, recursively delete it
                    $this->deleteDirectory($filePath);
                }
            }
        }
        
        closedir($handle);
        
        return rmdir($dir);
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\accords\FileAccess::isURL()
     */
    public function isURL() : bool {
        $pattern = '/^(https?|ftp):\/\/[^\s\/$.?#].[^\s]*$/i';
        
        if(preg_match($pattern, $this->getName())){
            return true;
        }
        
        return false;
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\accords\FileAccess::isClosed()
     */
    public function isClosed() : bool {
        return $this->closed;
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\accords\FileAccess::isDirectory()
     */
    public function isDirectory() : bool {
        return is_dir($this->getName());
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\accords\FileAccess::isFile()
     */
    public function isFile() : bool {
        return !$this->isDirectory();
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\accords\FileAccess::listFiles()
     */
    public function listFiles(bool $recursive = false) : Collections {
        $files = collect();
        
        if(!$this->isDirectory()){
            return $files;
        }
        
        foreach(array_diff(scandir($this->getName()), [".", ".."]) as $filepath){
            $file = new File("{$this->getName()}/{$filepath}");
            
            if($recursive){
                if($file->isDirectory()){
                    $this->recursiveFileScanning($file, $files);
                    continue;
                }
            }
            
            $files->add($file);
        }
        
        return $files;
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\accords\FileAccess::archived()
     */
    public function archived(){
        if(!$this->isDirectory()){
            throw new IOException("Cannot be archived. The file must be a directory");
        }
        
        if(!$this->exists()){
            throw new FileNotFoundException("Archiving cannot be done as the directory doesn't exist: {$this->getBasename()}");
        }
        
        if(extension_loaded("zip")){
            $zip = new ZipArchive();
            
            $zipFileName = "{$this->name}.zip";
            
            /*
             *
             * when archived file already exists, the zip file name will have a dated pattern
             * after the base folder name: e.g. folder_YmdHis.zip
             *
             */
            if(@file_exists($zipFileName)){
                $zipFileName = "{$this->name}_".date("YmdHis").".zip";
            }
            
            if($zip->open($zipFileName, ZipArchive::CREATE) === true){
                $dirIterator = new RecursiveDirectoryIterator($this->name);
                $iterator = new RecursiveIteratorIterator($dirIterator);
                
                foreach($iterator as $file){
                    if(!$file->isDir()){
                        $relativePath = substr($file->getPathname(), strlen($this->name) + 1);
                        $zip->addFile($file->getPathname(), $relativePath);
                    }
                }
                
                $zip->close();
                
                return $zipFileName;
            }
            
            return false;
        }
        
        return null;
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\accords\FileAccess::isArchived()
     */
    public function isArchived() : bool {
        if($this->isDirectory()){
            return false;
        }
        
        $archivedExts = [".zip", ".tar", ".gz", "tar.gz", ".tgz", ".rar"];
        
        if(in_array($this->getExtensionName(), $archivedExts)){
            return true;
        }
        
        return false;
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\accords\FileAccess::extract()
     */
    public function extract($to = null){
        if($this->isDirectory()){
            throw new IOException("Archive Extraction Error: File is a directory");
        }
        
        if(!$this->exists()){
            throw new FileNotFoundException("Extraction cannot be done as file doesn't exist: {$this->getBasename()}");
        }
        
        $basename = $this->getBasename();
        
        if(is_blank($to)){
            $to = str_replace($basename, "", $this->name);
        }
        
        $ext = $this->getExtensionName();
        
        if($this->isArchived()){
            $lastPosOfExt = strrpos($basename, $ext);
            
            $to = "{$to}".substr($basename, 0, $lastPosOfExt);
        }else{
            throw new IOException("Invalid Archived File ({$ext}).");
        }
        
        /*
         *
         * this indicates that the archived directory is already extracted.
         *
         */
        if(@file_exists($to)){
            return 2;
        }
        
        if(extension_loaded("zip")){
            $zip = new ZipArchive();
            
            if($zip->open($this->name) === TRUE){
                $zip->extractTo($to);
                $zip->close();
                
                return true;
            }
            
            return false;
        }
        
        return null;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\accords\FileAccess::require()
     */
    public function require() : mixed {
        if($this->isURL()){
            throw new IOException("File is a URL data loader");
        }
        
        if(!$this->exists()){
            throw new IOException("Cannot require non-existent file: {$this->name}");
        }
        
        if($this->isDirectory()){
            throw new IOException("Cannot require a directory");
        }
        
        return require $this->name;
    }
    
    /**
     *
     * @param File $directory
     * @param Collections $files
     */
    protected function recursiveFileScanning(File $directory, Collections $files){
        $files->add($directory);
        
        $directory->listFiles()->each(function(File $file) use ($files){
            if($file->isDirectory()){
                $this->recursiveFileScanning($file, $files);
            }else{
                $files->add($file);
            }
        });
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\accords\FileAccess::close()
     */
    public function close() : void {
        if(@is_null($this->handle)){
            goto closing;
        }
        
        if(!$this->isClosed()){
            if($this->isDirectory()){
                @closedir($this->handle);
            }else{
                @fclose($this->handle);
            }
            
            closing:
            $this->closed = true;
        }
    }
    
    /**
     *
     * {@inheritDoc}
     * @see IteratorAggregate::getIterator()
     */
    #[\ReturnTypeWillChange]
    public function getIterator() {
        if(!$this->isDirectory()){
            throw new IOException("File object cannot be iterated. Not a directory.");
        }
        
        return new ArrayIterator(
            $this->listFiles(true)->toArray());
    }
    
    /**
     *
     * {@inheritDoc}
     * @see Countable::count()
     */
    #[\ReturnTypeWillChange]
    public function count(){
        if(!$this->isDirectory()){
            throw new IOException("File object cannot be iterated. Not a directory.");
        }
        
        return $this
        ->listFiles(true)
        ->count();
    }
}