<?php
namespace app\providers;

use ReflectionClass;
use lib\inner\ApplicationGate;
use lib\inner\arrangements\ServiceProvider;
use lib\instances\CommandInstance;
use lib\util\Collections;
use lib\util\GateFinder;
use lib\util\annotation\Annotation;
use lib\util\annotation\AnnotationProperty;
use lib\util\annotation\AnnotationUtil;
use lib\util\cli\Console;
use lib\util\controller\AnnotatedControllerMethodLocator;
use lib\util\cli\CommandBean;
use lib\inner\App;

class CommandAnnotationServiceProvider extends ServiceProvider{
    
    protected $forCLI = true;
    
    /**
     * 
     * @var AnnotatedControllerMethodLocator
     */
    protected $locator;
    
    /**
     * 
     * @param ApplicationGate $gate
     */
    public function boot(ApplicationGate $gate){
        $this->scanAnnotatedControllerMethods($gate);
        $this->scanAnnotatedCommandInstances();
    }
    
    /**
     * 
     * get commands from annotated controller methods
     * 
     * @param ApplicationGate $gate
     */
    protected function scanAnnotatedControllerMethods($gate){
        $this->locator = $gate->make(AnnotatedControllerMethodLocator::class);
        
        // find annotated controller methods
        (new GateFinder(relative_path(preferences("structure")->get("controllers")), true))->load(function($class){
            $this->locator->addController($class);
        });
            
        $this->locator->scanQualifiedQueries();
        
        $this->locator->getQueries()->each(function(Collections $annotations, $query){
            $this->readControllerCommandAnnotation($annotations, $query);
        });
    }
    
    /**
     *
     * this holds the value of base namespace for middlewares globally
     *
     * @var string
     */
    protected $globalNamespace;
    
    /**
     * 
     * @param Collections $annotations
     * @param mixed $query
     */
    protected function readControllerCommandAnnotation($annotations, $query){
        if(collect(explode("@", $query))->count() < 2){
            $topAnnotations = $this->locator->getQueries()->get($query);
            
            $this->globalNamespace = $topAnnotations->get("@MidClassNS");
            
            return;
        }
        
        $command = $annotations->get("@Command");
        $cmddesc = $annotations->get("@CommandDesc");
        $middleware = $annotations->get("@Middleware");
        $mdcbasensp = $annotations->get("@MidClassNS") ?: $this->globalNamespace;
        
        // initalizing the base namespace value for middlewares
        $mdcbasensp = $mdcbasensp ? $mdcbasensp->get("base") : false;
        
        if(!$command){
            return;
        }
        
        $cmdname = $command->get("name");
        
        if($cmddesc){
            $cmddesc = $cmddesc->get("desc") ?: "No description available.";
        }
        
        $commandBean = Console::command($command->get("keywords"), $query)->details([
            "name" => $cmdname ?: "Unnamed",
            "desc" => $cmddesc
        ]);
        
        if(!is_blank($middleware)){
            $this->alignMiddlewares($middleware, $mdcbasensp, $commandBean);
        }
    }
    
    /**
     * 
     * @param Collections $middleware
     * @param string $mdcbasensp
     * @param CommandBean $commandBean
     */
    protected function alignMiddlewares($middleware, $mdcbasensp, CommandBean $commandBean){
        $middlewares = [];
        
        foreach($middleware as $reusedmid){
            $baseNamespace = $reusedmid->get("baseNS");
            $classes = $reusedmid->get("classes");
            
            // if a classname was found as a string type (not an array)
            if(is_string($classes)){
                $stringed = $this->middlewareNamespacing($classes,
                    $mdcbasensp,
                    $baseNamespace);
                
                $this->appendIfNotStored($middlewares, $stringed);
                
                continue;
            }
            
            // default
            foreach($classes as $class){
                $class = $this->middlewareNamespacing($class,
                    $mdcbasensp,
                    $baseNamespace);
                
                $this->appendIfNotStored($middlewares, $class);
            }
        }
        
        $commandBean->middlewares($middlewares);
    }
    
    /**
     *
     * @param string $class
     * @param string $midbasensp
     * @param string $baseNamespace
     *
     * @return string|mixed
     */
    protected function middlewareNamespacing($class, $midbasensp, $baseNamespace){
        $definite = $class;
        
        // if defined, this will be the default base namespace
        // for middleware classes
        if($midbasensp){
            $definite = "{$midbasensp}\\{$class}";
        }
        
        // this explicitly sets its own base namespace for the set of
        // middleware objects within the '@Middleware' annotation
        if($baseNamespace){
            $definite = "{$baseNamespace}\\{$class}";
        }
        
        return $definite;
    }
    
    /**
     * eliminates duplicate values within strings
     *
     * @param array $data
     * @param mixed $toAppend
     */
    protected function appendIfNotStored(array &$data, $toAppend){
        if(!in_array($toAppend, $data)){
            $data[] = $toAppend;
        }
    }
    
    /**
     * 
     * get commands from annotated command instances.
     * 
     */
    protected function scanAnnotatedCommandInstances(){
        (new GateFinder("app/instances/commands", true))->load(function($cmdinst){
            if(!App::checkInheritance($cmdinst, CommandInstance::class)){
                return;
            }
            
            $rc = new ReflectionClass($cmdinst);
            
            $annotation = (new Annotation("Commands"))
                ->setProperty(new AnnotationProperty("keywords", "array", true));
            
            $properties = [];
                
            if(AnnotationUtil::hasAnnotation($rc, $annotation, $properties)){
                Console::command($properties->get("keywords"), $cmdinst);
            }
        });
    }
}