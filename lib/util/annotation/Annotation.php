<?php
namespace lib\util\annotation;

use lib\util\Collections;

class Annotation {
    
    /**
     * 
     * @var string
     */
    protected $name;
    
    /**
     * 
     * @var Collections
     */
    protected $properties;
    
    /**
     * 
     * @var boolean
     */
    protected $reusable = false;
    
    /**
     * 
     * @param String $name
     */
    public function __construct($name){
        $this->name = $name;
        $this->properties = collect();
    }
    
    /**
     * 
     * @param AnnotationProperty|array $property
     * @return \lib\util\annotation\Annotation
     */
    public function setProperty($property){
        if(is_array($property)){
            $property = new AnnotationProperty(...$property);
        }
        
        $this->properties->put($property->getName(), $property);
        
        return $this;
    }
    
    /**
     * 
     * sets an annotation to be reusable
     * 
     * @param bool $reusable
     * @return \lib\util\annotation\Annotation
     */
    public function setReusable(bool $reusable){
        $this->reusable = $reusable;
        
        return $this;
    }
    
    /**
     * 
     * determines if an annotation can be used several times
     * 
     * @return boolean
     */
    public function isReusable(){
        return $this->reusable;
    }
    
    /**
     * 
     * @return string
     */
    public function getName(){
        return "@{$this->name}";
    }
    
    /**
     * 
     * @return \lib\util\Collections
     */
    public function getProperties(){
        return $this->properties;
    }
    
    /**
     * 
     * @return string
     */
    public function __toString(){
        return $this->getName();
    }
}