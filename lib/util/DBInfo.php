<?php
namespace lib\util;

class DBInfo {
        
    /**
     * 
     * @return boolean|mixed
     */
    public static function getDatabaseEngine(){
        $info = collect(DB_INFO);
        
        return $info->get("DB-Engine");
    }
    
    /**
     * 
     * @return boolean|mixed
     */
    public static function getCurrentlyUsedDatabase(){ 
        $info = collect(DB_INFO);
        
        return $info->get("Current-Database");
    }
    
    /**
     * 
     * @return boolean
     */
    public static function isSuccessfullyConnected(){
        return defined("DB_INFO");
    }
}