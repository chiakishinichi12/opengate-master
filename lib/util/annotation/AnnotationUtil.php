<?php
namespace lib\util\annotation;

use InvalidArgumentException;
use ReflectionClass;
use ReflectionFunction;
use ReflectionMethod;
use ReflectionProperty;
use lib\inner\App;
use lib\util\Collections;

class AnnotationUtil {
    
    /**
     * 
     * checks if reflective object has an annotation
     * 
     * @param mixed $reflection
     * @param mixed $annotation
     * @throws InvalidArgumentException
     */
    public static function hasAnnotation($reflection, $annotation, &$properties = null){
        $reflectionClasses = [
            ReflectionClass::class,
            ReflectionProperty::class,
            ReflectionMethod::class,
            ReflectionFunction::class
        ];
        
        if(!in_array(class_name($reflection), $reflectionClasses)){
            throw new InvalidArgumentException("Argument#1: value is not a Reflection object");
        }
        
        if(! $annotation instanceof Annotation){
            if(is_string($annotation)){
                $annotation = new Annotation($annotation);
            }else{
                throw new InvalidArgumentException("Argument#2: value is not an instance of Annotation class");
            }
        }
        
        $parser = App::make(AnnotationParser::class)
            ->reference($reflection->getDocComment())
            ->annotations([$annotation])
            ->parse();
        
        if($annotation->getProperties()->count()){
            if(!is_null($properties)){
                $properties = $parser->getParsedValues()->get($annotation->getName());
            }
            
            return $parser->getParsedValues()->hasKey($annotation->getName());
        }else{
            return $parser->getParsedValues()->has($annotation->getName(), function($annot){
                if($annot instanceof Annotation){
                    return $annot->getName();
                }
            });
        }
    }
    
    /**
     * 
     * @param mixed $reflection
     * @param Annotation $annotation
     * 
     * @return Collections|null
     */
    public static function loadAnnotation(mixed $reflection, Annotation $annotation){
        $properties = collect();
        
        if(!self::hasAnnotation($reflection, $annotation, $properties)){
            return null;
        }
        
        return $properties;
    }
}