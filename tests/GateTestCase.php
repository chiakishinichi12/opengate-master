<?php
namespace tests;

use lib\inner\ApplicationGate;
use lib\inner\arrangements\console\Kernel;
use lib\util\cli\unittest\GateTestCore;

class GateTestCase extends GateTestCore{
   
    /**
     * 
     * {@inheritDoc}
     * @see \PHPUnit\Framework\TestCase::setUp()
     */    
    protected function setUp() : void {
        $this->warpApplicationGate()->context($this);
        
        $this->bootstrap();
    }    
    
    /**
     * 
     * @return ApplicationGate
     */
    public function warpApplicationGate(){
        $gate = require_once __DIR__."/../startup/gate.php";
        
        if(is_bool($gate)){
            return gate();
        }
        
        $gate->make(Kernel::class);
        
        return $gate;
    }
}