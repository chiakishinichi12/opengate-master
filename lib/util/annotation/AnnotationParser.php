<?php
namespace lib\util\annotation;

use ReflectionClass;
use ReflectionClassConstant;
use ReflectionFunction;
use ReflectionMethod;
use ReflectionProperty;
use lib\inner\App;
use lib\util\Collections;
use lib\util\EncapsulationHelper;
use lib\util\exceptions\AnnotationException;

/**
 * 
 * @author Jiya Sama
 *
 */
class AnnotationParser {
    
    /**
     * 
     * @var string
     */
    protected $reference;
    
    /**
     * 
     * @var array
     */
    protected $annotations;
    
    /**
     * 
     * @var Collections
     */
    protected $parsed;
    
    /**
     * 
     * @var Collections
     */
    protected $reusables;
    
    /**
     * 
     * @var EncapsulationHelper
     */
    protected $encapsulation;
    
    /**
     * 
     * 
     * constructtor
     * 
     */
    public function __construct(){
        $this->reusables = collect();
        $this->parsed = collect();
        
        $this->encapsulation = App::make(EncapsulationHelper::class)->instance($this);
    }
    
    /**
     * 
     * @param mixed $reference
     * 
     * @return mixed|object
     */
    public function reference(mixed $reference = null){
        return $this->encapsulation->propertyDefinition($this->{__FUNCTION__}, $reference);
    }
    
    /**
     * 
     * @param array $annotations
     * 
     * @return mixed|object
     */
    public function annotations(array $annotations = null){
        return $this->encapsulation->propertyDefinition($this->{__FUNCTION__}, $annotations);
    }
    
    /**
     * 
     * checks if properties are properly defined
     * 
     * @throws AnnotationException
     */
    protected function validateProperties(){
        /**
         * 
         * An annotation must only have 1 default property, otherwise will throw an exception
         */
        collect($this->annotations)->each(function(Annotation $annotation){
            $defaults = 0;
            
            $annotation->getProperties()->each(function(AnnotationProperty $property) use (&$defaults){
                if($property->isDefault()){
                    $defaults++;
                }
            });
            
            if($defaults > 1){
                throw new AnnotationException(
                    "Annotation '{$annotation->getName()}' have 2 or more default properties.");        
            }
        });
    }
    
    /**
     * 
     * parses annotation formatted string
     * 
     */
    public function parse(){
        $this->validateProperties();
        
        $matches = [];
        
        $reflections = [
            ReflectionClass::class, 
            ReflectionClassConstant::class,
            ReflectionMethod::class, 
            ReflectionProperty::class,
            ReflectionFunction::class,
        ];
        
        if(App::checkInheritance($this->reference, $reflections)){   
            $this->reference = $this->reference->getDocComment();
        }
                
        $matched = preg_match_all("/@([a-zA-Z]+)\s*(?:\(([^()]*(?:\((?2)\)[^()]*)*)\))?/s", 
            $this->reference, 
            $matches, 
            PREG_SET_ORDER);
        
        if($matched){
            collect($matches)->each(function($matched){
                $annotstr = $matched[0];
                
                if(!$this->hasParenthesesInAnnotation($annotstr)){
                    $matched = trim(str_replace("*", "", $annotstr));
                }
                
                $this->resolve($annotstr);
            });
        }
        
        // arranges reusable annotations within the main stack
        if($this->reusables->count()){
            $this->reusables->each(function($reusables, $annotationName){
                $this->parsed->put($annotationName, $reusables);
            });
            
            $this->reusables->clear();
        }
        
        return $this;
    }
        
    /**
     * 
     * @return \lib\util\Collections
     */
    public function getParsedValues(){
        return $this->parsed;
    }
    
    /**
     * 
     * @param string $keyAndValueFormattedString
     * @return array|boolean
     */
    protected function getKeyAndValuePair(string $keyAndValueFormattedString){
        $inKeyAndValueFormat = '/^\s*([a-zA-Z0-9_]+)\s*=\s*([^=]+)\s*$/';
        
        if(preg_match($inKeyAndValueFormat, $keyAndValueFormattedString)){
            return explode("=", $keyAndValueFormattedString);
        }
        
        return false;
    }
    
    /**
     * 
     * @param Collections $properties
     * @return NULL|unknown
     */
    protected function getDefaultProperty($properties){
        $default = null;
        
        $properties->each(function($item) use (&$default){
            if($item->isDefault()){
                $default = $item;
            }
        });
        
        return $default;
    }
    
    /**
     * 
     * @param string $annotation
     * @return boolean
     */
    protected function hasParenthesesInAnnotation($annotation) {
        return strpos($annotation, "(") !== false && strpos($annotation, ")") !== false;
    }
    
    /**
     * 
     * @param string $propertyName
     * @param Collections $properties
     * 
     * @return mixed|AnnotationProperty
     */
    protected function findExpectedAnnotationProperty(string $propertyName, Collections $properties){
        $found = collect($properties->keySet())
            ->binarySearch($propertyName)
            ->get();
                
        if(!is_null($found)){
            return $properties->get($found->value());
        }
        
        return null;
    }
    
    /**
     * 
     * @param Annotation $expectedAnnotation
     * @param AnnotationProperty $property
     * @param Array $pair
     * 
     * @throws AnnotationException
     */
    protected function checkTypeAndStoreValue($expectedAnnotation, $property, $pair){
        $returnedValue = App::evaluate("return {$pair[$property->getName()]}");
        $typeOfValue = gettype($returnedValue);
        
        $invalid_type = false;
        
        if(strcasecmp($typeOfValue, "null") === 0){
            goto init;
        }
        
        if(is_array($property->getType())){
            if(!in_array($typeOfValue, $property->getType())){
                $invalid_type = true;
                $property_types = "[".implode(", ", $property->getType())."]";
            }
        }else if(strcmp($typeOfValue, $property->getType()) !== 0){
            $invalid_type = true;
            $property_types = $property->getType();
        }
        
        if($invalid_type){
            throw new AnnotationException("[{$expectedAnnotation->getName()}] The returned value type doesn't match to the expected type(s)"
                ."{{$property_types}}. {{$typeOfValue}} found");
        }
        
        init:
        $pair[$property->getName()] = $returnedValue;
        
        if($this->parsed->hasKey($expectedAnnotation->getName())){
            $existing = $this->parsed->get($expectedAnnotation->getName());
            $existing->merge($pair);
            
            $this->parsed->put($expectedAnnotation->getName(), $existing);
        }else{
            $this->parsed->put($expectedAnnotation->getName(), collect($pair));
        }
    }
    
    /**
     * 
     * this marks the annotation as reusable
     * 
     * @param Annotation $expectedAnnotation
     */
    protected function appendIfReusable($expectedAnnotation){
        if(!$expectedAnnotation->isReusable()){
            goto end;
        }
        
        $existing = collect();
        
        if($this->reusables->hasKey($expectedAnnotation->getName())){
            $existing = $this->reusables->get($expectedAnnotation->getName());
        }
                   
        // cloning the last item of the stack to make sure every item of 
        // a reusable annotation stack will be having its own memory address
        $lastItemOfStack = clone $this->parsed->get($expectedAnnotation->getName());
        
        // adding the cloned item to the nested stack of 'reusable' stack
        $existing->add($lastItemOfStack);
        
        $this->reusables->put($expectedAnnotation->getName(), $existing);
        
        end:
    }
    
    /**
     * splits the words using comma, but when a comma is inside the square bracket, 
     * it will be excluded from splitting
     * 
     * @param string $string
     * @return Collections
     */
    protected function customCommaSplit($string) {
        $result = [];
        $inBracket = false;
        $inQuote = false;
        $current = "";
        
        for ($i = 0; $i < strlen($string); $i++) {
            $char = $string[$i];
            
            if ($char === "[") {
                $inBracket = true;
                $current .= $char;
            } elseif ($char === "]") {
                $inBracket = false;
                $current .= $char;
            } elseif ($char === "\"") {
                $inQuote = !$inQuote; // Toggle whether we are inside a quoted string
                $current .= $char;
            } elseif ($char === "," && !$inBracket && !$inQuote) {
                // Split only if we're not inside brackets or quotes
                $result[] = trim($current, " \t\n\r\0\x0B");
                $current = "";
            } else {
                $current .= $char;
            }
        }
        
        // Add the last element
        $result[] = trim($current, " \t\n\r\0\x0B");
        
        return collect($result);
    }
    
    /**
     * 
     * @param string $matched
     * @param array $annotations
     * @throws AnnotationException
     */
    protected function resolve($matched){
        $annotationName = trim(preg_replace("/\([^()]*+(?:(?R)[^()]*)*+\)/", "", $matched));
        
        $annotations = collect($this->annotations);
        
        $toCompare = function($annotation){
            return $annotation->getName();
        };
        
        $expectedAnnotation = $annotations->binarySearch($annotationName)
            ->toCompare($toCompare)
            ->get();
            
        if(!is_null($expectedAnnotation)){
            $parenthesis = [];
            
            if(preg_match_all("/\([^()]*+(?:(?R)[^()]*)*+\)/", $matched, $parenthesis)){
                $this->resolveParenthesis($expectedAnnotation->value(), $parenthesis);
            }else{
                $this->parsed->put($expectedAnnotation->getName(), $expectedAnnotation->value());
            }
        }
    }
    
    /**
     * 
     * @param array $parenthesis
     * @return Collections
     */
    protected function pairStack(array $parenthesis){        
        $pstring = $parenthesis[0][0];
        
        $propvals = $this->customCommaSplit(substr($pstring, 1, strlen($pstring) - 2));
        
        $stack = collect();
            
        foreach($propvals as $prop){
            if($pair = $this->getKeyAndValuePair($prop)){
                $stack->put($pair[0], $pair[1]);
            }else{
                $stack->add($prop);
            }
        }
        
        return $stack;
    }
    
    /**
     * 
     * @param string $annotationName
     * @param Annotation $expectedAnnotation
     * @param array $parenthesis
     * @throws AnnotationException
     */
    protected function resolveParenthesis(Annotation $expectedAnnotation, $parenthesis){        
        $pairings = $this->pairStack($parenthesis);
        
        // fills all unused properties with 'null'
        foreach($expectedAnnotation->getProperties() as $property){
            if(!collect($pairings->keySet())->has($property->getName())){
                if($property->isDefault()){
                    continue;
                }
                
                $this->checkTypeAndStoreValue(
                    $expectedAnnotation, 
                    $property, 
                    [$property->getName() => null]);
            }
        }
                
        if($length = $pairings->length()){
            switch($length){
                case 1:
                    $pkset = $pairings->keySet();
                    
                    if(is_string($key = $pkset[0])){
                        $property = $this->findExpectedAnnotationProperty($key, $expectedAnnotation->getProperties());
                        
                        if(!is_null($property) && $property instanceof AnnotationProperty){
                            $this->checkTypeAndStoreValue($expectedAnnotation, $property, [$key => $pairings->get($key)]);
                        }else{
                            throw new AnnotationException("Undefined Annotation Property for {$expectedAnnotation->getName()}: {{$key}}");
                        }
                    }else{
                        if(is_null($default = $this->getDefaultProperty($expectedAnnotation->getProperties()))){
                            throw new AnnotationException("No default property for {$expectedAnnotation->getName()}");
                        }
                        
                        $this->checkTypeAndStoreValue($expectedAnnotation, $default,
                            [$default->getName() => $pairings->get(0)]);
                    }
                    break;
                default:                    
                    foreach($pairings as $key => $value){
                        if(is_numeric($key)){
                            throw new AnnotationException("Unmatched Pattern {{$value}}: not a key=value pair");
                        }
                        
                        $property = $this->findExpectedAnnotationProperty($key, $expectedAnnotation->getProperties());
                        
                        if(!is_null($property)){
                            $thepair = [$key => $value];
                            
                            $this->checkTypeAndStoreValue($expectedAnnotation, $property, $thepair);
                        }else{
                            throw new AnnotationException("Undefined Annotation Property for {$expectedAnnotation->getName()}: {{$key}}");
                        }
                    }
                    break;
            }
            
            // will leave a mark to the iterated annotation as reusable if true
            $this->appendIfReusable($expectedAnnotation);            
        }
    }
}