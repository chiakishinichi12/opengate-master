<?php
namespace lib\inner\arrangements\http;

use Closure;
use InvalidArgumentException;
use lib\util\StringUtil;
use lib\util\validator\Validator;
use lib\inner\App;
use lib\inner\OpenObject;
use lib\inner\json\JsonUtility;

class HttpValidator extends Validator{
        
    /**
     * 
     * @Ignore
     * 
     * Handles the API call response, providing a standardized JSON output.
     * 
     * If the API call is intercepted (as determined by the `isIntercepted` method),
     * it sends a 400 HTTP status code and halts further execution, outputting the error
     * response. Otherwise, it just prints nothing.
     * 
     * @return HttpValidator
     */
    public function responseToApiCall(){
        if(!$this->isIntercepted()){
            goto ends;
        }
        
        response(400);
        
        $result = [
            "status_code" => response()->statusCode(),
            "invalidations" => $this->failures()
        ];
        
        out()->haltln(JsonUtility::toEncode($result));
        
        ends:
        return $this;
    }
    
    /**
     * 
     * @Ignore
     * 
     * @param Closure $with
     * 
     * returns 'false' if no invalidations, otherwise, will return a redirection 
     * going back to the previous page with invalidation remarks and inputted data.
     * 
     * @return HttpValidator
     */
    public function redirectBack(Closure $with = null){
        if(!$this->isIntercepted()){
            goto ends;
        }
        
        $redirector = response()->redirect()
            ->with("og.validator", [
                "instance" => $this,
                "inputted" => App::make(OpenObject::class)->store(request()->post())
            ]);
            
        if(!is_null($with)){
            $with($redirector);
        }
        
        $redirector->back()->send();
        
        ends:
        return $this;
    }
    
    /**
     * 
     * @Ignore
     * 
     * a validator response that will depend on the currently-used instance type.
     * will just echo json string if it's API. otherwise, redirection
     * 
     * @param Closure $with - this will be only useful if redirection is set to utilization
     * 
     * @return HttpValidator
     */
    public function reflectIntercept(Closure $with = null){
        switch($this->core->instanceType()){
            case "page":
                return $this->redirectBack($with);
            case "api":
                return $this->responseToApiCall();
        }
    }
    
    /**
     * 
     * @Ignore
     * 
     * returns 'false' if the interception occurred. otherwise, 
     * will proceed to redirection with a successful remark
     * 
     * @param string $uri
     * @param Closure $with
     * 
     * @return boolean|Redirector
     */
    public function redirectSuccess(string $uri, Closure $with = null){
        if($this->isIntercepted()){
            return false;
        }
        
        // with default message "Success"
        $redirector = response()->redirect($uri)
            ->with("og.validator.message", "Success");
        
        if(!is_null($with)){
            $with($redirector);
        }
        
        $redirector->send();
    }
    
    /**
     * 
     * @Ignore
     * 
     * @param string $param
     * @return boolean
     */
    protected function isFileParameter($param){
        return !is_null(request()->file($param));
    }
    
    /**
     * 
     * @Message("Please fill out this field")
     * 
     * @param string $param
     * @param mixed $value
     */
    protected function required($param, $value){
        if($this->isFileParameter($param)){
            if(!request()->containsFile($param)){                
                $this->invalid[$param][__FUNCTION__] = "No file uploaded";
            }
        }else{
            parent::required($param, $value);
        }
    }
    
    /**
     * 
     * @Message("Invalid file extension: %s")
     * 
     * @param string $extensions
     * @param string $param
     * @param mixed $value
     */
    protected function fileExtension($extensions, $param, $value){
        if(!$this->isFileParameter($param)){
            throw new InvalidParameterException("'{$param}' is not a file parameter");
        }
                
        if(!request()->containsFile($param)){
            return;
        }
        
        $extensions = explode(",", $extensions);
        
        $uext = StringUtil::extractExtensionName($value["name"]);
        
        if(!in_array($uext, $extensions, true)){
            $this->appendFormattedFailure($param, __FUNCTION__, $uext);
        }
    }
    
    /**
     *
     * @Message("Uploaded file exceeds the allowed max file size")
     *
     * @param int $size
     * @param string $param
     * @param mixed $value
     */
    protected function maxfsize($size, $param, $value){
        if(!$this->isFileParameter($param)){
            throw new InvalidParameterException("'{$param}' is not a file parameter");
        }
        
        if(!is_numeric($size)){
            throw new InvalidArgumentException("Argument#1: must be numeric");
        }
        
        if(!request()->containsFile($param)){
            return;
        }
        
        // Set the maximum file size limit (in bytes)
        $maxFileSize = doubleval($size) * 1024 * 1024; 
        $uplFileSize = $value["size"];
        
        if($uplFileSize > $maxFileSize){
            $this->appendFailure($param, __FUNCTION__);
        }
    }
}

