<?php

namespace lib\util\model;

use lib\traits\ModelCustom;
use lib\traits\Sanitation;
use lib\util\Collections;
use lib\util\exceptions\UnknownPropertyException;
use lib\util\model\relational\RelationalModel;


/**
 * 
 * OpenGate ambicious ORM
 * 
 * @author Jiya Sama
 * 
 * @method static \lib\util\collections\ModelCollections all()
 * @method static \lib\util\builders\SelectBuilder query()
 * @method static \lib\util\model\Model first(array $whereArgs = null, $fields = [])
 * @method static \lib\util\model\Model firstOrFail(array $whereArgs, $fields = [])
 * @method static \lib\util\model\Model firstOrNew(array $whereArgs, array $attributesInCaseItsNew = [])
 * @method static \lib\util\model\Model firstOrCreate(array $whereArgs, array $attributesInCaseItsNew = [])
 * @method static \lib\util\model\Model find(string $id, array $fields = [])
 * @method static \lib\util\model\Model findOrFail(string $id, array $fields = [])
 * @method static \lib\util\model\Model whereIn(string $column, array $possibilities)
 * @method static boolean create(array $colAndValuePair = [])
 * @method static boolean updateOrCreate(array $whereArgs, array $toModify)
 * @method static \lib\mocks\Mockery mockery()
 * @method static \lib\util\builders\SelectBuilder withTrashed()
 * @method static \lib\util\builders\SelectBuilder SelectBuilder onlyTrashed()
 * @method static \lib\util\model\Model chunkById(int $rowsPerLoop, $loadCallback, string $direction = "ASC")
 * @method static \lib\util\paginators\QueryPaginator paginate(int $rowsPerPage, int $page = 1)
 * @method static \lib\util\collections\ModelCollections with(string $relationship)
 * @method static \lib\util\builders\ModelQueryBuilder whereBelongsTo(Model $relation)
 * @method static \lib\util\builders\ModelQueryBuilder whereHas(string $relationship, \Closure $forBuilder = null, string $boolOp = "AND")
 */
class Model {
    
    use Sanitation, ModelCustom, RelationalModel;
    
    /**
     * 
     * @var Collections
     */
    protected $columns;
    
    /**
     * 
     * @var boolean
     */
    protected $stored = false; 
    
    /**
     * 
     * @var array
     */
    protected $outprops = [];
    
    /**
     * 
     * @var boolean
     */
    protected $is_view = false;
    
    /**
     * 
     * @param array $outprops
     */
    public function __construct(array $outprops = null){
        $this->initComponents($outprops);
    }
    
    /**
     * 
     * @param mixed $propname
     * 
     * @return bool
     */
    public function __isset(string $propname){
        return isset($this->outprops[$propname]);
    }
    
    /**
     * 
     * @param string $propname
     * @throws UnknownPropertyException
     */
    public function __get(string $propname){     
        $value = $this->retrievePropertyValue($propname);
        
        if(is_null($value)){
            throw new UnknownPropertyException("[".class_name($this)."] Unknown Model Property: '{$propname}'.");
        }
        
        return $value;
    }
    
    /**
     * 
     * @param string $propname
     * @param mixed $propvalue
     */
    public function __set(string $propname, mixed $propvalue){
        $value = $this->retrievePropertyValue($propname);
        
        if(!is_null($value)){
            $this->overwrite[] = $propname;
        }
        
        $this->outprops[$propname] = $propvalue;
    }
    
    /**
     * 
     * @param string $method
     * @param array $params
     * 
     * @return mixed
     */
    public function __call(string $method, array $params){        
        if(method_exists($this, $method)){            
            if($this->isAllowedProtectedMethod($method)){
                return $this->$method(...$params);
            }
        }
        
        return $this->builderInv($method, $params);
    }
    
    /**
     * 
     * @param string $method
     * @param array $params
     * 
     * @return mixed
     */
    public static function __callstatic(string $method, array $params){
        return (new static)->$method(...$params);
    }
    
    /**
     * 
     * 
     * @return array|string
     */
    public function __toString(){
        return print_r($this->getDefinedColumnVars(), true);
    }
}