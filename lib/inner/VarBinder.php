<?php
namespace lib\inner;

use lib\util\builders\SessionValue;
use lib\util\collections\CollectionItem;
use lib\util\nebula\ViewRenderer;
use lib\util\Collections;

trait VarBinder{
    
    /**
     * 
     * Stores bindings as an associative array.
     * 
     * @var array
     */
    protected $bindings = [];
    
    /**
     *
     * Binds a value to a name.
     * If the value is an instance of SessionValue or CollectionItem, 
     * it retrieves the actual value by calling `value()`.
     *
     * @param string $name
     * @param mixed $value
     */
    protected function bind(string $name, mixed $value){
        if(App::checkInheritance($value, [SessionValue::class, CollectionItem::class])){
            $value = $value->value();
        }
        
        $this->bindings[$name] = $value;
    }
    
    /**
     * 
     * Binds multiple values from an array or Collections instance.
     * Iterates over each item and calls bindIfDefined to only bind values that are not null.
     * 
     * @param Collections|array $stack
     */
    protected function bindSet(Collections|array $stack){
        if(is_array($stack)){
            $stack = collect($stack);
        }
        
        $stack->each(function($value, $name){
           $this->bindIfDefined($name, $value); 
        });
    }
    
    /**
     * Binds a value to a name only if the value is not null.
     *
     * @param string $name
     * @param mixed $value
     * 
     * @return boolean
     */
    protected function bindIfDefined(string $name, mixed $value){
        if(!is_null($value)){
            $this->bind($name, $value);
            
            return true;
        }
        
        return false;
    }
    
    /**
     * 
     * Retrieves the current bindings.
     * 
     * @return array
     */
    public function bindings(){
        return $this->bindings;
    }
    
    /**
     * Passes the current bindings to the view renderer for a specific view.
     *
     * @param string $viewPath
     *
     * @return ViewRenderer
     */
    protected function morph(string $viewPath){
        return morph($viewPath, $this->bindings);
    }
}

