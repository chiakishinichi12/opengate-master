<?php
namespace lib\util\nebula\interpretations;

use lib\util\exceptions\MorphInterpretationException;
use lib\util\Collections;

trait DirectiveThrowers {
    
    /**
     * 
     * explicitly requires an expression to provide on the directive
     * 
     */
    protected function requireParenthesis(){
        if(is_null($this->exp)){
            throw new MorphInterpretationException(
                "Parenthesis Specification is required for @{$this->name} directive");
        }
    }
    
    /**
     * 
     * splits a string to parse the expression as directive parameters
     * 
     * @param number $expects
     * @return Collections
     */
    protected function parameters($expects = 0){
        $params = collect(preg_split('/,(?=(?:[^"]*"[^"]*")*[^"]*$)/', $this->exp));
        
        if($expects > 0){
            if($params->length() < $expects){
                throw new MorphInterpretationException(
                    "@{$this->name} expects {$expects} or more parameters. {$params->length()} passed");
            }
        }
        
        return $params;
    }
    
    /**
     * 
     * evaluates if a parameter is a PHP expresstion (variable, property or method call), will throw an exception if it's not
     * 
     * @param string $string - Directive parameter
     * @param string $title - Exception title
     */
    protected function expectsPhpExpression($string, $title = "#var") {
        $matched = preg_match('/^(\$[a-zA-Z_\x80-\xff][a-zA-Z0-9_\x80-\xff]*(->(?:[a-zA-Z_\x80-\xff][a-zA-Z0-9_\x80-\xff]*|\w+\([^()]*\)))*|\w+\([^()]*\))$/', $string);
        
        if(!$matched){
            throw new MorphInterpretationException(
                "{@{$this->name}} {$title}: expected PHP expression (variable, property, method or function call). [{$string} given]");
        }
    }
}