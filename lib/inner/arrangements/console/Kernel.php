<?php
namespace lib\inner\arrangements\console;

use lib\inner\ApplicationGate;
use lib\util\cli\Console;
use lib\util\file\FileUtil;

class Kernel{
    
    /**
     * 
     * @var ApplicationGate
     */
    protected $gate;
    
    /**
     * 
     * @var array
     */
    protected $bootstrappers = [
        \lib\startup\HandleExceptions::class,
        \lib\startup\General::class,
        \lib\startup\BootCommandLineUtils::class,
        \lib\startup\ProviderHandler::class
    ];
    
    /**
     * 
     * @param ApplicationGate $gate
     */
    public function __construct(ApplicationGate $gate){
        $this->gate = $gate;
        
        $this->bootstrap();
    }
    
    /**
     *
     * bootstrapping function
     *
     */
    protected function bootstrap(){
        if(!$this->gate->hasBeenBootstrapped()){
            $this->gate->bootstrapWith($this->bootstrappers);
        }
    }
    
    /**
     * 
     * handles termination callback
     * 
     */
    public function terminate(){
        $this->gate->terminate();
    }
    
    /**
     * 
     * collects arguments inputted using CLI
     * 
     * @return mixed
     */
    public function captureArguments(){
        FileUtil::requireIfExists(base_path("routes/align_cli.php"));
        
        return Console::captureArguments();
    }
}