<?php

namespace lib\instances;

use lib\inner\WebAppCore;

abstract class APIInstance extends WebAppCore {
        
    /**
     * 
     * @param array $config
     */
    public function __construct(){
        parent::__construct();
    }
    
    /**
     * 
     * validating request
     */
    public function inspect(){
        $this->onSysCheck("api");
    }
}