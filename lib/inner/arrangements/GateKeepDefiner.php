<?php
namespace lib\inner\arrangements;

use lib\exceptions\OpenGateException;
use lib\inner\App;
use lib\util\Collections;
use lib\util\exceptions\ClassNotFoundException;

/**
 * 
 * @author Antonius Aurelius
 *
 */
class GateKeepDefiner{
    
    /**
     * 
     * @var Collections
     */
    protected $container;
    
    /**
     * 
     * @var Collections
     */
    protected $strict;
    
    public function __construct(){
        $this->container = collect();
        
        $this->strict = collect();
    }
    
    /**
     * 
     * @param string $class
     * @return NULL|mixed
     */
    public function fetchValue($class){        
        $class = $this->evaluateClassName($class);
        
        if($this->container->hasKey($class)){
            return $this->container->get($class);
        }else if(!is_blank($found = $this->findInstanceOf($class))){
            return $this->container->get($found);
        }
        
        return null;
    }
    
    /**
     *
     * @param string $class
     */
    public function remove($class){
        $class = $this->evaluateClassName($class);
        
        $this->container->remove($class);
        
        if(($index = $this->strict->getKey($class)) !== false){
            $this->strict->remove($index);
        }
    }
    
    /**
     *
     * @param mixed $class
     * @param mixed $value
     * @param boolean $strict
     * @throws ClassNotFoundException
     * @throws OpenGateException
     */
    public function handleValue($class, $value, $strict = false){
        if(is_array($class)){
            foreach($class as $inst){
                $this->appendKeptValue($inst, $value, $strict);
            }
        }else if(is_string($class)){
            $this->appendKeptValue($class, $value);
        }
    }
    
    /**
     * 
     * @param string $class
     * @param mixed $value
     * @param boolean $strict
     */
    protected function appendKeptValue($class, $value, $strict = false){
        $class = $this->evaluateClassName($class);
        
        if($this->container->hasKey($class)){
            if($this->strict->has($class)){
                throw new OpenGateException("The value for '{$class}' cannot be modified");
            }
        }else{
            if($strict && !$this->strict->has($class)){
                $this->strict->add($class);
            }
        }
        
        $this->container->put($class, $value);
    }
    
    /**
     * 
     * @param string $sub
     * 
     * @return string|null
     */
    protected function findInstanceOf(string $sub){
        $classes = $this->container->keySet();
        
        foreach($classes as $class){
            if(App::checkInheritance($sub, $class)){
                return $class;
            }
        }
        
        return null;
    }
    
    /**
     *
     * @param string $class
     * @throws ClassNotFoundException
     * @return string
     */
    protected function evaluateClassName($class){
        if(is_object($class)){
            $class = class_name($class);
        }
        
        if(!class_exists($class)){
            throw new ClassNotFoundException("Target class '{$class}' doesn't exist");
        }
        
        return $class;
    }
}

