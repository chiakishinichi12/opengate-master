<?php
namespace lib\util\controller\warper;

use lib\inner\ApplicationGate;
use lib\traits\WarperCommons;
use lib\util\controller\Controller;

class ControllerList{
    
    use WarperCommons;
    
    /**
     *
     * @var ApplicationGate
     */
    protected $gate;
    
    public function __construct(ApplicationGate $gate){
        $this->gate = $gate;
    }
    
    public function showList(){
        out()->println("Established Controller Classes:\n");
        
        $this->classList(preferences("structure")->get("controllers"), Controller::class, 
            fn() => out()->haltln("<error>No controller class found.</error>"));
    }
}

