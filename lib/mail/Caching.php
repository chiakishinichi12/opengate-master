<?php
namespace lib\mail;

use lib\util\file\FileUtil;

trait Caching {
    
    /**
     * 
     * @param array $recipients
     * @param string $subject
     * @param string $content
     * @param string $engine
     * @param boolean $sent
     */
    protected function logMailCache($recipients, $subject, $content, $engine, $sent){
        $logDir = preferences("mail")->get("mail_logs");
        
        $statusdir = $sent ? "success" : "failed";
        
        $file = "{$logDir}/".date("Ymd")."/{$statusdir}/".date("His").".log";
        
        $recipients = implode(",", array_map(function($recipient){
            return $recipient->email();
        }, $recipients));
        
        $header = "---------------------------------------------------\n";
        $header .= "| Subject: {$subject}\n";
        $header .= "---------------------------------------------------\n";
        $header .= "| Recipient(s): [{$recipients}] \n";
        $header .= "---------------------------------------------------\n";
        $header .= "| Mailer Engine: [{$engine}] \n";
        $header .= "---------------------------------------------------\n";
        $header .= "| Content: \n";
        $header .= "---------------------------------------------------\n\n";
        
        $content = "{$header}{$content}";
        
        FileUtil::write($file, $content);
    }
}