<?php
namespace lib\util\model\warper;

use lib\traits\WarperCommons;
use lib\util\Collections;
use lib\util\cli\LongOption;
use lib\util\cli\WarpArgv;
use lib\util\file\FileUtil;
use lib\util\migration\ShowTables;


class ModelClassCreation{
    
    use WarperCommons;
    
    /**
     *
     * @var string
     */
    protected $warping = "Model";
    
    /**
     *
     * @var Collections
     */
    protected $args;
    
    /**
     * 
     * @var LongOption
     */
    protected $table_name;
    
    /**
     * 
     * @var LongOption
     */
    protected $dbms;
    
    public function __construct(WarpArgv $argv){
        $this->args = $argv->obtainArguments();
        
        $this->table_name = $argv->findLongOption("--table");
        
        $this->dbms = $argv->findLongOption("--dbms") ?: "mysql";
    }
    
    public function write(){
        if($this->args->count() !== 2){
            return;
        }
        
        $template = FileUtil::getContents(base_path("lib/util/model/prototype/ModelBlueprint.tmp"), true);
        
        $model_name = $this->args->get(1);
        
        $this->makeStructuredDirectory($modelpath = preferences("structure")->get("models"));
        
        if(!is_null($errmsg = $this->evaluateDBTable())){
            out()->println($errmsg);
            return;
        }
        
        $toFind = collect([
            [
                "find" => "[Model_Name]",
                "replace" => $model_name
            ],
            [
                "find" => "[Date_Created]",
                "replace" => date(STANDARD_TIMESTAMP_FORMAT)
            ],
            [
                "find" => "[Model_Namespace]",
                "replace" => str_replace("/", "\\", relative_path($modelpath))
            ],
            [
                "find" => "[DB_Table_Name]",
                "replace" => $this->table_name->value()
            ]
        ]);
        
        $this->writeWarpedScript($template, 
            $toFind, 
            $modelpath, 
            $model_name);
    }
    
    protected function evaluateDBTable(){
        if(is_null($this->table_name)){
            return "<error>A DB Table must be specified. {--table=\"table_name\"}</error>";
        }
        
        $table = $this->table_name->value();
        
        if(is_null($table)){
            return "<error>No DB Table defined on the option.</error>";
        }
        
        if(!is_string($this->dbms)){
            if(is_blank($this->dbms->value())){
                return "<error>DBMS option has instance. value cannot be null or empty</error>";
            }
            
            $this->dbms = $this->dbms->value();
        }
        
        if(!ShowTables::getInstance($this->dbms)->getTables()->has($table)){
            return "<error>Table '{$table}' not found.</error>";
        }
        
        return null;
    }
}

