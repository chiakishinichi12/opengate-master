<?php
namespace lib\util\model;

use lib\util\EncapsulationHelper;
use lib\inner\App;
use lib\traits\Sanitation;

class QueryFunction {
    
    use Sanitation;
    
    /**
     * 
     * @var EncapsulationHelper
     */
    protected $encapsulator;
    
    /**
     * 
     * @var string
     */
    protected $name;
    
    /**
     * 
     * @var array
     */
    protected $params = [];
    
    /**
     * 
     * @var boolean
     */
    protected $parametric = false;
    
    public function __construct(){
        $this->encapsulator = App::make(EncapsulationHelper::class)->instance($this);
    }
    
    /**
     * 
     * @param array $stack
     * @return QueryFunction|object
     */
    public function params(array $stack){
        return $this->encapsulator->propertyDefinition($this->{__FUNCTION__}, $stack);
    }
    
    /**
     * 
     * @param string $value
     * @return QueryFunction|object
     */
    public function name(string $value){
        return $this->encapsulator->propertyDefinition($this->{__FUNCTION__}, $value);
    }
    
    /**
     * 
     * @param bool $flag
     * @return mixed|object
     */
    public function parametric(bool $flag){
        return $this->encapsulator->propertyDefinition($this->{__FUNCTION__}, $flag);
    }
    
    /**
     * 
     * @return string
     */
    public function expression(){
        $expression = "{$this->name}";
        
        if($this->parametric){
            foreach($this->params as $key => $value){
                $this->params[$key] = "\"{$this->escape($value)}\"";
            }
            
            $expression = "{$expression}(".implode(",", $this->params).")";
        }
        
        return $expression;
    }
    
    /**
     * 
     * @return string
     */
    public function __toString(){
        return $this->expression();
    }
}

