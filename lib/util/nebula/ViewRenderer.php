<?php
namespace lib\util\nebula;

use lib\util\file\FileUtil;
use lib\util\exceptions\ViewLocatingException;
use lib\util\Collections;
use lib\inner\App;

class ViewRenderer {
    
    /**
     * 
     * @var string
     */
    protected $base;
    
    /**
     * 
     * @var string
     */
    protected $view;
    
    /**
     * 
     * @var array
     */
    protected $data;
    
    /**
     * 
     * @var Collections
     */
    protected $morphs;
    
    /**
     * 
     * @var string
     */
    protected $cache;
    
    public function __construct(){
        $setup = preferences("setup");
        
        $this->base = $setup->get("view_base_path");
        
        $this->cache = $setup->get("view_morph_cache");
        
        $this->morphs = App::make(ViewMorphMarker::class)->morphs();
    }
    
    /**
     * 
     * @param array|mixed $data
     * 
     * @return \lib\util\nebula\ViewRenderer
     */
    public function setBindings($data){
        $this->data = $data;
        
        return $this;
    }
    
    /**
     * 
     * @param string $view
     * @return \lib\util\nebula\ViewRenderer
     */
    public function setView($view){
        $this->view = $view;
        
        return $this;
    }
    
    /**
     * 
     * checks if a view exists
     * 
     * @param string $view
     * 
     * @return boolean
     */
    public function exists($view){
        $relativePath = str_replace(".", "/", $view).".morph.php";
        
        return !is_null($this->morphs->get($relativePath));
    }
    
    /**
     * 
     * checks if the template file exists
     * 
     * if exists, it will return the file path of a view
     * 
     * @throws ViewLocatingException
     * @return string
     */
    protected function checkViewAvailability(){
        $relativePath = str_replace(".", "/", $this->view).".morph.php";
        
        if(is_null($morphedPath = $this->morphs->get($relativePath))){
            $this->throwViewNotFound();
        }
        
        $viewFile = "{$this->cache}/{$morphedPath}";
        
        if(!FileUtil::exists($viewFile)){
            if(FileUtil::exists(substr($viewFile, 0, strripos($viewFile, ".php")))){
                throw new ViewLocatingException("'{$this->view}' could be a directory or some other file type");
            }else{
                $this->throwViewNotFound();
            }            
        }
        
        return $viewFile;
    }
    
    /**
     * 
     * simply throws an exception when invoked
     * 
     * @throws ViewLocatingException
     */
    protected function throwViewNotFound(){
        throw new ViewLocatingException("No morph view was found out of '{$this->view}'");
    }
    
    /**
     * 
     * @param string $filepath
     */
    protected function obtainRequiredTemplate($filepath){
        if(!is_null($this->data) && is_array($this->data)){
            extract($this->data);
        }
        
        require $filepath;
    }
    
    /**
     * 
     * returns the rendered template data as a string.
     * 
     * @return string
     */
    public function buffered(){
        $viewfile = $this->checkViewAvailability();
        
        ob_start();
        
        $this->obtainRequiredTemplate($viewfile);
        
        return ob_get_clean();
    }
    
    /**
     * 
     * echoes the buffered template data 
     * 
     */
    public function render(){
        echo $this->buffered();
    }
    
    /**
     * 
     * .... so that they can echo this renderer object
     * 
     * @return string|unknown
     */
    public function __toString(){
        try{
            return $this->buffered();
        }catch(ViewLocatingException $ex){
            return $ex->getMessage();
        }
    }
}