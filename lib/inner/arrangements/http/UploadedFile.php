<?php
namespace lib\inner\arrangements\http;

use ArrayAccess;
use ArrayIterator;
use Closure;
use Countable;
use InvalidArgumentException;
use IteratorAggregate;
use lib\util\Collections;
use lib\util\StringUtil;
use lib\util\exceptions\UploadFailureException;
use lib\util\file\File;
use lib\util\file\FileUtil;

/**
 * 
 * Represents an uploaded file in a web application.
 * This can be used as a substitute for $_FILES stack.
 * 
 * @author Antonius Aurelius
 *
 */
class UploadedFile implements ArrayAccess, Countable, IteratorAggregate {
    
    /**
     * 
     * @var array
     */
    protected $value;
    
    /**
     * 
     * @var string
     */
    protected $newName;
    
    /**
     * 
     * @var string
     */
    protected $param;
    
    /**
     * 
     * @var Collections
     */
    protected $files;
    
    /**
     * 
     * @var array
     */
    protected $accepts = [];
    
    /**
     * 
     * @var boolean
     */
    protected $strict = false;
    
    /**
     *
     * @param string $fileparam
     */
    public function get(string $param){
        if(!is_null($this->value)){
            goto ends;
        }
        
        $this->param = $param;
        
        if(!is_blank($params = request()->file($param))){
            $this->value = $params;
            $this->files = collect();
        }
        
        ends:
        return $this;
    }
    
    /**
     * setting a new filename for a single uploaded file only
     * 
     * @param string $newName
     * @return \lib\inner\arrangements\http\UploadedFile
     */
    public function newName($newName){
        $this->newName = $newName;
        
        return $this;
    }
    
    /**
     * 
     * @throws UploadFailureException
     */
    protected function throwIfValueNotSuitable(){
        if(is_blank($this->value)){
            throw new UploadFailureException("Undefined file param: '{$this->param}'");
        }
        
        if(is_array($this->value)){
            if($this->strict && is_blank($this->value["tmp_name"])){
                throw new UploadFailureException("No upload data found.");
            }
        }else{
            throw new UploadFailureException("data type must be an array. ".class_name($this->value)." found");
        }
    }
    
    /**
     * 
     * @param bool $strict
     * 
     * @return \lib\inner\arrangements\http\UploadedFile
     */
    public function strict(bool $strict){
        $this->strict = $strict;
        
        return $this;
    }
    
    /**
     * 
     * checks if there are multiples files uploaded
     * 
     * @return boolean
     */
    public function isMultipleFiles(){
        $this->throwIfValueNotSuitable();
        
        return is_array($this->value["tmp_name"]);
    }
    
    /**
     * @inheritdoc
     *
     *
     * @param mixed $offset
     *
     * return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset){ 
        $this->throwIfValueNotSuitable();
        
        return $this->value[$offset];
    }
    
    /**
     * @inheritdoc
     *
     * @param mixed $offset
     * @param mixed $value
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value){ 
        $this->throwIfValueNotSuitable();
        
        $this->value[$offset] = $value;
    }
    
    /**
     * @inheritdoc
     *
     *
     * @param mixed $offset
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset){
        $this->throwIfValueNotSuitable();
        
        unset($this->value[$offset]);
    }
    
    /**
     * @inheritdoc
     *
     *
     * @param mixed $offset
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset){
        $this->throwIfValueNotSuitable();
        
        return isset($this->value[$offset]);
    }
    
    /**
     * @inheritdoc
     *
     * @return \ArrayIterator
     */
    #[\ReturnTypeWillChange]
    public function getIterator(){        
        $stack = [];
        
        if(!is_blank($this->files)){
            $stack = $this->files->toArray();
        }
        
        return new ArrayIterator($stack);
    }
    
    /**
     * @inheritdoc
     *
     * @return int
     */
    #[\ReturnTypeWillChange]
    public function count(){
        $stack = [];
        
        if(!is_blank($this->files)){
            $stack = $this->files;
        }
        
        return count($stack);
    }
    
    /**
     * 
     * intercepting uploaded files which extension name 
     * is not included on the onlyAccepts extension stack
     * 
     * @param array $extensions
     * @return \lib\inner\arrangements\http\UploadedFile
     */
    public function onlyAccepts(array $extensions){
        $this->accepts = $extensions;
        
        return $this;
    }
    
    /**
     * 
     * defaultly move the uploaded file to the configured upload directory
     * 
     * @return \lib\inner\arrangements\http\UploadedFile
     */
    public function move(string $base = null){
        $directory = upload("local")->get("storage");
        
        if(!is_blank($base)){
            $directory = "{$directory}/{$base}";
        }
        
        return $this->moveTo($directory, true);    
    }
    
    /**
     * 
     * @param string|Closure $destination
     * @param boolean $manual
     * 
     * @return UploadedFile
     */
    public function moveTo($destination, $manual = false){        
        if($destination instanceof Closure){
            if(is_string($tmp = gate()->call($destination))){
                $destination = $tmp;
            }else{
                throw new InvalidArgumentException("moveTo() closure: Return value must be a string. "
                    .class_name($tmp)." found");
            }
        }
        
        if(!$manual){
            $destination = base_path("stockade/{$destination}");
        }
        
        /*
         * handles multiple uploaded files
         * 
         */
        if($this->isMultipleFiles()){            
            collect($this->value["tmp_name"])->each(function($tmpname, $index) use ($destination){
                $original = $this->value["name"][$index];
                
                $this->uploadingProcess($destination, $tmpname, $original);
            });
        }else{
            $this->uploadingProcess($destination, $this->value["tmp_name"], $this->value["name"]);
        }
        
        return $this;
    }
    
    /**
     * 
     * iterates all the files uploaded on the server
     * 
     * @param Closure $closure
     * 
     * @return UploadedFile
     */
    public function each($closure){
        $this->files->each(function($item) use ($closure){
            if(! $closure instanceof Closure){
                throw new InvalidArgumentException("Argument#1 must be an instance of Closure. "
                    .class_name($closure)." found");
            }
            
            $closure($item["file"], $item["uploaded"]);
        });
        
        return $this;
    }
    
    /**
     * 
     * checks if the uploaded file can proceed to moving to the new location.
     * will be moved to the intercepted file directory if extension name is not accepted.
     * 
     * this is where the interception happens
     * 
     * @param File $file
     * @param string $filepath
     * 
     * @return boolean
     */
    protected function canProceedToUpload(File $file, $newFilePath){
        $accepted = true;
        
        if(is_array($this->accepts) && !is_blank($this->accepts)){
            $ext = StringUtil::extractExtensionName($newFilePath);
            
            if(is_null($ext) || !in_array($ext, $this->accepts)){
                $ext = StringUtil::extractExtensionName($newFilePath);
                $name = date("YmdHis")."_".StringUtil::getRandomizedString(6);
                
                if(!is_blank($ext)){
                    $name = "{$name}{$ext}";
                }
                
                $file->moveTo(upload("local")->get("intercepted"), $name, true);
                                
                $accepted = false;
            }
        }
        
        $fileNotExists = !FileUtil::exists($newFilePath);
        
        /**
         * 
         * deletes temporary file if already exists
         * 
         */
        if(!$fileNotExists){
            $file->delete();
        }
        
        return $fileNotExists && $accepted;
    }
    
    /**
     * 
     * @param string $destination
     * @param string $tmpname
     * @param string $name
     * 
     * @throws UploadFailureException
     */
    protected function uploadingProcess($destination, $tmpname, $name){
        $uploadedFile = new File($tmpname);
        
        if(!$uploadedFile->exists()){
            return null;
        }
                
        $filename = $name;
        
        if(!is_blank($this->newName)){
            if($this->isMultipleFiles()
                && count($this->value["tmp_name"]) > 1){
                goto naming;
            }
            
            /*
             * automatically puts the extension name to the renamed file if not specified.
             */
            if(is_blank(StringUtil::extractExtensionName($this->newName))){
                $defaultExt = StringUtil::extractExtensionName($filename);
                
                /*
                 * checks if the primary uploaded source file also has extension name on it.
                 */
                if(!is_blank($this->newName)){
                    $this->newName = "{$this->newName}{$defaultExt}";
                }
            }
            
            $filename = $this->newName;
        }
        
        naming:
        $newFilePath = "{$destination}/{$filename}";
        
        $moved = false;
        
        /*
         * checks if the file already exists. if not, will proceed to storing the file.
         */
        if($this->canProceedToUpload($uploadedFile, $newFilePath)){            
            $moved = $uploadedFile->moveTo($destination, $filename, true);
        }
        
        $this->files->add([
            "file" => $uploadedFile,
            "uploaded" => $moved
        ]);
        
        $uploadedFile->close();
        
        return $moved;
    }
}