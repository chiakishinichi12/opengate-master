<?php
namespace lib\inner\arrangements\console;

use lib\exceptions\OpenGateException;
use lib\inner\ApplicationGate;
use lib\util\Collections;
use lib\util\accords\Outputter;
use lib\util\html\HTMLElement;
use lib\util\html\HTMLUtil;
use lib\inner\App;

class CLIOut implements Outputter{
        
    /**
     * 
     * @var Collections
     */
    protected $elements = [
        "error" => 91,
        "warning" => 93,
        "info" => 94,
        "primary" => 92,
        "magenta" => 95,
        "darkred" => 31,
        "darkyellow" => 33
    ];
    
    /**
     * 
     * @var boolean
     */
    protected $strict;
    
    /**
     * 
     * @var ApplicationGate
     */
    protected $gate;
    
    /**
     * 
     * @param ApplicationGate $gate
     */
    public function __construct(ApplicationGate $gate){
        $this->gate = $gate;
        
        $setup = preferences("setup");
        
        $this->strict = $setup->get("cli_outputter_strict");
        
        if(!$this->gate->checkCLI()){
            throw new OpenGateException(class_name($this)." cannot be used within non-cli instance.");
        }
        
        $this->elements = collect($this->elements);
    }
    
    /**
     * 
     * @param string $arg
     * @throws OpenGateException
     * @return mixed
     */
    protected function highlightedText($arg){
        $util = $this->gate->make(HTMLUtil::class)->setHTMLString($arg);
        
        $util->getHTMLElements()->each(function(HTMLElement $element) use(&$arg){
            if(!$this->elements->hasKey($element->getName())){
                if($this->strict){
                    throw new OpenGateException("Unknown highlighter tag: {$element->getName()}");
                }else{
                    goto ends;
                }
            }
            
            $highlighterInt = $this->elements->get($element->getName());
            
            if($background = $element->getAttributes()->get("background")){
                if(filter_var($background, FILTER_VALIDATE_BOOLEAN)){
                    $highlighterInt += 10;
                }
            }
            
            $replacement = "\033[{$highlighterInt}m{$element->getContent()}\033[0m";
            
            $arg = str_replace($element->getHTMLExpression(), $replacement, $arg);
            
            ends:
        });
        
        return $arg;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\accords\Outputter::print()
     */
    public function print($arg) : void {
        if(is_object($arg)){
            if(!method_exists($arg, "__toString")){
                $arg = print_r($arg, true);
            }
        }else if(is_array($arg)){
            $arg = print_r($arg, true);
        }
        
        echo $this->highlightedText($arg);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\accords\Outputter::println()
     */
    public function println($arg = "") : void {
        $this->print($arg);
        echo "\n";
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\accords\Outputter::printf()
     */
    public function printf($str, ...$args) : void {
        $arg = sprintf($str, ...$args);
        $arg = $this->highlightedText($arg);
        
        $this->print($arg);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\accords\Outputter::halt()
     */
    public function halt($arg) : void {
        $this->print($arg);
        
        exit(1);
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\accords\Outputter::halt()
     */
    public function haltln($arg = "") : void {
        $this->println($arg);
        
        exit(1);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\accords\Outputter::printbr()
     */
    public function printbr($arg = ""): void {
        if(App::isRunningUnitTest()){
            $this->print("{$arg}<br/>");
        }else{
            $this->println("<error>printbr() doesn't work on CLI context</error>"); 
        }
    }

}