<?php
namespace lib\util\nebula;

use lib\util\exceptions\LocalizationException;
use lib\util\file\FileUtil;
use lib\traits\ValueLocatorHelper;

/**
 * 
 * 
 * @author Jiya Sama
 * 
 * Manages retrieval of values from configuration files based on specified paths.
 * The configuration files, organized within a base directory, are expected to be in PHP format
 * and structured as associative arrays, allowing for hierarchical access to values.
 * This class provides methods to fetch values by specifying a dot-separated path to the desired value.
 * 
 * 
 */
class ValuesConfig {
    
    use ValueLocatorHelper;
    
    /**
     * 
     * @var string
     */
    protected $base;
    
    public function __construct(){
        $setup = preferences("setup");
        
        $this->base = $setup->get("values");
    }
    
    /**
     * 
     * @param array $explodedPath
     * @throws ValueContainerException
     * 
     * @return mixed|NULL
     */
    protected function retrieveValue(array $explodedPath){
        $values = FileUtil::requireIfExists("{$this->base}/{$explodedPath[0]}.php");
        
        if(is_null($values)){
            throw new ValueContainerException("[{$this->base}/{$explodedPath[0]}.php] file doesn't exist");
        }
        
        if(!is_array($values)){
            throw new ValueContainerException("[{$this->base}/{$explodedPath[0]}] file must return array");
        }
        
        unset($explodedPath[0]);
        
        $explodedPath = array_values($explodedPath);
        
        return $this->recursiveLocate($values, $explodedPath, 0);
    }
    
    /**
     * 
     * @param string $valuePath
     * @throws LocalizationException
     * 
     * @return mixed|NULL
     */
    public function get(string $valuePath){
        $explodedPath = explode(".", $valuePath);
        
        if(count($explodedPath) === 0){
            throw new LocalizationException("param \$transpath must be in 'object.name' format");
        }
        
        return $this->retrieveValue($explodedPath);
    }
}