<?php
namespace lib\util\model;

use lib\util\exceptions\UnknownPropertyException;

class TableColumn {
    
    /**
     * 
     * @var string
     */
    protected $tableName;
    
    /**
     * 
     * @var array
     */
    protected $outprops = [];
    
    public function __construct($tableInfo){
        if($tableInfo instanceof TableInfo){
            $this->tableName = $tableInfo->getTableName();
        }
    }
    
    /**
     * 
     * @param string $name
     * 
     * @return mixed
     */
    public function __isset($name){
        return isset($this->outprops[$name]);
    }
    
    /**
     * 
     * @param string $name
     * @param string $value
     */
    public function __set($name, $value){
        return $this->outprops[$name] = $value;
    }
    
    /**
     * 
     * @param string $name
     * @throws UnknownPropertyException
     * 
     * @return mixed
     */
    public function __get($name){
        if(!isset($this->outprops[$name])){
            throw new UnknownPropertyException("Unknown Property '{{$name}}'.");
        }
        
        return $this->outprops[$name];
    }
    
    /**
     * 
     * @return string
     */
    public function getTableName(){
        return $this->tableName;
    }
}