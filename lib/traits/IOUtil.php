<?php

namespace lib\traits;

trait IOUtil {
    
    /**
     * 
     * 
     * @param String $guide
     * @return string
     */
    private function scanUserInput(String $guide){
        $inputted = "";
        
        if(PHP_OS == "WINNT"){
            echo $guide;
            $inputted = stream_get_line(STDIN, 1024, PHP_EOL);
        }else{
            $inputted = readline($guide);
        }
        
        return $inputted;
    }
}