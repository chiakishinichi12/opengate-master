<?php
namespace lib\util\migration;

use Exception;
use lib\traits\SQLFeatures;
use lib\util\Collections;
use lib\util\datagate\DB;

class DescribeTable {
    
    use SQLFeatures;
    
    /**
     * 
     * @var DescribeTable
     */
    protected static $instance = null;
    
    /**
     * 
     * @var string
     */
    protected $dbms;
    
    /**
     * 
     * @var Collections
     */
    protected $columns;
    
    /**
     * 
     * @param string $dbms
     */
    protected function __construct($dbms){
        $this->dbms = $dbms;
    }
    
    /**
     * 
     * @param string $tablename
     * 
     * @return \lib\util\migration\DescribeTable
     */
    public function describe($tablename){
        // MySQL
        try{
            $this->columns = DB::connection($this->dbms)->raw("DESC `{$tablename}`");
        }catch(Exception $ex){
            $this->columns = collect();
        }
        
        return $this;
    }
    
    /**
     * 
     * @return array|NULL
     */
    public function colnames(){
        if(!is_null($this->columns())){
            return collect($this->columns()->map(function($item){
                // MySQL
                return $item["Field"];
            }));
        }
            
        return null;
    }
    
    /**
     * 
     * @return \lib\util\migration\Collections|NULL
     */
    public function columns(){
        if($this->columns !== false && $this->columns->count()){
            return $this->columns;
        }
        
        return null;
    }
    
    /**
     * 
     * @param string $dbms
     * 
     * @return \lib\util\migration\DescribeTable
     */
    public static function getInstance($dbms = "mysql"){
        if(self::$instance === null){
            self::$instance = new DescribeTable($dbms);
        }
        
        return self::$instance;
    }
}