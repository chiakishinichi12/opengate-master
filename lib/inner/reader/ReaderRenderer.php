<?php
namespace lib\inner\reader;

use lib\exceptions\OpenGateException;
use lib\inner\App;
use lib\inner\arrangements\http\Redirector;
use lib\util\ReflectionUtil;
use lib\util\nebula\View;
use lib\util\nebula\ViewRenderer;
use lib\inner\WebAppCore;
use lib\util\controller\Controller;

/**
 * Class ReaderRenderer
 *
 * This class is responsible for rendering the reader's core functionality while managing
 * middleware execution before and after the core process. It facilitates the integration
 * of middleware logic and ensures that the core listener is executed properly.
 *
 */
class ReaderRenderer {
    
    /**
     * 
     * @var ReaderGate
     */
    protected $readerGate;
    
    /**
     * 
     * @var array
     */
    protected $bindings;
    
    /**
     * Sets the ReaderGate instance.
     *
     * @param ReaderGate $readerGate
     * @return $this
     */
    public function readerGate(ReaderGate $readerGate){
        $this->readerGate = $readerGate;
        
        return $this;
    }
    
    /**
     * Executes the 'before' method of each middleware, if defined.
     *
     * Iterates over the resolved middlewares and calls their 'before' methods.
     * If a middleware's 'before' method returns false, the middleware's halt method is called.
     * If the 'before' method returns an instance of Redirector, it sends a redirect response.
     */
    protected function middlewareBefore(){
        foreach($this->readerGate->resolvedMiddlewares() as $middleware){
            if(!method_exists($middleware, "before")){
                continue;
            }
            
            $invocation = App::call([$middleware, "before"]);
            
            if($invocation === false){
                $middleware->halt();
            }
            
            if($invocation instanceof Redirector){
                $invocation->send();
            }
        }
    }
    
    /**
     * Executes the 'after' method of each middleware, if defined.
     *
     * Similar to middlewareBefore, but this method is called after the core listener has been executed.
     */
    protected function middlewareAfter(){
        foreach($this->readerGate->resolvedMiddlewares() as $middleware){
            if(!method_exists($middleware, "after")){
                continue;
            }
            
            $invocation = App::call([$middleware, "after"]);
            
            if($invocation === false){
                $middleware->halt();
            }
            
            if($invocation instanceof Redirector){
                $invocation->send();
            }
        }
    }
    
    /**
     * Renders the core functionality.
     *
     * This method first ensures the core has a 'listen' method, then executes
     * middlewareBefore(), calls the core's 'listen' method, and finally executes middlewareAfter().
     *
     * @throws OpenGateException if the core does not have a 'listen' method.
     */
    public function render(){
        $resolvedCore = $this->readerGate->resolvedCore();
        
        if(!method_exists($resolvedCore, "listen")){            
            throw new OpenGateException("[WebAppCore Issue: ".class_name($resolvedCore)
                ."] listen() method isn't defined");
        }
        
        $method = $this->settingUpCore($resolvedCore);
        
        $this->middlewareBefore();
        
        $invoke = call_user_func($method, ...ReflectionUtil::resolveParameterData($method));
        
        // initial binding referencing
        $this->bindings = $resolvedCore->bindings();
        
        $this->resolveReturnValue($invoke);
        
        $this->middlewareAfter();
    }
    
    /**
     * 
     * @param WebAppCore $resolvedCore
     */
    protected function settingUpCore(WebAppCore $resolvedCore){
        if(method_exists($resolvedCore, "configure")){
            App::call([$resolvedCore, "configure"]);
        }
        
        $resolvedCore->loadResponseContentType();
        $resolvedCore->validateRequest();
        
        return [$resolvedCore, "listen"];
    }
    
    /**
     * 
     * @param mixed $invoke
     */
    protected function resolveReturnValue($invoke){
        if(is_array($invoke) || is_callable($invoke)){
            $hasBinder = false;
            
            if(is_array($invoke) && count($invoke)){
                if(!App::checkInheritance($invoke[0], Controller::class)){
                    goto resolves;
                }
                
                /*
                 * instantiating controller
                 */
                $invoke[0] = App::make($invoke[0]);
                
                App::instance(Controller::class, $invoke[0]);
                
                $hasBinder = true;
            }
            
            $called = call_user_func($invoke, ...ReflectionUtil::resolveParameterData($invoke));
            
            if($hasBinder){
                // this one replaces the bindings made on the WebAppCore's side
                $this->bindings = $invoke[0]->bindings();
            }
            
            $invoke = $called;
        }
        
        resolves:
        $this->renderingReturnValue($invoke);
    }
    
    /**
     *
     * the one that will do something when the function/method
     * has a return value
     *
     * @param mixed $return
     */
    protected function renderingReturnValue($return){
        if(is_blank($return)){
            return;
        }
        
        if(is_string($return)){
            if(View::exists($return)){
                morph($return, $this->bindings)->render();
            }else{
                out()->println($return);
            }
        }else if(is_object($return)){
            if(App::checkInheritance($return, ViewRenderer::class)){
                $return->render();
            }else if(App::checkInheritance($return, Redirector::class)){
                $return->send();
            }else if(method_exists($return, "__toString")){
                out()->println($return);
            }
        }
    }
}