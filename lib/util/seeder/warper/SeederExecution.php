<?php
namespace lib\util\seeder\warper;

use lib\inner\ApplicationGate;
use lib\util\Collections;
use lib\util\GateFinder;
use lib\util\StringUtil;
use lib\util\cli\LongOption;
use lib\util\cli\WarpArgv;
use lib\util\exceptions\SeederException;
use lib\util\file\FileUtil;
use lib\util\seeder\Seeder;
use lib\inner\App;

class SeederExecution {
    
    /**
     * 
     * @var unknown
     */
    protected $gate;
    
    /**
     * 
     * @var Collection
     */
    protected $args;
    
    /**
     * 
     * @var Collections
     */
    protected $seeders;
    
    /**
     * 
     * @var LongOption
     */
    protected $inputClass;
    
    public function __construct(ApplicationGate $gate, WarpArgv $argv){
        $this->gate = $gate;
        
        $this->args = $argv->obtainArguments();
        
        $this->seeders = collect();
        
        $this->inputClass = $argv->findLongOption("--class");
        
        $this->validateAndStore();
    }
    
    protected function validateAndStore(){
        $failedCallback = function(){
            out()->haltln("<error background=\"true\">No seeder class found.</error>\n");
        };
        
        if(!FileUtil::exists("dataspace/seeds")){
            $this->gate->call($failedCallback);
        }
        
        $inputtedClassname = $this->inputClass?->value();
        
        (new GateFinder("dataspace/seeds", true))->failedCallback($failedCallback)->load(function($class)
            use($inputtedClassname){
            if(!App::checkInheritance($class, Seeder::class)){
                throw new SeederException("Class {{$class}} is not an instance of ".Seeder::class);
            }
            
            $seeder = $this->gate->make($class);
            
            if(!method_exists($seeder, "run")){
                throw new SeederException("Method run() is not defined in {$class}");
            }
            
            if($inputtedClassname && !StringUtil::endsWith($class, $inputtedClassname)){
                return;
            }
            
            $this->seeders->add($seeder);
        });
        
        if(!is_null($inputtedClassname) && !$this->seeders->count()){
            out()->haltln("<error background=\"true\">Seeder class '{$inputtedClassname}' doesn't exist</error>");
        }
    }
    
    public function exec(){
        $this->seeders->each(function(Seeder $seeder, $index){
           $seeder->invoke();
           out()->println("<info>".class_basename($seeder)."</info> executed successfully");
        });
    }
    
}