<?php
namespace lib\commands\classes;

use lib\commands\CommonMethods;
use lib\inner\App;
use lib\instances\CommandInstance;
use lib\util\cli\WarpArgv;
use lib\util\model\warper\ModelClassCreation;
use lib\util\model\warper\ModelList;

/**
 *
 * @Commands(["model"])
 *
 */
class WarpModel extends CommandInstance{
    
    use CommonMethods;
    
    protected $name = "model";
    
    protected $desc = "Eloquent ORM feature";
    
    /**
     *
     * @Argument("creates model class. [option(s): { --dbms(O), --table(R) }]")
     *
     */
    public function create(){
        App::make(ModelClassCreation::class)->write();
    }
    
    /**
     *
     * @Argument("show all user-defined model classes")
     *
     */
    public function list(){
        App::make(ModelList::class)->showList();
    }
    
    /**
     * 
     * @param WarpArgv $argv
     */
    public function listen(WarpArgv $argv){
        $this->execution($argv);
    }
}

