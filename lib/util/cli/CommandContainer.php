<?php

namespace lib\util\cli;

use lib\util\Collections;

class CommandContainer {
            
    /**
     * 
     * @var array
     */
    protected $commands;
    
    public function __construct(){
        $this->commands = collect();
    }
    
    /**
     * 
     * @param CommandBean $bean
     */
    public function appendCommand(CommandBean $bean){
        $this->commands->add($bean);
    }
    
    /**
     * 
     * @return Collections
     */
    public function getCommands(){
        return $this->commands;
    }
}