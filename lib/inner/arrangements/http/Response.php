<?php
namespace lib\inner\arrangements\http;

use lib\util\ResponseCode;
use lib\util\StringUtil;
use lib\util\nebula\View;
use lib\util\Collections;
use lib\inner\App;

class Response {
    
    /**
     * 
     * @var Request
     */
    protected $request;
        
    /**
     * 
     * @var string|int
     */
    protected $responseCode;
    
    /**
     * 
     * @var Collections
     */
    protected $headers;
    
    /**
     * 
     * @param Request $request
     */
    public function __construct(Request $request){
        $this->request  = $request;
        
        $this->responseCode = http_response_code();
        
        $this->initResponseHeaders();
    }
    
    /**
     * 
     * retrieves sent beforehand response headers
     * 
     */
    protected function initResponseHeaders(){
        $this->headers = collect();
        
        foreach(headers_list() as $header){
            $explheader = collect(explode(":", $header))->cmap(fn($item) => trim($item));
            
            if($explheader->length() === 2){
                $this->headers->put(...$explheader->toArray());
            }else{
                $this->headers->add($header);
            }
        }
    }
    
    /**
     * setting redirection
     * 
     * @param string $to
     * @param int $code
     * 
     * @return Redirector
     * 
     */
    public function redirect(string $to = null, int $code = null){
        $redirector = new Redirector($to);
        
        if(!is_null($code)){
            $redirector->status($code);
        }
        
        return $redirector;
    }
    
    /**
     * checks if the header is already sent
     * 
     * @param string $headerName
     * 
     * @return boolean
     */
    protected function headerAlreadySent($headerName){
        foreach(headers_list() as $header){
            if(StringUtil::startsWith($header, $headerName)){
                return true;
            }
        }        
        
        return false;
    }
    
    /**
     * 
     * @param array $headers
     * @param boolean $forceSend
     * 
     * @return Response
     */
    public function setHeaders(array $headers, $forceSend = false){
        collect($headers)->each(function($value, $name) use ($forceSend){
            if($this->headerAlreadySent($name) && !$forceSend){
                return;
            } 
            
            $this->headers->put($name, $value);
            
            @header("{$name}: {$value}");
        });  
        
        return $this;
    }
    
    /**
     * 
     * @return Collections
     */
    public function getAlreadySentHeaders(){
        return $this->headers;
    }
    
    /**
     * alias of getAlreadSentHeaders()
     * 
     * @return \lib\util\Collections
     */
    public function headers(){
        return $this->getAlreadySentHeaders();
    }
    
    /**
     * 
     * @param int $code
     * 
     * @return int|Response
     */
    public function statusCode(int $code = null){
        if(is_null($code)){
            return $this->responseCode;
        }
        
        // ensures that response code is a 3-digit number.
        if(!($code >= 100 && $code <= 999)){
            return;
        }
        
        http_response_code($this->responseCode = $code);
        
        return $this;
    }
    
    /**
     * 
     * checks if the response code indicates error.
     * 
     * @return boolean
     */
    public function isErrResponseCode(){
        return $this->responseCode >= 400;
    }
    
    /**
     * 
     * halts the response output with a message
     * 
     * @param string $message
     */
    public function halts(string $message){
        if(!$this->isErrResponseCode()){
            return;
        }
        
        if(!App::isRunningUnitTest()){
            out()->haltln($message);
        }else{
            out()->println($message);
        }
    }
    
    /**
     * 
     * halts the response output with the default error page.
     * 
     * @param string $desc
     * @param string $displayed
     * 
     */
    public function errorTemplate($desc = null, $displayed = null){        
        if(!$this->isErrResponseCode()){
            return;
        }
        
        $errpage = collect(preferences("structure")->get("error_page"));
        
        if(!$errpage->count()){
            return;
        }
        
        if(is_blank($desc)){
            $desc = ResponseCode::get($this->responseCode);
        }
        
        $bindings = [
            "code" => $this->responseCode,
            "desc" => $desc
        ];
        
        if(is_blank($displayed)){
            if($err = $errpage->get($this->responseCode)){
                $displayed = $err;
            }else if($default = $errpage->get("default")){
                $displayed = $default;
            }
        }
        
        if(!is_blank($displayed) && View::exists($displayed)){
            morph($displayed, $bindings)->render();
        }
        
        if(!App::isRunningUnitTest()){
            exit(1);
        }
    }
}