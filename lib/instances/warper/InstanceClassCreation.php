<?php
namespace lib\instances\warper;

use lib\util\cli\WarpArgv;
use lib\traits\WarperCommons;
use lib\util\cli\LongOption;
use lib\util\file\FileUtil;
use lib\util\StringUtil;

class InstanceClassCreation {
    
    use WarperCommons;
    
    /**
     * 
     * @var string
     */
    protected $warping = "Instance";
    
    /**
     *
     * @var Collections
     */
    protected $args;
    
    /**
     * 
     * @var LongOption
     */
    protected $instClassName;
    
    /**
     * 
     * @var LongOption
     */
    protected $identifier;
    
    /**
     * 
     * @var string
     */
    protected $instances;
    
    /**
     * 
     * @var string
     */
    protected $prototype;
    
    /**
     * 
     * @param WarpArgv $argv
     */
    public function __construct(WarpArgv $argv){
        // required
        $this->instClassName = $argv->findLongOption("--cli|--page|--api");
        
        // required
        $this->identifier = $argv->findLongOption("--identifier");
        
        $this->validate();
        
        $this->instances = $this->resolveDefaultDirectory();
        
        $this->prototype = base_path("lib/instances/prototype");
    }
    
    /**
     * 
     * validates if the long options are defined.
     * 
     */
    protected function validate(){
        if(is_null($this->instClassName)){
            out()->haltln("<error>instance type is required. { --cli=(class) | --page=(class) | --api=(class) }</error>");
        }
        
        if(is_blank($this->instClassName->value())){
            out()->haltln("<error>instance class name is required.</error>");
        }
        
        if(is_null($this->identifier)){
            out()->haltln("<error>instance identifier must be defined { --identifier }</error>");
        }
        
        if(is_blank($this->identifier->value())){
            out()->haltln("<error>instance identifier must not be blanked.</error>");
        }
    }
    
    /**
     * 
     * @return string|mixed
     */
    protected function resolveDefaultDirectory(){
        $instances = preferences("structure")->get("instances");
        
        switch($this->instClassName->name()){
            case "--page":
                $instances = "{$instances}/page";
                break;
            case "--api":
                $instances = "{$instances}/api";
                break;
            case "--cli":
                $instances = "{$instances}/commands";
                break;
        }
        
        return $instances;
    }
    
    /**
     * 
     * @return \lib\util\Collections
     */
    protected function resolveTemplate(){
        $templateFile = null;
        
        switch($this->instClassName->name()){
            case "--page":
                $templateFile = "{$this->prototype}/PageInstanceBlueprint.tmp";
                break;
            case "--api":
                $templateFile = "{$this->prototype}/ApiInstanceBlueprint.tmp";
                break;
            case "--cli":
                $templateFile = "{$this->prototype}/CommandInstanceBlueprint.tmp";
                break;
        }
        
        return FileUtil::getContents($templateFile, true);
    }
    
    /**
     * 
     * @return string
     */
    protected function resolveIdentifier(){
        $identifier = $this->identifier->value();
        
        if(StringUtil::contains($identifier, ",")){            
            $identifier = collect(StringUtil::explode(",", $identifier))
                ->implode(",", fn($item) => $this->finalizeIdentifierValue(trim($item)));
        }else{
            $identifier = $this->finalizeIdentifierValue($identifier);
        }
        
        return "[{$identifier}]";
    }
    
    /**
     * 
     * @param string $str
     * 
     * @return string
     */
    protected function finalizeIdentifierValue(string $str){
        switch($this->instClassName->name()){
            case "--api":
            case "--page":
                $str = "/{$str}";
                break;
        }
        
        return "\"{$str}\"";
    }
    
    /**
     * 
     * generating instance class
     * 
     */
    public function write(){
        $template = $this->resolveTemplate();
        
        $instance_name = $this->instClassName->value();
        
        $this->alterAndCreate($instance_name, $this->instances);
        
        $toFind = collect([
            [
                "find" => "[Instance_Class]",
                "replace" => $instance_name
            ],
            [
                "find" => "[Instance_Namespace]",
                "replace" => str_replace("/", "\\", relative_path($this->instances))
            ],
            [
                "find" => "[Identifier]",
                "replace" => $this->resolveIdentifier()
            ]
        ]);
        
        $this->writeWarpedScript($template,
            $toFind,
            $this->instances,
            $instance_name);
    }
}

