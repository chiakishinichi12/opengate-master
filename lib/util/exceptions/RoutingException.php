<?php

namespace lib\util\exceptions;

use lib\exceptions\OpenGateException;

class RoutingException extends OpenGateException {
    
    protected $route;
    
    public function __construct(String $message, String $route = null){
        parent::__construct($message);
        
        $this->route = $route;
    }
}