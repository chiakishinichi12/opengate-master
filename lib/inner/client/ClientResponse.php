<?php
namespace lib\inner\client;

use stdClass;
use lib\inner\App;
use lib\util\EncapsulationHelper;
use lib\inner\json\JsonUtility;
use lib\inner\OpenObject;

class ClientResponse {
    
    /**
     * 
     * @var RequestProcessor
     */
    protected $handler;
    
    /**
     * 
     * @var stdClass
     */
    protected $json;
    
    /**
     * 
     * @var stdClass
     */
    protected $xml;
    
    /**
     * 
     * @var EncapsulationHelper
     */
    protected $encapsulation;
    
    /**
     * 
     * @param RequestProcessor $handler
     */
    public function __construct(RequestProcessor $handler){
        $this->handler = $handler;
        
        $this->encapsulation = App::make(EncapsulationHelper::class)->instance($this);
    }
    
    /**
     * 
     * @return number|mixed
     */
    public function code(){
        return $this->handler->getResponseCode();
    }
    
    /**
     * 
     * @return string
     */
    public function body(){
        return $this->handler->getResponseBody();
    }
    
    /**
     * Parses XML data and returns the result as an array.
     * 
     * @return mixed
     */
    public function xml(){
        return $this->encapsulation->defineIfUndefined(
            $this->{__FUNCTION__}, xml_decode($this->body()));
    }
    
    /**
     * 
     * Parses the response body as JSON and returns the decoded data.
     * 
     * @param boolean $associative
     * 
     * @return OpenObject|array
     */
    public function json($associative = false){
        $decoded = JsonUtility::parse($this->body());
        
        if($associative){
            $decoded = $decoded->toArray();
        }
        
        return $this->encapsulation->defineIfUndefined(
            $this->{__FUNCTION__}, $decoded);
    }
    
    /**
     * 
     * Redirects the response to a URL if the response body contains a valid URL.
     * 
     */
    public function redirect(){
        if(filter_var(trim($this->body()), FILTER_VALIDATE_URL)){
            if(!App::checkCLI()){
                response()->redirect($this->body())->send();
            }else{
                out()->println("... <warning> Redirecting to </warning>"
                    ." <magenta>{$this->body()}</magenta>");
            }
               
            return true;
        }
        
        return false;
    }
    
    /**
     * 
     * @return \lib\util\Collections
     */
    public function headers(){
        return $this->handler->getResponseHeaders();
    }
    
    /**
     * 
     * @param boolean $headers
     * 
     * @return string
     */
    public function details($headers = false){
        $details = "[Client URL: {$this->handler->url()}]\n\n"
        ."Code: {$this->code()}\n"
        ."Body:\n\n{$this->body()}\n\n";
        
        if($headers){
            $details .= "Headers:\n\n{$this->headers()->implode("\n")}";
        }
        
        return $details;
    }
    
    /**
     * 
     * @return string
     */
    public function __toString(){
        return $this->details();
    }
}