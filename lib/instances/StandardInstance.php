<?php

namespace lib\instances;

use lib\inner\ApplicationGate;
use lib\inner\WebAppCore;

final class StandardInstance extends WebAppCore {
            
    /**
     * 
     * @var mixed
     */
    protected $invokable;
    
    public function __construct(){
        parent::__construct();
    }
    
    /**
     * 
     * @param string $type
     */
    public function validateType($type){
        $this->onSysCheck($type);
    }
    
    /**
     *
     * @param mixed $invokable
     */
    public function invokable($invokable){
        $this->invokable = $invokable;
    }
    
    /**
     * 
     * this is where the closure is invoked
     * 
     * @param ApplicationGate $gate
     */
    public function listen(ApplicationGate $gate){
        if(!is_blank($this->invokable)){
            return $this->invokable;
        }
    }
}