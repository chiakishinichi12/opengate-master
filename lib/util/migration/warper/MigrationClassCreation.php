<?php
namespace lib\util\migration\warper;

use lib\traits\WarperCommons;
use lib\util\Collections;
use lib\util\StringUtil;
use lib\util\cli\LongOption;
use lib\util\cli\WarpArgv;
use lib\util\file\FileUtil;

class MigrationClassCreation {
    
    use WarperCommons;
    
    /**
     * 
     * @var string
     */
    protected $warping = "Migration";
    
    /**
     * 
     * @var Collections
     */
    protected $args;
    
    /**
     * 
     * @var LongOption
     */
    protected $view;
    
    /**
     * 
     * @var LongOption
     */
    protected $alter;
    
    /**
     * 
     * @param WarpArgv $argv
     */
    public function __construct(WarpArgv $argv){
        $this->args = $argv->obtainArguments();
        
        $this->view = $argv->findLongOption("--view");
        
        $this->alter = $argv->findLongOption("--alter");
    }
    
    /**
     * 
     * Processes the given migration name to determine the table name and template file.
     * 
     * If the migration involves a view or an alter operation, the corresponding table name 
     * and template are determined. Otherwise, the table name is generated from the migration 
     * name by converting it to lowercase and splitting it by capital letters.
     * 
     * @param string $migrationName
     * @return string[]|string
     */
    protected function warpTableNameAndTemplate(string $migrationName){
        $table_name = strtolower(implode("_", StringUtil::splitByCapital($migrationName)));
        
        // Check if either $this->view or $this->alter is not null
        if(!is_null($this->view) || !is_null($this->alter)){            
            if(!is_null($this->view)){ // Handle the case where $this->view is not null
                $option = $this->view->name();
                $table_name = $this->view->value();
                // Specify the template for view migrations
                $alterTemplate = "MigrationView.tmp";
            }else if(!is_null($this->alter)){ // Handle the case where $this->alter is not null
                $option = $this->alter->name();
                $table_name = $this->alter->value();
                // Specify the template for alter migrations
                $alterTemplate = "MigrationAlter.tmp";
            }
            
            // Ensure the table or view name is not empty
            if(is_blank($table_name)){
                // Output an error and terminate if the table name is blank
                out()->haltln("<error>{{$option}}: Table (or view) name cannot be empty.</error>");
            }
            
            return [$table_name, $alterTemplate];
        }
        
        return $table_name;
    }
    
    public function write(){
        if($this->args->count() < 2){
            return;
        }
        
        // default Migration class prototype
        $template = FileUtil::getContents(base_path("lib/util/migration/prototype/Migration.tmp"), true);
        
        $migration_name = $this->args->get(1);
        
        $warped = $this->warpTableNameAndTemplate($migration_name);
        
        if(is_array($warped)){
            $table_name = $warped[0];
            $alterOrViewTemplate = $warped[1];
            
            $template = FileUtil::getContents(base_path(
                "lib/util/migration/prototype/{$alterOrViewTemplate}"), true);
        }else{
            $table_name = $warped;
        }
        
        $this->makeStructuredDirectory($migpath = preferences("structure")->get("migrations"));
        
        $migration_name = "Migration{$migration_name}";
        
        $toFind = collect([
            [
                "find" => "[Table_Name]",
                "replace" => $table_name
            ],
            [
                "find" => "[Migration_Class_Name]",
                "replace" => $migration_name
            ],
            [
                "find" => "[Date_Created]",
                "replace" => date(STANDARD_TIMESTAMP_FORMAT)
            ],
            [
                "find" => "[Migration_Namespace]",
                "replace" => str_replace("/", "\\", relative_path($migpath))
            ]
        ]);
        
        $this->writeWarpedScript($template, 
            $toFind, 
            $migpath, 
            $migration_name);
    }
}