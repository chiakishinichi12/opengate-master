<?php

namespace lib\util\route;

use lib\util\EncapsulationHelper;
use lib\inner\App;

class RouteBean {
    
    /**
     * 
     * @var string|array
     */
    protected $uri;
    
    /**
     * 
     * @var mixed
     */
    protected $query;
    
    /**
     * 
     * @var array
     */
    protected $middlewares;
    
    /**
     * 
     * @var string
     */
    protected $method;
    
    /**
     * 
     * @var string
     */
    protected $type = "page";
    
    /**
     * 
     * @var array
     */
    protected $responseConfig = [];
    
    /**
     * 
     * @var EncapsulationHelper
     */
    protected $encapsulation;
    
    /**
     * 
     * defines the uri to be found
     * 
     * @param string $uri
     * @param mixed $query
     */
    public function __construct(string|array $uri, mixed $query){
        $this->encapsulation = App::make(EncapsulationHelper::class)->instance($this);
        
        $this->uri = $uri;
        
        $this->query = $query;
    }
    
    /**
     *
     * @param mixed $query
     *
     * @return mixed|\lib\util\route\RouteBean
     */
    public function query($query = null){
        return $this->encapsulation->propertyDefinition($this->{__FUNCTION__}, $query);
    }
    
    /**
     * 
     * @param string $uri
     * 
     * @return mixed|\lib\util\route\RouteBean
     */
    public function uri(string $uri = null){
        return $this->encapsulation->propertyDefinition($this->{__FUNCTION__}, $uri);
    }
    
    /**
     * 
     * sets the middlewares to be aligned with web instance|routed response
     * 
     * @param array $mids
     * 
     * @return \lib\util\route\RouteBean|array
     */
    public function middlewares(array $mids = null){
        return $this->encapsulation->propertyDefinition($this->{__FUNCTION__}, $mids);
    }
    
    /**
     * sets the expected method for the response
     * 
     * @param string $method
     * 
     * @return \lib\util\route\RouteBean|string
     */
    public function method(string $method = null){
        // if the Argument#1 and method property doesn't have reference 
        // and response config has 'method' element, it will return the element value
        if(is_blank($method) && is_blank($this->{__FUNCTION__}) && !is_blank($this->responseConfig) 
            && isset($this->responseConfig[__FUNCTION__])){
            return $this->responseConfig[__FUNCTION__];
        }
        
        return $this->encapsulation->propertyDefinition($this->{__FUNCTION__}, $method);
    }
    
    /**
     * sets the web instance type for called routing method
     * 
     * @param string $type
     * 
     * @return \lib\util\route\RouteBean|string
     */
    public function type(string $type = null){
        return $this->encapsulation->propertyDefinition($this->{__FUNCTION__}, $type);
    }
    
    /**
     * 
     * @param array $responseConfig
     * 
     * @return \lib\util\route\RouteBean|array
     */
    public function responseConfig(array $responseConfig = null){
        return $this->encapsulation->propertyArrayMerge($this->{__FUNCTION__}, $responseConfig);
    }
}