<?php
namespace lib\util\cli;

use ReflectionClass;
use ReflectionProperty;
use lib\commands\classes\WarpGateHelp;
use lib\exceptions\OpenGateException;
use lib\inner\App;
use lib\instances\CommandInstance;
use lib\util\Reader;

class ConsoleGate {
        
    /**
     * 
     * @var CommandContainer
     */
    protected $container;
    
    public function __construct(){
        if(!App::checkCLI()){
            throw new OpenGateException("Not a CLI execution");
        }
        
        $this->container = App::make(CommandContainer::class);
    }
    
    /**
     * 
     * capturing arguments passed on command line interface
     * 
     * @return void|number
     */
    public function captureArguments(){
        $clistatus = 0;
        
        $inputtedcmd = App::make(WarpArgv::class)->getInputtedCommand();
        
        // the first element is referring to the name of the file
        if(is_null($inputtedcmd)){
            Reader::readInstance(WarpGateHelp::class);
            return;
        }
        
        $locator = App::make(CommandLocator::class)->locateCommand();
        
        if(!$locator->isLocated()){
            out()->println("<darkred background=\"true\"> Command '{$inputtedcmd}' is not defined. </darkred>");
            $clistatus = -1;
        }
        
        return $clistatus;
    }
    
    /**
     * 
     * @param mixed $stacked
     * @param string $insert
     */
    protected function compareAndThrowIfTrue($stacked, $insert){
        if((is_array($stacked) && in_array($insert, $stacked, true)) 
            || is_string($stacked) && strcmp($insert, $stacked) === 0){
            throw new OpenGateException("Command already exist: '{$insert}'");
        }
    }
    
    /**
     * 
     * @param CommandBean $commandBean
     */
    protected function checkCommandThenAppend(CommandBean $commandBean){
        $this->container->getCommands()->each(function(CommandBean $item) use ($commandBean){
            $stacked = $item->command();
            $insert = $commandBean->command();
                    
            if(is_array($insert)){
                foreach($insert as $phrase){
                    $this->compareAndThrowIfTrue($stacked, $phrase);
                }
            }else if(is_string($insert)){
                $this->compareAndThrowIfTrue($stacked, $insert);
            }
        });
        
        $this->container->appendCommand($commandBean);
    }
    
    /**
     * 
     * @param string $command
     * @param mixed $query
     * 
     * @return CommandBean
     */
    public function command($command, $query){        
        $commandBean = new CommandBean($command, $query);
        
        if(is_string($query) && class_exists($query)){
            $reflc = new ReflectionClass($query);
            $props = $reflc->getProperties(ReflectionProperty::IS_PROTECTED);
            $values = [];
            
            $instance = App::make($query);
            
            if(!App::checkInheritance($instance, CommandInstance::class)){
                throw new OpenGateException("Class {$query} must be an instance of ".CommandInstance::class);
            }
            
            foreach($props as $prop){
                $prop->setAccessible(true);
                $values[$prop->name] = $prop->getValue($instance);
            }
            
            $commandBean->details($values);
        }
        
        $this->checkCommandThenAppend($commandBean);
        
        return $commandBean;
    }
}