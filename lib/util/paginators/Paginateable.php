<?php
namespace lib\util\paginators;

use lib\util\Paginator;

/**
 * This interface defines the contract for paginating a data set. 
 * Implementing classes must provide the ability to retrieve a portion 
 * of records based on the number of rows per page and the current page.
 * 
 * @author Jiya Sama
 */
interface Paginateable {
    
    /**
     * 
     * Returns a Paginator instance containing the paginated results.
     * 
     * @param int $rowsPerPage
     * @param int $page
     */
    function paginate(int $rowsPerPage, int $page = 1) : Paginator;
}

