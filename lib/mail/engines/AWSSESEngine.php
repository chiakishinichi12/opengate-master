<?php
namespace lib\mail\engines;

use Aws\Exception\AwsException;
use Aws\Exception\CredentialsException;
use Aws\Ses\SesClient;
use lib\mail\Caching;
use lib\mail\Dispatchable;
use lib\mail\Engine;
use lib\mail\engines\aws\RawMessageHelper;
use lib\util\exceptions\MailDispatchException;
use lib\util\exceptions\MailEngineResolverException;
use lib\util\nebula\View;
use lib\util\nebula\ViewRenderer;

class AWSSESEngine implements Engine{
    
    use Caching;
    
    /**
     *
     * @var array
     */
    protected $credentials;
    
    /**
     * 
     * @var boolean
     */
    protected $loaded = false;
    
    /**
     *
     * @var string
     */
    protected $subject = "";
    
    /**
     *
     * @var array
     */
    protected $recipients = [];
    
    /**
     *
     * @var array
     */
    protected $attachments = [];
    
    /**
     * 
     * @var SesClient
     */
    protected $sesClient;
    
    /**
     * 
     * @var string
     */
    protected $content;
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\mail\Engine::initMailerEngine()
     */
    public function initMailerEngine(array $credentials){
        $this->credentials = $credentials;
        
        if(!class_exists(SesClient::class)){
            return;
        }
                
        $this->clientInstance();
        
        $this->loaded = true;
    }
    
    /**
     * 
     * defining SesClient instance.
     * 
     */
    protected function clientInstance(){
        $this->sesClient = new SesClient([
            "version" => $this->credentials["version"],
            "region" => $this->credentials["region"],
            "credentials" => [
                "key" => $this->credentials["key"],
                "secret" => $this->credentials["secret"]
            ]
        ]);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\mail\Engine::attachments()
     */
    public function attachments(array $attachments){
        $this->attachments = $attachments;
        
        return $this;
    }

    /**
     * 
     * {@inheritDoc}
     * @see \lib\mail\Engine::subject()
     */
    public function subject(string $subject){
        $this->subject = $subject;
        
        return $this;
    }

    /**
     * 
     * {@inheritDoc}
     * @see \lib\mail\Engine::recipients()
     */
    public function recipients(array $recipients){
        $this->recipients = array_merge($this->recipients, $recipients);
        
        return $this;
    }
    
    /**
     *
     * @param Dispatchable $dispatchable
     * @return \lib\util\nebula\ViewRenderer|string
     */
    protected function evaluateContent(Dispatchable $dispatchable){
        $content = $dispatchable->content();
        
        if($content instanceof ViewRenderer){
            $content = $dispatchable->content()->buffered();
        }else if(is_string($content)){
            if(View::exists($content)){
                $content = morph($content, $dispatchable->bindings())->buffered();
            }
        }else{
            throw new MailDispatchException("Content instance must be a string or ViewRenderer. ".gettext($content)." found");
        }
        
        return $content;
    }
    
    /**
     * 
     * @param Dispatchable $dispatchable
     * 
     * @return string
     */
    protected function messageFormat(Dispatchable $dispatchable){
        $helper = new RawMessageHelper(
            $this->credentials["sender_mail"], 
            $this->recipients);
        
        if(!is_blank($this->credentials["name"])){
            $helper->altName($this->credentials["name"]);
        }
        
        $helper->subject($this->subject)
            ->charset($this->credentials["charset"])
            ->isHtml($dispatchable->isHtml());
        
        $this->content = $this->evaluateContent($dispatchable);
        
        $helper->content($this->content);
        
        if(!is_blank($this->attachments)){
            $helper->attachments($this->attachments);
        }
                
        return $helper->build();
    }
    
    /**
     * 
     * Intercepts and validates the email dispatchable before sending.
     *
     * This method is responsible for checking and preparing the email dispatchable
     * object for sending. It verifies that the email engine (SesClient) is loaded,
     * that the provided object is an instance of the Dispatchable interface, and
     * that essential email components, such as recipients and subject, are not blank.
     * Additionally, it allows the inclusion of attachments if provided.
     * 
     * @param Dispatchable $dispatchable
     * 
     * @throws MailEngineResolverException
     * 
     * @throws MailDispatchException
     */
    protected function intercept($dispatchable){
        if(!$this->loaded){
            throw new MailEngineResolverException(
                "Class ".SesClient::class." isn't loaded. do *composer require aws/aws-sdk-php*");
        }
        
        if(!($dispatchable instanceof Dispatchable)){
            throw new MailDispatchException("Mailable object is not an instance of Dispatcher");
        }
        
        if(!is_blank($dispatchable->subject())){
            $this->subject = $dispatchable->subject();
        }
        
        $notBlank = [
            "recipients" => $this->recipients,
            "subject" => $this->subject
        ];
        
        foreach($notBlank as $key => $value){
            if(is_blank($value)){
                throw new MailDispatchException("'{$key}' shouldn't be blank/empty");
            }
        }
        
        if(!is_null($dispatchable->attachments())){
            $this->attachments = $dispatchable->attachments();
        }
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\mail\Engine::triggerSend()
     */
    public function triggerSend(Dispatchable $dispatchable){
        $this->intercept($dispatchable);
        
        try{
            $message = $this->messageFormat($dispatchable);
            
            $result = $this->sesClient->sendRawEmail([
                "RawMessage" => [
                    "Data" => $message
                ]
            ]);
        }catch(AwsException $ex){
            if($this->credentials["error_trace"]){
                // output error message if fails
                echo $ex->getMessage();
                echo "The email was not sent. Error message: {$ex->getAwsErrorMessage()}\n";
            }
        }catch(CredentialsException $ex){
            if($this->credentials["error_trace"]){
                // output error message if fails
                echo $ex->getMessage();
            }
        }
        
        $status = isset($result["MessageId"]);
        
        $this->logMailCache(
            $this->recipients,
            $this->subject,
            $this->content,
            class_basename(class_name($this)),
            $status);
        
        return $status;
    }    
}