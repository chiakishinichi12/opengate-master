<?php
/**
 * 
 * this preference script was created to be modified according to system requirement
 * 
 */
return [
    /**
     * 
     * application's version
     * 
     */
    "app_version" => "1.0.0", 
    
    /**
     * 
     * application's root folder
     * 
     */
    "base_path" => config("base_path", __DIR__."/../"),
];