<?php
namespace lib\util;

use lib\exceptions\OpenGateException;

/**
 * 
 * class provides utility functions to encapsulate and manipulate properties of an instance object. 
 * It ensures that properties are properly set, merged, and validated within the context of the encapsulated instance.
 * 
 * @author Jiya Sama
 *
 */
class EncapsulationHelper{
    
    /**
     * 
     * @var Collections
     */
    protected $propstack;
    
    /**
     * 
     * @var object
     */
    protected $instance;
    
    /**
     * 
     * @param object $instance
     * 
     * @return \lib\util\EncapsulationHelper
     */
    public function instance(&$instance){
        $this->instance = $instance;
        
        $this->propstack = collect();
        
        return $this;
    }
    
    /**
     * 
     * checks if the instance is defined and if it's an object
     * 
     */
    protected function validate(){
        if(is_null($this->instance)){
            throw new OpenGateException("no instance defined");
        }
        
        if(!is_object($this->instance)){
            throw new OpenGateException("instance must be an object");
        }
    }
    
    /**
     * 
     * @param string $funcname
     * @param mixed $value
     * 
     * @return mixed|object
     */
    public function appendProperty(string $funcname, $value = null){
        $this->validate();
        
        if(is_null($value)){
            return $this->propstack->get($funcname);
        }
        
        $this->propstack->put($funcname, $value);
        
        return $this->instance;
    }
    
    /**
     * 
     * @param string $funcname
     * @param array $value
     * 
     * @return mixed|object
     */
    public function appendArrayMerge(string $funcname, array $value = null){
        $this->validate();
        
        if(is_null($value)){
            return $this->propstack->get($funcname);
        }
        
        $potential = $this->propstack->get($funcname) ?: $value;
        
        if($this->propstack->hasKey($funcname) && is_array($potential)){
            $potential = array_merge($potential, $value);
        }
        
        $this->propstack->put($funcname, $potential);
        
        return $this->instance;
    }
    
    /**
     * 
     * @param string $funcname
     * @param mixed $value
     * 
     * @return NULL|mixed
     */
    public function appendDefinitionIfUndefined(string $funcname, $value = null){
        $this->validate();
        
        if(is_null($this->propstack->get($funcname))){
            $this->propstack->put($funcname, $value);
        }
        
        return $this->propstack->get($funcname);
    }
    
    /**
     * 
     * determines if a property is already defined and then return its value
     * 
     * @param mixed $var
     * @param mixed $value
     * 
     * @return mixed
     */
    public function defineIfUndefined(&$var, $value = null){
        $this->validate();
        
        if(is_null($var)){
            $var = $value;
        }
        
        return $var;
    }
    
    /**
     * 
     * @param mixed $var
     * @param mixed $value
     * @return mixed|object
     */
    public function propertyDefinition(&$var, $value = null){
        $this->validate();
        
        if(is_null($value)){
            return $var;
        }
        
        $var = $value;
        
        return $this->instance;
    }
    
    /**
     * 
     * @param mixed $var
     * @param array $value
     * @return array|object
     */
    public function propertyArrayMerge(&$var, array $value = null){
        $this->validate();
        
        if(is_null($value)){
            return $var;
        }
        
        $var = array_merge($var, $value);
        
        return $this->instance;
    }
}