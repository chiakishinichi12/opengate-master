<?php
namespace lib\commands\classes;

use lib\commands\CommonMethods;
use lib\inner\App;
use lib\instances\CommandInstance;
use lib\util\cli\WarpArgv;
use lib\util\seeder\warper\SeederClassCreation;
use lib\util\seeder\warper\SeederExecution;
use lib\util\seeder\warper\SeederList;

/**
 *
 * @Commands(["seeder", "seed", "sd"])
 *
 */
class WarpSeeder extends CommandInstance {
    
    use CommonMethods;
    
    protected $name = "seeder";
    
    protected $desc = "Database seeder feature";
    
    /**
     *
     * @Argument("creates seeder class")
     *
     */
    public function create(){
        App::make(SeederClassCreation::class)->write();
    }
    
    /**
     * 
     * @Argument("runs seeder class")
     * 
     */
    public function run(){
        App::make(SeederExecution::class)->exec();
    }
    
    /**
     *
     * @Argument("shows all created seeders")
     *
     */
    public function list(){
        App::make(SeederList::class)->showList();
    }
    
    /**
     * 
     * @param WarpArgv $argv
     */
    public function listen(WarpArgv $argv){
        $this->execution($argv);
    }
}