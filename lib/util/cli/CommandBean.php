<?php

namespace lib\util\cli;

use lib\util\EncapsulationHelper;
use lib\inner\App;

class CommandBean {
    
    /**
     * 
     * @var string | array
     */
    protected $command;
    
    /**
     * 
     * @var mixed
     */
    protected $query;
    
    /**
     * 
     * @var array
     */
    protected $middlewares = [];
    
    /**
     * 
     * @var array
     */
    protected $details = [];
    
    /**
     * 
     * @var EncapsulationHelper
     */
    protected $encapsulation;
    
    /**
     * 
     * @param mixed $command
     * @param mixed $query
     */
    public function __construct($command, $query){
        $this->encapsulation = App::make(EncapsulationHelper::class)->instance($this);
        
        $this->command = $command;
        $this->query = $query;
    }
    
    /**
     * 
     * @param string $query
     * @return mixed|object
     */
    public function query($query = null){
        return $this->encapsulation->propertyDefinition($this->{__FUNCTION__}, $query);
    }
    
    /**
     * 
     * @param string|array $command
     * @return mixed|object
     */
    public function command($command = null){
        return $this->encapsulation->propertyDefinition($this->{__FUNCTION__}, $command);
    }
    
    /**
     * 
     * @param array $middlewares
     * @return mixed|object
     */
    public function middlewares(array $middlewares = null){
        return $this->encapsulation->propertyDefinition($this->{__FUNCTION__}, $middlewares);
    }
    
    /**
     * defines description for the registered command
     * 
     * @param string $details
     * @return \lib\util\cli\CommandBean
     */
    public function details(array $details = null){
        return $this->encapsulation->propertyArrayMerge($this->{__FUNCTION__}, $details);
    }
}