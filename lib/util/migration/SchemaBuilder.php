<?php

namespace lib\util\migration;

use Closure;
use lib\traits\SQLFeatures;
use lib\util\StringUtil;
use lib\util\builders\SelectBuilder;
use lib\util\cli\WarpArgv;
use lib\util\datagate\DB;
use lib\util\exceptions\MigrationException;

class SchemaBuilder {
    
    use SQLFeatures;
    
    /**
     * 
     * @var string
     */
    protected $dbms = "mysql";
    
    /**
     * 
     * @var WarpArgv
     */
    protected $args;
    
    /**
     * 
     * @param WarpArgv $argv
     */
    public function __construct(WarpArgv $argv){
        $this->args = $argv->obtainArguments();
    }
    
    /**
     * 
     * @param string $dbms
     * @return \lib\util\migration\SchemaBuilder
     */
    public function connection(string $dbms){
        $this->dbms = $dbms;
        
        return $this;
    }
    
    /**
     * 
     * returns 'true' is the command is to reveal the migration commands
     * 
     * @return boolean
     */
    protected function isRevealing(){
        return strcasecmp($this->args->get(0), "reveal") === 0;
    }
    
    /**
     * 
     * @param string $tblname
     * @param Closure $callback
     * 
     * @return mixed|boolean
     */
    public function create(string $tblname, Closure $callback){
        if(!$this->isRevealing() && $this->hasTable($tblname) !== -1){
            goto nothing;
        }

        $table = new Table($tblname);
        
        if($callback instanceof Closure){
            $callback($table);
            
            $creation = $table->build();
            
            if(!is_blank($creation)){
                if($this->isRevealing()){
                    out()->println("<info>[{$this->dbms}]</info> <warning>{$creation}</warning>");
                }else{
                    ShowTables::getInstance($this->dbms)->addTable($tblname, $this);
                    
                    return DB::connection($this->dbms)->execute($creation);
                }
            }
        }
        
        nothing:
        return false;
    }
    
    /**
     * 
     * @param string $viewName
     * @param string|callable $query
     * 
     * @throws MigrationException
     * 
     * @return boolean
     */
    public function view($viewName, $query){
        if(!$this->isRevealing() && $this->hasTable($viewName) !== -1){
            goto nothing;
        }
        
        $creation = "CREATE VIEW `{$viewName}` AS ";
        
        if(is_callable($query)){
            $query = gate()->call($query);
            
            if(!is_string($query)){
                throw new MigrationException("The callable function must return a string-typed value");
            }    
        }else if($query instanceof SelectBuilder){
            $query = $query->queryString();
        }
        
        if(!is_blank($query)){
            $creation .= $query;
            
            if($this->isRevealing()){
                out()->println("<info>[{$this->dbms}]</info> <warning>{$creation}</warning>");
            }else{
                return DB::connection($this->dbms)->execute($creation);
            }
        }
        
        nothing:
        return false;
    }
    
    /**
     * 
     * @param string $tblname
     * @param Closure $callback
     * 
     * @return boolean
     */
    public function table($tblname, $callback){        
        $verdict = false;
        
        if(!$this->isRevealing() && $this->hasTable($tblname) === -1){
            goto finish;
        }
        
        $table = new Table($tblname);
        
        if($callback instanceof Closure){
            $callback($table);
                        
            if(!$this->isRevealing()){
                $columns = DescribeTable::getInstance()->describe($tblname);
            }
            
            if(is_blank($altering = $table->alter())){    
                return $verdict;
            }
            
            foreach($altering as $index => $altersql){
                $willExecute = true;
                $indicated_cmd = "MODIFY";
                
                if($this->isRevealing()){
                    goto modification;
                }
                
                if(StringUtil::contains($altersql, "ADD")){
                    $indicated_cmd = "ADD";
                    
                    if($columns->colnames()?->has($index)){
                        out()->println("<error>Column `{$tblname}`.`{$index}` already exists</error>");
                        $willExecute = false;
                    }
                }
                
                if(StringUtil::contains($altersql, "DROP")){
                    $indicated_cmd = "DROP";
                    
                    if(StringUtil::contains($altersql, "DROP DEFAULT")){
                        $find = $columns->columns()->binarySearch($index)
                            ->toCompare(function($item){
                                return $item["Field"];
                            })
                            ->get();
        
                        if(!is_null($find)){
                            $find = $find->value();
                            
                            if(is_null($find["Default"])){
                                out()->println("<error>No Default Value for `{$tblname}`.`{$index}`</error>");
                                $willExecute = false;
                            }
                        }else{
                            $willExecute = false;
                        }
                    }else if(!$columns->colnames()->has($index)){
                        out()->println("<error>No column `{$tblname}`.`{$index}`</error>");
                        $willExecute = false;
                    }
                }
                
                if(StringUtil::contains($altersql, "MODIFY") || 
                    StringUtil::contains($altersql, "CHANGE")){
                    $find = $columns->columns()?->binarySearch($index)
                        ->toCompare(function($item){
                            return $item["Field"];
                        })
                        ->get();
                    
                    if(is_null($find)){
                        out()->println("<error>No column `{$tblname}`.`{$index}`</error>");
                        $willExecute = false;        
                    }
                }
                
                modification:
                if($willExecute){
                    if($this->isRevealing()){
                        out()->println("<info>[{$this->dbms}]</info> <warning>{$altersql}</warning>");
                    }else{
                        if($verdict = DB::connection($this->dbms)->execute($altersql)){
                            out()->println("<warning>Column `{$tblname}`.`{$index}`"
                            ." successfully altered [{$indicated_cmd}]</warning>");
                        }else{
                            out()->println("<error>Column `{$tblname}`.`{$index}`"
                            ." wasn't successfully altered [{$indicated_cmd}]</error>");
                        }
                    }
                }
            }
        }
        
        finish:
        return $verdict;
    }
    
    /**
     * returns index of a tablename found in a container
     * 
     * @param string $tblname
     * @return number|mixed
     */
    public function hasTable($tblname){
        $index = -1;
                        
        $find = ShowTables::getInstance()
            ->getTables()
            ->binarySearch($tblname)
            ->get();
        
        if(!is_null($find)){
            $index = $find->key();
        }
        
        return $index;
    }
    
    /**
     * 
     * @param string $tblname
     * @param bool $isView
     * 
     * @return boolean
     */
    public function dropIfExists($tblname, $isView = false){
        if(!$this->isRevealing() && ($index = $this->hasTable($tblname)) === -1){
            return false;
        }
        
        $toDrop = ($isView ? "VIEW" : "TABLE");
        
        $dropstmt = "DROP {$toDrop} IF EXISTS `{$tblname}`";
        
        if($this->isRevealing()){
            out()->println("<info>[{$this->dbms}]</info> <warning>{$dropstmt}</warning>");
        }else{
            if(DB::connection($this->dbms)->execute($dropstmt)){
                ShowTables::getInstance($this->dbms)->removeTable($index, $this);
            }
        }
        
        return true;
    }
 }