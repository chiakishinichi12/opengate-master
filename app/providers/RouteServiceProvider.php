<?php
namespace app\providers;

use lib\inner\ApplicationGate;
use lib\inner\arrangements\ServiceProvider;
use lib\util\GateFinder;
use lib\util\StringUtil;
use lib\util\file\File;
use lib\util\route\Route;

class RouteServiceProvider extends ServiceProvider{
    
    public function boot(ApplicationGate $gate){
        (new GateFinder("routes"))->load(function(File $file){
            if(StringUtil::contains($file->getBasename(), "cli")){
               return; 
            } 
            
            Route::globalType(StringUtil::contains($file->getBasename(), "api") ? "api" : "page");
            
            $file->require();
        });
    }
}