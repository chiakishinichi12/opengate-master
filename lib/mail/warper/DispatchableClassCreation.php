<?php
namespace lib\mail\warper;

use lib\traits\WarperCommons;
use lib\util\Collections;
use lib\util\cli\WarpArgv;
use lib\util\file\FileUtil;

class DispatchableClassCreation {
    
    use WarperCommons;
    
    /**
     *
     * @var string
     */
    protected $warping = "Mailer";
    
    /**
     * 
     * @var Collections
     */
    protected $args;
    
    /**
     * 
     * @var string
     */
    protected $subject;
    
    /**
     * 
     * @var string
     */
    protected $prototype;
    
    /**
     * 
     * @var string
     */
    protected $mailers;
    
    public function __construct(WarpArgv $argv){
        $this->args = $argv->obtainArguments();
        
        $this->subject = $argv->findLongOption("--subject");
        
        $this->mailers = preferences("structure")->get("mailers");
        
        $this->prototype = base_path("lib/mail/prototype");
    }
    
        /**
     *
     * @return \lib\util\Collections
     */
    protected function template(){
        $basetemplate = "DispatchableClass.tmp";
        
        if(!is_null($this->subject)){
            $basetemplate = "SubjectedDispatchable.tmp";
        }
        
        return FileUtil::getContents("{$this->prototype}/{$basetemplate}", true);
    }
    
    public function write(){
        if($this->args->count() < 2){
            return;
        }
        
        $template = $this->template();
        
        $mailer_name = $this->args->get(1);
        
        $this->alterAndCreate($mailer_name, $this->mailers);
        
        $toFind = collect([
            [
                "find" => "[Dispatchable_Class_Name]",
                "replace" => $mailer_name
            ],
            [
                "find" => "[Mailer_Namespace]",
                "replace" => str_replace("/", "\\", relative_path($this->mailers))
            ]
        ]);
        
        if(!is_null($this->subject)){
            if(!is_null($errmsg = $this->evaluateCreation())){
                out()->println("<error>{$errmsg}</error>");
                return;
            }
            
            $toFind->append([
                "find" => "[Mail_Subject]",
                "replace" => $this->subject->value()
            ]);
        }
        
        $this->writeWarpedScript($template,
            $toFind,
            $this->mailers,
            $mailer_name);
    }
    
    protected function evaluateCreation(){
        if(is_blank($this->subject->value())){
            return "Controller Primary route cannot be blanked.";
        }
        
        return null;
    }
}

