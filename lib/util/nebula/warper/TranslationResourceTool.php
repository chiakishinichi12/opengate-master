<?php
namespace lib\util\nebula\warper;

use ReflectionMethod;
use lib\inner\Locale;
use lib\traits\WarperCommons;
use lib\util\Collections;
use lib\util\GateFinder;
use lib\util\ReflectionUtil;
use lib\util\StringUtil;
use lib\util\annotation\Annotation;
use lib\util\annotation\AnnotationUtil;
use lib\util\cli\LongOption;
use lib\util\cli\WarpArgv;
use lib\util\file\File;
use lib\util\file\FileUtil;

/**
 * 
 * @author Jiya Sama
 *
 */
class TranslationResourceTool {
    
    use WarperCommons;
    
    /**
     *
     * @var string
     */
    protected $warping = "Nebula resource";
    
    /**
     * 
     * @var Collections
     */
    protected $args;
    
    /**
     * 
     * @var string
     */
    protected $nebulas;
    
    /**
     * 
     * @var LongOption
     */
    protected $language;
    
    /**
     * 
     * @var LongOption
     */
    protected $copy;
    
    /**
     * 
     * @var string
     */
    protected $prototype;
    
    /**
     * 
     * @param WarpArgv $argv
     */
    public function __construct(WarpArgv $argv){
        $this->args = $argv->obtainArguments();
        
        $this->nebulas = base_path("nebula/lang");
        
        $this->language = $argv->findLongOption("--language");
        
        $this->copy = $argv->findLongOption("--copy");
        
        $this->prototype = base_path("lib/util/nebula/prototype/lang.tmp");
    }
    
    /**
     * 
     * @Argument("lists the existing translation directories")
     * 
     */
    protected function codes(){
        foreach(Locale::languages() as $language){
            out()->println("<warning>{$language}</warning>");
        }
    }
    
    /**
     *
     * @Argument("creates translation directory. { --language=langcode, --copy }. \n NOTE: --copy refers to copying the content of translation files")
     *
     */
    protected function establish(){
        if(is_null($this->language)){
            out()->haltln("[ERROR]: <error>Language can't be established. the option is undefined</error>");
        }
        
        if(is_blank($this->language->value())){
            out()->haltln("[ERROR]: <error>Language can't be established. the option is blanked</error>");
        }
        
        $restrictedChars = ["/", "\\", "."];
        
        if(StringUtil::contains($this->language->value(), $restrictedChars)){
            out()->haltln("[ERROR]: <error>Translation directory cannot contain "
                .implode(", ", $restrictedChars)."</error>");
        }
        
        $langdir = new File("{$this->nebulas}/{$this->language->value()}");
        
        if($langdir->exists()){
            out()->haltln("[ERROR]: <error>Language directory already exists {{$this->language->value()}}</error>");
        }
        
        $langdir->makeDir();
        
        $this->copyIfContainsTranslations($langdir);
        
        out()->println("<warning>Language {$this->language->value()} has been established</warning>");
    }
    
    /**
     * 
     * @Ignore
     * 
     * @param File $langdir
     */
    protected function copyIfContainsTranslations(File $langdir){
        $defaultLocale = preferences("setup")->get("locale");
        
        (new GateFinder(relative_path("{$this->nebulas}/{$defaultLocale}")))
        ->load(function(File $file) use ($langdir){            
            $copiedResourcePath = "{$langdir->getName()}/{$file->getBasename()}";
            
            if(is_null($this->copy)){
                $template = FileUtil::getContents($this->prototype, true);
                
                $this->writeWarpedScript($template,
                    collect(),
                    $langdir->getName(),
                    $this->removeExtension($file->getBasename()));
            }else{
                $file->copyTo($copiedResourcePath);
                
                out()->println("<warning>Copied: </warning><info>".basename($copiedResourcePath)."</info>");
            }
        });
    }
    
    /**
     * 
     * @Argument("lists all the transation tool commands and their descriptions")
     * 
     */
    protected function help(){
        $methods = ReflectionUtil::getDeclaredMethods($this, ReflectionMethod::IS_PROTECTED);
        
        $methods->each(function($method){
            if(AnnotationUtil::hasAnnotation($method, new Annotation("Ignore"))){
                return;
            }
            
            out()->println("[<magenta> {$method->getName()} </magenta>]");
            
            $argument = AnnotationUtil::loadAnnotation($method, (new Annotation("Argument"))
                ->setProperty(["desc", "string", true]));
            
            out()->println("= <warning>{$argument->get("desc")}</warning>\n");
        });
    }
    
    /**
     * 
     * @Ignore
     * 
     * @param string $path
     * @return \lib\util\nebula\warper\string|string
     */
    protected function removeExtension(string $path) {
        // Find the position of the last dot
        $lastDotPosition = strrpos($path, '.');
        
        // If there's no dot, return the path as it is
        if($lastDotPosition === false){
            return $path;
        }
        
        // Remove the extension
        return substr($path, 0, $lastDotPosition);
    }
    
    /**
     * 
     * command execution
     * 
     */
    public function utilize(){
        if($this->args->count() < 2){
            $this->help();
            return;
        }
        
        $method = $this->args->get(1);
        
        if(method_exists($this, $method)){
            $rm = new ReflectionMethod($this, $method);
            
            if(AnnotationUtil::hasAnnotation($rm, new Annotation("Ignore"))){
                out()->haltln("[ERROR]: <error>Forbidden command</error>");        
            }
        }else{
            out()->haltln("[ERROR]: <error>Unknown trans. resource tool command: {{$method}}</error>");
        }
            
        call_user_func([$this, $method]);
    }
}

