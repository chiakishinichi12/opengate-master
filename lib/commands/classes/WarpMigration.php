<?php
namespace lib\commands\classes;

use lib\commands\CommonMethods;
use lib\inner\App;
use lib\inner\ApplicationGate;
use lib\instances\CommandInstance;
use lib\util\cli\WarpArgv;
use lib\util\migration\warper\MigrationClassCreation;
use lib\util\migration\warper\MigrationList;
use lib\util\migration\warper\MigrationRefresh;
use lib\util\migration\warper\MigrationReveal;
use lib\util\migration\warper\MigrationReverse;
use lib\util\migration\warper\MigrationSync;

/**
 *
 * @Commands(["mig", "migrate"])
 *
 */
class WarpMigration extends CommandInstance{
    
    use CommonMethods;
    
    protected $name = "migration";
    
    protected $desc = "Database migration feature";
    
    /**
     * 
     * @Argument("creates migration class { --alter=(string), --view=(string) }")
     * 
     */
    public function create(){
        App::make(MigrationClassCreation::class)->write();
    }
    
    /**
     *
     * @Argument("syncs the table creation. { --class=(migration_name) }\n  special values [table-creator, table-modifier]")
     *
     */
    public function sync(){
        App::make(MigrationSync::class)->migrate();
    }
    
    /**
     * 
     * @Argument("reversing table migration. { --class=(migration_name) }\n  special values [table-creator, table-modifier]")
     * 
     */
    public function reverse(){
       App::make(MigrationReverse::class)->reverse(); 
    }
    
    /**
     *
     * @Argument("displays created migration ")
     *
     */
    public function list(){
        App::make(MigrationList::class)->showList();
    }
    
    /**
     *
     * @Argument("reveals SQL commands used in migration(s). { --class=(migration_name) }\n  special values [table-creator, table-modifier]")
     *
     */
    public function reveal(){
        App::make(MigrationReveal::class)->reveal();
    }
    
    /**
     *
     * @Argument("refreshes created migration(s). { --class=(migration_name) }\n  special values [table-creator, table-modifier]")
     *
     */
    public function refresh(){
        App::make(MigrationRefresh::class)->refresh();
    }
    
    /**
     * 
     * @param ApplicationGate $gate
     * @param WarpArgv $argv
     */
    public function listen(WarpArgv $argv){
        $this->execution($argv);
    }
}