<?php
namespace lib\util;

use Closure;
use lib\util\file\File;

class GateFinder {
    
    /**
     * 
     * @var string
     */
    protected $relativePath;
    
    /**
     * 
     * @var string
     */
    protected $absolutePath;
    
    /**
     * 
     * @var bool
     */
    protected $lookingForClass;
    
    /**
     * 
     * @var array
     */
    protected $extensions = [];
    
    /**
     * 
     * @var Closure
     */
    protected $failedCallback;
    
    /**
     * 
     * @param String $relativePath
     * @param bool $lookingForClass
     */
    public function __construct(String $relativePath = null, bool $lookingForClass = false){
        $this->relativePath = $relativePath;
        $this->lookingForClass = $lookingForClass;
    }
    
    /**
     * bypasses the usage of relative path. absolute path will be acknowledge instead
     * 
     * @param String $absolutePath
     */
    public function absolutePath(String $absolutePath){
        $this->absolutePath = $absolutePath;
        
        return $this;
    }
    
    /**
     * 
     * @param array $extensions
     * @return \lib\util\GateFinder
     */
    public function filterExtensions(Array $extensions){
        $this->extensions = $extensions;
        
        return $this;
    }
    
    /**
     * 
     * @param Closure $failedCallback
     * @return \lib\util\GateFinder
     */
    public function failedCallback($failedCallback){
        $this->failedCallback = $failedCallback;
        
        return $this;
    }
    
    /**
     * 
     * calls failure callback if it's defined
     * 
     */
    protected function invokeFailureCallback(){
        if(!is_null($this->failedCallback)){
            gate()->call($this->failedCallback);
        }
    }
    
    /**
     * 
     * @param Closure $closure
     */
    public function load($closure){
       if(! $closure instanceof Closure){
           return;
       }
        
       $directory = new File($this->absolutePath 
           ? $this->absolutePath 
           : base_path($this->relativePath)); 
       
       if(!$directory->exists()){
           $this->invokeFailureCallback();
           return;
       }
       
       $files = $directory->listFiles(true);
       
       if(!$files->count()){
           $this->invokeFailureCallback();
       }
       
       $files->each(function(File $file) use ($closure){           
           if($this->lookingForClass){
               if($file->isDirectory()){
                   return;
               }
               
               $main_namespace = $this->relativePath;
               $starting_position = strpos($file->getName(), $main_namespace);
               
               if($starting_position !== false){
                   $trimmedPath = substr($file->getName(), $starting_position + strlen($main_namespace));
                   $potential_classname = str_replace("/", "\\", "{$main_namespace}{$trimmedPath}");
               
                   if(StringUtil::endsWith($potential_classname, ".php")){
                       $potential_classname = substr($potential_classname, 0, strrpos($potential_classname, ".php"));
                   }else{
                        goto ends;
                   }
               
                   if(class_exists($potential_classname)){
                        $closure($potential_classname);
                   }
               }
           }else{
               if(count($this->extensions) 
                   && !in_array($file->getExtensionName(), $this->extensions)){
                   goto ends;
               }
               
               $closure($file);
           }
           
           ends:
           return;
       });
    }
}
