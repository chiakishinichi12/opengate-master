<?php

use lib\util\nebula\interpretations\Directive;
use lib\util\nebula\interpretations\DirectiveThrowers;
use lib\util\nebula\interpretations\Keyword;
use lib\util\file\FileUtil;
use lib\util\Collections;
use lib\util\exceptions\ViewLocatingException;
use lib\util\file\File;
use lib\util\StringUtil;
use lib\inner\App;
use lib\util\nebula\ViewMorphMarker;

return new class implements Directive, Keyword {
    
    use DirectiveThrowers;
    
    /**
     *
     * @var string
     */
    protected $name;
    
    /**
     *
     * @var string
     */
    protected $exp;
    
    /**
     *
     * @var Collections
     */
    protected $morphs;
    
    /**
     *
     * @var File
     */
    protected $file;
    
    /**
     *
     * @var string
     */
    protected $viewBase;
    
    /**
     *
     * @var string
     */
    protected $viewPath;
    
    public function __construct(){
        $base = basename(__FILE__);
        $setup = preferences("setup");
        
        $this->name = substr($base, 0, strrpos($base, ".php"));
        
        $this->viewBase = $setup->get("view_base_path");
        
        $this->morphs = App::make(ViewMorphMarker::class)->morphs();
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\nebula\interpretations\Directive::obtains()
     */
    public function obtains($file, $interpreter){
        $this->file = $file;
        
        return $this;
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\nebula\interpretations\Directive::parenthesis()
     */
    public function parenthesis($exp){
        $this->exp = $exp;
        
        return $this;
    }
    
    /**
     *
     *
     * @return mixed|string
     */
    protected function includeRelativePath(){
        // if windows, automatically replace the file separator
        // into linux-based separator
        $path = str_replace("\\", "/", $this->file->getName());
        
        $noBaseFileDir = substr(
            substr($path, 0, strripos($path, "/")),
            strlen($this->viewBase) + 1);
        
        $currentFile = substr($path, strlen($this->viewBase) + 1);
        
        $this->viewPath = str_replace("\"", "", $this->exp);
        
        $unmasked  = str_replace(".", "/", $this->viewPath).".morph.php";
        
        if(StringUtil::contains($currentFile, "/")){
            $relative  = "{$noBaseFileDir}/{$unmasked}";
            $encrypted = $this->morphs->get($relative);
            
            if(!is_null($encrypted)){
                return str_replace("{$noBaseFileDir}/", "", $encrypted);
            }
        }
        
        return $this->morphs->get($unmasked);
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\nebula\interpretations\Directive::interpretation()
     */
    public function interpretation(){
        $this->requireParenthesis();
        
        if(!is_null($morphPath = $this->includeRelativePath())){
            $this->exp = str_replace($this->viewPath, $morphPath, $this->exp);
        }else{
            throw new ViewLocatingException("<b>[INCLUSION ERROR]</b> No matching view could be found "
                ."for '{$this->viewPath}' in {$this->file->getBasename()}");
        }
        
        return "<?php {$this->name}({$this->exp}) ?>";
    }
    
};