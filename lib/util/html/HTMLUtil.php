<?php
namespace lib\util\html;

use lib\util\Collections;
use lib\util\StringUtil;

class HTMLUtil {
    
    protected $expression;
    
    /**
     * 
     * @param string $expression
     * @return \lib\util\html\HTMLUtil
     */
    public function setHTMLString($expression){
        $this->expression = $expression;
        
        return $this;
    }
    
    /**
     * 
     * @return \lib\util\Collections
     */
    public function getHTMLElements(){
        $elements = collect();
        
        $this->recursiveMatching($elements, $this->expression);
        
        return $elements;
    }
    
    /**
     * 
     * @param Collections $elements
     * @param string $html
     * @return \lib\util\Collections
     */
    protected function recursiveMatching($elements, $html){
        $matches = [];
        $attrMatches = [];
        
        // compliance to PHP 8.* update
        if(is_null($html)){
            return;
        }
        
        $pattern = '/<(\w+)([^>]*)>(.*?)<\/\1>/s';
        preg_match_all($pattern, $html, $matches, PREG_SET_ORDER);
        
        collect($matches)->each(function($match) use ($attrMatches, $elements){
            $expression = $match[0];
            $tagName = $match[1];
            $attributes = $match[2];
            $content = $match[3];
            
            if(StringUtil::hasHTMLExpression($content)){
                $this->recursiveMatching($elements, $content);
            }
            
            preg_match_all('/(\w+)\s*=\s*(?:"([^"]*)"|\'([^\']*)\')/', $attributes, $attrMatches, PREG_SET_ORDER);
            
            $attrArray = collect();
            
            collect($attrMatches)->each(function($attrMatch) use ($attrArray){
                $attrName = $attrMatch[1];
                $attrValue = $attrMatch[2] ?: $attrMatch[3];
                $attrArray->put ($attrName, $attrValue);
            });
                
            $elements->add(new HTMLElement($tagName, $attrArray, $content, $expression));
        });
    }
}