<?php
namespace lib\inner;

use lib\util\nebula\Translation;

/**
 * 
 * 
 * @author Jiya Sama
 *
 * @name Locale
 *
 * 
 * @method static void set($lang)
 * @method static mixed get()
 * @method static mixed trans($transPath, $params)
 * @method static boolean exists($lang)
 * @method static mixed localeTrans($transPath, $params = null, $locale = "en")
 * @method static mixed arr($transPath, $locale = "en")
 * @method static \lib\util\Collections languages()
 * @method static string browser()
 * @method static string ipLoc()
 * @method static string|null dynamic()
 * @method static string iso()
 *
 */
class Locale extends Accessor {
    
    const JAPANESE = "ja";
    
    /**
     * 
     * @var array
     */
    protected $alias = [
        "set" => "setLocale",
        "get" => "getLocale",
        "trans" => "get",
        "exists" => "isLocaleAvailable",
        "localeTrans" => "localizedTrans",
        "arr" => "localizedArray", 
        "languages" => "getAvailableLanguages",
        "browser" => "getBrowserLanguage",
        "ipLoc" => "getIpAddressLanguage"
    ];
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\inner\Accessor::warpedInstance()
     */
    public function warpedInstance(){
        return Translation::class;
    }
}