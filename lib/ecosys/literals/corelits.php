<?php
/*
 * 
 * Framework Version
 * 
 */
define("OPENGATE_VERSION", "7.28.14");
/*
 * =======================================================
 */
define("NO_STRING", "");
define("NO_COUNT", 0);
define("STANDARD_TIMESTAMP_FORMAT", "Y-m-d H:i:s");