<?php
namespace lib\inner\arrangements\http;

use lib\inner\ApplicationGate;
use lib\util\route\Route;

class Kernel{
    
    /**
     * 
     * @var ApplicationGate
     */
    protected $gate;
    
    /**
     * 
     * @var array
     */
    protected $bootstrappers = [
        \lib\startup\HandleExceptions::class,
        \lib\startup\General::class,
        \lib\startup\BootHttpApplicationUtil::class,
        \lib\startup\StartSession::class,
        \lib\startup\ProviderHandler::class
    ];
    
    /**
     * 
     * @param ApplicationGate $gate
     */
    public function __construct(ApplicationGate $gate){
        $this->gate = $gate;
        $this->bootstrap();
    }
    
    /**
     *
     * bootstrapping function
     *
     */
    protected function bootstrap(){
        if(!$this->gate->hasBeenBootstrapped()){
            $this->gate->bootstrapWith($this->bootstrappers);
        }
        
        Route::release();
    }
    
    /**
     * 
     * termination callback
     * 
     */
    public function terminate(){
        $this->gate->terminate();
    }
}