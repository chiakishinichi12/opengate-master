<?php
namespace lib\util\model\relational;

use lib\util\Collections;
use lib\util\builders\SelectBuilder;
use lib\util\model\Model;

interface RelationalMaterial {
    
    /**
     * 
     * @param mixed $class
     * @return RelationalMaterial|Model|string
     */
    function ownerModel(mixed $class = null);
    
    /**
     * 
     * @param mixed $class
     * @return RelationalMaterial|Model|string
     */
    function foreignModel(mixed $class = null);
    
    /**
     * 
     * @param string $value
     * @return RelationalMaterial|string
     */
    function foreignKey(string $value = null);
    
    /**
     * 
     * @param string $value
     * @return RelationalMaterial|string
     */
    public function ownerKey(string $value = null);
    
    /**
     * 
     * @return Model|Collections|mixed
     * 
     */
    function fetched() : Model|Collections|SelectBuilder ;
    
    /**
     *
     * evaluates if the relational model has a default model instance
     *
     * @return boolean
     */
    function hasDefault() : bool;
    
    /**
     *
     * sets a default parent model when a child doesn't have one
     *
     * @param array|callable $default
     * @return \lib\util\model\relational\RelationalMaterial
     */
    function withDefault(array|callable|Model $default = null) : RelationalMaterial;
    
    /**
     *
     * retrieves the Model object out of queried relationship clause
     *
     * @return \lib\util\model\relational\RelationalMaterial
     */
    function retrieve() : RelationalMaterial;
}

