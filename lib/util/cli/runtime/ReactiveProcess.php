<?php
namespace lib\util\cli\runtime;

use Closure;

/**
 * 
 * 
 * @author Antonius Aurelius
 *
 */
class ReactiveProcess {
    
    /**
     * 
     * @var string
     */
    protected $command;
    
    /**
     * 
     * @var string
     */
    protected $mode = "r";
    
    /**
     * 
     * @var Process
     */
    protected $process;
    
    /**
     * 
     * @var Closure
     */
    protected $outputcb;
    
    /**
     * 
     * @var Closure
     */
    protected $completecb;
    
    /**
     * 
     * @var string
     */
    protected $directory;
    
    /**
     * 
     * @param string $command
     * @return \lib\util\cli\runtime\ReactiveProcess
     */
    public function command(string $command){
        $this->command = $command;
        
        return $this;
    }
    
    /**
     * 
     * @param string $mode
     * @return \lib\util\cli\runtime\ReactiveProcess
     */
    public function mode(string $mode){
       $this->mode = $mode;
       
       return $this;
    }
    
    /**
     * 
     * @param string $directory
     * @return \lib\util\cli\runtime\ReactiveProcess
     */
    public function directory(string $directory){
        $this->directory = $directory;
        
        return $this;
    }
    
    /**
     * 
     * this must be the last method on the chain
     * 
     * @return \lib\util\cli\runtime\ReactiveProcess
     */
    public function exec(){        
        $this->process = new Process($this->command);
        
        if(!is_blank($this->directory)){
            $this->process->setDirectory($this->directory);
        }
        
        $this->reading($this->mode);
        
        $this->finalize();
        
        return $this;
    }
    
    /**
     * 
     * @param Closure $outputcb
     * @return \lib\util\cli\runtime\ReactiveProcess
     */
    public function during(Closure $outputcb){
        $this->outputcb = $outputcb;
        
        return $this;
    }
    
    /**
     * 
     * @param string $mode
     */
    protected function reading(string $mode){
        $this->process->start($mode);
        
        $linePosition = 1;
        
        while($this->process->isRunning()){
            $line = $this->process->getOutput();
            
            if(!is_null($this->outputcb)){                
                call_user_func($this->outputcb, $line, $linePosition, $this->process);
            }
            
            $linePosition++;
        }
        
        $this->process->wait();
    }
    
    /**
     * 
     * during post execution callback
     * 
     */
    protected function finalize(){
        if(!is_null($this->completecb)){
            call_user_func($this->completecb, $this->process);
        }
    }
    
    /**
     * 
     * @param Closure $completecb
     * @return \lib\util\cli\runtime\ReactiveProcess
     */
    public function complete(Closure $completecb){
        $this->completecb = $completecb;
        
        return $this;    
    }
}

