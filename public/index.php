<?php

use lib\inner\ApplicationGate;
use lib\inner\arrangements\http\Kernel;

/**
 * 
 * all framework inclusions will start here.
 * consider this as Laravel's parody.
 * 
 * but to be honest, I'm a big fan of that framework
 * Taylor Otwell's idea brings peace to the world
 * 
 */
require_once __DIR__."/../lib/master.php";

/**
 * 
 * @var ApplicationGate $gate
 */
$gate = require_once __DIR__."/../startup/gate.php";

$gate->initAppReader();

$kernel = $gate->make(Kernel::class);

$kernel->terminate();