<?php
namespace lib\util\model\warper;

use ReflectionClass;
use lib\inner\ApplicationGate;
use lib\util\GateFinder;
use lib\util\exceptions\ModelException;
use lib\util\exceptions\SQLException;
use lib\util\file\FileUtil;
use lib\util\model\Model;
use lib\inner\App;

class ModelList{
    
    /**
     *
     * @var ApplicationGate
     */
    protected $gate;
    
    public function __construct(ApplicationGate $gate){
        $this->gate = $gate;
    }
    
    public function showList(){
        echo "Established Model Classes:\n\n";
        
        $failedCallback = function(){
            out()->println("<error>No model classes found.</error>");
            die();
        };
        
        $modelspath = preferences("structure")->get("models");
        
        if(!FileUtil::exists($modelspath)){
            $this->gate->call($failedCallback);
        }
        
        (new GateFinder(relative_path($modelspath), true))->failedCallback($failedCallback)->load(function($class){
            if(!App::checkInheritance($class, Model::class)){
                throw new ModelException("Class {{$class}} is not an instance of ".Model::class);
            }
            
            $table_name = (new ReflectionClass($class))->getProperty("table_name");
            
            if(!is_null($table_name)){
                $table_name->setAccessible(true);
                
                try{
                    $table_name = "<magenta>[{$table_name->getValue(new $class())}]</magenta>";
                }catch(SQLException $ex){
                    $table_name = "{<warning>{$ex->getMessage()}</warning>}";
                }
            }else{
                $table_name = "";
            }
            
            out()->println("{$class} {$table_name}");
        });
    }
}

