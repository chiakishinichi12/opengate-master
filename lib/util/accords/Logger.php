<?php
namespace lib\util\accords;

interface Logger {
    /**
     * 
     * @param unknown $content
     * @param string $logType
     * @param boolean $templated
     * @param unknown $saveToDir
     */
    function log($content, $logType = LOG_INFO, $templated = true, $saveToDir = null);
}