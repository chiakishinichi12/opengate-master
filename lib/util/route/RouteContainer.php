<?php

namespace lib\util\route;

use lib\util\Collections;

class RouteContainer {
     
    /**
     * 
     * @var Collections
     */
    protected $routes;
    
    public function __construct(){    
        $this->routes = collect();
    }
    
    /**
     * 
     * @param RouteBean $routeBean
     */
    public function appendRoute(RouteBean $routeBean){
        $this->routes->add($routeBean);
    }
    
    /**
     * 
     * @return \lib\util\Collections
     */
    public function getRoutes(){
        return $this->routes;
    }
}