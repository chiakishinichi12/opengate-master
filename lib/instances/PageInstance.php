<?php

namespace lib\instances;

use lib\inner\WebAppCore;

abstract class PageInstance extends WebAppCore {
       
    /**
     * 
     * @param string $method
     */
    public function __construct(string $method = "GET"){
        parent::__construct();
        
        $this->responseConfig([
            "method" => $method,   
            "contentType" => "text/html"
        ]);
    }   
    
    /**
     * 
     * validating request
     */
    public function inspect(){
        $this->onSysCheck("page");
    }
}