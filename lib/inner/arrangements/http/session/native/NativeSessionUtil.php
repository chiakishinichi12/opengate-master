<?php
namespace lib\inner\arrangements\http\session\native;

use lib\exceptions\OpenGateException;
use lib\inner\App;
use lib\inner\ApplicationGate;
use lib\inner\arrangements\http\ApiResponseThrower;
use lib\inner\arrangements\http\session\SessionLifetime;
use lib\util\accords\SessionUtil;
use lib\util\builders\SessionValue;
use lib\util\exceptions\SessionException;

/**
 * 
 * @author Jiya Sama
 *
 * Session library that uses native session functions.
 *
 */
class NativeSessionUtil implements SessionUtil{
    
    use SessionLifetime, ApiResponseThrower;
    
    /**
     * 
     * @var ApplicationGate
     */
    protected $gate;
    
    /**
     * 
     * @var string
     */
    protected $name;
    
    /**
     * 
     * @param ApplicationGate $gate
     */
    public function __construct(ApplicationGate $gate){
        $this->gate = $gate;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\accords\SessionUtil::initSessionLibrary()
     */
    public function initSessionLibrary(){
        if(session_status() === PHP_SESSION_NONE && !App::checkCLI()){
            session_start();
            
            $this->evalSessionExpiration();
        }   
    }
    
    /**
     *
     * @param string $name
     * @param mixed $value
     */
    public function set($name, $value){
        $this->throwIfOnApiResponse(__FUNCTION__);
        
        if(!$this->active($name)){
            /**
             * 
             * when there's expiration defined.
             * 
             */
            $this->limitSession($name);
            
            $_SESSION[$name] = $value;
        }
    }
    
    /**
     *
     * @param string $name
     * @return SessionValue
     */
    public function get($name){
        $this->monitorSession($name);
        
        return new SessionValue($name, $_SESSION[$name]);
    }
    
    /**
     *
     * @param string $name
     */
    public function unset($name){
        $this->throwIfOnApiResponse(__FUNCTION__);
        
        $this->monitorSession($name);
        
        unset($_SESSION[$name]);
    }
    
    /**
     *
     * @param string $name
     * @param mixed $value
     */
    public function modify($name, $value){
        $this->throwIfOnApiResponse(__FUNCTION__);
        
        $this->monitorSession($name);
        
        $_SESSION[$name] = $value;
    }
    
    /**
     *
     * @param string $name
     * @return boolean
     */
    public function active($name){
        return isset($_SESSION[$name]);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\accords\SessionUtil::expires()
     */
    public function expires($duration){
        $this->duration = $duration;
    }
    
    /**
     *
     * clear all the session variables and its values.
     */
    public function purgeAll(){
        $this->throwIfOnApiResponse(__FUNCTION__);
        
        if(session_status() === PHP_SESSION_ACTIVE){
            session_unset();
            session_destroy();
        }
    }
    
    /**
     *
     * @param string $name
     * @throws OpenGateException
     */
    protected function monitorSession($name){
        if(!$this->active($name)){
            throw new SessionException("Session '{$name}' is not defined");
        }
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\accords\SessionUtil::temporary()
     */
    public function temporary($name, $value = null){
        if(is_null($value)){
            if($this->active($this->tempsname)){
                $temps = $_SESSION[$this->tempsname];
                
                return collect($temps)->has($name);
            }
            
            return false;
        }
        
        $tempdata = [$name];
        
        if($this->active($this->tempsname)){
            $oldset = $this->get($this->tempsname)->value();
            
            if(collect($oldset)->has($name)){
                return;
            }
            
            $tempdata = array_merge($tempdata, $oldset);
            $this->modify($this->tempsname, $tempdata);
        }else{
            $this->set($this->tempsname, $tempdata);
        }
        
        if(!$this->active($name)){
            $this->set($name, $value);
        }
    }

    /**
     * 
     * removes all temporary session data when already used 
     * 
     */
    public function __destruct(){
        $this->rmvtemp();
    }
}