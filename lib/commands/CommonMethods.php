<?php
namespace lib\commands;

use ReflectionMethod;
use lib\inner\App;
use lib\util\ReflectionUtil;
use lib\util\annotation\Annotation;
use lib\util\annotation\AnnotationProperty;
use lib\util\annotation\AnnotationUtil;
use lib\util\cli\WarpArgv;

trait CommonMethods {
    
    /**
     *
     * @Argument("shows the list of available %s commands")
     *
     */
    public function help(){
        $argument = (new Annotation("Argument"))
        ->setProperty(new AnnotationProperty("description", "string", true));
        
        $iteration = function(ReflectionMethod $method, $index) use ($argument){
            $properties = collect();
            
            if(AnnotationUtil::hasAnnotation($method, $argument, $properties)){
                $description = $properties->get("description");
                
                if(strcmp($method->getName(), "help") === 0){
                    $description = sprintf($description, $this->name);
                }
                
                out()->println("[ <magenta>{$method->getName()}</magenta> ]\n"
                ."= <warning>{$description}</warning>\n");
            }
        };
        
        ReflectionUtil::getDeclaredMethods($this, ReflectionMethod::IS_PUBLIC)->each($iteration);
    }
    
    /**
     * 
     * @param WarpArgv $argv
     */
    protected function execution(WarpArgv $argv){
        $args = $argv->obtainArguments();
        
        if(!$args->count()){
            out()->println("<error>No arguments found.</error>\n");
            $this->help();
            return;
        }
        
        $func = $args->get(0);
        
        if(method_exists($this, $func)){
            App::call([$this, $func]);
        }
    }
}

