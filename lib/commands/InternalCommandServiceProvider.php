<?php
namespace lib\commands;

use ReflectionClass;
use lib\inner\ApplicationGate;
use lib\inner\arrangements\ServiceProvider;
use lib\util\GateFinder;
use lib\util\annotation\AnnotationUtil;
use lib\util\annotation\Annotation;
use lib\util\annotation\AnnotationProperty;
use lib\util\cli\Console;

class InternalCommandServiceProvider extends ServiceProvider{
    
    /**
     * 
     * @var boolean
     */
    protected $forCLI = true;
    
    /**
     * 
     * @param ApplicationGate $gate
     */
    public function boot(ApplicationGate $gate){
        (new GateFinder("lib/commands/classes", true))->load(function($class){
            $rc = new ReflectionClass($class);
            
            $annotation = (new Annotation("Commands"))
                ->setProperty(new AnnotationProperty("keywords", "array", true));
                        
            $properties = [];
            
            if(AnnotationUtil::hasAnnotation($rc, $annotation, $properties)){
                Console::command($properties->get("keywords"), $class);
            }
        });
    }
}