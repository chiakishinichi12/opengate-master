<?php
namespace lib\inner\arrangements\http\session\file;

use Firebase\JWT\JWT;
use lib\inner\arrangements\http\CookieGate;
use lib\util\StringUtil;
use lib\util\builders\CookieFactory;
use lib\util\file\File;
use lib\util\file\FileUtil;
use Firebase\JWT\SignatureInvalidException;
use lib\util\Collections;

class SessionCookie{
    
    /**
     * 
     * @var string
     */
    protected const COOKIE_NAME = "warp_og_session";
    
    /**
     * 
     * @return object|mixed|NULL
     */
    public static function obtain(){      
        if($cookie = CookieGate::get(self::COOKIE_NAME)){
            try{
                $payload = JWT::decode($cookie, self::getSecretKey(), ["HS256"]);
            }catch(\Exception $e){
                /**
                 * 
                 * this will catch whenever the key file is lost or ammended by some circumstances
                 * 
                 */
                if($e instanceof SignatureInvalidException){
                    $payload = self::rectifySessionCookie($cookie);
                }
            }

            return $payload;
        }
                
        return null;
    }
    
    /**
     * 
     * resolves session data when the session key is deleted/modified
     * 
     */
    protected static function rectifySessionCookie($cookie){
        $tokens = explode(".", $cookie);
        $pref = preferences("session");
                
        $newSecretKey = self::getSecretKey();
        
        $payload = json_decode(base64_decode($tokens[1]));
                
        $cookie = CookieFactory::create(self::COOKIE_NAME)
        ->value(JWT::encode($payload, $newSecretKey))
        ->path("/")
        ->httpOnly($pref->get("http_only"))
        ->samesite($pref->get("same_site"))
        ->expires(self::getSessionIdLifetime($pref));
        
        $modified = CookieGate::modify($cookie);
                
        return $modified ? $payload : false;
    }
    
    /**
     * 
     * returns secret key from the key file for the session token payload.
     * creates a new key file with a new secret whenever it's ammended or deleted.
     * 
     * @return string|\lib\util\Collections|NULL
     */
    protected static function getSecretKey(){
        $keypath = base_path("stockade/sesskey");
                
        if(is_null($secret = FileUtil::getContents($keypath))){
            $secret = base64_encode(StringUtil::getRandomizedString(400));
            FileUtil::write($keypath, $secret, "w+");
        }
        
        return $secret;
    }
    
    /**
     * 
     * @param Collections $pref
     * @return number
     */
    protected static function getSessionIdLifetime(Collections $pref){
        if(!is_null($lifetime = $pref->get("session_id_lifetime"))){
            return time() + $lifetime;
        }
        
        /*
         * if null, session is set to last for 30 days
         *
         */
        return time() + 30 * 86400;
    }
    
    /**
     * @param int $duration
     * 
     * @return boolean
     */
    public static function create($duration = null){ 
        $pref = preferences("session");
        
        $duration = self::getSessionIdLifetime($pref);
        
        $payload = [
            "sessionId" => StringUtil::getRandomizedString(50),
            "dateCreated" => date(STANDARD_TIMESTAMP_FORMAT),
            "toExpire" => date("Y-m-d H:i:s", $duration)
        ];
        
        $cval = JWT::encode($payload, self::getSecretKey());
        
        $cookie = CookieFactory::create(self::COOKIE_NAME)
        ->value($cval)
        ->path("/")
        ->httpOnly($pref->get("http_only"))
        ->samesite($pref->get("same_site"))
        ->expires($duration);
        
        $created = CookieGate::set($cookie);
        
        return $created ? $payload : false;
    }
    
    /**
     * 
     * completely removes all session instances on the cookies and cache
     * 
     */
    public static function destroy(){
        CookieGate::delete(self::COOKIE_NAME);
        
        $secret = new File(base_path("stockade/sesskey"));
        
        if($secret->exists()){
            $secret->delete();
        }
    }
}

