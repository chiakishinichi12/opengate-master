<?php
namespace lib\handlers;

use Throwable;
use lib\util\ConfigVar;

trait ExceptionHandlerCallbacks{
    
    /**
     * 
     * @param Throwable $e
     */
    protected function cliExceptionCallback(Throwable $e){
        $timezone = ConfigVar::get("system_date_timezone");
        
        $currdate = date(STANDARD_TIMESTAMP_FORMAT);
        
        $traceResult = "<darkred background=\"true\"> [UNCAUGHT EXCEPTION {".get_class($e)."}]:</darkred> \n";
        $traceResult .= "<info>{$currdate} ({$timezone})</info>\n";
        
        $traceResult .= " {<magenta>{$e->getMessage()}</magenta>} on Line {$e->getLine()} ({$e->getFile()})\n";
        $traceResult .= "\n Traces:\n\n";
        
        foreach($e->getTrace() as $k => $trace){
            $traceNum = $k + 1;
            
            $traceCollect = collect($trace);
            
            $line = $traceCollect->get("line");
            $file = $traceCollect->get("file");
            $clss = $traceCollect->get("class");
            $func = $traceCollect->get("function");
            $type = $traceCollect->get("type");
            
            if(!empty($clss)){
                $clss = "{$clss}{$type}";
            }
            
            $traceResult .= " {$traceNum}.) <darkyellow>Line {$line}</darkyellow> of [{$file} | <warning>{$clss}{$func}()</warning>]\n";
        }
        
        out()->haltln($traceResult);
    }
    
    /**
     * 
     * @param Throwable $e
     */
    protected function webExceptionCallback(Throwable $e){
        $traceResult = "<title>[!!]{$e->getMessage()}</title>";
        $traceResult .= "<div style=\"margin:5px;border:1px solid black;padding:10px;\">";
        $traceResult .= "<code>";
        $traceResult .= "!![<strong>UNCAUGHT EXCEPTION {".get_class($e)."}</strong>]: {$e->getMessage()} on ";
        $traceResult .= "<strong>Line {$e->getLine()}</strong> <br/>({$e->getFile()})<br/>";
        
        $timezone = ConfigVar::get("system_date_timezone");
        $currdate = date(STANDARD_TIMESTAMP_FORMAT);
        
        $traceResult .= "<br/>[TIMESTAMP]: <strong>{$currdate}</strong> ({$timezone})<br/>";
        $traceResult .= "<br/><strong>Traces</strong><br/>";
        
        foreach($e->getTrace() as $k => $trace){
            $traceNum = $k + 1;
            
            $traceCollect = collect($trace);
            
            $line = $traceCollect->get("line");
            $file = $traceCollect->get("file");
            $clss = $traceCollect->get("class");
            $func = $traceCollect->get("function");
            $type = $traceCollect->get("type");
            
            if(!empty($clss))
                $clss = "{$clss}{$type}";
                
                $traceResult .= "<br/> {$traceNum}.) <strong>Line {$line}</strong> of [{$file} | <strong>{$clss}{$func}()</strong>]";
        }
        
        $traceResult .= "<br/><br/>";
        $traceResult .= "<hr/>";
        $traceResult .= "<strong>Server-Software: </strong>{$this->request->server("SERVER_SOFTWARE")}";
        $traceResult .= "<br/><br/><hr/>";
        
        $url = "{$this->request->getProtocolUsed()}{$this->request->server("HTTP_HOST")}{$this->request->server("REQUEST_URI")}";
        
        $traceResult .= "<strong>Request Details[{$this->request->server("REQUEST_METHOD")}]</strong><br/>";
        $traceResult .= "<strong>URL</strong>: {$url}";
        $traceResult .= "<br/><br/>";
        $traceResult .= "==<strong> Request Headers </strong>==<br/><br/>";
        
        foreach($this->request->headers() as $header => $value){
            $traceResult .= "[<strong>{$header}</strong>] {$value}<br/>";
        }
        
        $traceResult .= "<br/>";
        $traceResult .= "<hr/>";
        $traceResult .= "<strong>Response Details</strong><br/><br/>";
        $traceResult .= "Status Code: {$this->response->statusCode()}<br/>";
        $traceResult .= "</code>";
        $traceResult .= "</div>";
        
        out()->haltln($traceResult);
    }
    
    /**
     *
     * @param Throwable $e
     * @return string
     */
    protected function reportingContent(Throwable $e){
        $exception_class = get_class($e);
        
        $errorTraceBuilder = "[UNCAUGHT EXCEPTION: {$exception_class}] {$e->getMessage()} on Line {$e->getLine()}\n";
        $errorTraceBuilder .= "{{$e->getFile()}}\n\n";
        
        $currdate = date(STANDARD_TIMESTAMP_FORMAT);
        
        $timezone = preferences("setup")->get("system_date_timezone");
        
        $errorTraceBuilder .= "[TIMESTAMP]: {$currdate} ({$timezone})\n\n[Traces]\n\n";
        
        foreach($e->getTrace() as $index => $trace){
            $traceNum = $index + 1;
            
            $traceCollect = collect($trace);
            
            $line = $traceCollect->get("line");
            $file = $traceCollect->get("file");
            $clss = $traceCollect->get("class");
            $func = $traceCollect->get("function");
            $type = $traceCollect->get("type");
            
            if(!empty($clss)){
                $clss = "{$clss}{$type}";
            }
            
            $tracestr = "{$traceNum}.) Line {$line} of [{$file} | {$clss}{$func}()]\n";
            
            $errorTraceBuilder .= $tracestr;
        }
        
        return $errorTraceBuilder;
    }
}

