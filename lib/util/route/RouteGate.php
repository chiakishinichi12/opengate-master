<?php
namespace lib\util\route;

use Closure;
use lib\exceptions\OpenGateException;
use lib\inner\App;
use lib\instances\APIInstance;
use lib\instances\CommandInstance;
use lib\instances\PageInstance;
use lib\util\exceptions\MethodNotFoundException;
use lib\util\exceptions\RoutingException;

class RouteGate {
        
    /**
     * 
     * @var array
     */
    protected $methods = [
        "get", 
        "post", 
        "put", 
        "patch",
        "delete", 
        "any"
    ];
    
    /**
     * 
     * @var string
     */
    protected $expectedType;
    
    /**
     * 
     * sets the route tracking enabled
     * 
     */
    public function release(){
        $locator = App::make(RouteLocator::class)->locateRoute();
        
        if(!$locator->isLocated()){
            response()
            ->statusCode(404)
            ->errorTemplate();
        }
    }
    
    /**
     * 
     * @param string|array $uri
     * @param mixed $query
     * 
     * @return \lib\util\route\RouteBean
     */
    protected function appendRoute(string|array $uri, mixed $query){
        $routeBean = new RouteBean($uri, $query);
        
        App::make(RouteContainer::class)->appendRoute($routeBean);
        
        return $routeBean;
    }
    
    /**
     * 
     * @param string $type
     * @param string $method
     * @return string[]
     */
    protected function defaultResponseConfig(string $type, string $method){
        $contentType = $type === "api" ? "text/json" : "text/html";
        
        $responseConfig = [
            "contentType" => $contentType,
            "method" => $method
        ];
        
        return $responseConfig;
    }
    
    /**
     * 
     * @param bool $throw
     * @param string $message
     * @return boolean
     */
    protected function throwingOrJustFalse(bool $throw, string $message){
        if($throw){
            throw new OpenGateException($message);
        }
        
        return false;
    }
    
    /**
     * 
     * @param string $uri
     * @param string $instance
     * 
     */
    public function responseInstance(string|array $uri, string $instance){
        $this->isResponseInstance($instance, true);
        
        $instObject = App::make($instance);
        
        if(method_exists($instObject, "configure")){
            App::call([$instObject, "configure"]);
        }
        
        return $this->appendRoute($uri, $instance)
            ->type($this->getResponseTypeByInstance($instance))
            ->responseConfig($instObject->responseConfig())
            ->middlewares($instObject->middlewares());
    }
    
    /**
     * 
     * @param mixed $instance
     * 
     * @return string
     */
    protected function getResponseTypeByInstance(mixed $instance){
        if(App::checkInheritance($instance, APIInstance::class)){
            return "api";
        }
        
        return "page";
    }
    
    /**
     *
     * @param string $instance
     * @param boolean $throw
     * @throws OpenGateException
     * @return boolean
     */
    protected function isResponseInstance($instance, $throw = false){
        if(is_object($instance)){
            $instance = class_name($instance);
        }
        
        if(!class_exists($instance)){
            return $this->throwingOrJustFalse($throw, "Class '{$instance}' doesn't exist");
        }
        
        if(!App::checkInheritance($instance, [APIInstance::class, PageInstance::class, CommandInstance::class])){
            return $this->throwingOrJustFalse($throw, "Class '{$instance}' is not a subclass of APIInstance|PageInstance|CommandInstance");
        }
        
        return true;
    }
    
    /**
     * 
     * returns true if an expected method is valid
     * 
     * @param string $method
     * 
     * @return boolean
     */
    public function isValidMethod(string $method){
        return in_array(strtolower($method), $this->methods, true);
    }
    
    /**
     * 
     * @param array $hosts
     * @param callable $callback
     */
    public function subdomain(array $hosts, $callback){
        $host = request()->server("HTTP_HOST");
        
        if(in_array($host, $hosts, true)){
            if($callback instanceof Closure){
                App::call($callback);
            }
        }
    }
    
    /**
     * 
     * @param string $type
     * @return string
     */
    public function globalType(string $type = null){
        if(is_blank($type)){
            return $this->expectedType;
        }
        
        switch(strtolower($type)){
            case "page":
            case "api":
                $this->expectedType = $type;
                break;
            default:
                throw new RoutingException("Unknown response type: {{$type}}");
        }
    }
    
    /**
     * 
     * @param string $method
     * @param ...$params
     * @return \lib\util\route\RouteBean
     */
    public function __call($method, $params){
        if(in_array(strtolower($method), $this->methods, true)){
            $pstack = collect($params);
            
            if($pstack->length() >= 2){
                $rtype = $pstack->get(2) ?: $this->globalType();
                
                $config = $this->defaultResponseConfig($rtype, $method);
                
                return $this->appendRoute(...$params)
                    ->method($method)
                    ->responseConfig($config)
                    ->type($rtype);
            }
        }else if(!method_exists($this, $method)){
            throw new MethodNotFoundException("Unknown method: ".class_name($this)."::{$method}");
        }
        
        $this->$method(...$params);
    }
}