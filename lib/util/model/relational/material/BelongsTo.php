<?php
namespace lib\util\model\relational\material;

use lib\inner\App;
use lib\util\Collections;
use lib\util\EncapsulationHelper;
use lib\util\model\Model;
use lib\util\model\relational\RelationalMaterial;
use lib\util\model\relational\RelationalModelException;
use lib\util\builders\SelectBuilder;

class BelongsTo implements RelationalMaterial{
        
    use MaterialCommon;
    
    /**
     * 
     * @var EncapsulationHelper
     */
    protected $encapsulator;
    
    /**
     * 
     * @var Model
     */
    protected $default;
    
    public function __construct(){
        $this->encapsulator = App::make(EncapsulationHelper::class)->instance($this);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\model\relational\RelationalMaterial::ownerModel()
     */
    public function ownerModel(mixed $class = null) {
        return $this->encapsulator->appendProperty(__FUNCTION__, $class);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\model\relational\RelationalMaterial::foreignModel()
     */
    public function foreignModel(mixed $class = null) {
        return $this->encapsulator->appendProperty(__FUNCTION__, $class);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\model\relational\RelationalMaterial::foreignKey()
     */
    public function foreignKey(string $value = null) {
        return $this->encapsulator->propertyDefinition($this->{__FUNCTION__}, $value);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\model\relational\RelationalMaterial::ownerKey()
     */
    public function ownerKey(string $value = null) {
        return $this->encapsulator->propertyDefinition($this->{__FUNCTION__}, $value);
    }
    
    /**
     *
     * setting defaults according to their primary key.
     *
     */
    protected function initialize(){
        // associated to the current model which uses the foreign key from relation model
        if(is_blank($this->foreignKey)){
            $this->foreignKey = $this->relation
            ->getPrimaryKey()
            ->getName();
        }
        
        // associated to the foreign model
        if(is_blank($this->ownerKey)){
            $this->ownerKey = $this->relation
            ->getPrimaryKey()
            ->getName();
        }
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\model\relational\RelationalMaterial::retrieve()
     */
    public function retrieve() : RelationalMaterial{
        $this->materialize();
        
        if(isset($this->model->{$this->foreignKey})){
            $foreignKeyValue = $this->model->{$this->foreignKey};
            
            $this->retrieved = $this->relation->first(
                [$this->ownerKey => $foreignKeyValue]
            );
        }
        
        if(is_null($this->retrieved) && !is_null($this->default)){
            $this->retrieved = $this->default;
        }
        
        return $this;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\model\relational\RelationalMaterial::hasDefault()
     */
    public function hasDefault() : bool {
        return !is_null($this->default);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\model\relational\RelationalMaterial::withDefault()
     */
    public function withDefault(array|callable|Model $default = null) : RelationalMaterial{
        $parent = $this->foreignModel();
        
        if(!is_string($parent)){
            $parent = class_name($this->foreignModel());
        }
        
        $this->default = new $parent;
        
        if(is_array($default)){
            foreach($default as $property => $value){
                $this->default->$property = $value;
            }
        }else if(is_object($default)){
            if($default instanceof Model){
                if(!App::checkInheritance($default, $parent)){
                    throw new RelationalModelException("Default Parent Model is not an instance of {$parent} class");
                }
            
                $this->default = $default;
            
            }else if(is_callable($default)){
                $default($this->default);
            }
        }
        
        return $this;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\model\relational\RelationalMaterial::fetched()
     */
    public function fetched() : Model|Collections|SelectBuilder {
        return $this->retrieved;
    }
}