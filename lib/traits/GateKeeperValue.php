<?php
namespace lib\traits;

use lib\util\GateKeeper;

trait GateKeeperValue {
    
    protected function retrieveValue(){
        /*
         * 
         * obtaining the values warped by some possibilities of utilizing GateKeeper
         * 
         */
        return GateKeeper::fetchValue($this);
    }
}