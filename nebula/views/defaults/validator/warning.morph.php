<div class="uk-alert-warning" uk-alert>
    <a href class="uk-alert-close" uk-close></a>
    <p>{{ $message }}</p>
</div>