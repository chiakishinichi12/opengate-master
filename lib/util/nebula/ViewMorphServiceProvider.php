<?php
namespace lib\util\nebula;

use lib\inner\ApplicationGate;
use lib\inner\arrangements\ServiceProvider;
use lib\util\Collections;
use lib\util\GateFinder;
use lib\util\file\File;
use lib\util\file\FileUtil;
use lib\inner\App;

class ViewMorphServiceProvider extends ServiceProvider{
    
    /**
     * 
     * @var boolean
     */
    protected $forAll = true;
    
    /**
     * 
     * @var ApplicationGate
     */
    protected $gate;
    
    /**
     * 
     * @var string
     */
    protected $base;
    
    /**
     * 
     * @var string
     */
    protected $cache;
    
    /**
     * 
     * @var Collections
     */
    protected $morphs;
    
    /**
     * 
     * @var ViewMorphMarker
     */
    protected $marker;
    
    /**
     * 
     * @param ApplicationGate $gate
     */
    public function boot(ApplicationGate $gate){
        $this->gate = $gate;
        
        $setup = preferences("setup");
        
        $this->base = $setup->get("view_base_path");
        
        $this->cache = $setup->get("view_morph_cache");
        
        App::singleton(ViewMorphMarker::class);
        
        $this->marker = App::make(ViewMorphMarker::class);
        
        $this->morphs = $this->marker->morphs();
        
        $this->checkExistingMorphAvailability();        
        $this->loadViewTemplates($gate);
    }
    
    /**
     * 
     * checks the availability of a morphed view on cache and marker
     * if the morphed file isn't log on the marker. it will be automatically deleted.
     * 
     */
    protected function checkExistingMorphAvailability(){
        $currentCount = $this->morphs->count();
        
        $this->morphs->each(function($morphedPath, $relativePath){
            if(!FileUtil::exists("{$this->base}/{$relativePath}")){
                $this->morphs->remove($relativePath);
            }
        });
        
        if($currentCount > $this->morphs->count()){
            $this->marker->push();
        }
        
        (new GateFinder())
        ->absolutePath($this->cache)
        ->load(function(File $file){
            if(!$file->isDirectory()){
                $morphedRelativePath = substr($file->getName(), strlen($this->cache) + 1);
                
                if(!$this->morphs->has($morphedRelativePath)){
                    $file->delete();
                }
            }
        });
    }
    
    /**
     * 
     * creates morphed code file @ stockade caching dir
     * 
     */
    protected function loadViewTemplates(){
        $interpreter = $this->gate->make(ViewMorphInterpreter::class);
        
        (new GateFinder())
        ->absolutePath($this->base)
        ->load(function(File $file) use ($interpreter){
            $relativePath = substr($file->getName(), strlen($this->base) + 1);
            
            $interpreter->morph($file, $relativePath);
        });
        
        $interpreter->archived();
    }
}