<?php
namespace lib\inner;

use lib\inner\arrangements\ContextualComponent;

/**
 * 
 * @method static void setCurrentContextualItem(mixed $currentContextualItem)
 * @method static mixed getCurrentContextualItem()
 * @method static string getController
 * @method static bool isController()
 * @method static bool isClosure()
 * @method static bool isInstance()
 * @method static bool handles(string|array $controller)
 */
class Context extends Accessor{
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\inner\Accessor::warpedInstance()
     */
    public function warpedInstance(){
        return ContextualComponent::class;
    }
}

