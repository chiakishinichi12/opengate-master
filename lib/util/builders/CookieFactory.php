<?php
namespace lib\util\builders;

class CookieFactory {
    
    /**
     * 
     * @param string $cookieName
     * 
     * @return \lib\util\builders\Cookie
     */
    public static function create($cookieName){
        return new Cookie($cookieName);
    }
}