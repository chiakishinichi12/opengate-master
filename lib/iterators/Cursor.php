<?php

namespace lib\iterators;

interface Cursor {
    /**
     * determines if cursor has began fetching
     * 
     * @return bool
     */
    function hasBegan(): bool;
    
    /**
     * 
     * Move the cursor to the first row.
     * @return boolean
     */
    function moveToFirst(): bool ;
    
    /**
     * 
     * Move the cursor to the last row.
     * @return boolean
     */
    function moveToLast(): bool ;
    
    /**
     * 
     * Move the cursor to the next row.
     * @return boolean
     */
    function moveToNext(): bool ;
    
    /**
     * Move the cursor to the previous row.
     * @return boolean
     */
    function moveToPrevious(): bool ;
    
    /**
     * 
     * Move the cursor to an absolute position.
     * 
     * @param int $index
     * @return boolean
     */
    function moveToPosition(int $index): bool ;
    
    /**
     * 
     * Returns whether the cursor is pointing to the last row.
     * @return boolean
     */
    function isLast(): bool ;
    
    /**
     * 
     * 
     * Returns whether the cursor is pointing to the first row.
     * @return boolean
     */
    function isFirst(): bool ;
    
    /**
     * Returns true if the value in the indicated column is null.
     * 
     * @param int $index
     * @return boolean
     */
    function isNull(int $index): bool ;
    
    /**
     * 
     * Returns the object that has the cursored index
     * @return mixed
     */
    function getCursoredItem();
}