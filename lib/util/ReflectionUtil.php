<?php
namespace lib\util;

use ReflectionClass;
use ReflectionFunction;
use ReflectionMethod;
use lib\inner\App;
use lib\util\route\RouteArgs;
use lib\util\cli\WarpArgv;

class ReflectionUtil {

    /**
     * 
     * To resolve and map parameter values for a given closure or method 
     * based on class type hints or URI data, with fallbacks to default values 
     * or empty strings.
     * 
     * @param callable|array $closureObject
     * 
     * @return Collections
     */
    public static function resolveParameterData($closureObject){
        $paramdata = App::checkCLI() 
            ? App::make(WarpArgv::class)->obtainArguments()
            : App::make(RouteArgs::class)->getValues();
        
        if(App::isRunningUnitTest()){
            $paramdata = App::make(RouteArgs::class)->getValues();
        }
            
        if(is_array($closureObject)){
            $args = (new ReflectionMethod(...$closureObject))->getParameters();
        }else{
            $args = (new ReflectionFunction($closureObject))->getParameters();
        }
        
        $resolvedArguments = collect();
        
        collect($args)->each(function($arg, $index) use ($resolvedArguments, $paramdata){
            if(!is_null($param = Tool::getParameterClassName($arg))){
                $resolvedArguments->put($index, App::make($param));
            }else{
                if(!$paramdata->hasBegan()){
                    if($paramdata->moveToFirst()){
                        $resolvedArguments->put($index, $paramdata->getCursoredItem());
                    }else{
                        goto obtainDefaultValue;
                    }
                }else if($paramdata->moveToNext()){
                    $resolvedArguments->put($index, $paramdata->getCursoredItem());
                }else{
                    obtainDefaultValue:
                    if($arg->isDefaultValueAvailable()){
                        $resolvedArguments->put($index, $arg->getDefaultValue());
                    }else{
                        $resolvedArguments->put($index, "");
                    }
                }
            }
        });
                
        return $resolvedArguments;
    }
    
    /**
     * 
     * iterates all declared methods within a class
     * 
     * @param ReflectionClass|string $reflection
     * @param int $filter
     * 
     * @return Collections
     */
    public static function getDeclaredMethods($reflection, $filter, $withInherited = false){
        $declaredMethods = collect();
        
        if(! $reflection instanceof ReflectionClass){
            if(!is_object($reflection)){
                if(is_string($reflection) && !class_exists($reflection)){
                    goto ends;
                }
            }
            
            $reflection = new ReflectionClass($reflection); 
        }
        
        $iteration = function(ReflectionMethod $method) use ($reflection, $declaredMethods, $withInherited){
            if(!$withInherited){
                if($method->getDeclaringClass()->getName() === $reflection->getName()){
                    $declaredMethods->add($method);
                }
            }else{
                $declaredMethods->add($method);
            }
        };
        
        collect($reflection->getMethods($filter))->each($iteration);
        
        ends:
        return $declaredMethods;
    }
}