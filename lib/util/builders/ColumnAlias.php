<?php

namespace lib\util\builders;

use lib\traits\Sanitation;

/**
 * 
 * @author Jiya Sama
 *
 */
class ColumnAlias {
    
    use Sanitation;
    
    /**
     * 
     * @var CondVal|string
     */
    public $real;
    
    /**
     *
     * @var CondVal|string
     */
    public $alias;
    
    /**
     * 
     * @param unknown $real
     * @param unknown $alias
     */
    protected function __construct($real, $alias){
        $this->real  = $real;
        $this->alias = $alias;
    }

    /**
     * 
     * @return string
     */
    public function __toString(){
        $this->alias = $this->escape($this->alias);
        
        if(is_string($this->real)){            
            $enclosed = $this->isAggregateFuncUsage($this->real);
            $this->hasInstanceColumn($this->real, !$enclosed);
        }

        return "{$this->real} AS {$this->alias}";
    }
    
    /**
     * 
     * @param mixed $real
     * @param mixed $alias
     * @return \lib\util\builders\ColumnAlias
     */
    public static function rep($real, $alias){
        return new ColumnAlias($real, $alias);
    }
}