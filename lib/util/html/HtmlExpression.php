<?php
namespace lib\util\html;

/**
 * Represents an HTML expression.
 *
 * This class allows for encapsulating HTML expressions as objects, providing a convenient
 * way to work with HTML content programmatically.
 */
class HtmlExpression{
    
    protected $exp;
    
    /**
     * 
     * @param string $exp
     */
    public function __construct(string $exp){
        $this->exp = $exp;
    }
    
    /**
     * 
     * @return string
     */
    public function __toString(){
        return $this->exp;
    }
}