<?php

namespace lib\util\cli;

use lib\exceptions\OpenGateException;
use lib\inner\App;
use lib\traits\IOUtil;

final class IO {
    
    use IOUtil;
    
    public function __construct(){
        if(!App::checkCLI())
            throw new OpenGateException("IO Cannot be utilized on the web");
    }
    
    /**
     * 
     * @param String $guide
     * @return string
     */
    public function readData(String $guide){
        return $this->scanUserInput($guide);
    }
}