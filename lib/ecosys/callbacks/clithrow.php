<?php

use lib\util\ConfigVar;

function cliExceptionCallback(Throwable $e){
    $timezone = ConfigVar::get("system_date_timezone");
    
    $currdate = date(STANDARD_TIMESTAMP_FORMAT);
    
    out()->print("<darkred background=\"true\"> [UNCAUGHT EXCEPTION {".get_class($e)."}]:</darkred> ");
    
    if(isset($currdate)){
        out()->println("<info>{$currdate} ({$timezone})</info>");
    }
    
    out()->println(" {<magenta>{$e->getMessage()}</magenta>} on Line {$e->getLine()} ({$e->getFile()})");
    out()->println("\n Traces:\n");
    
    foreach($e->getTrace() as $k => $trace){
        $traceNum = $k + 1;
        
        $traceCollect = collect($trace);
        
        $line = $traceCollect->get("line");
        $file = $traceCollect->get("file");
        $clss = $traceCollect->get("class");
        $func = $traceCollect->get("function");
        $type = $traceCollect->get("type");
        
        if(!empty($clss)){
            $clss = "{$clss}{$type}";
        }
            
        out()->println(" {$traceNum}.) <darkyellow>Line {$line}</darkyellow> of [{$file} | <warning>{$clss}{$func}()</warning>]");
    }
    
    exit(1);
}