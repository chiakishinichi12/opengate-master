<?php
namespace lib\inner\json;

use lib\util\EncapsulationHelper;
use lib\inner\App;
use lib\exceptions\OpenGateException;

class JsonEncoder{
    
    /**
     * 
     * @var array|object
     */
    protected $material;
    
    /**
     * 
     * @var string
     */
    protected $encoded;
    
    /**
     * 
     * @var boolean
     */
    protected $minify = false;
    
    /**
     * 
     * @var integer
     */
    protected $depth = 512;
    
    /**
     * 
     * @var EncapsulationHelper
     */
    protected $encapsulator;
    
    public function __construct(){
        $this->encapsulator = App::make(EncapsulationHelper::class)->instance($this);
    }
    
    /**
     * 
     * @param object $value
     * @return object|JsonEncoder
     */
    public function material(array|object $material = null){
        return $this->encapsulator->propertyDefinition($this->{__FUNCTION__}, $material);
    }
    
    /**
     * 
     * @param bool $minify
     * @return bool|JsonEncoder
     */
    public function minify(bool $minify = null){
        return $this->encapsulator->propertyDefinition($this->{__FUNCTION__}, $minify);
    }
    
    /**
     * 
     * @param int $value
     * @return int|JsonEncoder
     */
    public function depth(int $value){
        return $this->encapsulator->propertyDefinition($this->{__FUNCTION__}, $value);
    }
    
    /**
     * 
     * @return \lib\inner\json\JsonEncoder
     */
    public function encode(){
        if(is_null($this->material)){
            throw new OpenGateException("No JSON encoding material defined");
        }
        
        $printFormat = !$this->minify ? JSON_PRETTY_PRINT : 0;
        
        $this->encoded = json_encode($this->material, $printFormat, $this->depth);
        
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function encoded(){
        return $this->encoded;
    }
    
    /**
     * 
     * @return string
     */
    public function __toString(){
        if(is_blank($this->encoded())){
            return "";
        }
        
        return $this->encode()->encoded();
    }
}

