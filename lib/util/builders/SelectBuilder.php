<?php

namespace lib\util\builders;

use Closure;
use lib\inner\App;
use lib\traits\Sanitation;
use lib\util\Collections;
use lib\util\Paginator;
use lib\util\SequelDump;
use lib\util\collections\ModelCollections;
use lib\util\datagate\DB;
use lib\util\datagate\SQLBuilderHelper;
use lib\util\exceptions\QueryBuildingException;
use lib\util\model\Model;
use lib\util\model\NotColumn;
use lib\util\model\QueryFunction;
use lib\util\model\TableColumn;
use lib\util\model\WhereExpression;
use lib\util\paginators\Paginateable;
use lib\util\paginators\QueryPaginator;
use lib\util\resolver\MethodInvocationResolver;

/**
 * 
 * @author Jiya Sama
 *
 */
class SelectBuilder implements Paginateable{
    
    use Sanitation, SQLBuilderHelper;
    
    /**
     * 
     * @var string
     */
    protected $table_name;
    
    /**
     * 
     * @var string
     */
    protected $columns = "*";
    
    /**
     * 
     * @var string
     */
    protected $where = "1";
    
    /**
     * 
     * @var array
     */
    protected $wheres = [];
    
    /**
     * 
     * @var string
     */
    protected $join = "";
    
    /**
     * 
     * @var array
     */
    protected $joins = [];
    
    /**
     * 
     * @var string
     */
    protected $having = "";
    
    /**
     * 
     * @var array
     */
    protected $havings = [];
    
    /**
     * 
     * @var string
     */
    protected $group_by = "";
    
    /**
     * 
     * @var string
     */
    protected $order_by = "";
    
    /**
     * 
     * @var string
     */
    protected $limit = "";
    
    /**
     * 
     * @var string
     */
    protected $modelClass = "";
    
    /**
     * 
     * @var boolean
     */
    protected $hasMultipleTables = false;
    
    /**
     * 
     * @param string|array $table_name
     * @param string $dbms
     */
    public function __construct($table_name, string $dbms = "mysql"){
        $this->dbms = $dbms;
        
        $this->initBuilder($table_name);
    }
    
    /**
     * 
     * @param mixed $table_name
     * @param mixed $qu
     */
    protected function initBuilder($table_name){
        if(is_array($table_name)){
            if(count($table_name) > 1){
                $this->hasMultipleTables = true;
            }
             
            $this->table_name = $this->aliasedTable($table_name, false);
        }else{
            $this->table_name = $table_name;
        }
    }
    
    /**
     * 
     * @param mixed $aliasedOrAggregation
     * @param string $operator
     * @param string $value
     * 
     * @return \lib\util\builders\SelectBuilder
     */
    public function orHaving($aliasedOrAggregation, $operator = null, $value = null){
        $this->having($aliasedOrAggregation, $operator, $value, "OR");
        
        return $this;
    }
    
    /**
     * 
     * @param mixed $aliasedOrAggregation
     * @param string $operator
     * @param string $value
     * @param string $boolean
     * @return \lib\util\builders\SelectBuilder
     */
    public function having($aliasedOrAggregation, $operator = null, $value = null, $boolean = "AND"){
        if(is_string($aliasedOrAggregation)){
            $havingExpression = $this->havingClause($aliasedOrAggregation, $operator, $value);
            
            if(count($this->havings)){
                $this->havings[] = "{$boolean} {$havingExpression} ";
            }else{
                $this->havings[] = "{$havingExpression} ";
            }
        }else if($aliasedOrAggregation instanceof Closure && is_null($operator)){
            $this->havingNested($aliasedOrAggregation, $boolean);
        }
        
        return $this;
    }
    
    /**
     * 
     * @param Closure $callback
     * @param string $boolean
     */
    protected function havingNested($callback, $boolean){
        $forNestedHave = $this->newBuilderInstance();
        $callback($forNestedHave);
        
        $nested = $forNestedHave->getStackedHavingClause();
        
        if(count($this->havings)){
            $this->havings[] = "{$boolean} ({$nested}) ";
        }else{
            $this->havings[] = "({$nested}) ";
        }
    }
    
    /**
     * 
     * @param string $aliasedOrAggregation
     * @param string $operator
     * @param string $value
     * @return string
     */
    protected function havingClause($aliasedOrAggregation, $operator, $value){
        $quoted = true;
        
        $frontingOperator = $this->provideSuitableOperator($operator, $value, $quoted);
        
        $enclosed = $this->isAggregateFuncUsage($aliasedOrAggregation);
        $this->hasInstanceColumn($aliasedOrAggregation, !$enclosed);
        
        if(!$this->hasInstanceColumn($value, false)){
            if($quoted && !is_blank($frontingOperator)){
                $value = "\"{$value}\"";
            }
        }
        
        return trim("{$aliasedOrAggregation} {$frontingOperator} {$value}");
    }
    
    /**
     * 
     * @param mixed $table_name
     * @param mixed $column1
     * @param string $operator
     * @param string $column2
     * @return \lib\util\builders\SelectBuilder
     */
    public function rightJoin($table_name, $column1 = null, $operator = null, $column2 = null){
        /**
         *
         * the value depends on the function name
         *
         * @var string $joinType
         */
        $joinType = strtoupper(str_replace("Join", "", __FUNCTION__));
        
        $this->joinCalls($joinType, $table_name, $column1, $operator, $column2);
        
        return $this;
    }
    
    /**
     * 
     * @return \lib\util\builders\SelectBuilder
     */
    protected function newBuilderInstance(){
        $builder = new SelectBuilder($this->table_name);
        
        $builder->hasMultipleTables = $this->hasMultipleTables();
        
        return $builder;
    }
    
    /**
     * 
     * @param mixed $table_name
     * @return \lib\util\builders\SelectBuilder
     */
    public function crossJoin($table_name){
        /**
         *
         * the value depends on the function name
         *
         * @var string $joinType
         */
        $joinType = strtoupper(str_replace("Join", "", __FUNCTION__));
        
        $this->joinCalls($joinType, $table_name, null, null, null);
        
        return $this;
    }
    
    /**
     *
     * @param mixed $table_name
     * @param mixed $column1
     * @param string $operator
     * @param string $column2
     * @return \lib\util\builders\SelectBuilder
     */
    public function leftJoin($table_name, $column1 = null, $operator = null, $column2 = null){
        /**
         *
         * the value depends on the function name
         *
         * @var string $joinType
         */
        $joinType = strtoupper(str_replace("Join", "", __FUNCTION__));
        
        $this->joinCalls($joinType, $table_name, $column1, $operator, $column2);
        
        return $this;
    }
    
    /**
     *
     * @param mixed $table_name
     * @param mixed $column1
     * @param string $operator
     * @param string $column2
     * @return \lib\util\builders\SelectBuilder
     */
    public function innerJoin($table_name, $column1 = null, $operator = null, $column2 = null){
        /**
         * 
         * the value depends on the function name
         * 
         * @var string $joinType
         */
        $joinType = strtoupper(str_replace("Join", "", __FUNCTION__));
        
        $this->joinCalls($joinType, $table_name, $column1, $operator, $column2);
        
        return $this;
    }
    
    /**
     * 
     * @param mixed $table_name
     * @param string $column1
     * @param string $operator
     * @param string $column2
     */
    protected function joinCalls($joinType, $table_name, $column1 = null, $operator = null, $column2 = null){        
        if(is_string($column1)){
            $plainJoinClause = $this->joinClause($table_name, $column1, $operator, $column2);
            
            $this->joins[] = "{$joinType} JOIN {$plainJoinClause} ";
        }else if($column1 instanceof Closure){
            $this->joinNested($joinType, $table_name, $column1, $operator, $column2); 
        }else if(is_null($column1) && strcmp($joinType, "CROSS") === 0){
            if(is_array($table_name)){
                $table_name = $this->aliasedTable($table_name);
            }
            
            $this->joins[] = "{$joinType} JOIN {$table_name}";
        }
    }
    
    /**
     * 
     * @param unknown $table_name
     * @param unknown $column1
     * @param unknown $operator
     * @param unknown $column2
     * @throws QueryBuildingException
     */
    protected function joinNested($joinType, $table_name, $column1, $operator, $column2){
        $forNestedJoin = $this->newBuilderInstance();
        
        $returning = $column1($forNestedJoin);
        
        if(is_array($table_name)){
            $table_name = $this->aliasedTable($table_name);
        }
        
        $nested = $forNestedJoin->getStackedJoinClause();
        
        if(strcmp($joinType, "CROSS") !== 0){
            if(!is_string($returning)){
                throw new QueryBuildingException("Joining Callback must return a string and it should be a table or aliased column");
            }
                
            $frontingOperator = $this->provideSuitableOperator($operator, $column2);
                
            $this->hasInstanceColumn($returning);
            $this->hasInstanceColumn($column2);
                
            if(!empty($nested)){
                $this->joins[] = "{$joinType} JOIN ({$table_name} {$nested}) ON {$returning} {$frontingOperator} {$column2} ";
            }
        }else{
            $this->joins[] = "{$joinType} JOIN ({$table_name} {$nested})";
        }
    }
    
    /**
     * 
     * @param array $aliasing
     * @param bool $first
     * @return string
     */
    protected function aliasedTable(array $aliasing, bool $first = true){
        $from = [];
        
        foreach($aliasing as $key => $tbl){
            $alias = is_string($key) ? " {$key}" : "";
            $from[] = "{$tbl}{$alias}";
            
            if($first){ 
                break;
            }
        }
        
        return implode(", ", $from);
    }
    
    /**
     * 
     * @param mixed $operator
     * @param mixed $changeValue
     * @return string
     */
    protected function provideSuitableOperator($operator, &$changeValue, &$quoted = true){
        if(is_null($operator)){
            $frontingOperator = "";
            goto returns;
        }
        
        switch(strtoupper($operator)){
            case "!=":
            case ">=":
            case "<=":
            case ">":
            case "<":
            case "=":
            case "LIKE":
            case "IN":
                $frontingOperator = $operator;
                break; 
            case "IS NULL":
            case "IS NOT NULL":
                $frontingOperator = "";
                $changeValue = $operator;
                $quoted = false;
                break;
            default:
                $frontingOperator = "=";
                $changeValue = $operator;
                break;
        }
        
        returns:
        return $frontingOperator;
    }
    
    /**
     * 
     * @param mixed $table_name
     * @param mixed $column1
     * @param mixed $operator
     * @param mixed $column2
     * @return string
     */
    protected function joinClause($table_name, $column1 = null, $operator = null, $column2 = null){
        $frontingOperator = $this->provideSuitableOperator($operator, $column2);
        
        if(is_array($table_name)){
            $table_name = $this->aliasedTable($table_name);
        }
        
        $this->hasInstanceColumn($column1);
        $this->hasInstanceColumn($column2);
        
        return "{$table_name} on {$column1} {$frontingOperator} {$column2}";
    }
    
    /**
     * 
     * @param Model $model
     */
    public function setModelClass(string $modelClass){
        $this->modelClass = $modelClass;
        
        return $this;
    }
    
    /**
     * 
     * @param array $cols
     * @param bool $distinct
     * @return \lib\util\builders\SelectBuilder
     */
    public function columns(array $cols, bool $distinct = false){
        $fields = [];
                
        foreach($cols as $key => $value){
            if(is_string($key)){
                // value is the alias
                $value = $this->escape($value);
                
                $enclosed = $this->isAggregateFuncUsage($key);
                $this->hasInstanceColumn($key, !$enclosed);
                
                $fields[] = "{$key} AS \"{$value}\"";
            }else if($value instanceof ColumnAlias){
                $fields[] = $value;
            }else{
                $enclosed = $this->isAggregateFuncUsage($key);
                $this->hasInstanceColumn($value, !$enclosed);
                $fields[] = $value;
            }
        }
                
        if(count($fields)){
            $this->columns = implode(", ", $fields);
            
            if($distinct){
                $this->columns = "DISTINCT {$this->columns}";
            }
        }
        
        return $this;
    }
    
    /**
     * 
     * @param bool $condition
     * @param unknown $whereBuilderFunc
     */
    public function ifWhat(bool $condition, $whereBuilderFunc){
        if($condition && ($whereBuilderFunc instanceof Closure)){
            $whereBuilderFunc($this);
        }
        
        return $this;
    }
    
    /**
     * 
     * @param string|callable $column
     * @param mixed $operator
     * @param mixed $value
     * @param string $boolean
     * 
     * @return \lib\util\builders\SelectBuilder
     */
    public function where($column, $operator = null, $value = null, $boolean = "AND"){
        if(is_string($column) || is_int($column)){
            $clause = $this->whereClause($column, $operator, $value);
                        
            if(count($this->wheres)){
                $this->wheres[] = " {$boolean} {$clause}";
            }else{
                $this->wheres[] = $clause;
            }
        }else if($column instanceof Closure && is_null($operator)){
            $this->whereNested($column, $boolean);
        }
        
        return $this;
    }
    
    
    /**
     * 
     * @param mixed $callback
     * @param mixed $boolean
     */
    protected function whereNested($callback, $boolean){
        $forNestedWhere = $this->newBuilderInstance();
        
        $callback($forNestedWhere);
        
        $nested = $forNestedWhere->getStackedWhereClause();
        
        if(!empty($nested)){
            if(count($this->wheres)){
                $this->wheres[] = " {$boolean} ({$nested})";
            }else{
                $this->wheres[] = "({$nested})";
            }
        }
    }
    
    /**
     * 
     * @param mixed $column
     * @param mixed $operator
     * @param mixed $value
     * @return \lib\util\builders\SelectBuilder
     */
    public function orWhere($column, $operator = null, $value = null){
        $this->where($column, $operator, $value, "OR");
        
        return $this;
    }
    
    /**
     * 
     * @param mixed $column
     * @param mixed $operator
     * @param mixed $value
     * @return string
     */
    protected function whereClause($column, $operator, $value){
        $quoted = true;
        
        $frontingOperator = $this->provideSuitableOperator($operator, $value, $quoted);
                
        if($value instanceof TableColumn){
            $enhanced = $value->columnName;
            
            // modify
            $this->hasInstanceColumn($enhanced);
            
            $value = $enhanced;
        }else if($value instanceof WhereExpression){
            $value = $value->expression;
        }else if($value instanceof NotColumn){
            $value = $value->sanitized();
        }else if($value instanceof QueryFunction){
            $value = $value->expression();
        }else if(is_array($value)){
            $value = collect($value)->cmap(function($item){
                return is_string($item) ? "\"{$this->escape($item)}\"" : $item;
            })->implode(", ");
            
            $value = "({$value})";
        }else if($this->hasMultipleTables()){
            if(!$this->hasInstanceColumn($value, false)){
                $value = $this->quoting($quoted, $frontingOperator, $value);
            }
        }else{
            $value = $this->quoting($quoted, $frontingOperator, $value);
        }
        
        $this->hasInstanceColumn($column);
        
        return trim("{$column} {$frontingOperator} {$value}");
    }
    
    /**
     * 
     * @param boolean $quoted
     * @param string $frontingOperator
     * @param mixed $value
     * @return string|mixed
     */
    protected function quoting($quoted, $frontingOperator, $value){
        if($quoted && !is_blank($frontingOperator)){
            return "\"{$value}\"";
        }
        
        return $value;
    }
    
    /**
     * 
     * @param SelectBuilder|string $query
     * @param string $boolOp
     * @return \lib\util\builders\SelectBuilder
     */
    public function whereExists(SelectBuilder|string $query, string $boolOp = "AND"){
        if(is_object($query)){
            $expression = "EXISTS({$query->limit(null)->queryString()})";
        }else{
            $expression = "EXISTS($query)";
        }
        
        if(count($this->wheres)){
            $expression = " {$boolOp} {$expression}";
        }
        
        $this->wheres[] = $expression;
        
        return $this;
    }
    
    /**
     * 
     * @param SelectBuilder|string $query
     * @return \lib\util\builders\SelectBuilder
     */
    public function orWhereExists(SelectBuilder|string $query){
        return $this->whereExists($query, "OR");
    }
    
    /**
     * 
     * @param string $whereClause
     * @param array|string $params
     * @param string $boolOp
     * 
     * @return \lib\util\builders\SelectBuilder
     */
    public function whereRaw(string $whereClause, array|string $params = null, string $boolOp = "AND"){
        if(!is_null($params)){
            if(is_array($params)){
                foreach($params as $k => $param){
                    $param = $this->escape($param);
                    $whereClause = str_replace("({$k})", "\"{$param}\"", $whereClause);
                }
            }else{
                $boolOp = $params;
            }
        }
        
        $expression = "{$whereClause} ";
        
        if(count($this->wheres)){
            $expression = " {$boolOp} {$expression}";
        }
        
        $this->wheres[] = $expression;
        
        return $this;
    }
    
    /**
     * 
     * @param string $whereClause
     * @param array|string $params
     * @return \lib\util\builders\SelectBuilder
     */
    public function orWhereRaw(string $whereClause, array|string $params = null){
        return $this->whereRaw($whereClause, $params, "OR");
    }
    
    /**
     * 
     * @param string $column
     * @return \lib\util\builders\SelectBuilder
     */
    public function groupBy($column, $withRollup = false){
        if(!empty($column)){
            if(is_array($column)){                
                $stack = [];
                
                foreach($column as $col){
                    $enclosed = $this->isAggregateFuncUsage($col);
                    $this->hasInstanceColumn($col, !$enclosed);
                    
                    $stack[] = $col;
                }
                
                $column = implode(", ", $stack);
            }else{
                $enclosed = $this->isAggregateFuncUsage($column);
                
                $this->hasInstanceColumn($column, !$enclosed);
            }
            
            $rollup = $withRollup ? "WITH ROLLUP" : "";
            
            $this->group_by = "GROUP BY {$column} {$rollup}";
        }
        
        return $this;
    }
    
    /**
     * 
     * @param String $column
     * @param String $direction
     * @return \lib\util\builders\SelectBuilder
     */
    public function orderBy(string $column, string $direction = "ASC"){
        $aggregate = $this->isAggregateFuncUsage($column);
        
        $this->hasInstanceColumn($column, !$aggregate);
        
        $this->order_by = "ORDER BY {$column} {$direction}";
        
        return $this;   
    }
    
    /**
     * 
     * @param int $offset
     * @param int $length
     * 
     * @return \lib\util\builders\SelectBuilder
     */
    public function limit(int $offset = null, int $length = null){
        // if offset is null, the limit clause will not be included on the query
        if(is_null($offset)){
            $this->limit = false;
            goto ends;
        }
        
        if($length !== null){
            $this->limit = "LIMIT {$offset}, {$length}";
        }else{
            // $offset variable will take its values as LIMIT length in case the $length is null
            $this->limit = "LIMIT {$offset}";
        }
        
        ends:
        return $this;
    }
    
    /**
     * 
     * echoes the SQL query generated by the builder
     * 
     * @return SelectBuilder
     */
    public function dump(){
        out()->println($this->queryString());
        
        return $this;
    }
    
    /**
     * 
     * Checks if the model instance has multiple tables.
     * 
     * This method returns a boolean value indicating whether the modek 
     * instance is associated with multiple tables. It can be useful in 
     * scenarios where the presence of multiple tables affects the behavior 
     * or output of the instance.
     * 
     * @return boolean
     */
    public function hasMultipleTables(){
        return $this->hasMultipleTables;
    }
    
    /**
     * 
     * @param String $refstr
     * 
     * @return \lib\util\builders\SelectBuilder
     */
    public function obtainQuery(string &$refstr){
        $refstr = $this->queryString();
        
        return $this;
    }
    
    /**
     * 
     * @param int $rowsPerPage
     * 
     * return QueryPaginator
     */
    public function paginate(int $rowsPerPage, int $page = 1) : Paginator{
        $this->checkIfTheresColumnDefined();
                
        return App::make(QueryPaginator::class)
            ->builder($this)
            ->rowsPerPage($rowsPerPage)
            ->fetchPageItems($page);
    }
    
    /**
     * 
     * checks if the builder has model class
     * 
     * @return boolean
     */
    public function hasModelClass(){
        if(is_blank($this->modelClass)){
            goto ends;
        }
        
        if(App::checkInheritance($this->modelClass, Model::class)){
            return true;
        }
        
        ends:
        return false;
    }
    
    /**
     * 
     * @return Collections
     */
    public function get(){
        $this->checkIfTheresColumnDefined();
        
        $str = $this->queryString();
        
        if(SequelDump::query()){
            out()->println($str);
        }
        
        $results = DB::connection($this->dbms)->raw($str);
        
        if($this->hasModelClass()){
            $modelStack = [];
            
            if($results instanceof Collections){
                foreach($results as $row){
                    $modelStack[] = $this->asModel($row);
                }
                
                $results = ModelCollections::storeStack($modelStack);
            }
        }
        
        if($results === false){
            $results = collect();
        }
                
        return $results;
    }
    
   /**
    * 
    * @throws QueryBuildingException
    */
    protected function checkIfTheresColumnDefined(){
        if($this->hasMultipleTables()){
            if($this->columns === "*"){
                throw new QueryBuildingException("One or more columns must be specified when there are 2 or more tables to be queried");
            }
        }
    }
    
    /**
     * 
     * @return \lib\util\Model|NULL
     */
    public function first(){
        $this->checkIfTheresColumnDefined();
        
        $results = $this->limit(0, 1)->get();
        
        if($results instanceof Collections && $results->count()){
            return $results->get(0);
        }
        
        return null;
    }
    
    /**
     * 
     * 
     * @return number
     */
    public function count(){
        return $this->aggregation(__FUNCTION__);
    }
    
    /**
     * 
     * @param string $column
     * 
     * @return number|unknown
     */
    public function max(string $column){
        return $this->aggregation(__FUNCTION__, $column);
    }
    
    /**
     * 
     * @param string $column
     * @return number|\lib\util\builders\unknown
     */
    public function min(string $column){
        return $this->aggregation(__FUNCTION__, $column);
    }
    
    /**
     * 
     * @param String $column
     * @return number|\lib\util\builders\unknown
     */
    public function avg(string $column){
        return $this->aggregation(__FUNCTION__, $column);
    }
    
    /**
     *
     * @param String $column
     * @return number|\lib\util\builders\unknown
     */
    public function sum(string $column){
        return $this->aggregation(__FUNCTION__, $column);
    }
    
    /**
     * 
     * @param string $func
     * @param string $column
     * @return mixed
     */
    protected function aggregation($func, $column = null){
        if(is_blank($column)){
            $column = "*";
        }
        
        $identifier = "{$func}({$column})";
        
        $cloned = clone $this;
        
        $cloned->columns([$identifier]);
        
        $retrieved = -1;
        
        $cloned->each(function($item) use (&$retrieved, $identifier){
            if($item instanceof Model){
                $item = $item->getDefinedColumnVars();
            }
                       
            $retrieved = a($item, $identifier, $retrieved);
        });
        
        return $retrieved;
    }
    
    /**
     *
     * @param int $rowPerLoop
     * @param Closure $loadCallback
     * @param array $orderBy
     */
    public function chunk(int $rowsPerLoop, Closure $loadCallback){
        $offset = 0;
        
        while($data = $this->limit($offset, $rowsPerLoop)->get()){
            if($data->count() === 0){
                break;
            }
                
            $loadCallback($data);
                    
            $offset += $rowsPerLoop;
        }
    }
    
    /**
     * 
     * @param string $method
     * @param array $params
     * 
     * @return mixed
     */
    public function __call(string $method, array $params){
        return App::make(MethodInvocationResolver::class)
        ->parent($this)
        ->instances([$this->get()])
        ->toCall($method)
        ->params($params)
        ->resolve()
        ->invoke();
    }
    
    /**
     *
     * @param array $data
     * 
     * @return Model
     */
    protected function asModel(array $data){
        if(is_null($this->modelClass)){
            return $data;
        }
            
        $modelcname = $this->modelClass;
        
        if(count($data)){
            $model = new $modelcname($data);
            $model->appendOriginal($data);
            $model->setStored(true);
            
            return $model;
        }
        
        return null;
    }
    
    /**
     * 
     * @return string
     */
    public function getStackedWhereClause(){
        return implode("", $this->wheres);
    }
    
    /**
     *
     * @return string
     */
    public function getStackedJoinClause(){
        return implode("", $this->joins);
    }
    
    /**
     * 
     * @return string
     */
    public function getStackedHavingClause(){
        return implode("", $this->havings);
    }
    
    /**
     * 
     * @return string
     */
    public function queryString(){
        if(count($this->wheres)){
            $this->where = implode("", $this->wheres);
        }
        
        if(count($this->joins)){
            $this->join = implode("", $this->joins);
        }
        
        if(count($this->havings)){
            $this->having = "HAVING ".implode("", $this->havings);
        }
        
        if(is_blank($this->limit)){
            $this->limit(0, 2000);
        }
        
        return "SELECT {$this->columns} "
            ."FROM {$this->table_name} {$this->join} "
            ."WHERE {$this->where} "
            ."{$this->group_by} "
            ."{$this->having} "
            ."{$this->order_by} "
            ."{$this->limit}";
    }
    
    /**
     * 
     * @return string
     */
    public function __toString(){
       return $this->queryString(); 
    }
}