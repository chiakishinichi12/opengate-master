<?php
namespace lib\inner\arrangements\http\url;

use lib\inner\arrangements\http\Request;
use lib\inner\json\JsonUtility;

class UrlHandler {
    
    /**
     * 
     * @var Request
     */
    protected $request;
    
    /**
     * 
     * @param Request $request
     */
    public function __construct(Request $request){
        $this->request = $request;
    }
    
    /**
     * 
     * @return string
     */
    public function current(){
        return session("og.current.url")->value();
    }
    
    /**
     *
     * @return string
     */
    public function previous(){
        return session("og.previous.url")->value();
    }
    
    /**
     * 
     * @return string|boolean
     */
    public function __toString(){
        return JsonUtility::toEncode([
            "current" => $this->current(),
            "previous" => $this->previous()
        ])->encoded();
    }
}