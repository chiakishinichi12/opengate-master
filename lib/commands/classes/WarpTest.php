<?php
namespace lib\commands\classes;

use ReflectionMethod;
use lib\commands\CommonMethods;
use lib\inner\App;
use lib\instances\CommandInstance;
use lib\util\Collections;
use lib\util\ElapsedTime;
use lib\util\GateFinder;
use lib\util\ReflectionUtil;
use lib\util\StringUtil;
use lib\util\cli\WarpArgv;
use lib\util\cli\runtime\Process;
use lib\util\cli\runtime\SystemProcess;
use lib\util\cli\unittest\warper\TestClassCreation;
use lib\util\exceptions\ClassNotFoundException;
use lib\util\exceptions\GateUnitTestException;
use tests\GateTestCase;

/**
 *
 * @Commands(["test"])
 *
 */
class WarpTest extends CommandInstance{
    
    use CommonMethods;
    
    /**
     * 
     * @var string
     */
    protected $name = "phpunit-test";
    
    /**
     * 
     * @var string
     */
    protected $desc = "Running Unit Tests";
    
    /**
     * 
     * @param string $testFile
     * @return number
     */
    protected function executeTest($testFile, $method = null){
        $filter = "";
        
        if(!is_null($method)){
            $filter = "--filter {$method} ";
        }
        
        $command = "php vendor/bin/phpunit {$filter}{$testFile}";
        
        $process = new Process($command);
        
        $builder = "";
        
        $process->start();
                        
        while($process->isRunning()){
            $line = $process->getOutput();
            $builder .= $line;
            if($this->isDisplayingStandardOutput()){
                out()->print("<warning>STDOUT:</warning>\t{$line}");
            }
        }
                
        $process->wait();
        
        $this->verdictFromGeneratedResult($testFile, StringUtil::getLinedString($builder), $method);
                
        return $process->getExitCode();
    }
    
    /**
     * 
     * @param string $testfile
     * @param Collections $result
     */
    protected function verdictFromGeneratedResult($testfile, $result, $onlyMethod = null){
        $testfile = str_replace("/", "\\", $testfile);
        $testclass = substr($testfile, 0, strrpos($testfile, ".php"));
        
        $verdicts = [];
        $messages = [];
        $tracings = [];
        $reasonin = [];
        
        $iterate = function(ReflectionMethod $method, $index) 
            use ($testclass, $result, $onlyMethod, &$verdicts, &$messages, &$tracings, &$reasonin){
                
            if(!is_null($onlyMethod) && strcmp($onlyMethod, $method->getName()) !== 0){
                return;
            }
                
            $potentialFailure = "{$testclass}::{$method->getName()}";
            $verdicts[$potentialFailure] = true;
            
            if(($result->valueContains("FAILURES!") || $result->valueContains("ERRORS!")) 
                && $potFailIndices = $result->valueContains($potentialFailure)){
                $verdicts[$potentialFailure] = false;        
                $messages[$potentialFailure] = trim($result->get($potFailIndices[0] + 1));
                
                $tracingIndex = strpos($messages[$potentialFailure], "Error:") !== false ? 3 : 4;
                
                $tracings[$potentialFailure] = trim($result->get($potFailIndices[0] + $tracingIndex));
                $reasonin[$potentialFailure] = trim($result->get($potFailIndices[0] + 2));
            }
        };
        
        ReflectionUtil::getDeclaredMethods($testclass, ReflectionMethod::IS_PUBLIC)->each($iterate);
        
        out()->println(PHP_EOL);
        
        foreach($verdicts as $potentialFailure => $verdict){
            $message = isset($messages[$potentialFailure]) ? "<warning>({$messages[$potentialFailure]})</warning>" : "";
            
            $verdict_str = $verdict ? "<primary>PASSED!</primary>" : "<error>FAILED</error>";
            out()->println("\t[{$verdict_str}]: {$potentialFailure} {$message}");
            
            if(!$verdict){
                out()->println("\t<info>{$tracings[$potentialFailure]}</info>");
                out()->println("\t<magenta>{$reasonin[$potentialFailure]}</magenta>");
            }
        }
    }
    
    /**
     * 
     * @param string $testclass
     * @throws GateUnitTestException
     */
    protected function evaluateTestClass($testclass){
        if(!App::checkInheritance($testclass, GateTestCase::class)){
            throw new GateUnitTestException("{$testclass} is not an instance of ".GateTestCase::class);
        }
                
        if(!StringUtil::endsWith(class_basename($testclass), "Test")){
            throw new GateUnitTestException("class name must end with 'Test'. {{$testclass}}.");
        }
        
        $evaluate = function(ReflectionMethod $method, $index){    
            if(!StringUtil::startsWith($method->getName(), "test")){
                throw new GateUnitTestException("method name must start with 'test'. {$method->getName()} found.");
            }
        };
        
        ReflectionUtil::getDeclaredMethods($testclass, ReflectionMethod::IS_PUBLIC)->each($evaluate);
    }
    
    /**
     * 
     * runs all available test scripts
     * 
     * @param Collections $args
     * @throws GateUnitTestException
     */
    protected function warpingTests($inputtedClassName){
        out()->println("\n<magenta>==== Warping Test ====</magenta>\n");
        
        $elapsed = new ElapsedTime();
        $elapsed->start();
        
        $exitCodes = collect();
        
        (new GateFinder("tests", true))->load(function($testclass) use ($exitCodes, $inputtedClassName){
            if(!is_blank($inputtedClassName)){
                if(!class_exists($inputtedClassName)){
                    throw new ClassNotFoundException("Class {$inputtedClassName} doesn't exist");
                }
                
                if(strcmp($inputtedClassName, $testclass) !== 0){
                    return;
                }
            }
            
            if(strcmp($testclass, GateTestCase::class) === 0){
                return;
            }
            
            $this->evaluateTestClass($testclass);
            
            out()->println("\n<warning>Evaluating {$testclass}...</warning>\n");
            
            $exitCode = $this->executeTest(str_replace("\\", "/", $testclass).".php");
            
            $exitCodes->put($testclass, $exitCode);
        });
           
        out()->println("\nElapsed Time: <warning>{$elapsed->formatElapsed()} ["
            .date(STANDARD_TIMESTAMP_FORMAT)." (".date_default_timezone_get().")]</warning>\n");
        
        $this->terminate($exitCodes);
    }
    
    /**
     * 
     * @param Collections $args
     */
    protected function warpingSingleMethodTest($testClassWithMethod){
        out()->println("\n<magenta>==== Warping Test ====</magenta>\n");
        
        if(is_blank($testClassWithMethod)){
            return;
        }
        
        $check = explode("::", $testClassWithMethod);
        
        $exitCodes = collect();
        
        if(count($check) === 2){ 
            $this->evaluateTestClass($check[0]);
            
            $elapsed = new ElapsedTime();
            $elapsed->start();
            
            if(!class_exists($check[0])){
                out()->println("[ERROR]: <error>Class {$check[0]} doesn't exists!</error>");
                exit(1);
            }
            
            if(!method_exists($check[0], $check[1])){
                out()->println("[ERROR]: <error>Method {$check[1]}() doesn't exists in {$check[0]} class!</error>");
                exit(1);
            }
            
            $check[0] = str_replace("\\", "/", $check[0]).".php"; 
                        
            $exitCodes->add($this->executeTest($check[0], $check[1]));
            
            out()->println("\nElapsed Time: <warning>{$elapsed->formatElapsed()} ["
                .date(STANDARD_TIMESTAMP_FORMAT)." (".date_default_timezone_get().")]</warning>\n");
        }else{
            out()->println("<error>ERROR:</error> Invalid Unit Test Method Specification: <magenta>{$testClassWithMethod}</magenta>");
            $exitCodes->add(1);
        }
        
        $this->terminate($exitCodes);
    }
    
    /**
     * 
     * executes the existing test scripts.
     * 
     */
    protected function warpWhateverTest(WarpArgv $argv){
        $inputtedClassOrMethod = $argv->findLongOption("--class")?->value();
                
        if(!is_null($inputtedClassOrMethod) && strpos($inputtedClassOrMethod, "::") !== false){
            $this->warpingSingleMethodTest($inputtedClassOrMethod);
        }else{
            $this->warpingTests($inputtedClassOrMethod);
        }
    }
    
    /**
     * 
     * @param Collections $exitCodes
     */
    protected function terminate($exitCodes){
        if($exitCodes->hasTheFollowing([1, 2])){
            exit(1);
        }
    }
    
    /**
     * 
     * checks if the standard output will be shown during testing
     * 
     * @return boolean
     */
    protected function isDisplayingStandardOutput(){
        $displaying = config("phpunit");
         
        if(!is_null($displaying)){
            return isset($displaying["display_standard_out"]) &&
                $displaying["display_standard_out"];
        }
        
        return false;
    }
    
    /**
     * success callback helper method
     * 
     * @param string $builder
     * @param string $line
     */
    protected function doSuccess(&$builder, $line){
        if($this->isDisplayingStandardOutput()){
            out()->print("<warning>STDOUT:</warning>\t{$line}");
        }
        
        $builder .= $line;
    }
    
    /**
     * 
     * listing all existing unit test scripts
     * 
     */
    protected function listAvailableTestScripts(){
        (new GateFinder("tests", true))->load(function($testclass){
            if($testclass === GateTestCase::class){
                return;
            }
            
            if(App::checkInheritance($testclass, GateTestCase::class)){
                out()->println("<primary>{$testclass}</primary>");
            }else{
                out()->println("<magenta>{$testclass}</magenta>");
            }
        });  
    }
    
    /**
     * 
     * @Argument("runs PHPUnit test scripts. { --class=(O) }")
     * 
     */
    public function run(WarpArgv $argv){
        $this->warpWhateverTest($argv);
    }
    
    /**
     *
     * @Argument("echoes current PHPUnit version.")
     *
     */
    public function version(){
        SystemProcess::command("php vendor/bin/phpunit --version")
        ->during(function($line){
            out()->print("<warning>{$line}</warning>");
        })
        ->exec();
    }
    
    /**
     *
     * @Argument("echoes GateTestCase classes.")
     *
     */
    public function list(){
        $this->listAvailableTestScripts();
    }
    
    /**
     *
     * @Argument("create a GateTestCase inherited class. { --type=(O) }")
     *
     */
    public function create(){
        App::make(TestClassCreation::class)->write();
    }
    
    /**
     * 
     * the main executioner of all the things that surrounds this script
     * 
     */
    public function listen(WarpArgv $argv){                
        $this->execution($argv);
        
    }
}