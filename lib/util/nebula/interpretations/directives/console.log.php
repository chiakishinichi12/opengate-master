<?php

use lib\util\StringUtil;
use lib\util\nebula\interpretations\Directive;
use lib\util\nebula\interpretations\DirectiveThrowers;
use lib\util\nebula\interpretations\Keyword;

/**
 * 
 * this is just a directive that only serve as a debugger functional on the JS' console section
 * 
 */
return new class implements Directive, Keyword {
    
    use DirectiveThrowers;
    
    /**
     *
     * @var string
     */
    protected $name;
    
    /**
     *
     * @var string
     */
    protected $exp;
    
    public function __construct(){
        $base = basename(__FILE__);
        
        $this->name = substr($base, 0, strrpos($base, ".php"));
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\nebula\interpretations\Directive::obtains()
     */
    public function obtains($file, $interpreter){
        
        return $this;
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\nebula\interpretations\Directive::parenthesis()
     */
    public function parenthesis($exp){
        $this->exp = $exp;
        
        return $this;
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\nebula\interpretations\Directive::interpretation()
     */
    public function interpretation(){
        $this->requireParenthesis();
        
        $params = $this->parameters(1);
        
        $valueToEcho = $params->get(0);
        $withScript = $params->get(1) ?: true;
        
        if(StringUtil::contains($valueToEcho, "\\")){
            $valueToEcho = str_replace("\\", "\\\\\\", $valueToEcho);
        }
        
        $phpcode = "console.log(\"<?={$valueToEcho}?>\")";
        
        if(filter_var($withScript, FILTER_VALIDATE_BOOLEAN)){
            $phpcode = "<script type=\"text/javascript\">{$phpcode}</script>";
        }
        
        return $phpcode;
    }
};