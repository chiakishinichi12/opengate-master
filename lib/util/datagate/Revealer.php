<?php
namespace lib\util\datagate;

use lib\inner\App;
use lib\util\EncapsulationHelper;

/**
 * 
 * This class will be used as a debugging tool for insertion, modification and deletion.
 * it dumps the SQL command used for committing changes
 * 
 * @author antonio
 *
 */
class Revealer{
    
    /**
     * 
     * @var EncapsulationHelper
     */
    protected $encapsulation;
    
    public function __construct(){
        $this->encapsulation = App::make(EncapsulationHelper::class)->instance($this);
    }
    
    /**
     * 
     * @param bool $reveal
     * @return mixed|object
     */
    public function query(bool $reveal = null){
       return $this->encapsulation->appendProperty(__FUNCTION__, $reveal); 
    }
    
    /**
     * 
     * @param bool $reveal
     * @return mixed|object
     */
    public function insertAndUpdate(bool $reveal = null){
        return $this->encapsulation->appendProperty(__FUNCTION__, $reveal);
    }
    
    /**
     * 
     * @param bool $reveal
     * @return mixed|object
     */
    public function deletion(bool $reveal = null){
        return $this->encapsulation->appendProperty(__FUNCTION__, $reveal);
    }
}

