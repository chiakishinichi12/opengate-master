<?php

namespace lib\util\cli;

use lib\util\Collections;
use lib\util\StringUtil;

class WarpArgv {
    
    /**
     * 
     * @var string
     */
    protected $inputted;
    
    /**
     * 
     * @var Collections
     */
    protected $arguments;
    
    /**
     * 
     * @var Collections
     */
    protected $longOptions;
    
    public function __construct(){        
        $this->arguments = collect();
        $this->longOptions = collect();
        
        $this->arrangeArgvStruct();
    }
    
    /**
     *
     * @return string|\lib\traits\unknown
     */
    public function getInputtedCommand(){
        return $this->inputted;
    }
    
    /**
     *
     * @return Collections
     */
    public function obtainArguments(){
        return $this->arguments;
    }
    
    /**
     * 
     * @return Collections
     */
    public function longOptions(){
        return $this->longOptions;
    }
    
    /**
     * 
     * @param string $optionName
     * 
     * @return LongOption|NULL
     */
    public function findLongOption(string $optionName){
        if(StringUtil::contains($optionName, "|")){
            $optionStack = StringUtil::explode("|", $optionName);
            
            foreach($optionStack as $option){
                $longOption = $this->locateLongOption(trim($option));
                
                if(!is_blank($longOption)){
                    return $longOption;
                }
            }
        }else if($longOption = $this->locateLongOption($optionName)){
            if(!is_blank($longOption)){
                return $longOption;
            }
        }
        
        return null;
    }
    
    /**
     * 
     * @param string $optionName
     * 
     * @return \lib\util\collections\CollectionItem|null
     */
    protected function locateLongOption(string $optionName){
        $longOption = $this->longOptions()
        ->binarySearch($optionName)
        ->toCompare(fn($option) => $option->name())
        ->get();
        
        // checks if an object was found by binary searching
        if(!is_blank($longOption)){
            $longOption = $longOption->value();
        }
        
        return $longOption;
    }
    
    /**
     * 
     * @param string $argd
     * 
     * @return boolean
     */
    protected function isLongOption(string $argd){
        return StringUtil::startsWith($argd, "--");
    }
    
    /**
     * 
     * @param string $argd
     */
    protected function appendArgument(string $argd){
        if($this->isLongOption($argd)){
            $argd = explode("=", $argd);
            
            $this->longOptions->put($argd[0], new LongOption(...$argd));
        }else{
            $this->arguments->add($argd);
        }
    }
    
    /**
     * 
     * 
     * defining collection stacks with data from command-line arguments
     */
    protected function arrangeArgvStruct(){
        $args = request()->server("argv") ?: [];
                
        foreach($args as $k => $argd){
            if(in_array($k, [0, 1])){
                if($k === 1){
                    $this->inputted = $argd;
                }
                continue;
            }
            
            $this->appendArgument($argd);
        }
    }
}