<?php
namespace lib\commands\classes;

use lib\instances\CommandInstance;
use lib\util\cli\WarpArgv;

/**
 *
 * @Commands(["serve", "srv"])
 *
 */
class WarpServer extends CommandInstance{
    
    protected $name = "serve";
    
    protected $desc = "Serve the application on the PHP development server";
    
    public function listen(WarpArgv $argv){
        $args = $argv->obtainArguments();
                
        $port = $args->get(0) ?: "8080";

        $defaultDir = base_path("server.php");
        
        out()->println("<info>Warping PHP Development Server ...</info>");
        
        shell_exec("php -S localhost:{$port} \"{$defaultDir}\"");        
    }
}