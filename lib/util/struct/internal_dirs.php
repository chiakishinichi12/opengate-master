<?php

return [
    "procedurals" => [
        "/ecosys",
        "/literals",
    ],
    "classes" => [
        "/exceptions",
        "/iterators",
        "/traits",
        "/util",
        "/inner",
        "/instances",
        "/commands"
    ]
];