<?php

use lib\exceptions\OpenGateException;
use lib\inner\App;
use lib\inner\Container;
use lib\inner\Locale;
use lib\inner\OpenObject;
use lib\inner\Values;
use lib\inner\WebAppCore;
use lib\inner\arrangements\http\CookieGate;
use lib\inner\arrangements\http\url\UrlHandler;
use lib\inner\client\RequestProcessor;
use lib\util\Collections;
use lib\util\ConfigVar;
use lib\util\GateKeeper;
use lib\util\HttpServiceClient;
use lib\util\StringUtil;
use lib\util\accords\Outputter;
use lib\util\accords\SessionUtil;
use lib\util\array\ArrayValueLocator;
use lib\util\builders\SessionValue;
use lib\util\exceptions\RoutingException;
use lib\util\html\HtmlExpression;
use lib\util\model\NotColumn;
use lib\util\model\QueryFunction;
use lib\util\model\TableColumn;
use lib\util\model\WhereExpression;
use lib\util\nebula\Translation;    
use lib\util\nebula\View;
use lib\util\nebula\ViewRenderer;
use lib\inner\json\JsonUtility;

/**
 * 
 * some server (like lighttpd and nginx) doesn't load this function
 * and it's pretty obvious because of its name starts with apache (which is also a server software name).
 * 
 */
if(!function_exists("apache_request_headers")){
    function apache_request_headers() {
        $arh = [];
        
        $rx_http = '/\AHTTP_/';
        
        foreach($_SERVER as $key => $val) {
            if(preg_match($rx_http, $key)) {
                $arh_key = preg_replace($rx_http, '', $key);
                $rx_matches = array();
                
                // do some nasty string manipulations to restore the original letter case
                // this should work in most cases
                
                $rx_matches = explode('_', $arh_key);
                
                if(count($rx_matches) > 0 and strlen($arh_key) > 2) {
                    foreach($rx_matches as $ak_key => $ak_val){
                        $rx_matches[$ak_key] = ucfirst($ak_val);
                    }
                    
                    $arh_key = implode('-', $rx_matches);
                }
                
                $arh[$arh_key] = $val;
            }
        }
        
        return $arh;
    }
}

if(!function_exists("relative_path")){
    /**
     *
     * trims the full filepath into relative path. 
     * throws an exception if file doesn't exist
     *
     * @param string $fullpath
     * @param string $param2
     * 
     * 
     * @return string
     */
    function relative_path(string $fullpath, string $suffix = ""){
        if(!is_blank($suffix)){
            $suffix = "/{$suffix}";
        }
        
        return substr($fullpath, strlen(base_path($suffix)));
    }
}

if(!function_exists("is_blank")){
    /**
     * 
     * determines if a value is some kind of blank
     * 
     * @param mixed $value
     * @return boolean
     */
    function is_blank($value){
        /*
         * 
         * null checking
         */
        if(is_null($value)){
            return true;
        }
        
        /*
         * 
         * array checking
         */
        if(is_array($value)){
            return !count($value);
        }
        
        /*
         *
         * string checking
         */
        if(is_string($value)){
            $value = preg_replace('/[\s\n\t\\\]+/', '', $value);
            
            return !strlen(trim($value));
        }
        
        /*
         * 
         * Collections checking 
         */
        if($value instanceof Collections){
            return !$value->isLoopable();
        }
        
        return false;
    }
}

if(!function_exists("base_path")){
    /**
     * 
     * returns the root of the project
     * 
     * @param string $relativePath
     * @return string
     */
    function base_path($relativePath = ''){
        $setup = __DIR__."/../../preferences/genesis.php";
        
        if(@file_exists($setup)){
            $setup = collect(require $setup);
        }
        
        return "{$setup->get("base_path")}{$relativePath}";
    }
}

if(!function_exists("config")){
    /**
     * 
     * retrieves configuration value from config.php
     * 
     * @param string $key
     * @param mixed $def
     * 
     * @return mixed|null
     */
    function config($key = null, $def = null){
        return ConfigVar::get($key, $def);
    }
}

if(!function_exists("gate")){
    /**
     * 
     * Get the available container instance
     * 
     * @param string|mixed $abstract
     * @param array $parameters
     * @return \lib\inner\ApplicationGate
     */
    function gate($abstract = null, $parameters = []){
        if(is_null($abstract)){
            return Container::getInstance();
        }
        
        return Container::getInstance()->make($abstract, $parameters);
    }
}

if(!function_exists("collect")){
    /**
     * creates/instantiates a Collection object with argument(s)
     * 
     * @param mixed ...$args
     * @return \lib\util\Collections
     */
    function collect(...$args){
        $variadicData = [];
        
        if($length = count($args)){
            if($length === 1 
                && (is_array($args[0]) || ($args[0] instanceof Collections))){
                $variadicData = $args[0];
                goto ends;
            }
            
            foreach($args as $arg){
                $variadicData[] = $arg;
            }
        }
        
        ends:
        return Collections::make($variadicData);
    }
}

if(!function_exists("with")){
    /**
     * return the given value, optionally passed through the given callback
     * 
     * @param mixed $value
     * @param callable $callback
     */
    function with($value, callable $callback = null){
        return is_null($callback) ? $value : $callback($value);
    }
}

if(!function_exists("fetch")){
    /**
     * 
     * Fetches data from the specified URL using the HttpServiceClient.
     * 
     *  This function uses the HttpServiceClient to send a request to the given URL and returns the response.
     * The type and structure of the returned data depend on the HttpServiceClient's implementation of the url() method.
     * 
     * @param string $url
     * 
     * @return RequestProcessor
     * 
     */
    function fetch(string $url){
        return HttpServiceClient::url($url);
    }
}

if(!function_exists("col")){
    /**
     *
     * @param String $colname
     * @return \lib\util\model\TableColumn
     */
    function col(string $colname){
        $column = new TableColumn(null);
        $column->columnName = $colname;
        
        return $column;
    }
}

if(!function_exists("class_name")){
    /**
     * returns a class name but will return a type name if not an object instead
     * 
     * @param string| $value
     */
    function class_name($value){
        $identity = "";
        
        if(is_object($value)){
            $identity = get_class($value);
        }else{
            $identity = gettype($value);
        }
        
        return $identity;
    }
}

if(!function_exists("session")){
    /**
     * @param mixed $name
     * @param mixed $data
     * 
     * @return lib\util\accords\SessionUtil|mixed|SessionValue
     */
    function session($name = null, $data = null){
        $session = gate(SessionUtil::class);
        
        if(!is_null($name) && !is_null($data)){
            if($session->active($name)){
                $session->modify($name, $data);
            }else{
                $session->set($name, $data);
            }
        }else if(!is_null($name)){
            if($session->active($name)){
                return $session->get($name);
            }else{
                return new SessionValue($name, null);
            }
        }
        
        return $session;
    }
}

if(!function_exists("csrf_token")){
    /**
     * returns the csrf token
     * 
     * @return string|null
     */
    function csrf_token(){
        if(gate()->checkCLI() && !gate()->isRunningUnitTest()){
            return null;
        }
        
        $referencedCsrfToken = gate()->isRunningUnitTest() 
            ? session("unit-csrf-token")->value()
            : CookieGate::get("csrf-token");
        
        if(!is_blank($referencedCsrfToken)){
            $csrfToken = $referencedCsrfToken;
        }else{
            $csrfToken = GateKeeper::fetchValue(WebAppCore::class);
        }
        
        return $csrfToken;
    }
}

if(!function_exists("url")){
    /**
     * 
     * @return UrlHandler
     */
    function url(){
        return App::make(UrlHandler::class);
    }
}

if(!function_exists("whexp")){
    /**
     *
     * @param String $expression
     * @param array $tables
     * @return \lib\util\model\WhereExpression
     */
    function whexp(String $expression, Array $columns, Array $values = []){
        foreach($columns as $key => $col){
            if(preg_match("/([A-Za-z0-9]+)\.([A-Za-z0-9]+)/", $col)){
                $splits = explode(".", $col);
                
                if(count($splits) === 2)
                    $col = "`{$splits[0]}`.`{$splits[1]}`";
            }else
                $col = "`{$col}`";
                
                $expression = str_replace("k({$key})", $col, $expression);
        }
        
        foreach($values as $key => $value){
            if(is_string($value)){
                // escaping values
                $value = str_replace("\'", "\\'", $value);
                $value = str_replace("\"", "\\\"", $value);
                
                $value = "\"{$value}\"";
            }
            
            $expression = str_replace("v({$key})", $value, $expression);
        }
        
        return new WhereExpression($expression, $columns);
    }
}

if(!function_exists("request")){   
    /**
     * returns the available Request instance
     * 
     * @return \lib\inner\arrangements\http\Request
     */
    function request(){
        return gate("request");
    }
}

if(!function_exists("response")){
    /**
     * returns the available Response instance
     *
     * @return \lib\inner\arrangements\http\Response
     */
    function response($code = null){
        $response = gate("response");
        
        if(!is_null($code)){
            $response->statusCode($code);
        }
        
        return $response;
    }
}

if(!function_exists("class_basename")){
    /**
     * returns the basename of the classpath or object
     * 
     * @param string|object $classname
     * @return string
     */
    function class_basename($classname){
        if(is_object($classname)){
            $classname = class_name($classname);
        }
        
        $classname = str_replace("\\", "/", $classname);
        
        return basename($classname);
    }
}

if(!function_exists("values")){
    /**
     * 
     * Retrieves values from the configuration using the specified path.
     * If a path is provided, this function delegates the retrieval to the ValuesConfig class.
     * If no path is provided, it returns all values stored within the Values class.
     * 
     * @param string $valuePath
     * @return \lib\inner\ApplicationGate|unknown
     */
    function values(string $valuePath = null){
        if(is_null($valuePath)){
            return gate(lib\util\nebula\ValuesConfig::class);
        }
        
        return Values::get($valuePath);
    }
}

if(!function_exists("al")){
    /**
     * 
     * @param array|Collections $data
     * @param string $path
     * @param mixed $newValue
     * 
     * @return mixed|NULL
     */
    function al(&$data, string|int $path, mixed $newValue = null){
        $locator = new ArrayValueLocator($data);
        
        if(is_int($path)){
            $path = "{$path}";
        }
        
        if(is_null($newValue)){
            return $locator->retrieve($path);
        }
        
        $locator->fill($path, $newValue);
    }
}

if(!function_exists("a")){
    /**
     * 
     * retrieving values without involving the use of reference operator
     * 
     * @param array|Collections $data
     * @param string $path
     * @param mixed $default
     */
    function a($data, string|int $path, mixed $default = null){
        return al($data, $path) ?: $default;
    }
}

if(!function_exists("trans")){
    /**
     * 
     * @param string $transPath
     * @param array $params
     * @param string $lang
     * 
     * @return string|Translation
     */
    function trans($transPath = null, $params = null, $lang = null){      
        if(is_null($lang)){
            $lang = Locale::get();
        }
        
        if(is_null($transPath) && is_null($params)){
            return gate(lib\util\nebula\Translation::class);
        }
        
        if(is_string($params)){
            $lang = $params;
            $params = [];
        }
        
        if(strcmp($lang, Locale::get()) !== 0){
            return Locale::localeTrans($transPath, $params, $lang);
        }
        
        return Locale::trans($transPath, $params);
    }
}

if(!function_exists("trans_arr")){
    /**
     *
     * @param string $transPath
     * @param string $lang
     * 
     * @return string|Translation
     */
    function trans_arr($transPath = null, $lang = null){
        if(is_null($lang)){
            $lang = Locale::get();
        }
        
        if(is_null($transPath)){
            return gate(lib\util\nebula\Translation::class);
        }
        
        return Locale::arr($transPath, $lang);
    }
}

if(!function_exists("faker")){
    /**
     * 
     * Create and return a Faker instance.
     *
     * This function uses the Faker library to generate a new Faker instance,
     * which can be used to generate fake data for testing and seeding the database.
     * 
     * @return \Faker\Generator
     */
    function faker(){
        return Faker\Factory::create();
    }
}

if(!function_exists("_v")){
    /**
     * 
     * Shortcut function for accessing values using the specified path.
     * Delegates the retrieval to the `values` function, allowing for concise access to configuration values.
     * 
     * @param string $valuePath
     * @return mixed|null
     */
    function _v(string $valuePath){
        return values($valuePath);
    }
}

if(!function_exists("_t")){
    /**
     * 
     * shorthand for trans() function
     * 
     * @param string $transPath
     * @param array|string $params
     * @param string $lang
     */
    function _t($transPath = null, $params = null, $lang = null){
        if(is_null($transPath)){
            return $transPath;
        }
        
        return trans($transPath, $params, $lang);
    }
}

if(!function_exists("_ta")){
    /**
     *
     * shorthand for trans_arr() function
     *
     * @param string $transPath
     * @param string $lang
     */
    function _ta($transPath = null, $lang = null){
        if(is_null($transPath)){
            return $transPath;
        }
        
        return trans_arr($transPath, $lang);
    }
}

if(!function_exists("nc")){
    /**
     * 
     * This function serves as a factory method for creating NotColumn objects,
     * which are used to indicate that a given string expression does not represent
     * a column in an SQL table. 
     * 
     * @param string $exp
     * @return \lib\util\model\NotColumn
     */
    function nc(string $exp){
        return new NotColumn($exp);
    }
}

if(!function_exists("preferences")){
    /**
     * 
     * @param string $name
     * @throws OpenGateException
     * 
     * @return NULL|\lib\util\Collections
     */
    function preferences($name){
        $config = base_path("preferences/{$name}.php");
        
        if(!@file_exists(($config))){
            return null;
        }
        
        $contents = require $config;
        
        if(!is_array($contents)){
            throw new OpenGateException(sprintf("'{$name}' doesn't hold array type. %s found", gettype($contents)));
        }
        
        return Collections::make($contents);
    }
}

if(!function_exists("pre")){
    /**
     * Prints the given argument within <pre> tags for preformatted text display.
     *
     * @param mixed $arg The argument to be displayed within <pre> tags.
     */
    function pre($arg, $dump = false){
        out()->print("<pre>");
        
        if($dump){
            var_dump($arg);
        }else{
            out()->printbr($arg);
        }
        
        out()->print("</pre>");
    }
}

if(!function_exists("out")){
    /**
     * 
     * get the available outputter
     * 
     * @return Outputter
     */
    function out(){
        $outputter = gate()->make("out");
        
        return $outputter;
    }
}

if(!function_exists("upload")){
    /**
     * 
     * @param string $key
     * @return NULL|\lib\util\Collections
     */
    function upload($key){
        $preference = preferences("upload");
        
        if(!$preference->hasKey($key)){
            return null;
        }
        
        return collect($preference->get($key));
    }
}

if(!function_exists("morph")){
    /**
     * 
     * Morphs a view, setting the view path and optional data bindings.
     *
     * @param string $viewPath The path to the view to be morphed.
     * @param array|null $bindings (Optional) Data bindings to be passed to the view.
     * 
     * @return ViewRenderer
     * 
     */
    function morph($viewPath, $bindings = null){
        return View::set($viewPath)->setBindings($bindings);
    }
}

if(!function_exists("e")){
    /**
     * 
     * encode html special characters in a string
     * 
     * @param string|HtmlExpression $str
     * @param boolean $doubleEncode
     * @return string
     */
    function e($str, $doubleEncode = true){
        if($str instanceof HtmlExpression){
            return $str;
        }
        
        return htmlspecialchars($str ?? '', ENT_QUOTES, 'UTF-8', $doubleEncode);
    }
}

if(!function_exists("trim_desc")){
    /**
     * 
     * Trims a given description string to a specified maximum length,
     * ensuring that it ends at the last whole word before appending ellipsis.
     * 
     * @param string $desc
     * @param number $maxlen
     * 
     * @return string
     */
    function trim_desc(string $desc, $maxlen = 300){
        if(mb_strlen($desc) > $maxlen) {
            $desc = mb_substr($desc, 0, $maxlen);
            // Trim to the last whole word
            $desc = mb_substr($desc, 0, mb_strrpos($desc, ' '));
            
            $desc .= '...';
        }
        
        return $desc;
    }
}

if(!function_exists("class_uses_traits")){
    /**
     * 
     * checks if a class uses specific trait(s)
     * 
     * @param string|object $class
     * @param string|array $traitNames
     * 
     * @return boolean
     */
    function class_uses_traits(string|object $class, string|array $traitNames){
        $usesTrait = false;
        
        if(is_object($class)){
            $class = class_name($class);
        }
        
        // Get class reflection
        $classReflection = new ReflectionClass($class);
        
        // Get traits used by the class
        $usedTraits = $classReflection->getTraitNames();
        
        // Check if the trait is among the used traits
        if(is_array($traitNames)){
            foreach($traitNames as $name){
                if(in_array($name, $usedTraits)){
                    $usesTrait = true;
                }
            }
        }else{
            if(in_array($traitNames, $usedTraits)) {
                $usesTrait = true;
            }
        }
        
        return $usesTrait;
    }
}

if(!function_exists("xml_encode")){
    /**
     * 
     * @param array|Collections $data
     * @param string $root
     * @param SimpleXMLElement $xml
     * @return string|boolean
     */
    function xml_encode($data, $root = "root", $item = "item", SimpleXMLElement $xml = null){
        if($xml === null){
            $xml = new SimpleXMLElement("<?xml version=\"1.0\" encoding=\"UTF-8\"?><{$root}/>");
        }
        
        if($data instanceof Collections){
            $data = $data->toArray();
        }
        
        if(!is_array($data)){
            throw new OpenGateException("\$data must be an array or an instance of ".Collections::class);
        }
        
        foreach($data as $key => $value){
            if(is_array($value)) {
                if(is_numeric($key)){
                    $key = $item; // Dealing with <0/>..<n/> issues
                }
                
                // recursively locates nested tags
                xml_encode($value, $key, $item, $xml->addChild($key));
                
                continue;
            }
            
            // if it's an object but doesn't override the magic method __toString()
            if(is_object($value) && !method_exists($value, "__toString")){
                continue;
            }
            
            $xml->addChild($key, !is_null($value) ? htmlspecialchars($value) : null);
        }
        
        // beautifies xml output
        $dom = dom_import_simplexml($xml)->ownerDocument;
        
        $dom->formatOutput = true;
        
        return $dom->saveXML();
    }
}

if(!function_exists("xml_decode")){
    /**
     * 
     * @param string $xmlstr
     * 
     * @return OpenObject|null
     */
    function xml_decode(string $xmlstr){
        try{
            $xml = simplexml_load_string($xmlstr);
        }catch(Exception $ex){
            $xml = false;
        }
        
        if($xml === false){
            return null;
        }
        
        // Convert SimpleXMLElement object to array
        return JsonUtility::parse(JsonUtility::toEncode($xml));
    }
}

if(!function_exists("open_object")){
    /**
     * 
     * @param array|string|Collections $params
     * 
     * @return OpenObject
     */
    function open_object(array|string|Collections $params = null, int $depth = 512){
        $openObject = gate(OpenObject::class);
        
        if(!is_null($openObject)){
            $openObject->store($params, $depth);
        }
        
        return $openObject;
    }
}

if(!function_exists("base_url")){
    /**
     * 
     * returns base url of the http application.
     * 
     * returns null if used on CLI
     * 
     * @return string|null
     */
    function base_url(){
        $request = request();
        
        if(gate()->checkCLI() && !gate()->isRunningUnitTest()){
            return null;
        }
        
        return "{$request->getProtocolUsed()}{$request->server("HTTP_HOST")}";
    }
}

if(!function_exists("localized_uri")){
    /**
     * Generates a localized URI based on the current locale settings.
     *
     * This function ensures that the provided URI string starts with a forward slash ('/').
     * If the current locale does not match the browser's locale, the function prepends
     * the current locale to the URI string.
     *
     * @param string $uri
     *
     * @return string
     */
    function localized_uri(string $uri){
        if(!StringUtil::startsWith($uri, "/")){
            throw new RoutingException("URI string must starts with /");
        }
        
        if(!is_null(Locale::dynamic()) && strcmp(Locale::get(), Locale::dynamic()) !== 0){
            $patterns = [
                // Pattern for /#scroll (i.e., /# followed by any word characters)
                "/^\/#[\w]+$/", 
                // Pattern for /?a=b&c=d (i.e., /? followed by one or more key=value pairs, including URL-encoded characters and + sign)
                "/^\/\?([a-zA-Z0-9_]+=[a-zA-Z0-9_%\+]*(&[a-zA-Z0-9_]+=[a-zA-Z0-9_%\+]*)*)$/",
                // Pattern for /#scroll?a=b (i.e., /# followed by word characters and query string, including URL-encoded characters and + sign)
                "/^\/#[\w]+\?([a-zA-Z0-9_]+=[a-zA-Z0-9_%\+]*(&[a-zA-Z0-9_]+=[a-zA-Z0-9_%\+]*)*)$/"
            ];
            
            if(substr_count($uri, "/") === 1){
                if(StringUtil::matches($patterns, $uri)){
                    $uri = substr($uri, 1);
                }
            }
            
            $uri = "/".Locale::get().$uri;
        }
        
        return $uri;
    }
}

if(!function_exists("omit_lang_code")){
    /**
     *
     * Removes the language code from the beginning of a URI if it matches any of the supported languages.
     *
     * This function iterates over the list of supported languages and checks if the URI starts
     * with any of the language codes. If a match is found, the language code is removed from the URI.
     *
     * @param string $uri
     *
     * @return string
     */
    function omit_lang_code(string $uri){
        $uriparts = StringUtil::explode("/", $uri);
        
        if(!is_blank($uriparts) && Locale::languages()->has($uriparts[1])){
            $trimmed = substr($uri, strlen("/{$uriparts[1]}"));
            
            if(is_blank($trimmed)){
                $trimmed = "/";
            }
            
            return $trimmed;
        }
        
        return $uri;
    }
}

if(!function_exists("locale_option_links")){
    /**
     * Generates localized links for each supported language.
     *
     * This method trims the language code from the current request URI and then iterates
     * over the supported languages to generate a localized link for each language. If the
     * language code matches the browser's locale, it omits the language code from the link.
     * It also ensures that the generated link does not end with a trailing slash.
     *
     */
    function locale_option_links(){
        $links = [];
                
        $trimmeduri = omit_lang_code(request()->server("REQUEST_URI"));
        
        foreach(Locale::languages() as $langcode){
            $localeRoute = "/{$langcode}";
            
            if(strcmp($langcode, Locale::dynamic()) === 0){
                $localeRoute = "";
            }
            
            $merge = "{$localeRoute}{$trimmeduri}";
            
            if(strlen($merge) > 1 && StringUtil::endsWith($merge, "/")){
                $merge = substr($merge, 0, strlen($merge) - 1);
            }
            
            $links[$langcode] = $merge;
        }
        
        return $links;
    }
}

if(!function_exists("now")){
    /**
     * 
     * returns current timestamp sql expression
     * 
     * @param string $dbms
     * @return QueryFunction
     */
    function now(string $dbms = null){
        $dbms = $dbms ?: "mysql";
        
        $nowf = gate(QueryFunction::class);
        
        switch($dbms){
            case "mysql":
                $nowf->name("now")->parametric(true);
                break;
        }
        
        return $nowf;
    }
}