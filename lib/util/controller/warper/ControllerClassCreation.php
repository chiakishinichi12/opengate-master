<?php
namespace lib\util\controller\warper;

use lib\traits\WarperCommons;
use lib\util\Collections;
use lib\util\cli\LongOption;
use lib\util\cli\WarpArgv;
use lib\util\file\FileUtil;

class ControllerClassCreation {
    
    use WarperCommons;
    
    /**
     *
     * @var string
     */
    protected $warping = "Controller";
    
    /**
     *
     * @var Collections
     */
    protected $args;
    
    /**
     * 
     * @var LongOption
     */
    protected $primary;
    
    /**
     * 
     * @var string
     */
    protected $controllers;
    
    /**
     * 
     * @var string
     */
    protected $prototype;
    
    /**
     * 
     * @param WarpArgv $argv
     */
    public function __construct(WarpArgv $argv){
        $this->args = $argv->obtainArguments();
        
        $this->primary = $argv->findLongOption("--primary");
        
        $this->controllers = preferences("structure")->get("controllers");
        
        $this->prototype = base_path("lib/util/controller/prototype");
    }
    
    /**
     *
     * @return \lib\util\Collections
     */
    protected function template(){
        $basetemplate = "ControllerDefault.tmp";
        
        if(!is_null($this->primary)){
            $basetemplate = "ControllerPrimary.tmp";
        }
        
        return FileUtil::getContents("{$this->prototype}/{$basetemplate}", true);
    }
    
    public function write(){
        if($this->args->count() < 2){
            return;
        }
        
        $template = $this->template();
        
        $controller_name = $this->args->get(1);
        
        $this->alterAndCreate($controller_name, $this->controllers);
        
        $toFind = collect([
            [
                "find" => "[Controller_Class_Name]",
                "replace" => $controller_name
            ],
            [
                "find" => "[Controller_Namespace]",
                "replace" => str_replace("/", "\\", relative_path($this->controllers))
            ]
        ]);
        
        if(!is_null($this->primary)){
            if(!is_null($errmsg = $this->evaluateCreation())){
                out()->println("<error>{$errmsg}</error>");
                return;
            }
            
            $toFind->append([
                "find" => "[Primary_Route]",
                "replace" => "/{$this->primary->value()}"
            ]);
        }
        
        $this->writeWarpedScript($template,
            $toFind,
            $this->controllers,
            $controller_name);
    }
    
    protected function evaluateCreation(){
        if(is_blank($this->primary->value())){
            return "Controller Primary route cannot be blanked.";
        }
        
        return null;
    }
}

