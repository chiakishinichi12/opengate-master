<?php
namespace app\providers;

use lib\inner\ApplicationGate;
use lib\inner\Locale;
use lib\inner\arrangements\ServiceProvider;
use lib\util\StringUtil;
use lib\util\route\RouteArgs;

class HttpLocaleServiceProvider extends ServiceProvider {
    
    /**
     *
     * @var boolean
     */
    protected $forCLI = false;
    
    /**
     *
     * @param ApplicationGate $gate
     */
    public function boot(ApplicationGate $gate) {        
        // retrieving browser's|IP address' default locale
        $langcode = Locale::dynamic();
        
        $uriLang = StringUtil::explode("/", $gate->make(RouteArgs::class)->getCurrentURIString());
        
        // if the 2nd element of an array (out of exploded route string) is a locale string,
        // will define the langcode with it's value.
        if(!is_blank($uriLang) && Locale::languages()->has($uriLang[1])){
            $langcode = $uriLang[1];
        }
        
        // overwrites the first langcode assignment only if the 'locale' request parameter contains locale string
        // absorbs request locale setting (abstaining 'GET' priviledge)
        if(!is_blank($formlocale = request()->input("locale")) && is_blank(request()->get("locale"))){
            $langcode = $formlocale;
        }
        
        // sets a new default locale if the locale string exists within nebula directory
        if(!is_null($langcode) && Locale::exists($langcode)){
            Locale::set($langcode);
        }
    }
}

