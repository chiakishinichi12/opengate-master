<?php
namespace lib\util\model\relational\material;

use lib\inner\App;
use lib\util\Collections;
use lib\util\EncapsulationHelper;
use lib\util\builders\SelectBuilder;
use lib\util\model\Model;
use lib\util\model\relational\RelationalMaterial;
use lib\util\model\relational\RelationalModelException;

class HasOne implements RelationalMaterial{
    
    use MaterialCommon;
    
    /**
     * 
     * @var EncapsulationHelper
     */
    protected $encapsulator;
    
    /**
     * 
     * @var SelectBuilder
     */
    protected $builder;
    
    /**
     * 
     * @var SelectBuilder
     */
    protected $temp;
    
    /**
     * 
     * @var Model
     */
    protected $default;
    
    public function __construct(){
        $this->encapsulator = App::make(EncapsulationHelper::class)->instance($this);
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\model\relational\RelationalMaterial::ownerModel()
     */
    public function ownerModel(mixed $class = null){        
        return $this->encapsulator->appendProperty(__FUNCTION__, $class);
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\model\relational\RelationalMaterial::foreignModel()
     */
    public function foreignModel(mixed $class = null){
        return $this->encapsulator->appendProperty(__FUNCTION__, $class);
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\model\relational\RelationalMaterial::foreignKey()
     */
    public function foreignKey(string $value = null){
        return $this->encapsulator->propertyDefinition($this->{__FUNCTION__}, $value);
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\model\relational\RelationalMaterial::ownerKey()
     */
    public function ownerKey(string $value = null){
        return $this->encapsulator->propertyDefinition($this->{__FUNCTION__}, $value);
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\model\relational\RelationalMaterial::hasDefault()
     */
    public function hasDefault() : bool {
        return !is_null($this->default);
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\model\relational\RelationalMaterial::withDefault()
     */
    public function withDefault(array|callable|Model $default = null) : RelationalMaterial {
        $parent = $this->foreignModel();
        
        if(!is_string($parent)){
            $parent = class_name($this->foreignModel());
        }
        
        $this->default = new $parent;
        
        if(is_array($default)){
            foreach($default as $property => $value){
                $this->default->$property = $value;
            }
        }else if(is_object($default)){
            if($default instanceof Model){
                if(!App::checkInheritance($default, $parent)){
                    throw new RelationalModelException("Default Parent Model is not an instance of {$parent} class");
                }
                
                $this->default = $default;
            }else if(is_callable($default)){
                $default($this->default);
            }
        }
        
        return $this;
    }
    
    /**
     *
     * setting defaults according to their primary key.
     *
     */
    protected function initialize(){        
        // associated to the current model which uses the foreign key from relation model
        if(is_blank($this->foreignKey)){
            $this->foreignKey = $this->model
            ->getPrimaryKey()
            ->getName();
        }
        
        // associated to the foreign model
        if(is_blank($this->ownerKey)){
            $this->ownerKey = $this->model
            ->getPrimaryKey()
            ->getName();
        }
        
        $this->builder = $this->relation->query();
        
        if(isset($this->model->{$this->ownerKey})){            
            if(!is_blank($foreignKeyValue = $this->model->{$this->ownerKey})){
                $this->builder->where($this->foreignKey, $foreignKeyValue);
            }
        }
        
        $this->temp = clone $this->builder;
        $this->temp->limit();
    }
    
    /**
     * 
     * @return \lib\util\model\relational\material\HasOne
     */
    public function withBuilder(){
        if(is_null($this->model)){
            $this->materialize();
        }
        
        return $this;
    }
    
    /**
     * 
     * @param string $timestampColumn
     * @return \lib\util\model\relational\material\HasOne
     */
    public function latestOfMany(string $timestampColumn = null){
        $timestampColumn = $timestampColumn ?: "create_time";
        
        return $this->whereAggregate("max", $timestampColumn);
    }
    
    /**
     * 
     * @param string $timestampColumn
     * @return \lib\util\model\relational\material\HasOne
     */
    public function oldestOfMany(string $timestampColumn = null){
        $timestampColumn = $timestampColumn ?: "create_time";
        
        return $this->whereAggregate("min", $timestampColumn);
    }
    
    /**
     * 
     * @param string|array|callable $column
     * @param string|callable $function
     * 
     * @return \lib\util\model\relational\material\HasOne
     */
    public function ofMany(string|array|callable $column, string|callable $function = null){
        if(is_string($column) && is_string($function)){
            return $this->whereAggregate($function, $column);
        }
        
        if(is_callable($column)){
            $function = $column;
            $column = [];
        }
        
        if(is_array($column)){
            foreach($column as $col => $func){
                $this->whereAggregate($func, $col);
            }
            
            if(is_callable($function)){
                $function($this->builder);
            }
        }
        
        return $this;
    }
    
    /**
     * 
     * @param string $aggFunc
     * @param string $column
     * @return \lib\util\model\relational\material\HasOne
     */
    protected function whereAggregate(string $aggFunc, string $column){
        $maxquery = clone $this->temp;
        
        $maxquery->columns(["{$aggFunc}({$column})"])->limit(null);
        
        $this->builder->whereRaw("{$column} = ({$maxquery->queryString()})");
        
        return $this;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\model\relational\RelationalMaterial::retrieve()
     */
    public function retrieve() : RelationalMaterial {
        if(!is_null($this->retrieved)){
            goto ends;
        }
        
        $this->retrieved = $this->builder->first();
        
        if(is_null($this->retrieved) && !is_null($this->default)){
            $this->retrieved = $this->default;
        }
        
        ends:
        return $this;
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\model\relational\RelationalMaterial::fetched()
     */
    public function fetched() : Model|Collections|SelectBuilder {
        return $this->retrieved;
    }
}

