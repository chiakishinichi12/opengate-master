<?php
namespace tests\feature;

use tests\GateTestCase;

class FeatureTest extends GateTestCase{
    
    public function testStringManipulation(){
        $lyrics = "if I could fall into the <what?>";
        
        $lyrics = str_replace("<what?>", "sky", $lyrics);
        
        $this -> assertTrue(strpos($lyrics, "sky") !== false, "get me to the writer of the song!");
    }
}