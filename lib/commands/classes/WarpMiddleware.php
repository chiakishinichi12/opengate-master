<?php
namespace lib\commands\classes;

use lib\instances\CommandInstance;
use lib\commands\CommonMethods;
use lib\util\cli\WarpArgv;
use lib\inner\App;
use lib\util\middleware\warper\MiddlewareClassCreation;

/**
 *
 * @Commands(["md", "middleware", "mid"])
 *
 */
class WarpMiddleware extends CommandInstance{
    
    use CommonMethods;
   
    protected $name = "middleware";
    
    protected $desc = "middleware establishment feature";
    
    /**
     *
     * @Argument("creates middleware class. [option(s): { --defined(O) }]")
     *
     */
    public function create(){
        App::make(MiddlewareClassCreation::class)->write();
    }
    
    /**
     * 
     * @param WarpArgv $argv
     */
    public function listen(WarpArgv $argv){
        $this->execution($argv);
    }
}

