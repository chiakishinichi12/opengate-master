<?php
namespace lib\util\seeder\warper;

use lib\inner\ApplicationGate;
use lib\util\GateFinder;
use lib\util\exceptions\SeederException;
use lib\util\file\FileUtil;
use lib\util\seeder\Seeder;
use lib\inner\App;

class SeederList {
    
    /**
     * 
     * @var ApplicationGate
     */
    protected $gate;
    
    public function __construct(ApplicationGate $gate){
        $this->gate = $gate;
    }
    
    public function showList(){
        echo "Established Seeder Classes:\n\n";
        
        $failedCallback = function(){
            out()->println("<error>No seeder class found.</error>");
            die();
        };
        
        $seedpath = preferences("structure")->get("seeders");
        
        if(!FileUtil::exists($seedpath)){
            $this->gate->call($failedCallback);
        }
        
        (new GateFinder(relative_path($seedpath), true))->failedCallback($failedCallback)->load(function($class){
            if(!App::checkInheritance($class, Seeder::class)){
                throw new SeederException("Class {{$class}} is not an instance of ".Seeder::class);
            }
            
            out()->println("<magenta>{$class}</magenta>");
        });
    }
}