<?php

use lib\util\nebula\interpretations\Directive;
use lib\util\nebula\interpretations\DirectiveThrowers;
use lib\util\nebula\interpretations\Keyword;

return new class implements Directive, Keyword{
    
    use DirectiveThrowers;
    
    /**
     *
     * @var string
     */
    protected $name;
    
    /**
     *
     * @var string
     */
    protected $exp;
    
    public function __construct(){
        $base = basename(__FILE__);
        
        $this->name = substr($base, 0, strrpos($base, ".php"));
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\nebula\interpretations\Directive::obtains()
     */
    public function obtains($file, $interpreter){
        
        return $this;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\nebula\interpretations\Directive::parenthesis()
     */
    public function parenthesis($exp){
        $this->exp = $exp;
        
        return $this;
    }
    
    /**
     * 
     * has parenthesis but optional.
     * in other words, this directive can have boolean expressions
     * 
     * {@inheritDoc}
     * @see \lib\util\nebula\interpretations\Directive::interpretation()
     */
    public function interpretation(){
        
        $code = "<?php {$this->name}; ?>";
        
        if(!is_null($this->exp)){
            $code = "<?php if({$this->exp}) {$this->name}; ?>";
        }
        
        return $code;
    }
    
};