<?php
namespace lib\util\datagate;

use lib\inner\Accessor;

/**
 * 
 * @author antonio
 *
 * @method static \lib\util\datagate\DatagateHelper connection(string $dbms)
 * @method static boolean isConnected()
 * @method static \lib\util\datagate\DatagateHelper errorDump()
 * @method static \lib\util\builders\SelectBuilder table(string $table)
 * @method static mixed raw(string $sql, array $args = [])
 * @method static mixed execute(string $sql, array $args = [])
 *
 */
class DB extends Accessor{
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\inner\Accessor::warpedInstance()
     */
    public function warpedInstance(){
        return DatagateHelper::class;
    }
}