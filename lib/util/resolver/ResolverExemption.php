<?php
namespace lib\util\resolver;

use lib\inner\App;
use lib\util\EncapsulationHelper;

class ResolverExemption{
    
    /**
     * 
     * @var EncapsulationHelper
     */
    protected $encapsulator;
    
    public function __construct(){
        $this->encapsulator = App::make(EncapsulationHelper::class)->instance($this);
    }
    
    /**
     * 
     * @param string $name
     * @return ResolverExemption|object
     */
    public function method(string $name = null){
        return $this->encapsulator->appendProperty(__FUNCTION__, $name);
    }
    
    /**
     * 
     * @param bool $exp
     * @return ResolverExemption|object
     */
    public function condition(bool $exp = null){
        return $this->encapsulator->appendProperty(__FUNCTION__, $exp);
    }
    
    /**
     * 
     * @param string $class
     * @return ResolverExemption|object
     */
    public function owner(string $class = null){
        return $this->encapsulator->appendProperty(__FUNCTION__, $class);
    }
}

