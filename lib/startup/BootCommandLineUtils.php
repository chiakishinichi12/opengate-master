<?php
namespace lib\startup;

use lib\inner\ApplicationGate;
use lib\inner\arrangements\console\CLIOut;
use lib\inner\arrangements\http\Request;
use lib\inner\arrangements\http\Response;
use lib\util\cli\CommandContainer;
use lib\util\cli\ConsoleGate;
use lib\util\cli\IO;
use lib\util\cli\WarpArgv;


class BootCommandLineUtils {
    
    /**
     * 
     * @var ApplicationGate
     */
    protected $gate;
    
    public function bootstrap(ApplicationGate $gate){
        $this->gate = $gate;
        
        $this->gate->singleton(CommandContainer::class);
        
        $this->gate->singleton(ConsoleGate::class);
        
        /**
         * 
         * will be utilized to retrieve command line arguments
         * 
         */
        $this->bindUtil(Request::class, "request");
        
        /*
         *
         * response handler
         *
         */
        $this->bindUtil(Response::class, "response");
        
        /**
         * 
         * an object that will hold the values obtained by the argv.
         * 
         */
        $this->bindUtil(WarpArgv::class, "warp_argv");
        
        /**
         * 
         * an object that will scan inputted data from cli
         * 
         */
        $this->bindUtil(IO::class, "io");
        
        /**
         * 
         * CLI's outputter object
         * 
         */
        $this->bindUtil(CLIOut::class, "out");
    }
     
    /**
     * 
     * @param string $classname
     * @param string $alias
     */
    protected function bindUtil($classname, $alias){
        $this->gate->singleton($classname);
        $this->gate->alias($classname, $alias);
    }
}