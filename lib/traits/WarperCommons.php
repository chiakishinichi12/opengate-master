<?php
namespace lib\traits;

use Closure;
use lib\util\Collections;
use lib\util\GateFinder;
use lib\util\exceptions\NotInstanceException;
use lib\util\file\File;
use lib\util\file\FileUtil;
use lib\inner\App;

trait WarperCommons{
        
    /**
     * 
     * @Ignore
     * 
     * @param string $directory
     * @param string $instance
     * @param Closure $failedCallback
     */
    protected function classList(string $directory, string $instance, Closure $failedCallback = null){
        if(!FileUtil::exists($directory)){
            if(!is_null($failedCallback)){
                $this->gate->call($failedCallback);
            }
        }
        
        (new GateFinder(relative_path($directory), true))
        ->failedCallback($failedCallback)
        ->load(function($class) use ($instance){
            if(!App::checkInheritance($class, $instance)){
                throw new NotInstanceException("Class {{$class}} is not an instance of {$instance}");
            }
            
            out()->println("<magenta>{$class}</magenta>");
        });
    }
    
    /**
     * 
     * @Ignore
     * 
     * @param string $file
     * @param string $directory
     */
    protected function alterAndCreate(string &$file, string &$directory){
        // this will be used in case there will a subdirectory indicated within
        // the designated file parent directory
        $file_full_path = "{$directory}/{$file}";
        
        // altering the parent directory according to the full path's value
        $directory = substr($file_full_path, 0, strrpos($file_full_path, "/"));
        
        // removing trails on file naming (especially when it's a class name)
        $file = class_basename($file);
        
        $this->makeStructuredDirectory($directory);
    }
    
    /**
     * 
     * @Ignore
     * 
     * will create a directory for a dataspace (seeds, mockeries, and migration) 
     * if not exists. 
     * 
     * @param string $directory
     */
    protected function makeStructuredDirectory(string $directory){
        if(!FileUtil::exists($directory)){
            if((new File($directory))->makeDir()){
                out()->println("<info>Designated {$this->warping} Directory Created! {{$directory}}</info>");
            }
        }
    }
    
    /**
     * 
     * @Ignore
     * 
     * pushing the creation of a 
     * 
     * @param Collections $template
     * @param array $toFind
     * @param string $dirpath
     * @param string $classname
     */
    protected function writeWarpedScript(Collections $template, Collections $toFind, string $dirpath, string $classname){
        $classcontent = "";
        
        $template->each(function($line, $ln) use ($toFind, &$classcontent){
            $toFind->each(function($item, $index) use (&$line){
                if(strpos($line, $item["find"]) !== false){
                    $line = str_replace($item["find"], $item["replace"], $line);
                }
            });
                
            $classcontent .= $line;
        });
            
        $classpath = "{$dirpath}/{$classname}.php";
            
        if(!FileUtil::exists($classpath)){
            FileUtil::write($classpath, $classcontent);
            
            out()->println("<primary>{$this->warping} [{$classname}] created</primary>");
        }else{
            out()->println("<warning>{$this->warping} [{$classname}] already exists</warning>");
        }
    }
}

