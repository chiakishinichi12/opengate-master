<?php
namespace lib\util\mockeries\warper;

use ReflectionClass;
use lib\traits\WarperCommons;
use lib\util\Collections;
use lib\util\GateFinder;
use lib\util\cli\LongOption;
use lib\util\cli\WarpArgv;
use lib\util\file\FileUtil;

/**
 * 
 * @author Jiya Sama
 *
 */
class MockeryClassCreation{
    
    use WarperCommons;
    
    /**
     * 
     * @var string
     */
    protected $warping = "Mockery";
    
    /**
     *
     * @var Collections
     */
    protected $args;
    
    /**
     * 
     * @var LongOption
     */
    protected $model;
    
    /**
     * 
     * @var string
     */
    protected $prototype;
    
    /**
     * 
     * @var string
     */
    protected $mockeries;
    
    public function __construct(WarpArgv $argv){
        $this->args = $argv->obtainArguments();
        
        $this->model = $argv->findLongOption("--model");
        
        $this->mockeries = preferences("structure")->get("mockeries");
        
        $this->prototype = base_path("lib/util/mockeries/prototype");
    }
    
    /**
     * 
     * @return \lib\util\Collections
     */
    protected function template(){
        $basetemplate = "MockeryDefault.tmp";
        
        if(!is_null($this->model)){
            $basetemplate = "MockeryDefined.tmp";
        }
        
        return FileUtil::getContents("{$this->prototype}/{$basetemplate}", true);
    }
    
    /**
     * 
     * creates a new dataspace mockery script
     * 
     */
    public function write(){
        if($this->args->count() < 2){
            return;
        }
        
        $template = $this->template();
        
        $mockery_name = $this->args->get(1);
        
        $this->makeStructuredDirectory($this->mockeries);
        
        $toFind = collect([
            [
                "find" => "[Mockery_Name]",
                "replace" => $mockery_name
            ],
            [
                "find" => "[Date_Created]",
                "replace" => date(STANDARD_TIMESTAMP_FORMAT)
            ],
            [
                "find" => "[Mockery_Namespace]",
                "replace" => str_replace("/", "\\", relative_path($this->mockeries))
            ]
        ]);
        
        if(!is_null($this->model)){
            if(!is_null($errmsg = $this->evaluateModel())){
                out()->println($errmsg);
                return;
            }
            
            $toFind->merge([
                [
                    "find" => "[Model_Basename]",
                    "replace" => class_basename($this->model->value())
                ],
                [
                    "find" => "[Model_Use_Classpath]",
                    "replace" => $this->model->value()
                ]
            ]);
        }
        
        $this->writeWarpedScript($template, 
            $toFind, 
            $this->mockeries, 
            $mockery_name);
    }
    
    /**
     * 
     * Evaluates the existence and uniqueness of a model class within mockery instances.
     * 
     * @return string|NULL
     */
    protected function evaluateModel(){
        $modelClass = $this->model->value();
        
        // evaluates if a model class exists
        if(!class_exists($modelClass)){
            return "<error>Model class '{$modelClass}' doesn't exist.</error>";
        }
        
        // iterates and evaluates if a model is already used within a mockery class
        $models = collect();
        
        (new GateFinder(relative_path($this->mockeries), true))->load(function($class) use ($models){
            $rc = new ReflectionClass($class);
            
            $model = $rc->getProperty("model");
            $model->setAccessible(true);
            
            $models->put($class, $model->getValue(new $class()));
        });
        
        if($models->has($modelClass)){
            $mockery = $models->getKey($modelClass);
            
            return "<error>Model class {{$modelClass}} is already being used by '{$mockery}'</error>";
        }
        
        return null;
    }
}

