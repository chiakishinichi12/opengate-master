<?php
namespace lib\inner\arrangements\http\session\file;

use lib\util\Collections;

class SessionDataContainer {
    /**
     * 
     * @var Collections
     */
    public $sessions;
    
    /**
     * 
     * @var Collections
     */
    public $urls;
    
    /**
     * 
     * @var string
     */
    public $expiration;
    
    public function __construct(){
        $this->sessions = collect();
        $this->urls = collect();
    }
}