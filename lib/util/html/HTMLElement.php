<?php
namespace lib\util\html;

use lib\util\Collections;

class HTMLElement {
    
    /**
     * 
     * @var string
     */
    protected $name;
    
    /**
     * 
     * @var Collections
     */
    protected $attributes;
    
    /**
     * 
     * @var string
     */
    protected $content;
    
    /**
     * 
     * @var string
     */
    protected $htmlExpression;
    
    /**
     * 
     * @param string $name
     * @param Collections $attributes
     * @param string $content
     * @param string $htmlExpression
     */
    public function __construct($name, $attributes, $content, $htmlExpression){
        $this->name = $name;
        $this->attributes = $attributes;
        $this->content = $content;
        $this->htmlExpression = $htmlExpression;
    }
    
    /**
     * 
     * @return string
     */
    public function getName(){
        return $this->name;
    }
    
    /**
     * 
     * @return \lib\util\Collections
     */
    public function getAttributes(){
        return $this->attributes;
    }
    
    /**
     * 
     * @return string
     */
    public function getContent(){
        return $this->content;
    }
    
    /**
     * 
     * @return string
     */
    public function getHTMLExpression(){
        return $this->htmlExpression;
    }
}