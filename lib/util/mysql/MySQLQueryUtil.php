<?php

namespace lib\util\mysql;

use PDO;
use PDOException;
use mysqli;
use lib\traits\Sanitation;
use lib\util\Collections;
use lib\util\QueryUtil;
use lib\util\exceptions\SQLException;

class MySQLQueryUtil implements QueryUtil {
    
    use Sanitation;
    
    private static $instance = null;
    
    /**
     * 
     * @var PDO
     */
    private $driver;
    
    /**
     * 
     * @var string
     */
    private $sqlError;
    
    /**
     * 
     * @var int
     */
    private $affectedRows;
    
    private function __construct($driver){
        $this->driver = $driver;
    }
    
    /**
     * 
     * @param string $sql
     * @param boolean $query
     * @throws SQLException
     * @return mixed[]|number|boolean
     */
    private function processSQL($sql, $query = false){
        if(is_null($this->driver)){
            throw new SQLException("MySQL Connection wasn't established");
        }
            
        try{
            $fetching = $this->driver->prepare($sql);
            $isSuccess = $fetching->execute();
                        
            $data = [];
            
            if(!$query){
                goto done;
            }
            
            fetching:
            while($row = $fetching->fetch(PDO::FETCH_ASSOC)){
                $data[] = $row;
            }
            
            done:
            $result = count($data) ? $data : $isSuccess;
            
            $this->affectedRows = $fetching->rowCount();
            
            // closing statement object
            $fetching = null;
            
            return $result;
        }catch(PDOException $e){
            $this->sqlError = $e->getMessage();
            
            return false;
        }
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see QueryUtil::realEscapeString()
     */
    public function realEscapeString($str){    
        return $this->escape($str);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see QueryUtil::executeQuery()
     */
    public function executeQuery(String $sql, Array $args = []){
        $sql = $this->parametricSQL($sql, $args);
        
        $result = $this->processSQL($sql, true);
        
        if($result === false){
            throw new SQLException("MySQL Error: {$this->sqlError}");
        }
            
        if(is_array($result)){
            return Collections::make($result);
        }
                
        return false;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see QueryUtil::executeUpdate()
     */
    public function executeUpdate(String $sql, Array $args = []){
        $sql = $this->parametricSQL($sql, $args);
        
        $isSuccess = $this->processSQL($sql);
                        
        if($isSuccess === false){
            throw new SQLException("MySQL Error: {$this->sqlError}");
        }
            
        return $isSuccess; 
    }
    
    /**
     * 
     * @return number
     */
    public function getAffectedRows(){
        return $this->affectedRows;
    }
    
    /**
     * 
     * @param String $sql
     * @param array $args
     * @return \lib\util\mysql\String
     */
    private function parametricSQL(String $sql, Array $args){
        foreach($args as $key => $val){
            if(is_string($val)){
                $val = $this->escape($val);
                $val = "\"{$val}\"";
            }
            
            $sql = str_replace("({$key})", $val, $sql);
        }

        return $sql;
    }
    
    /**
     * 
     * @param mysqli $driver
     * @return \lib\util\mysql\MySQLQueryUtil
     */
    public static function getInstance($driver){
        if(self::$instance === null){
            self::$instance = new MySQLQueryUtil($driver);
        }
        
        return self::$instance;
    }
    
}