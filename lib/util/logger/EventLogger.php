<?php
namespace lib\util\logger;

use lib\inner\App;
use lib\inner\ApplicationGate;
use lib\util\accords\Logger;
use lib\util\file\FileUtil;

class EventLogger implements Logger{
    
    /**
     * 
     * @var ApplicationGate
     */
    protected $gate;
    
    /**
     * 
     * @param ApplicationGate $gate
     */
    public function __construct(ApplicationGate $gate){
        $this->gate = $gate;
    }
    
    /**
     *
     * @param string $content
     * @param int $logType
     * @param boolean $templated
     * @param string $saveToDir
     *
     * <br/><br/>
     * if the 4th parameter has a value, the $logType specification will be ignored as the content
     * will be saved to a new designated directory
     */
    public function log($content, $logType = LOG_INFO, $templated = true, $saveToDir = null){
        $logTypeDir = $this->getLogTypeFolderName($logType);
        
        $datedFname = date("Y-m-d");
        $logDir = base_path("stockade/logs/{$logTypeDir}");
        
        if(!is_null($saveToDir)){
            $logDir = $saveToDir;
        }
            
        $logFile = "{$logDir}/Log_{$datedFname}.log";
            
        if($templated){
            $content = $this->logDefaultTemplate($content);
        }
                
        FileUtil::write($logFile, $content);
    }
    
    /**
     * 
     * @param string $content
     * @return string
     */
    protected function logDefaultTemplate($content){
        if(!App::checkCLI()){
            return $this->httpLogTemplate($content);
        }
        
        $timestamp  = date("Y-m-d H:i:s");
        
        return "[{$timestamp}]\n\n{$content}\n\n";
    }
    
    /**
     * 
     * @param string $content
     * @return string
     */
    protected function httpLogTemplate($content){
        $timestamp  = date("Y-m-d H:i:s");
        
        $ipAddress = request()->server("REMOTE_ADDR");
        $reqUri = request()->server("REQUEST_URI");
        $reqMet = request()->server("REQUEST_METHOD");
        $rcode = response()->statusCode();
        
        $requestDetails = "({$reqUri} [{$reqMet}][{$rcode}])";
        
        return "[{$timestamp}][{$ipAddress}]\n{$requestDetails}\n\n{$content}\n\n";
    }
    
    /**
     *
     * @param integer $logType
     * @return string
     */
    protected function getLogTypeFolderName($logType){
        switch($logType){
            case LOG_INFO:
                return "info";
            case LOG_WARNING:
                return "warning";
            case LOG_ERR:
                return "error";
        }
    }
}