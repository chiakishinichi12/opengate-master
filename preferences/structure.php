<?php
/**
 * 
 * configuration for the default directories and responses.
 * 
 */
return [
    /**
     * 
     * sets the default directory for all controllers
     * 
     */
    "controllers" => base_path("app/controller"),
    
    /**
     *
     * sets the default directory for all core inherited instances
     *
     */
    "instances" => base_path("app/instances"),
    
    /**
     * 
     * sets the default directory for all user-defined models
     * 
     */
    "models" => base_path("app/models"),
    
    /**
     * 
     * sets the default directory for all user-defined middlewares
     * 
     */
    "middlewares" => base_path("app/middlewares"),
    
    /**
     * 
     * sets the default directory for all user-defined service providers
     * 
     */
    "providers" => base_path("app/providers"),
    
    /**
     * 
     * sets the default directory for all user-defined mailer classes
     * 
     */
    "mailers" => base_path("app/mailers"),
    
    /**
     *
     * sets the default directory for all user-defined values roller classes
     *
     */
    "rollers" => base_path("app/roller"),
    
    /**
     * 
     * sets the default directory for all migration classes
     * 
     */
    "migrations" => base_path("dataspace/migrations"),
    
    /**
     *
     * sets the default directory for all migration classes
     *
     */
    "seeders" => base_path("dataspace/seeds"),
    
    /**
     *
     * sets the default directory for all mockery classes
     *
     */
    "mockeries" => base_path("dataspace/mocks"),
    
    /**
     *
     * sets the default directory for all test classes
     *
     */
    "tests" => base_path("tests"),
    
    /**
     * 
     * sets the default error page when something wrong happens on the requests
     * 
     */
    "error_page" => config("error_page", [
        "default" => "defaults.default_error"
    ])
];