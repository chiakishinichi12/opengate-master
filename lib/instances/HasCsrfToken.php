<?php

namespace lib\instances;

use lib\inner\App;
use lib\inner\arrangements\http\CookieGate;
use lib\util\builders\CookieFactory;

trait HasCsrfToken {
    
    /**
     *
     * sets CSRF token for request security
     *
     */
    protected function defineCsrfTokenCookie($csrfToken){
        if($csrfToken->get("enabled")){
            $generatedToken = $this->retrieveValue();
                                    
            if(!App::isRunningUnitTest()){
                CookieGate::set(CookieFactory::create("csrf-token")
                    ->value($generatedToken)
                    ->path("/")
                    ->httpOnly(true)
                    ->samesite("lax"));
                
            }else{
                session("unit-csrf-token", $generatedToken);
            }
        }
    }
}