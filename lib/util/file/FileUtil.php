<?php
namespace lib\util\file;

use lib\util\Collections;

/**
 * 
 * Sets of shorthanded file processing methods
 * 
 * 
 * @author antonio
 *
 */
class FileUtil {
    
    /**
     * 
     * @param string $filename
     * @param boolean $downloadable
     * @param array $headers
     * 
     * @return boolean|number
     */
    public static function read(string $filename, bool $downloadable = false, array $headers = []){
        $file = new File($filename);
        
        if($file->exists()){
            return false;
        }
        
        return $file->read($downloadable, $headers);
    }
    
    /**
     * 
     * A method that can do the same thing with the native file_get_contents() function
     * but it's more durable when it comes to handling big data. it will only return a 
     * value if the file exists
     * 
     * @param String $name
     * @param boolean $asCollection
     * 
     * @return Collections|string|null
     */
    public static function getContents(string $name, $asCollection = false){
        $file = new File($name);
                
        try{
            if($file->exists() || $file->isURL()){
                $contents = $file->getContentsAsCollection();
                
                if($asCollection){
                    return $contents;
                }
                
                $contents = $file->getContents();
                
                $file->close();
                
                return $contents;
            }
        }catch(\Throwable $ex){
            // "An error occurred while processing the file or URL"
            return null;
        }
        
        return null;
    }
    
    /**
     * Writes or stores data to a designated file path. 
     * if the file doesn't exists, it will create one
     * 
     * @param string $filepath
     * @param mixed $content
     * @param string $mode
     * 
     * @return bool|mixed
     */
    public static function write(string $filepath, $content, string $mode = "a"){
        $file = new File($filepath);
        
        $written = $file->write($content, $mode);
        
        $file->close();
        
        /**
         * 
         * nullified to efficiently handle gargabe collection?
         * but pls tell me if the nullification of the variable below
         * is reasonable. Thanks!
         * 
         */
        $file = null;
        
        return $written;
    }
    
    /**
     * 
     * retrieves all the files within the directory
     * 
     * @param String $dirname
     * @return bool|NULL|\lib\util\Collections
     */
    public static function getDirFiles(string $dirname){
        $file = new File($dirname);
        
        if(!$file->exists()){
            return null;
        }
        
        if($file->isDirectory()){
            $files = $file->listFiles(true);
            
            $file->close();
            
            return $files;
        }
        
        return false;
    }
    
    /**
     * 
     * Simple copies the file to a designated destination. 
     * the file's basename doesn't have to be specified if the source string is not a URL
     *  
     * 
     * @param String $source
     * @param String $destination
     */
    public static function copy(string $source, string $destination){
        $file = new File($source);
        
        $copied = $file->copyTo($destination);
        
        $file->close();
        
        return $copied;
    }
    
    /**
     * Require a PHP file if it exists, otherwise return null.
     *
     * The path to the PHP file to require.
     * @param string $filepath 
     *
     * @return mixed
     */
    public static function requireIfExists($filepath){
        if(self::exists($filepath)){
            return require $filepath;
        }
        
        return null;
    }
    
    /**
     * 
     * determines if the file exists
     * 
     * @param string $path
     * @return boolean
     */
    public static function exists($path){
        return (new File($path))->exists();
    }
}