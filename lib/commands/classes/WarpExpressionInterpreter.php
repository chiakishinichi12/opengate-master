<?php
namespace lib\commands\classes;

use lib\instances\CommandInstance;
use lib\util\cli\WarpArgv;
use lib\inner\App;

/**
 *
 * @Commands(["interpret", "int"])
 *
 */
class WarpExpressionInterpreter extends CommandInstance {
    
    /**
     * 
     * @var string
     */
    protected $name = "interpreter";
    
    /**
     * 
     * @var string
     */
    protected $desc = "Code evaluator feature";
    
    /**
     * 
     * @param WarpArgv $argv
     */
    public function listen(WarpArgv $argv){
        $expression = $argv->findLongOption("--exp|--expression");
        
        if(is_null($expression)){
            out()->haltln("<error>No expression option was defined.</error> "
                ."<error background=\"true\">{ --exp | --expression }</error>");
        }
        
        if(is_blank($expression->value())){
            out()->haltln("<error>{ --expression | --exp } must not be blanked</error>");
        }
        
        App::evaluate($expression->value());
    }
}

