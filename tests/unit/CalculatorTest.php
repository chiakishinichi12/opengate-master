<?php
namespace tests\unit;

use tests\GateTestCase;

class CalculatorTest extends GateTestCase{
    
    public function testAddition(){
        $add = 5 + 3;
        
        $this -> assertTrue($add === 8, "Unequaled Adds");
    }
    
    public function testSubtraction(){
        $minus = 5 - 3;
        
        $this -> assertTrue($minus === 2, "Unequaled Subs!");
    }
}