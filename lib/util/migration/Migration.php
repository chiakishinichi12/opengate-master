<?php
namespace lib\util\migration;

use ReflectionClass;
use lib\util\StringUtil;
use lib\util\annotation\Annotation;
use lib\util\annotation\AnnotationProperty;
use lib\util\annotation\AnnotationUtil;
use lib\util\Collections;

class Migration {
    
    /**
     * 
     * @var string
     */
    protected $dbms = "mysql";
    
    /**
     * 
     * @var Collections
     */
    protected $properties;
    
    /**
     * 
     * @var boolean
     */
    protected $ignoreReveal = false;
    
    /**
     * 
     * @return boolean
     */
    public function isAnnotated(){
        $class = get_class($this);
        
        $annotation = (new Annotation("ForTable"))
            ->setProperty(new AnnotationProperty("name", "string", true));
        
        // this sets an indicator if a specified table name
        // within the annotation is already created. if created,
        // it will put a remark to the migration class as 'Synced'.
        // otherwise, the remark will be 'Unsynced'
        $this->properties = collect();            
            
        $annotated = AnnotationUtil::hasAnnotation(
            new ReflectionClass($class), $annotation,
            $this->properties);
        
        return $annotated;
    }
    
    /**
     * 
     * @return boolean
     */
    public function isSynced(){
        if(!$this->isAnnotated()){
            return false;
        }
        
        $table_name = $this->properties->get("name");
        
        $table_name = strtolower(implode("_", StringUtil::splitByCapital($table_name)));
        
        return ShowTables::getInstance($this->dbms)
            ->getTables()
            ->has($table_name);
    }
    
    /**
     * 
     * @return boolean
     */
    public function ignoreReveal(){
        return $this->ignoreReveal;
    }
    
    /**
     * 
     * methods up() and down() must be defined on the subclass of this class
     * 
     * 
     */
}