<?php
namespace lib\util;

class ElapsedTime {
    
    /**
     * 
     * @var int
     */
    protected $startTime;
    
    /**
     * 
     * starts the timer
     * 
     * @return \lib\util\ElapsedTime
     */
    public function start() {
        $this->startTime = microtime(true);
        
        return $this;
    }
    
    /**
     * 
     * gets the actual elapsed time (nanoseconds format?)
     * 
     * @throws RuntimeException
     * @return number
     */
    public function getElapsed() {
        if ($this->startTime === null) {
            throw new RuntimeException("Call start() method before getElapsed().");
        }
        
        $currentTime = microtime(true);
        
        return $currentTime - $this->startTime;
    }
    
    /**
     * 
     * returns the formatted elapsed time
     * 
     * @param number $decimals
     * @return string
     */
    public function formatElapsed($decimals = 2) {
        $elapsed = $this->getElapsed();
        
        return number_format($elapsed, $decimals)." seconds";
    }
}