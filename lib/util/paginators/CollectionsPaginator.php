<?php

namespace lib\util\paginators;

use lib\util\Collections;
use lib\util\Paginator;
use lib\util\exceptions\PaginationException;

class CollectionsPaginator extends Paginator {
    
    /**
     *
     * @var int
     */
    protected $currentPage = 0;
    
    /**
     * 
     * @var Collections
     */
    protected $stack;
    
    /**
     * 
     * @param Collections $stack
     * @return \lib\util\paginators\CollectionsPaginator
     */
    public function stack(Collections $stack){
        $this->stack = $stack;
        
        $this->items = clone $this->stack;
        $this->items->clear();
        
        return $this;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\Paginator::fetchPageItems()
     */
    public function fetchPageItems(int $page){     
        $this->calculatePageCount();
        
        if(!is_blank($this->scannedPageNumber())){
            $page = $this->scannedPageNumber();
        }
        
        // if the page is negative
        if($page <= 0){
            $page = 1;
        }else if($page > $this->getPageCount()){
            $page = $this->getPageCount();
        }
        
        $this->currentPage = $page;
        
        $start = ($page - 1) * $this->rowsPerPage();
                        
        if(is_null($this->stack)){
            throw new PaginationException("Item stack is not defined");
        }
                        
        for($index = $start; $index < ($start + $this->rowsPerPage()); $index++){
            if(is_blank($this->stack->get($index))){
                break;
            }
            
            $this->items->add($this->stack->get($index));
        }
        
        return $this;
    }    
}