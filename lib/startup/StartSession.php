<?php
namespace lib\startup;

use lib\inner\ApplicationGate;
use lib\util\accords\SessionUtil;
use lib\util\Collections;
use lib\util\exceptions\SessionException;

class StartSession {
    
    protected $drivers = [
        "native" => \lib\inner\arrangements\http\session\native\NativeSessionUtil::class,
        "file" => \lib\inner\arrangements\http\session\file\FileSessionUtil::class
    ];
    
    /**
     * 
     * @param ApplicationGate $gate
     */
    public function bootstrap(ApplicationGate $gate){
        $session = $this->establishPreferredSessionLibrary($gate);
        
        /**
         * 
         * initalizes the session library
         * 
         */
        $session->initSessionLibrary();
    }
    
    /**
     * 
     * @param ApplicationGate $gate
     * @param Collections $sessionConf
     * @return boolean|mixed|\lib\inner\unknown|NULL
     */
    protected function makeSessionInstance(ApplicationGate $gate, Collections $sessionConf){
        $driver = $sessionConf->get("driver");
        
        if(!in_array($driver, array_keys($this->drivers))){
            throw new SessionException("'{$driver}' is not a session driver");
        }
        
        $gate->singleton(\lib\util\accords\SessionUtil::class, $this->drivers[$driver]);
        
        return $gate->make(SessionUtil::class);
    }
    
    /**
     * 
     * @param ApplicationGate $gate
     */
    protected function establishPreferredSessionLibrary(ApplicationGate $gate){
        $sessionConf = preferences("session");
        
        $duration = $sessionConf->get("duration");
         
        $session = $this->makeSessionInstance($gate, $sessionConf);
                
        if($duration){
            $session->expires($duration);
        }
        
        return $session;
    }
}