<?php
namespace lib\util\cli\unittest\tool;

use lib\inner\App;
use lib\util\Collections;
use lib\util\accords\SessionUtil;
use lib\util\exceptions\SessionException;
use lib\util\builders\SessionValue;
use lib\inner\arrangements\http\ApiResponseThrower;

/**
 * 
 * @author Antonius Aurelius
 *
 */
class UnitSessionUtil implements SessionUtil {
    
    use ApiResponseThrower;
    
    /**
     * 
     * @var Collections
     */
    protected $stack;
    
    /**
     * 
     * @var Collections
     */
    protected $temporary;
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\accords\SessionUtil::initSessionLibrary()
     */
    public function initSessionLibrary(){
        if(!App::isRunningUnitTest()){
            throw new SessionException(
                "UnitSessionUtil concern: the util cannot be utilized outside the unit test context");
        }
        
        $this->stack = collect();
        
        $this->temporary = collect();
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\accords\SessionUtil::modify()
     */
    public function modify($name, $value){
        $this->throwIfOnApiResponse(__FUNCTION__);
        
        $this->monitorSession($name);
        
        $this->stack->put($name, $value);
    }

    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\accords\SessionUtil::temporary()
     */
    public function temporary($name, $value = null){
        if(is_blank($value)){
            return $this->temporary->has($name);
        }
        
        if(!$this->temporary->has($name)){
            $this->temporary->add($name);
        }
        
        if($this->active($name)){
            $this->modify($name, $value);
        }else{
            $this->set($name, $value);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\accords\SessionUtil::expires()
     */
    public function expires($duration){
        // not necessary for now
    }

    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\accords\SessionUtil::set()
     */
    public function set($name, $value){
        $this->throwIfOnApiResponse(__FUNCTION__);
        
        if(!$this->active($name)){
            $this->stack->put($name, $value);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\accords\SessionUtil::get()
     */
    public function get($name){
        $this->monitorSession($name);
        
        return new SessionValue($name, $this->stack->get($name));
    }

    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\accords\SessionUtil::active()
     */
    public function active($name){
        return $this->stack->hasKey($name);
    }

    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\accords\SessionUtil::purgeAll()
     */
    public function purgeAll(){
        $this->throwIfOnApiResponse(__FUNCTION__);
        
        if($this->stack->isLoopable()){
            $this->stack->clear();
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\accords\SessionUtil::unset()
     */
    public function unset($name){
        $this->throwIfOnApiResponse(__FUNCTION__);
        
        $this->monitorSession($name);
        
        $this->stack->remove($name);
    }
    
    /**
     * 
     * @param string $name
     */
    protected function monitorSession(string $name){
        if(!$this->stack->get($name)){
            throw new SessionException("Session '{$name}' is not defined");
        }
    }
    
    /**
     * 
     * destructor
     * 
     */
    public function __destruct(){
        $this->temporary->each(function(string $name, int $index){
            if(!$this->active($name)){
                $this->temporary->remove($index);
            }
        });
    }
}

