<?php
namespace lib\inner\arrangements;

use lib\inner\App;
use lib\util\controller\Controller;
use lib\inner\WebAppCore;
use lib\exceptions\OpenGateException;

/**
 * 
 * @author Antonius Aurelius
 *
 */
class ContextualComponent{
    
    /**
     * 
     * @var mixed
     */
    protected $currentContextualItem;
    
    /**
     * 
     * setting current contextual item. it can be used only before reader rendering.
     * 
     * @param mixed $currentContextualItem
     * 
     * @throws OpenGateException
     */
    public function setCurrentContextualItem(mixed $currentContextualItem){
        if(!is_null($this->currentContextualItem) && !App::isRunningUnitTest()){
            throw new OpenGateException("Contextual Component cannot reinstate or change contextual instance");
        }
        
        $this->currentContextualItem = $currentContextualItem;
    }
    
    /**
     * 
     * returns the current contextual item. (Controller, Closure, or Instance)
     * 
     * @return mixed
     */
    public function getCurrentContextualItem(){
       return $this->currentContextualItem; 
    }
    
    /**
     * 
     * retrieves controller class name if the current contextual item is a controller
     * 
     * @return string
     */
    public function getController(){
        if(!is_array($this->currentContextualItem)){
            return null;
        }
        
        $class = $this->currentContextualItem[0];
        
        if(!App::checkInheritance($class, Controller::class)){
            return null;
        }
        
        return $class;
    }
    
    /**
     * 
     * checks if the current contextual item is a controller
     * 
     * @return boolean
     */
    public function isController(){
        if(is_null($this->currentContextualItem)){
            return false;
        }
        
        if(!is_array($this->currentContextualItem)){
            return false;
        }
        
        return is_array($this->currentContextualItem) 
            && App::checkInheritance($this->currentContextualItem[0], Controller::class);
    }
    
    /**
     * 
     * checks if the contextual queried result came from closure
     * 
     * @return boolean
     */
    public function isClosure(){
        if(is_null($this->currentContextualItem)){
            return false;
        }
        
        return is_callable($this->currentContextualItem);
    }
    
    /**
     * 
     * checks if the current contextual item is an instance of WebAppCore
     * 
     * @return boolean
     */
    public function isInstance(){
        if(is_null($this->currentContextualItem)){
            return false;
        }
        
        return is_string($this->currentContextualItem) 
            && App::checkInheritance($this->currentContextualItem, WebAppCore::class);
    }
    
    /**
     * 
     * evaluates if the current context handles a controller.
     * 
     * @param string|array $controller
     * @return boolean
     */
    public function handles(string|array $controller){
        if(!$this->isController()){
            return false;
        }
        
        if(is_array($controller)){
            return in_array($this->getController(), $controller, true); 
        }
        
        return strcmp($this->getController(), $controller) === 0;
    }
}

