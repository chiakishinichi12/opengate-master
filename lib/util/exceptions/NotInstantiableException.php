<?php
namespace lib\util\exceptions;

use lib\exceptions\OpenGateException;

class NotInstantiableException extends OpenGateException{
    
}