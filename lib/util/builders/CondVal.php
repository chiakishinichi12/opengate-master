<?php

namespace lib\util\builders;

use Closure;
use lib\traits\Sanitation;
use lib\util\Collections;

/**
 * 
 * @author Jiya Sama
 *
 */
class CondVal {
    
    use Sanitation;
    
    /**
     * 
     * @var Collections
     */
    protected $conds;
    
    /**
     *
     * @var Collections
     */
    protected $whens;
    
    /**
     * 
     * @var the class' value
     */
    protected $value;
    
    /**
     * 
     * constructor
     * 
     */
    protected function __construct(){
        $this->conds = collect();
        $this->whens = collect();
    }
    
    /**
     * 
     * @param mixed $column
     * @param string $operator
     * @param mixed $value
     * @param mixed $thenValue
     * @return \lib\util\builders\CondVal
     */
    public function when($column, $operator = null, $value = null, $thenValue = null){        
        $this->whenExp($column, $operator, $value);
        
        $clause = $this->getStackedWhenCondBuild();
        
        if($column instanceof Closure){
            $thenValue = $operator;
        }else if(is_null($thenValue) && !is_null($value)){
            $thenValue = $value;
        }
        
        $thenValue = $this->enclosingStringValue($thenValue);
        
        $this->whens->add("WHEN {$clause} THEN {$thenValue} ");
        
        // resets array
        $this->conds->clear();
        
        return $this;
    }
    
    /**
     * 
     * @param unknown $column
     * @param unknown $operator
     * @param unknown $value
     * @return \lib\util\builders\CondVal
     */
    public function orWhenExp($column, $operator = null, $value = null){
        $this->whenExp($column, $operator, $value, "OR");
        
        return $this;
    }
    
    /**
     * 
     * this must be used only within the closure function of when
     * 
     */
    public function whenExp($column, $operator = null, $value = null, $boolean = "AND"){
        if(is_string($column)){
            $clause = $this->whenClause($column, $operator, $value);
            
            if(count($this->conds))
                $this->conds->add(" {$boolean} {$clause}");
            else
                $this->conds->add("{$clause}");
        }else if($column instanceof Closure)
            $this->whenExpNested($column, $boolean);
        
        return $this;
    }
    
    /**
     * 
     * @param Closure $callback
     * @param string $boolean
     */
    protected function whenExpNested($callback, $boolean){
        $forNestedWhenExp = new CondVal();
        $callback($forNestedWhenExp);
        
        $nested = $forNestedWhenExp->getStackedWhenCondBuild();
        
        if(!empty($nested)){
            if(count($this->conds))
                $this->conds->add(" {$boolean} ({$nested})");
            else
                $this->conds->add("({$nested})");
        }
    }
    
    protected function whenClause($column, $operator, $value){
        $frontingOperator = $this->provideSuitableOperator($operator, $value);
        
        $value      = $this->enclosingStringValue($value);
        $column     = $this->enclosingInstanceCol($column);
        
        return "{$column} {$frontingOperator} {$value}";
    }
    
    /**
     *
     * @param mixed $operator
     * @param mixed $changeValue
     * @return string
     */
    protected function provideSuitableOperator($operator, &$changeValue, &$quoted = true){
        $assignments = function () use ($operator, &$changeValue){
            $changeValue = $operator;
        };
        
        switch(strtoupper($operator)){
            case "!=":
            case ">=":
            case "<=":
            case ">":
            case "<":
            case "=":
                $frontingOperator = $operator;
                break;
            case "IS NULL":
            case "IS NOT NULL":
                $frontingOperator = "";
                $assignments();
                $quoted = false;
                break;
            default:
                $frontingOperator = "=";
                $assignments();
                break;
        }
        
        return $frontingOperator;
    }
    
    /**
     * 
     * @param mixed $elseValue
     * @return \lib\util\builders\CondVal
     */
    public function otherwise($elseValue){
        $elseValue  = $this->enclosingStringValue($elseValue);
        
        $this->whens->add("ELSE {$elseValue} ");
        
        return $this;
    }
    
    /**
     * 
     * this will check if the value of the first operand is also a column variable
     * 
     * @param unknown $value
     * @return string|unknown
     */
    protected function enclosingStringValue($value){
        if(is_string($value)){
            $temp = $value;
            
            if(!$this->hasInstanceColumn($temp)){
                $value = $this->escape($value);
                $value = "\"{$value}\"";
            }else{
                $value = $temp;
            }
        }
        
        return $value;
    }
    
    /**
     * 
     * only checks if the column pattern matches to the object chaining
     * 
     * @param unknown $condition
     * @return unknown
     */
    protected function enclosingInstanceCol($condition){
        $old = $condition;
        
        if(!$this->hasInstanceColumn($condition))
            $condition = $old;
            
        return $condition;
    }
    
    /**
     * 
     * @return string
     */
    protected function getStackedWhenCondBuild(){
        return $this->conds->implode("");
    }
    
    /**
     * 
     * @return string
     */
    protected function getStackedWhen(){
        return $this->whens->implode("");
    }
    
    /**
     * 
     * @return string
     */
    public function __toString(){
        return $this->value;
    }
    
    /**
     * 
     * @param Closure $builderFunc
     * @return \lib\util\builders\CondVal
     */
    public static function caseBuild($builderFunc){
        $cv = new CondVal();
        
        $caseClause = "CASE ";
        
        if($builderFunc instanceof Closure){
            $builderFunc($cv);
            $stacked = $cv->getStackedWhen();
            
            if(!empty($stacked))
                $caseClause .= $stacked;
        }
        
        $caseClause .= " END";
        
        $cv->value = $caseClause;
        
        return $cv;
    }
    
    /**
     * 
     * @param mixed $column
     * @param string $operator
     * @param string $value
     * @param mixed $iftrue
     * @param mixed $iffalse
     * @return \lib\util\builders\CondVal
     */
    public static function whatIf($column, $operator, $value, $iftrue = null, $iffalse = null){
        $cv = new CondVal();
        
        if(is_string($column)){
            $cv->whenExp($column, $operator, $value);
            
            if(is_null($iffalse)){
                $iffalse = $iftrue;
                $iftrue = $value;
            }
        }else if($column instanceof Closure){
            $cv->whenExp($column);
            
            if(is_null($iftrue)){
                $iftrue = $operator;
            }
            
            if(is_null($iffalse)){
                $iffalse = $value;
            }
        }
        
        $iftrue  = $cv->enclosingStringValue($iftrue);
        $iffalse = $cv->enclosingStringValue($iffalse);
        
        $condition = $cv->getStackedWhenCondBuild();
        
        $cv->value = "IF({$condition}, {$iftrue}, {$iffalse})";
        
        return $cv;
    }
    
    public static function ifNull($columnOrExpression, $iftrue){
        $cv = new CondVal();
        
        $iftrue = $cv->enclosingStringValue($iftrue);
        $columnOrExpression = $cv->enclosingInstanceCol($columnOrExpression);
        
        $cv->value = "IFNULL({$columnOrExpression}, {$iftrue})";
        
        return $cv;
    }
}