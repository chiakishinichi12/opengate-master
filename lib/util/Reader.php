<?php

namespace lib\util;

use lib\inner\Accessor;
use lib\inner\reader\ReaderGate;

/**
 * 
 * @author antonio
 *
 * @method static void initInstanceCallback(\Closure $callback)
 */
class Reader extends Accessor{

    /**
     * 
     * @var array
     */
    protected $alias = [
        "initInstanceCallback" => "setInitInstanceCallback"
    ];
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\inner\Accessor::warpedInstance()
     */
    public function warpedInstance(){
        return ReaderGate::class;
    }
}