<?php
namespace lib\inner\arrangements\http\session;

use lib\util\builders\SessionValue;

trait SessionLifetime {
    
    /**
     *
     * @var int
     */
    protected $duration = -1;
    
    /**
     *
     * @var string
     */
    protected $tempsname = "og_temporary_data_flagger";
    
    /**
     *
     * adds duration to the registered session
     *
     * @param string $name
     */
    protected function limitSession($name){
        $expSessionName = "og_session_exp";
        
        if($this->active($expSessionName)){
            $expiration = $this->get($expSessionName)->value();
            
            $expiration["user_sessions"][] = [
                "name" => $name,
                "lifetime" => time() + $expiration["duration"]
            ];
            
            $this->modify($expSessionName, $expiration);
        }
    }
    
    /**
     *
     * @param SessionValue $expiration
     */
    protected function unsettingExpiredSession($expiration){
        $expval = $expiration->value();
        
        $collectSession = collect($expval["user_sessions"]);
        
        $collectSession->each(function($limited, $key) use ($collectSession){
            if(!$this->active($limited["name"])){
                return;
            }
            
            if(time() > $limited["lifetime"]){
                $this->unset($limited["name"]);
                $collectSession->remove($key);
            }
        });
            
        $expval["user_sessions"] = $collectSession->toArray();
            
        $expiration->modify($expval);
    }
    
    /**
     *
     * setting expiration integer
     *
     */
    protected function evalSessionExpiration(){
        $expSessionName = "og_session_exp";
        
        if($this->duration > 0){
            if(!$this->active($expSessionName)){
                $expStack = [
                    "duration" => $this->duration,
                    "user_sessions" => []
                ];
                
                $this->set($expSessionName, $expStack);
            }else{
                $this->unsettingExpiredSession($this->get($expSessionName));
            }
        }else if($this->active($expSessionName)){
            $this->unset($expSessionName);
        }
    }
    
    /**
     *
     * removing used temporary sessions
     *
     */
    protected function rmvtemp(){
        if($this->active($this->tempsname)){
            $temporaries = collect($this->get($this->tempsname)->value());
            
            $temporaries->each(function($tn, $index) use ($temporaries){
                if(!$this->active($tn)){
                    $temporaries->remove($index);
                }
            });
                
            if($temporaries->isLoopable()){
                $this->modify($this->tempsname, $temporaries->toArray());
            }else{
                $this->unset($this->tempsname);
            }
        }
    }
}