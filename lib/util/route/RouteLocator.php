<?php

namespace lib\util\route;

use lib\inner\WebAppCore;
use lib\util\Reader;
use lib\util\controller\Controller;
use lib\util\exceptions\ClassNotFoundException;
use lib\util\exceptions\ControllerException;
use lib\inner\App;
use lib\inner\Context;

class RouteLocator {
        
    /**
     * 
     * @var boolean
     */
    protected $located = false;
    
    /**
     * 
     * @return \lib\util\route\RouteLocator
     */
    public function locateRoute(){
        $routeArgs = App::make(RouteArgs::class);
        
        if($routeArgs->hasFound()){
            $this->located = true;
            
            $this->determineQueryAction($routeArgs->getLocatedRouteBean());
        }
        
        return $this;
    }
    
    /**
     * 
     * @return boolean
     */
    public function isLocated(){
        return $this->located;
    }
    
    /**
     * 
     * @param RouteBean $routeBean
     * 
     */
    protected function determineQueryAction(RouteBean $routeBean){
        if(is_array($routeBean->query()) && count($routeBean->query()) === 2){
            $routeBean->query(implode("@", $routeBean->query()));
        }
        
        if(is_string($routeBean->query())){
            if($this->isControllerExpression($routeBean->query())){
                $routeBean->query($this->controllerAlignment($routeBean));
            }
        }
        
        $this->renderReaderResponse($routeBean);
    }
    
    /**
     *
     * @param string $expression
     */
    protected function isControllerExpression(string $expression){
        return preg_match("/([a-zA-Z0-9]+)@([a-zA-Z0-9]+)/", $expression);
    }
    
    /**
     * 
     * @param RouteBean $routeBean
     * @throws ControllerException
     * @return array
     */
    protected function controllerAlignment(RouteBean $routeBean){        
        $action = explode("@", $routeBean->query());
        
        if(count($action) === 2){
            $class = $action[0];
            $method = $action[1];
            
            if(!class_exists($class)){
                throw new ClassNotFoundException("Controller class '{$class}' doesn't exist [{$class}::$method]");
            }
            
            if(!App::checkInheritance($class, Controller::class)){
                throw new ControllerException("{$class} is not an instance of ".Controller::class);
            }
            
            if(!method_exists($class, $method)){
                throw new ControllerException("Controller method {{$method}} doesn't exist.", $class, $method);
            }
            
            return [$class, $method];
        }
    }
    
    /**
     * 
     * @param RouteBean $routeBean
     */
    protected function renderReaderResponse(RouteBean $routeBean){
        $responseConfig = $routeBean->responseConfig();
        
        if($routeBean->type() === "api" && isset($responseConfig["contentType"])){
            unset($responseConfig["contentType"]);
        }
        
        $routeBean->responseConfig($responseConfig);
        
        $locatedRouteBeanQuery = $routeBean->query();
        
        Context::setCurrentContextualItem($locatedRouteBeanQuery);
        
        if(is_string($locatedRouteBeanQuery) && class_exists($locatedRouteBeanQuery) 
            && App::checkInheritance($locatedRouteBeanQuery, WebAppCore::class)){
            Reader::readInstance($locatedRouteBeanQuery);
        }else{
            Reader::readAccess($routeBean);
        }
    }
}