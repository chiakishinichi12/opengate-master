<?php
namespace lib\inner\arrangements\http;

use InvalidArgumentException;
use lib\inner\App;
use lib\util\Collections;
use lib\util\exceptions\MethodNotFoundException;
use lib\util\exceptions\UnknownPropertyException;
use lib\util\validator\Verifiable;
use lib\inner\json\JsonUtility;

/**
 * 
 * @author antonio
 *
 * @method mixed get(string $param = null)
 * @method mixed post(string $param = null)
 * @method mixed server(string $param = null)
 * 
 */
class Request implements Verifiable{
    
    use RequestUnit;
    
    /**
     * 
     * @var Collections
     */
    protected $headers;
    
    public function __construct(){
        $this->headers = collect();
        
        if(!is_blank($apacheHeaders = apache_request_headers())){
            $this->headers->addAll($apacheHeaders);
        }
    }
    
    /**
     * 
     * @param string $prop
     * 
     * @return boolean
     */
    public function __isset(string $propname){
        return !is_blank($this->input($propname));
    }
    
    /**
     * 
     * @param string $propname
     * 
     * @return NULL|string|\lib\inner\arrangements\http\Request
     */
    public function __get(string $propname){
        $data = $this->input($propname);
        
        if(is_null($data)){
            return null;
        }
        
        return $data;
    }
    
    /**
     * 
     * @param string $propname
     * @param string $newValue
     * 
     * @throws UnknownPropertyException
     */
    public function __set(string $propname, string $newValue){
        if(is_blank($this->input($propname))){
            throw new UnknownPropertyException("Unknown request param: {$propname}");
        }
        
        throw new UnknownPropertyException("Cannot modify request param value: {$propname}");
    }
    
    /**
     * 
     * 
     * @return string
     */
    public function __toString(){
        $method = strtolower($this->method());
        
        if(in_array($method, ["get", "post"])){
            return print_r($this->$method()->toJSONString(), true);
        }
        
        return $this->input();
    }
    
    /**
     *
     * @param string $method
     * @param array $args
     */
    public function __call($method, $args){
        if(!method_exists($this, $method)){
            if(in_array($method, ["get", "post", "server"])){
                return $this->stdinvoke(strtoupper($method), ...$args);
            }else{
                throw new MethodNotFoundException("{$method}() doesn't exist");
            }
        }
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\validator\Verifiable::verify()
     */
    public function verify(mixed $param){
        $value = $this->file($param);
        
        if(is_null($value)){
            $value = $this->$param ?: "";
        }
        
        return $value;
    }
    
    /**
     * 
     * @param mixed $class
     * @param array $params
     * 
     * @return HttpValidator
     */
    public function validation($class, $params = null){
        if(is_array($class)){
            $params = $class;
            $class = HttpValidator::class;
        }else if(!App::checkInheritance($class, [HttpValidator::class])){
            throw new InvalidArgumentException("Argument#1: Class '{$class}' is not an instance of ".HttpValidator::class);
        }
        
        return App::make($class)
            ->params($params)
            ->verifiable($this);
    }
    
    /**
     * 
     * @param string $name
     * @return NULL|mixed
     */
    public function cookie($name){
        return CookieGate::get($name);
    }
    
    /**
     * 
     * retrieves data from superglobal variable
     * 
     * @param string $method
     * @param string $param
     * @return mixed|Collections
     */
    protected function stdinvoke(string $method, string $param = null){  
        $parameters = collect($GLOBALS["_{$method}"]);
        
        if(is_null($param)){
            return $parameters;
        }
        
        return $parameters->get($param);
    }    
    
    /**
     * Retrieves data from $_FILES based on the provided param name.  
     * 
     * @param string $param
     * @return array|null
     */
    public function file(string $param){
        $data = null;
        
        if(isset($_FILES[$param])){
            $data = $_FILES[$param];
        }
        
        return $data;
    }
    
    /**
     * 
     * checks if file parameter is available
     * 
     * @param string $param
     * @return boolean
     */
    public function containsFile($param){
        if(is_null($file = $this->file($param))){
            return false;
        }
        
        return !is_blank(a($file, "name"));
    }
    
    /**
     * 
     * returns request method
     * 
     * @return string|null
     */
    public function method(){
        return $this->server("REQUEST_METHOD");
    }
    
    /**
     * 
     * retrieves remote ip address (IPv4 | IPv6)
     * 
     * @return mixed
     */
    public function ip(){
        return $this->server("REMOTE_ADDR");
    }
    
    /**
     * 
     * checks the used request method
     * 
     * @param string|array $methods
     * 
     * @return boolean
     * 
     * @throws InvalidArgumentException
     */
    public function is(string|array $methods){
        if(is_array($methods)){
            // suppressing case sensitive comparison
            foreach($methods as $method){
                if(is_string($method)){
                    if(strcasecmp($this->method(), $method) === 0){
                        return true;
                    }
                }else{                    
                    throw new InvalidArgumentException(
                        "Argument#1: Array or String is the only expected type. ".class_name($method)." found");
                }
            }
        }
        
        return strcasecmp($this->method(), $methods) === 0;
    }
    
    /**
     * Returns the currently used protocol
     *
     * @return string
     */
    public function getProtocolUsed(){
        if ($this->server("HTTPS") == 'on' ||
            $this->server("HTTPS") == 1 ||
            $this->server('HTTP_X_FORWARDED_PROTO') == 'https') {
                $protocol = 'https://';
        } else {
            $protocol = 'http://';
        }
            
        return $protocol;
    }
    
    /**
     * Retrieves request headers
     * 
     * @return Collections
     */
    public function headers(){    
        return $this->headers;
    }
    
    /**
     * 
     * @return mixed|\lib\inner\arrangements\http\mixed|string|boolean
     */
    protected function retrieveInput(){
        if(App::isRunningUnitTest()){
            return $this->unitTestInput ?: "";
        }
        
        return file_get_contents("php://input");
    }
    
    /**
     * Retrieves data from php://input based on the value of the parameter.
     * will return the raw request string if 'raw' parameter gets true.
     * 
     * it can be used as if you're using post and get methods only if JSON string 
     * is sent as a request string.
     * 
     * @param string $pname
     * @return string|unknown
     */
    public function input($pname = null){        
        $inputs = $this->retrieveInput();
        
        if(is_null($pname)){ 
            return $inputs;
        }
        
        if(strlen($inputs) !== 0){
            $pdata = [];
            
            if(!App::checkJSONString($inputs)){
                parse_str($inputs, $pdata);
                $pdata = collect($pdata);
            }else{
                $pdata = JsonUtility::parse($inputs);
            }
            
            
        }else{
            $pdata = $this->post()->count() ? $this->post() : $this->get();
        }
        
        return $pdata->get($pname);
    }
}