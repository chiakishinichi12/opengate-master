<?php
namespace lib\util;

use lib\inner\Accessor;
use lib\util\datagate\Revealer;

/**
 * 
 * @author antonio
 * 
 * @method static mixed insertAndUpdate(bool $reveal);
 * @method static mixed query(bool $reveal);
 * @method static mixed deletion(bool $reveal);
 *
 */
class SequelDump extends Accessor{
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\inner\Accessor::warpedInstance()
     */
    public function warpedInstance(){
       return Revealer::class; 
    }
}

