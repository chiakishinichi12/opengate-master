<?php

namespace lib\util;


abstract class Middleware {
        
    /**
     * 
     * default message string when middleware halted the thread
     * 
     * @var string
     */
    protected $halt_message = "";
    
    /**
     * 
     * response status code when the middleware is about to halt something
     * 
     * @var integer
     */
    protected $status_code = 401;
    
    /**
     * 
     * halts the response output with a status code.
     * 
     */
    public function halt(){
        response()->statusCode($this->status_code);
        
        out()->print($this->halt_message);
        
        exit(2);
    }
}