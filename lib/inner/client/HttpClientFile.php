<?php
namespace lib\inner\client;

use CURLFile;
use InvalidArgumentException;
use lib\util\file\File;
use lib\util\exceptions\FileNotFoundException;

class HttpClientFile {
    
    /**
     * 
     * @param string|File $file
     * 
     * @return CURLFile
     */
    public static function transfer($file, $newname = null){
        if(is_string($file)){
            $file = new File($file);
        }
        
        if(! $file instanceof File){
            throw new InvalidArgumentException("Argument#1: must be a string or an instance of ".File::class);
        }
        
        if(!$file->exists()){
            throw new FileNotFoundException("File doesn't exist: {$file->getName()}");
        }
        
        return new CURLFile($file->getName(), $file->getMimeType(), $newname);
    }
}