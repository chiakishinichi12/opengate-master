<!DOCTYPE html>
<html lang="{{ lib\inner\Locale::get() }}">
	<head>
		<title>Welcome!</title>
	</head>
	<body>
		Hello There! <br/>
		OpenGate <sup>{{ OPENGATE_VERSION }}</sup> is here to provide you flexible PHP development
		<br/>
		<br/>
		PHP Version: {{ PHP_VERSION }}
	</body>
</html>