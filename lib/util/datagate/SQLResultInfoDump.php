<?php
namespace lib\util\datagate;

trait SQLResultInfoDump{
    
    /**
     *
     * @var string
     */
    protected $error = "";
    
    public function errorDump(){
        /**
         * 
         * prints the error message caused by a query or connection error
         * 
         */
        if(!is_blank($this->error)){
            out()->println($this->error);
        }
        
        return $this;
    }
    
}