<?php
namespace lib\util\accords;

interface Outputter {
    
    /**
     * 
     * @param unknown $arg
     */
    function print($arg) : void;
    
    /**
     * 
     * @param string $arg
     */
    function println($arg = "") : void;
    
    /**
     *
     * @param string $arg
     */
    function printbr($arg = "") : void;
    
    /**
     * 
     * @param string $str
     * @param mixed ...$args
     */
    function printf($str, ...$args) : void;
    
    /**
     * 
     * @param string $arg
     */
    function halt($arg) : void;
    
    /**
     * 
     * @param string $arg
     */
    function haltln($arg = "") : void;
}