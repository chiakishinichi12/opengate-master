<?php

namespace lib\traits;

trait Sanitation {
    
    /**
     *
     * @param String $column
     * @param boolean $enclosed (enclosed the unmatched phrase for pattern to represent as a column)
     * @return number
     */
    protected function hasInstanceColumn(&$column, $enclosed = true){
        $matches = [];
        
        $hasInstance = false;
        
        $column = $this->escape($column);
        
        if(preg_match_all("/^[a-zA-Z0-9_\-]+\.[a-zA-Z0-9_\-]+$/", $column, $matches, PREG_SET_ORDER)){
            foreach($matches as $matching_stack){
                $splits = explode(".", trim($matching_stack[0]));
                
                if(count($splits) === 2){
                    if(strpos($splits[0], " ") !== false){
                        $spaceSlice = explode(" ", $splits[0]);
                        $splits[0] = $spaceSlice[count($spaceSlice) - 1];
                    }
                    
                    if(strpos($splits[1], " ") !== false){
                        $spaceSlice = explode(" ", $splits[1]);
                        $splits[1] = $spaceSlice[0];
                    }
                    
                    $enclosing = "{$splits[0]}.{$splits[1]}";
                    
                    $column = str_replace($enclosing, "`{$splits[0]}`.`{$splits[1]}`", $column);
                    
                    $hasInstance = true;
                }
            }
        }
            
        
        return $hasInstance;
    }
    
    /**
     *
     * @param string $expression
     * @return number
     */
    protected function isAggregateFuncUsage(string $expression){
        $aggregateFuncRegex = "/(\bmin\b|\bmax\b|\bsum\b|\bavg\b|\bcount\b)\(([a-zA-Z0-9_\-\*\.]+)\)/";
        
        $matched = preg_match($aggregateFuncRegex, strtolower($expression));
        
        return $matched;
    }
    
    /**
     *
     * @param string $toescape
     * @return mixed
     */
    protected function escape($toescape){
        $sequences = [
            "\n" => "\\\n", 
            "'" => "\'", 
            "\"" => "\\\"", 
            "\t" => "\\\t",
            "\u" => "\\\\u"
        ];
        
        if(!is_string($toescape)){
            goto returns;
        }
        
        foreach($sequences as $sequence => $escaped){
            $toescape = str_replace($sequence, $escaped, $toescape);
        }
        
        returns:
        return $toescape;
    }
}