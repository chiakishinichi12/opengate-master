<?php
namespace lib\inner\arrangements\http;

use lib\util\Collections;
use lib\util\StringUtil;
use lib\inner\App;

/**
 * 
 * The Redirector class provides a convenient way to manage HTTP redirections in PHP applications.
 * It allows for specifying various types of redirection targets, custom HTTP status codes,
 * flashing data to the session, and more.
 * 
 * @author Jiya Sama
 *
 */
class Redirector {
    
    use ApiResponseThrower;
    
    /**
     * 
     * @var string
     */
    protected $to;
    
    /**
     * 
     * @var integer
     */
    protected $status = 302;
    
    /**
     * 
     * @var Collections
     */
    protected $sessions;
    
    /**
     * 
     * @var Collections
     */
    protected $headers;
    
    /**
     * 
     * @param string $to
     */
    public function __construct(string $to = null){
        $this->to = $to;
        $this->sessions = collect();
        $this->headers = collect();
    }
    
    /**
     * 
     * @param int|string $status
     * @return \lib\inner\arrangements\http\Redirector
     */
    public function status(int $status){
        $this->status = $status;
        
        return $this;
    }
    
    /**
     * appends flash sessions for a next request
     * 
     * @param mixed $name
     * @param mixed $data
     * @return \lib\inner\arrangements\http\Redirector
     */
    public function withSession($name, $data){
        $this->sessions->put($name, $data);
        
        return $this;
    }
    
    /**
     * 
     * sets the redirection string to the previous url 
     * 
     * @return \lib\inner\arrangements\http\Redirector
     */
    public function back(){
        $this->to = url()->previous();
        
        return $this;
    }
    
    /**
     * 
     * alias of withSession() method
     * 
     * @param mixed $name
     * @param mixed $data
     * @return \lib\inner\arrangements\http\Redirector
     */
    public function with($name, $data){
        return $this->withSession($name, $data);
    }
    
    /**
     * 
     * @param string $name
     * @param string $value
     * 
     * @return \lib\inner\arrangements\http\Redirector
     */
    public function withHeader($name, $value){
        $this->headers->put($name, $value);
        
        return $this;
    }
    
    /**
     * 
     * @param array $headers
     * 
     * @return \lib\inner\arrangements\http\Redirector
     */
    public function withHeaders(Array $headers){
        $this->headers->addAll($headers);
        
        return $this;
    }
    
    /**
     * 
     * This method appends custom headers and session data to the HTTP response.
     * It iterates through the collection of headers and sessions, setting each header
     * and session data in accordance with their respective names and values.
     * 
     */
    protected function appendSessionsAndHeaders(){        
        response()->setHeaders($this->headers->toArray(), true);
            
        $this->sessions->each(function($data, $name){
            session()->temporary($name, $data);
        });
    }
    
    /**
     * 
     * checks if the redirection reference is valid
     * 
     * @return boolean
     */
    protected function isValidReference(){
        // Check if the string is a valid URL
        if(filter_var($this->to, FILTER_VALIDATE_URL) !== false) {
            return true;
        }
        
        // Regex pattern for relative URIs
        $relativeUriPattern = '/^\/[a-zA-Z0-9\-._~\/]*$/';
        
        // Check if the string matches the relative URI pattern
        if(preg_match($relativeUriPattern, $this->to)) {
            return true;
        }
        
        return false;
    }
    
    /**
     * 
     * exception thrower
     * 
     */
    protected function throwIfInvalid(){
        // throws if the api response is on strict mode
        $this->throwIfOnApiResponse();
        
        // throws if there's no provided url
        if(is_blank($this->to)){
            throw new RedirectionException("No URL or URI defined");
        }
        
        // throws if the provided URL is invalid
        if(!$this->isValidReference()){
            throw new RedirectionException("Invalid redirection reference: {$this->to}");
        }
    }
    
    /**
     * 
     * pushes redirection to the specified URI
     * 
     */
    public function send(){
        $this->throwIfInvalid();
        
        /*
         * The Expires header is used in HTTP responses to specify a date and time in the past 
         * or future when the response should be considered stale and no longer valid. When a 
         * browser or client receives a response with an Expires header, it will cache the 
         * response until the specified expiration date and time. After that point, the 
         * client will make a new request to the server to retrieve a fresh copy of the resource. 
         * 
         */
        $this->withHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT");
        
        $this->appendSessionsAndHeaders();
        
        $code = is_integer($this->status) 
            ? (string) $this->status
            : $this->status;
        
        /*
         * 
         * intercepting status code and request processing
         * 
         */ 
        if(strlen($code) === 3 && StringUtil::startsWith($code, "3")){
            response($code);
            
            response()->setHeaders([
                "Location" => $this->to
            ], true);
            
            if(!App::isRunningUnitTest()){
                exit(0);
            }
        }
    }
}