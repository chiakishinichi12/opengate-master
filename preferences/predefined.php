<?php

/**
 * 
 * Configuration settings for paginator link and validator issue
 * Emphasizing that these settings provide the default template paths
 * for pagination links and validation results
 */
return [
    /*
     * 
     * default template path for paginator links
     * 
     */
    "paginator-link" => "defaults.pagination.flexible",
    
    /*
     *
     * default template path for validator results
     * 
     */
    "validator-issue" => "defaults.validator.warning"
];