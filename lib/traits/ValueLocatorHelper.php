<?php
namespace lib\traits;

use OutOfBoundsException;
use lib\util\Collections;
use lib\util\collections\CollectionItem;

trait ValueLocatorHelper {
    
    /**
     * 
     * @var boolean
     */
    protected $strict = true;
    
    /**
     *
     * Recursively locates a value within a nested array using a dotted path.
     *
     * This function navigates through a nested array using dot notation to locate
     * a specific value specified by the given dotted path. If the value is found,
     * it returns the value; otherwise, it returns null.
     *
     * @param array|Collections $data
     * @param array|Collections $explodedPath
     * @param int $index
     *
     * @return mixed|null
     */
    protected function recursiveLocate($data, $explodedPath, int $index){
        if(!isset($explodedPath[$index])){
            return null;
        }
        
        $identifier = $explodedPath[$index];
        
        if(!isset($data[$identifier])){
            
            return null;
        }
        
        $value = $data[$identifier];
        
        if($value instanceof CollectionItem){
            $value = $value->value();
        }
        
        if(is_array($value)){
            // if the last identifier refers to an array,
            // then that array will be returned.
            if($index === (count($explodedPath) - 1)){
                return $value;
            }
            
            return $this->recursiveLocate($value,
                $explodedPath,
                $index + 1);
        }
        
        if($index !== (count($explodedPath) - 1)){
            $this->throwIterationException($explodedPath, $identifier, "Value Retrieve Error");
        }
        
        return $value;
    }
    
    /**
     *
     * Recursively updates a nested array based on a given key path.
     *
     * @param array|mixed $data
     * @param array $explodedPath
     * @param mixed $newValue
     * @param int $index
     *
     */
    protected function recursiveUpdate(&$data, $explodedPath, $newValue, $index){
        if(!isset($explodedPath[$index])){
            return;
        }
        
        $identifier = $explodedPath[$index];
        
        if(!isset($data[$identifier])){
            if($this->strict){
                $keypath = implode(".", $explodedPath);
                
                throw new OutOfBoundsException("Unknown array notation: {$keypath}");
            }else{
                goto fill;
            }
        }
        
        if(is_array($data[$identifier])){
            // if the last identifier refers to an array,
            // then that array will be returned.
            if($index === (count($explodedPath) - 1)){         
                $data[$identifier] = $newValue;
                
                return;
            }
            
            return $this->recursiveUpdate($data[$identifier],
                $explodedPath,
                $newValue,
                $index + 1);
        }
         
        if($index !== (count($explodedPath) - 1)){            
            $this->throwIterationException($explodedPath, $identifier, "Value Update Error");
        }
        
        fill:
        $data[$identifier] = $newValue;
    }
    
    /**
     * 
     * @param array $explodedPath
     * @param string $identifier
     * @param string $errorName
     */
    protected function throwIterationException($explodedPath, $identifier, $errorName){
        $explodedPath = collect($explodedPath)->implode(".", function($item) use ($identifier){
            if(strcmp($item, $identifier) === 0){
                $item = "<strong>{$identifier}</strong>";
            }
            
            return $item;
        });
        
        $mapping = "({$explodedPath})";
        
        throw new OutOfBoundsException("{$errorName}: iteration stops at '{$identifier}' {$mapping}");
    }
}