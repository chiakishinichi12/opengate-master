<?php
namespace lib\util\datagate;

use lib\util\DBInfo;
use lib\util\exceptions\SQLException;

trait SQLBuilderHelper {
    
    /**
     * 
     * @var string
     */
    protected $dbms;
    
    /**
     * 
     * @param String $table_name
     * @throws SQLException
     * 
     * @return \lib\util\Collections
     */
    public function fetchTableInfo(string $table_name){
        if(!DBInfo::isSuccessfullyConnected()){
            throw new SQLException("Table info cannot be fetched. Connection to the database wasn't made");
        }
        
        $schema = DBInfo::getCurrentlyUsedDatabase();
        
        $info_schema = "SELECT table_name, table_type
            FROM INFORMATION_SCHEMA.TABLES
            WHERE TABLE_SCHEMA = '{$schema}'
            AND TABLE_NAME = '{$table_name}'";
        
        return collect([
            "columns" => DB::connection($this->dbms)->raw("DESC `{$table_name}`"),
            "definition" => DB::connection($this->dbms)->raw($info_schema)
        ]);
    }
}