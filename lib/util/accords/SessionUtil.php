<?php
namespace lib\util\accords;

interface SessionUtil {
    
    /**
     * 
     * this is where everything about session instance is initialized
     * 
     */
    function initSessionLibrary();
    
    /**
     * sets the name and value of a session. won't trigger if already created
     * 
     * @param string $name
     * @param mixed $value
     */
    function set($name, $value);
    
    /**
     * returns the session value by its name
     * 
     * @param string $name
     */
    function get($name);
    
    /**
     * deleting the session
     * 
     * @param string $name
     */
    function unset($name);
    
    /**
     * updates the value of a session
     * 
     * @param string $name
     * @param mixed $value
     */
    function modify($name, $value);
    
    /**
     * checks if the session is still active/defined
     * 
     * @param string $name
     */
    function active($name);
    
    /**
     * sets session expiration
     * 
     * @param int $duration
     */
    function expires($duration);
    
    /**
     * 
     * clears all available sessions
     */
    function purgeAll();
    
    /**
     * 
     * sets and flags a temporary session data.
     * once accessed, the session data will be removed from the storage 
     * 
     * @param string $name
     * @param string $value
     */
    function temporary($name, $value = null);
}