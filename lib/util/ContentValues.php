<?php

namespace lib\util;

use ArrayIterator;
use IteratorAggregate;
use Traversable;

class ContentValues implements IteratorAggregate {
    
    /**
     * 
     * @var string
     */
    protected $tablename;
    
    /**
     * 
     * @var array
     */
    protected $container = [];
    
    /**
     * 
     * @var string
     */
    public const NOW = "NOW::FUNCTION";
    
    /**
     * 
     * @var string
     */
    public const UUID = "UUID:STR";
    
    /**
     * 
     * @param string $tablename
     */
    public function __construct(string $tablename){
        $this->tablename = $tablename;
    }
    
    /**
     * 
     * @return string
     */
    public function getTableName(){
        return $this->tablename;
    }
    
    /**
     * 
     * @param string $key
     * @param mixed $value
     * 
     * @return \lib\util\ContentValues
     */
    public function put(string $key, mixed $value){
        $this->container[$key] = $value;
        
        return $this;
    }
    
    /**
     * 
     * @param string $key
     * @return \lib\util\ContentValues
     */
    public function pop(string $key){
        if(isset($this->container[$key])){
            unset($this->container[$key]);
        }
        
        return $this;
    }
    
    /**
     * 
     * @return array
     */
    public function getContents(){
        return $this->container;
    }
    
    /**
     * 
     * @return Traversable
     */
    public function getIterator(): Traversable {
        return new ArrayIterator($this->container);
    }

    /**
     * 
     * @return string
     */
    public function __toString(){
        return print_r($this->container, true);
    }
}