<?php
namespace lib\util\model\relational\material;

use lib\inner\App;
use lib\util\Collections;
use lib\util\EncapsulationHelper;
use lib\util\builders\SelectBuilder;
use lib\util\model\Model;
use lib\util\model\relational\RelationalMaterial;

class HasMany implements RelationalMaterial{
    
    use MaterialCommon;
    
    /**
     * 
     * @var EncapsulationHelper
     */
    protected $encapsulator;
    
    public function __construct(){
        $this->encapsulator = App::make(EncapsulationHelper::class)->instance($this);
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\model\relational\RelationalMaterial::ownerModel()
     */
    public function ownerModel(mixed $class = null){
        return $this->encapsulator->appendProperty(__FUNCTION__, $class);
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\model\relational\RelationalMaterial::foreignModel()
     */
    public function foreignModel(mixed $class = null){
        return $this->encapsulator->appendProperty(__FUNCTION__, $class);
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\model\relational\RelationalMaterial::foreignKey()
     */
    public function foreignKey(string $value = null){
        return $this->encapsulator->propertyDefinition($this->{__FUNCTION__}, $value);
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\model\relational\RelationalMaterial::ownerKey()
     */
    public function ownerKey(string $value = null){
        return $this->encapsulator->propertyDefinition($this->{__FUNCTION__}, $value);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\model\relational\RelationalMaterial::hasDefault()
     */
    public function hasDefault() : bool {
        return false;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\model\relational\RelationalMaterial::withDefault()
     */
    public function withDefault(array|callable|Model $default = null) : RelationalMaterial {
        return $this;
    }
    
    /**
     *
     * setting defaults according to their primary and foreign key.
     *
     */
    protected function initialize(){
        // associated to the current model which uses the foreign key from relation model
        if(is_blank($this->foreignKey)){
            $this->foreignKey = $this->model
            ->getPrimaryKey()
            ->getName();
        }
        
        // associated to the foreign model
        if(is_blank($this->ownerKey)){
            $this->ownerKey = $this->model
            ->getPrimaryKey()
            ->getName();
        }
    }
    
    /**
     * 
     * @return HasOne
     */
    public function one(){
        $this->materializeIfNot();
        
        return App::make(HasOne::class)
        ->ownerModel($this->model)
        ->ownerKey($this->ownerKey)
        ->foreignModel($this->foreignModel())
        ->foreignKey($this->foreignKey)
        ->withBuilder();
    }
    
    /**
     * 
     * materializes model instances if they are not referenced yet.
     * 
     */
    protected function materializeIfNot(){
        if(is_null($this->model)){
            $this->materialize();
        }
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\model\relational\RelationalMaterial::retrieve()
     */
    public function retrieve() : RelationalMaterial {
        if(!is_null($this->retrieved)){
            goto ends;
        }
        
        $this->materializeIfNot();
        
        if(isset($this->model->{$this->ownerKey})){
            if(!is_blank($foreignKeyValue = $this->model->{$this->ownerKey})){
                $this->relation->where($this->foreignKey, $foreignKeyValue);
            }
        }
        
        $this->retrieved = $this->relation;
            
        ends:
        return $this;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\model\relational\RelationalMaterial::fetched()
     */
    public function fetched() : Model|Collections|SelectBuilder {
        return $this->retrieved;
    }
}

