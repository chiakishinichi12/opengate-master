<?php
namespace lib\util\cli\unittest\tool;

use PHPUnit\Framework\Constraint\IsIdentical;
use lib\inner\App;
use lib\inner\arrangements\http\Request;
use lib\inner\arrangements\http\Response;
use lib\util\StringUtil;
use lib\util\exceptions\MethodNotFoundException;
use lib\util\file\FileUtil;
use lib\util\route\Route;
use lib\util\route\RouteArgs;
use tests\GateTestCase;
use app\providers\HttpLocaleServiceProvider;
use lib\inner\Locale;
use lib\inner\json\JsonUtility;

/**
 * 
 * @author Jiya Sama
 *
 * @method InternalHTTPRequestMockery get(string $uri, array $temporary)
 * @method InternalHTTPRequestMockery post(string $uri, array $temporary)
 * @method InternalHTTPRequestMockery put(string $uri, array $temporary)
 * @method InternalHTTPRequestMockery patch(string $uri, array $temporary)
 * @method InternalHTTPRequestMockery delete(string $uri, array $temporary)
 * @method InternalHTTPRequestMockery acceptAny(string $uri, array $temporary)
 * 
 */
class InternalHTTPRequestMockery extends GateTestCase {
    
    /**
     * 
     * @var string
     */
    protected $content;
    
    /**
     * 
     * @var string
     */
    protected $uri;
    
    /**
     * 
     * @var string
     */
    protected $method;
    
    /**
     * 
     * @var boolean
     */
    protected $csrfToken = false;
    
    /**
     * 
     * @var array
     */
    protected $params = [];
    
    /**
     * 
     * @var string
     */
    protected $input = "";
    
    /**
     * 
     * @var RouteBean
     */
    protected $routeBean;
    
    /**
     * 
     * @param Request $request
     * @param Response $response
     */
    public function __construct(protected Request $request, protected Response $response){
        if(App::checkCLI() && App::isRunningUnitTest()){
            $this->initHttpRequestComponents();
        }
    }
    
    /**
     * 
     * defines superglobal values used on http request
     * 
     */
    protected function initHttpRequestComponents(){  
        // it all begins with 200 as default response code
        $this->response->statusCode(200);
        
        // If REMOTE_ADDR is blank (CLI context), simulate it by fetching the public IP
        // Use the ipify API to retrieve the public IP address, or generate a fake IP using Faker as a fallback
        if(is_blank($this->request->server("REMOTE_ADDR"))){
            $_SERVER["REMOTE_ADDR"] = FileUtil::getContents("https://api.ipify.org") ?: faker()->ipv4();
        }
        
        // Check if the "HTTP_HOST" server variable is blank (not set or empty).
        // If it is, set a default value of "pseudohost.fake" to avoid potential errors
        // in subsequent operations that depend on "HTTP_HOST".
        if(is_blank($this->request->server("HTTP_HOST"))){
            $_SERVER["HTTP_HOST"] = "pseudohost.fake";
        }
    }
    
    /**
     * 
     * Invokes the specified HTTP method for the given URI and sets temporary session data if provided.
     * Ensures the request method and URI are valid and releases the route
     * 
     * @param string $method
     * @param string $uri
     * @param array $temporarySession 
     * 
     * @return \lib\util\cli\unittest\tool\InternalHTTPRequestMockery
     */
    protected function invokeRequestMethod(string $method, string $uri, array $temporary = null){
        $this->uri = $uri;
        
        $this->method = $method;
        
        $this->request->arrangeRequestURI($uri)->arrangeUnitTestMethod(strtoupper($method));
        
        // sets temporary session if not null
        if(!is_blank($temporary)){
            foreach($temporary as $name => $value){
                session()->temporary($name, $value);
            }
        }
        
        $this->routeBean = $routeBean = App::make(RouteArgs::class)
            ->alignURIValues()
            ->getLocatedRouteBean();
        
        // certifies http localization
        App::make(HttpLocaleServiceProvider::class)->boot(gate());
        
        if(is_null($routeBean)){
            $this->response->statusCode(404);
            goto finish;
        }
        
        if(strcasecmp($method, "acceptAny") !== 0){
            $requestUri = is_array($routeBean->uri()) 
                ? "[".implode(",", $routeBean->uri())."]"
                : $routeBean->uri();
            
           if(strcasecmp($routeBean->method(), "any") !== 0){
                $this->assertEqualsIgnoringCase($method, $routeBean->method(), 
                    "({$requestUri}) Trying to access '{$routeBean->method()}' route on '{$method}'");
           }
        }else{
            $this->request->arrangeUnitTestMethod($routeBean->method());
        }
        
        // evaluates if providing csrf-token on post parameters is true
        if($this->csrfToken){
            $this->appendCsrfToken();
        }
        
        ob_start();
        
        Route::release();
        
        $this->content = ob_get_clean();
        
        finish:
        return $this;
    }
    
    protected function appendCsrfToken(){
        if(!$this->request->is(["POST", "GET"])){
            if(App::checkJSONString($this->input)){
                $temp = JsonUtility::parse($this->input);
                $temp["csrf-token"] = csrf_token();
                
                $this->input = JsonUtility::toEncode($temp)->encoded();
                
                $this->input($this->input);
            }
        }else{
            $this->params["csrf-token"] = csrf_token();
            
            $this->params($this->params);
        }
    }
    
    /**
     * 
     * allows appending csrf token on the request param
     * 
     * @return \lib\util\cli\unittest\tool\InternalHTTPRequestMockery
     */
    public function provideCsrfToken(){
        $this->csrfToken = true;
        
        return $this;
    }
    
    /**
     * 
     * @NotInvokable
     * 
     * Asserts that a session key exists and optionally contains a specific message.
     * 
     * @param string $sessId
     * @param string $message
     */
    public function assertSessionHas(string $sessId, string $message = null){
        // Assert that the session key exists and is active
        $this->assertTrue(session()->active($sessId), 
            "Session key '{$sessId}' does not exist or is not active.");
        
        // If a message is provided, check if it is contained in the session value
        if(!is_blank($message)){
            $this->assertTrue(StringUtil::contains(session($sessId), $message), 
                "Session key '{$sessId}' does not contain the expected message fragment: '{$message}'.");
        }
    }
    
    /**
     * 
     * @NotInvokable
     * 
     * asserts if an endpoint is API instance
     * 
     */
    public function assertApi(){
        $this->assertNotNull($this->routeBean, 
            "({$this->uri}) Cannot assert api instance. No route bean was located");
        
        $this->assertEqualsIgnoringCase("api", $this->routeBean->type(), 
            "({$this->uri} [{$this->routeBean->type()}]) Route is not an API");
    }
    
    /**
     * @NotInvokable
     * 
     * Asserts that a session key is missing or has no value.
     *
     * @param string $sessId
     */
    public function assertSessionMissing(string $sessId){
       $this->assertNull(session($sessId)->value(), 
           "Session key '{$sessId}' is not missing or contains a value when it should be null."); 
    }
    
    /**
     * 
     * Sets multiple session values from an associative array
     * 
     * @param array $sessions
     * 
     * @return \lib\util\cli\unittest\tool\InternalHTTPRequestMockery
     */
    public function sessions(array $sessions){
        foreach($sessions as $name => $value){
            session($name, $value);
        }
        
        return $this;
    }
    
    /**
     * 
     * @NotInvokable
     * 
     * Returns the content captured from the HTTP response.
     * 
     * @return string
     */
    public function getContent(){
        return $this->content;
    }
    
    /**
     * 
     * @NotInvokable
     * 
     * Returns the HTTP response status code.
     * 
     * @return int
     */
    public function responseCode(){
        return $this->response->statusCode();
    }
    
    /**
     * 
     * @NotInvokable
     * 
     * Follows a redirection if a "Location" header exists in the response.
     * 
     * @return \lib\util\cli\unittest\tool\InternalHTTPRequestMockery|null
     */
    public function followRedirects(){
        $redirectionUri = $this->response->getAlreadySentHeaders()->get("Location");
        
        if(!is_blank($redirectionUri)){
            return $this->get($redirectionUri);
        }
    }
    
    /**
     * 
     * @NotInvokable
     * 
     * Asserts that the response redirects to the specified URI.
     * 
     * @param string $uri
     */
    public function assertRedirect(string $uri){
        // Check if the response code indicates a redirection
        $this->assertTrue($this->responseCode() >= 300 && $this->responseCode() <= 308,
            "Expected a redirection response (3xx status code), but received {$this->responseCode()}.");
        
        // Retrieve the "Location" header to verify the redirection URL
        $redirectionUri = $this->response->getAlreadySentHeaders()->get("Location");
        
        // Ensure the "Location" header is present
        $this->assertNotNull($redirectionUri,
            "Expected a 'Location' header for the redirection, but none was sent.");
        
        // Check if the redirection URI matches the expected URI
        $this->assertEquals($redirectionUri, $uri,
            "Expected redirection to '{$redirectionUri}', but redirected to '{$uri}'.");
    }
    
    /**
     * 
     * @NotInvokable
     * 
     * Asserts that the response status code matches the expected value.
     * 
     * @param int $code
     */
    public function assertStatus(int $code){        
        $this->assertEquals($code, $this->response->statusCode(), sprintf(
            "Expected status code %d but received %d. Response content: %s",
            $code,
            $this->response->statusCode(),
            $this->content
        ));
    }
    
    /**
     * 
     * @NotInvokable
     * 
     * validates if the routed URI is localized.
     * 
     */
    public function assertLocalized(){
        $this->assertNotNull($this->routeBean, 
            "({$this->uri}) Cannot assert localization. No route bean was located.");
        
        $localizedUris = collect();
        
        // all localized routes must have their uri property return an array.
        if(!is_array($this->routeBean->uri())){
            goto assertion;
        }
        
        $routeBeanUris = collect($this->routeBean->uri());
        
        foreach(Locale::languages() as $lang){
            $languri = "/{$lang}";
            
            $localizedUris->merge(
                $routeBeanUris->filter(function($uri) use ($languri, $routeBeanUris) {
                    return ($routeBeanUris->has("/") && $uri === $languri)  || 
                        StringUtil::startsWith($uri, "{$languri}/");
                })
            );
        }
        
        assertion:        
        $this->assertEquals(Locale::languages()->length(), $localizedUris->length(), 
            "($this->uri) Routed URI is not localized.");
    }
    
    /**
     * 
     * @NotInvokable
     * 
     * Assert Read-Only Configuration
     * 
     * This method verifies that the current route is configured as read-only.
     * 
     */
    public function assertReadonly(){
        $this->assertNotNull($this->routeBean,
            "({$this->uri}) Cannot assert readonly enabled. No route bean was located.");
        
        // Retrieve the "readonly" configuration from the route bean's response configuration.
        $readonly = collect($this->routeBean->responseConfig())->get("readonly");
        
        // Assert that the "readonly" flag is set and true.
        $this->assertTrue(!is_null($readonly) && $readonly, 
            "({$this->uri}) Route URI resource is not readonly");
    }
    
    /**
     * 
     * @NotInvokable
     * 
     * Asserts that the response content contains the specified string.
     * 
     * @param string $needle
     */
    public function assertSee(string $needle){
        $this->assertNotNull($this->content, "({$this->uri}) Response content is null");
        
        $this->assertTrue(
            StringUtil::contains($this->content, $needle), 
            "Response content doesn't contain '{$needle}'");
    }
    
    /**
     * 
     * @NotInvokable
     * 
     * Asserts that the response content does not contain the specified string.
     * 
     * @param string $needle
     */
    public function assertDontSee(string $needle){
        $this->assertNotTrue(
            StringUtil::contains($this->content, $needle),
            sprintf("Response content contains '{$needle}'"));
    }
    
    /**
     * 
     * @NotInvokable
     * 
     * Asserts that the HTTP method of the request matches the expected method.
     * 
     * @param string $method
     */
    public function assertMethod(string $method){
        $this->assertEqualsIgnoringCase($method, $this->request->method(), 
            "Expected request method '{$method}', but got '{$this->request->method()}'.");
    }
    
    /**
     * 
     * @NotInvokable
     * 
     * Asserts that a specific HTTP header has the expected value.
     * 
     * @param string $header
     * @param string $value
     */
    public function assertHeader(string $header, string $value){
        $hval = $this->response->getAlreadySentHeaders()->get($header);
        
        $this->assertNotNull($hval, "No value(s) found from header '{$header}'");
        
        $headerValues = collect(explode(";", $hval))->cmap(fn($item) => trim($item));
        
        $this->assertNotNull($headerValues->binarySearch($value)->get(), 
            "Header ({$header}) value ({$value}) not found.");
    }
    
    /**
     * 
     * Asserts that the response content is a valid JSON string.
     * 
     * @return OpenObject
     */
    protected function assertPotentialJsonString(){
        $this->assertTrue(App::checkJSONString($this->content),
            "[".strtoupper($this->method)."={$this->uri}] Response content is not a JSON string");
        
        return JsonUtility::parse($this->content);
    }
    
    /**
     * 
     * @NotInvokable
     * 
     * Asserts that the response JSON contains a specified subset of data.
     * 
     * @param array $jsonSubsetData
     */
    public function assertJsonSubset(array $jsonSubsetData){                
        $this->assertTrue($this->assertPotentialJsonString()->isSubset($jsonSubsetData), 
            "The array does not contain the expected subset.");
    }
    
    /**
     * 
     * @NotInvokable
     * 
     * Asserts that the response JSON is an exact match to the specified array.
     * 
     * @param array $exactJson
     */
    public function assertExactJson(array $exactJson){
        $responseJsonArray = $this->assertPotentialJsonString()->toArray();
        
        $this->assertThat($responseJsonArray, new IsIdentical($exactJson), 
            "Failed asserting that the JSON is exactly equal to the given array.");
    }
    
    /**
     * 
     * @NotInvokable
     * 
     * Asserts that the response JSON contains a specified fragment.
     * 
     * @param array $jsonFragment
     */
    public function assertJsonFragment(array $jsonFragment){
        $responseJsonCollection = collect($this->assertPotentialJsonString()->toArray());
        
        $this->assertTrue($responseJsonCollection->isFragment($jsonFragment), 
             "Fragment locating failed: {$responseJsonCollection->getFragmentLocatingError()}");
    }
    
    /**
     * 
     * Sets request post parameters for the HTTP request
     * 
     * @param array $data
     * 
     * @return \lib\util\cli\unittest\tool\InternalHTTPRequestMockery
     */
    public function params(array $data){        
        $this->params = array_merge($data);
        
        $this->request->arrangeUnitTestPost($this->params);
        
        return $this;
    }
    
    /**
     * 
     * Sets request input for the HTTP request
     * 
     * @param string $data
     * 
     * @return \lib\util\cli\unittest\tool\InternalHTTPRequestMockery
     */
    public function input(string $data){
        $this->input = $data;
        
        $this->request->arrangeUnitTestInput($this->data);
        
        return $this;
    }
    
    /**
     * Sets request headers for the HTTP request
     * 
     * @param array $headers
     * 
     * @return \lib\util\cli\unittest\tool\InternalHTTPRequestMockery
     */
    public function headers(array $headers){
        $this->request->arrangeUnitTestHeaders($headers);
        
        return $this;
    }
    
    /**
     * 
     * Handles dynamic method calls, allowing HTTP methods (GET, POST, etc.) to invoke requests.
     * Throws an exception if the method is unrecognized.
     * 
     * @param string $method
     * @param array $args
     */
    public function __call(string $method, array $args){
        if(!method_exists($this, $method)){
            $methods = collect("GET", "POST", "PUT", "PATCH", "DELETE", "ACCEPTANY");
            
            if($methods->has(strtoupper($method))){
                return $this->invokeRequestMethod($method, ...$args);
            }else{
                throw new MethodNotFoundException("Method is not recognized. {{$method}}()");
            }
        }
    }
}

