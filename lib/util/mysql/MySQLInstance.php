<?php

namespace lib\util\mysql;

use PDO;
use PDOException;
use function collect;
use lib\util\ConfigVar;
use lib\util\SQLInstance;
use lib\util\exceptions\SQLException;

class MySQLInstance implements SQLInstance {
    
    private static $instance = null;
    
    /**
     * 
     * @var PDO
     */
    private $conn = null;
    
    private function __construct(){
        $this->establish($this->conn);
    }
    
    private function establish(&$conn){    
        if($conn !== null){
            return;
        }
        
        $database = collect(ConfigVar::get("db_conn"));
                    
        if($mysql = collect($database->get("mysql"))){
            if(!$mysql){
                return false;
            }
            
            $connHost = $mysql->get("host");
            $username = $mysql->get("username");
            $password = $mysql->get("password");
            $database = $mysql->get("database");
            $connPort = $mysql->get("port");
            $charSet  = $mysql->get("charset");
            
            if($connHost && $username && $database && $connPort){
                try{
                    if(!is_null($connPort)){
                        $connPort = ":{$connPort}";
                    }
                    
                    $connStr = "mysql:host={$connHost}{$connPort};dbname={$database}";
                    
                    if(!is_null($charSet)){
                        $connStr.=";charset={$charSet}";
                    }
                    
                    $conn = new PDO($connStr, $username, $password);
                    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                    
                    if(!defined("DB_INFO")){
                        define("DB_INFO", [
                            "DB-Engine" => "MySQL",
                            "Current-Database" => $database,
                            "Connection-Host" => "{$connHost}{$connPort}"
                        ]);
                    }
                }catch(PDOException $e){
                    throw new SQLException($e->getMessage(), SQLException::CONN_ERROR);
                }
            }
        }
    }
    
    /**
     * 
     * @return PDO
     */
    public function getConnection(){
        return $this->conn;
    }
    
    /**
     * 
     * terminates mysql connection thread
     * it's a good practice to close db connection per execution to avoid data leaks
     * 
     */
    public function closeConnection(){        
        if($this->conn !== null && $this->conn->getAttribute(PDO::ATTR_DRIVER_NAME)){
            $this->conn = null;
        }
    }
    
    /**
     * 
     * closing connection for better performance
     * 
     */
    public function __destruct(){
        $this->closeConnection();
    }
    
    /**
     * 
     * 
     * 
     * @return MySQLInstance
     */
    public static function getInstance(){
        if(self::$instance === null){
            self::$instance = new MySQLInstance();
        }
        
        return self::$instance;
    }
}