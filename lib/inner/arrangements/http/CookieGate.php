<?php
namespace lib\inner\arrangements\http;

use lib\inner\Accessor;
use lib\util\CookieDataWarper;

/**
 * 
 * @method static void set(\lib\util\builders\Cookie $cookie)
 * @method static mixed|null get(string $cookieName)
 * @method static boolean modify(\lib\util\builders\Cookie $cookie)
 * @method static \lib\util\Collections all();
 * @method static boolean delete(string $cookieName)
 * @method static boolean clear()
 *
 */
class CookieGate extends Accessor{
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\inner\Accessor::warpedInstance()
     */
    public function warpedInstance(){
        return CookieDataWarper::class;
    }
}