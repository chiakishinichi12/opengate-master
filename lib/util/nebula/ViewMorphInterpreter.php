<?php
namespace lib\util\nebula;

use InvalidArgumentException;
use lib\util\Collections;
use lib\util\StringUtil;
use lib\util\exceptions\ViewLocatingException;
use lib\util\file\File;
use lib\util\file\FileUtil;
use lib\util\nebula\interpretations\Directive;
use lib\inner\App;

class ViewMorphInterpreter {
        
    /**
     * 
     * @var ViewMorphMarker
     */
    protected $marker;
    
    /**
     *
     * @var string
     */
    protected $cache;
    
    /**
     * 
     * @var string
     */
    protected $path;
    
    /**
     * 
     * @var Collections
     */
    protected $morphs;
    
    /**
     * 
     * @var Collections
     */
    protected $contents;
    
    /**
     * 
     * @var boolean
     */
    protected $strict = true;
    
    /**
     * 
     * @var boolean
     */
    protected $switchstart = false;
    
    /**
     * 
     * @var boolean
     */
    protected $hasfirstcase = false;
    
    public function __construct(){
        $setup = preferences("setup");
        
        $this->strict = $setup->get("view_morph_strict");
        
        $this->marker = App::make(ViewMorphMarker::class);
        
        $this->cache = $setup->get("view_morph_cache");
        
        $this->path = $setup->get("view_base_path");
        
        $this->contents = collect();
        
        $this->morphs = $this->marker->morphs();
    }
    
    /**
     * 
     * @param File $file
     * @param string $relativePath
     */
    public function morph(File $file, $relativePath){
        if($file->isDirectory() 
            || !StringUtil::endsWith($file->getBasename(), ".morph.php")){
            return;
        }
                
        $this->markingMorphedPath($relativePath, $file);
    }
    
    /**
     * 
     * @param boolean $switchstart
     * @throws InvalidArgumentException
     * @return boolean
     */
    public function switchStart($switchstart = null){
        if(is_blank($switchstart)){
            return $this->switchstart;
        }
        
        if(!is_bool($switchstart)){
            throw new InvalidArgumentException("Argument#1: expects boolean. "
                .class_name($switchstart)." found");
        }
        
        $this->switchstart = $switchstart;
    }
    
    /**
     *
     * @param boolean $hasfirstcase
     * @throws InvalidArgumentException
     * @return boolean
     */
    public function hasFirstCase($hasfirstcase = null){
        if(is_blank($hasfirstcase)){
            return $this->hasfirstcase;
        }
        
        if(!is_bool($hasfirstcase)){
            throw new InvalidArgumentException("Argument#1: expects boolean. "
                .class_name($hasfirstcase)." found");
        }
        
        $this->hasfirstcase = $hasfirstcase;
    }
    
    /**
     * 
     * Archive the contents of this object, transforming and storing them as specified,
     * while preserving original structure and evaluating certain expressions.
     *
     * This method serializes morphs, evaluates echo expressions, and processes directives
     * for each file in the 'contents' collection, storing the transformed content in a cache.
     * 
     */
    public function archived(){  
        $this->marker->push();
        
        $this->contents->each(function($file, $morphedPath){
            $morphContent = "";
            
            $file->getContentsAsCollection()->each(function($line, $ln) use (&$morphContent, $file){
                $matches = [];
                
                if($this->hasEchoExpression($line, $matches)){
                    $morphContent .= $this->evaluateEcho($line, $matches);
                }else if($this->hasDirective($line, $matches)){
                    $morphContent .= $this->evaluateDirective($line, $matches, $file);
                }else{
                    $morphContent .= $line;
                }
            });
            
            $morphContent .= "\n<?php //{$file->getName()} ?>";
            
            FileUtil::write("{$this->cache}/{$morphedPath}", $morphContent, "w+");
        });
    }
    
    /**
     * 
     * 
     * @param string $name
     * @return mixed|NULL
     */
    protected function loadDirective($name){
        $directivePath = base_path("lib/util/nebula/interpretations/directives");
        
        return FileUtil::requireIfExists("{$directivePath}/{$name}.php");
    }
    
    /**
     * 
     * @param string $line
     * @param array $matches
     * @return mixed
     */
    protected function evaluateEcho($line, $matches){
        $index = 1;
        
        foreach($matches[$index] as $key => $match){
            if(!empty(trim($match))){
                $replacement = "<?=e({$match})?>";
            }else{
                $match = $matches[$index + 1][$key];
                $replacement = "<?={$match}?>";
            }
            
            $line = str_replace($matches[$index - 1][$key], $replacement, $line);
        }
        
        return $line;
    }
    
    /**
     * 
     * @param string $line
     * @param array $matches
     * @throws ViewLocatingException
     * @return boolean|mixed
     */
    protected function evaluateDirective($line, $matches, $file){
        if(!($match = collect($matches))->count()){
            return false;
        }
        
        $subject = trim($match->get(0));
        
        $name = $this->directiveName($subject);
        $parn = $this->directiveParenthesis($name, $subject);
                
        if(!is_null($directive = $this->loadDirective($name))){
            if($directive instanceof Directive){
                $actualCode = $directive
                    ->obtains($file, $this)
                    ->parenthesis($parn)
                    ->interpretation();
                
                return str_replace($subject, $actualCode, $line);
            }
        }else{
            if($this->strict){
                throw new ViewLocatingException("Unknown Directive: '@{$name}' [{$file->getRelativePath()}]");
            }else{
                return $line;
            }
        }
    }
    
    /**
     * 
     * @param string $relativePath
     * @param string $morphContent
     */
    protected function markingMorphedPath($relativePath, $file){
        $encrypted = hash_hmac("sha256",
            uniqid(basename($relativePath)), 
            date(STANDARD_TIMESTAMP_FORMAT));
                
        $morphedPath = str_replace(basename($relativePath), $encrypted, $relativePath).".php";
        
        if($this->morphs->hasKey($relativePath)){
            $morphedPath = $this->morphs->get($relativePath);
        }
        
        $this->morphs->put($relativePath, $morphedPath);
        
        $this->contents->put($morphedPath, $file);
    }
    
    /**
     * 
     * @param string $subject
     * @return mixed|NULL
     */
    protected function directiveName($subject){
        $pattern = '/^\s*@([\w.]+)/';
                
        $matches = [];
        
        if(preg_match($pattern, $subject, $matches)) {
            // returns directive name
            return $matches[1]; 
        }
        
        return null;
    }
    
    /**
     * 
     * @param string $name
     * @param string $subject
     * @return mixed|NULL
     */
    protected function directiveParenthesis($name, $subject){
        $pattern = "/@{$name}\((.*)\)/";
        
        $matches = [];
        
        if (preg_match_all($pattern, $subject, $matches)) {
            // returns parenthesis data 
            return $matches[1][0];
        } 
        
        return null;
    }
    
    /**
     * 
     * @param string $line
     * @param array $matches
     * @return boolean
     */
    protected function hasDirective($line, &$matches = null){
        // Define the regular expression pattern for a Blade directive with optional parentheses
        $pattern = '/\s*@[\w.]+(?:\(.+\))?/';
        
        // Use preg_match to check if the line matches the pattern
        return preg_match($pattern, $line, $matches) === 1;
    }
    
    /**
     * 
     * @param string $input
     * @param array $matches
     * @return boolean
     */
    protected function hasEchoExpression($input, &$matches = null) {
        $pattern = "/\{\{\s*(.*?)\s*\}\}|\{\!\s*(.*?)\s*\!\}/";
        
        preg_match_all($pattern, $input, $matches);
                
        return count($matches[1]) || count($matches[2]);
    }
}