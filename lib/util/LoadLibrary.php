<?php

namespace lib\util;

/**
 * 
 * @author PH ANTHONY SUERTE
 *
 */
class LoadLibrary {

    /**
     * 
     * @var array
     */
    protected $directories = [];
    
    public function __construct(){
        $fpath = __DIR__."/struct/internal_dirs.php";
        
        if(@file_exists($fpath)){
            $this->directories = require_once $fpath;
        }
    }
    
    /**
     * 
     * load framework files and everything it gets
     * 
     */
    public function requiringFiles(){
        if(isset($this->directories["procedurals"])){
            foreach($this->directories["procedurals"] as $dir){
                $focusedDir = __DIR__."/..{$dir}";
                
                if(!@file_exists($focusedDir)){
                    continue;
                }
                    
                $this->recursiveInclusion($focusedDir);
            }
        }
        
        $root = __DIR__."/../../";
        
        $this->openGateAutoloader($root);
        
        /**
         *
         * considering the case the autoload and its class will be present on the project
         *
         */
        if(@file_exists("{$root}vendor/autoload.php")){
            require "{$root}vendor/autoload.php";
        }
    }
    
    /**
     * 
     * this function will try everything to make OG's library loading more flexible
     * 
     * @param unknown $root
     */
    protected function openGateAutoloader($root){
        spl_autoload_register(function($class) use ($root) {
            $filepath =  "{$root}{$class}.php";
            $filepath = str_replace("\\", "/", $filepath);
            
            if(@file_exists($filepath)){
                require_once $filepath;
            }
        });
    }
    
    /**
     * 
     * Smart scanning-and-then-inclusion
     * 
     * @param string $filepath
     */
    protected function recursiveInclusion($filepath){
        if(!@file_exists($filepath)){
            return;
        }
        
        if(@is_dir($filepath)){
            $dfs = array_diff(scandir($filepath), [".", ".."]);
            
            foreach($dfs as $df){
                $focused = "{$filepath}/{$df}";
                
                $structs = "util/struct";
                
                $info = pathinfo($focused);
                
                if(substr_compare($focused, $structs, -strlen($structs)) === 0 || 
                    substr_compare($filepath, $structs, -strlen($structs)) === 0){
                    continue;
                }
                
                if(is_dir($focused)){
                    $this->recursiveInclusion($focused);
                }else if(in_array($info["extension"], ["php"])){
                    require_once $focused;
                }
            }
        }else{
            require_once $filepath;
        }
    }
}