<?php

use lib\util\ConfigVar;

function webExceptionCallback(Throwable $e){
    echo "<title>[!!]{$e->getMessage()}</title>";
    
    echo "<div style=\"margin:5px;border:1px solid black;padding:10px;\">";
    echo "<code>";
    
    echo "!![<strong>UNCAUGHT EXCEPTION {".get_class($e)."}</strong>]: {$e->getMessage()} on ";
    echo "<strong>Line {$e->getLine()}</strong> <br/>({$e->getFile()})<br/>";
    
    if(class_exists(ConfigVar::class)){
        $timezone = ConfigVar::get("system_date_timezone");
        date_default_timezone_set($timezone);
        
        $currdate = date(STANDARD_TIMESTAMP_FORMAT);
        
        echo "<br/>[TIMESTAMP]: <strong>{$currdate}</strong> ({$timezone})<br/>";
    }
    
    echo "<br/><strong>Traces</strong><br/>";
    foreach($e->getTrace() as $k => $trace){
        $traceNum = $k + 1;
        
        $traceCollect = collect($trace);
        
        $line = $traceCollect->get("line");
        $file = $traceCollect->get("file");
        $clss = $traceCollect->get("class");
        $func = $traceCollect->get("function");
        $type = $traceCollect->get("type");
        
        if(!empty($clss))
            $clss = "{$clss}{$type}";
            
        echo "<br/> {$traceNum}.) <strong>Line {$line}</strong> of [{$file} | <strong>{$clss}{$func}()</strong>]";
    }
    
    $paramServer = function(String $elemName){
        return isset($_SERVER[$elemName])
            ? $_SERVER[$elemName]
            : "";
    };
    
    echo "<br/><br/>";
    echo "<hr/>";
    
    echo "<strong>Server-Software: </strong>{$paramServer("SERVER_SOFTWARE")}";
    
    echo "<br/><br/><hr/>";
   
    $protocol = function() use ($paramServer){
        if ($paramServer("HTTPS") == 'on' ||
            $paramServer("HTTPS") == 1 ||
            $paramServer('HTTP_X_FORWARDED_PROTO') == 'https') {
                $protocol = 'https://';
            } else {
                $protocol = 'http://';
            }
            
            return $protocol;
    };
    
    $requestMethod = $_SERVER["REQUEST_METHOD"];
    $url = "{$protocol()}{$paramServer("HTTP_HOST")}{$paramServer("REQUEST_URI")}";
    
    echo "<strong>Request Details[{$requestMethod}]</strong><br/>";
    echo "<strong>URL</strong>: {$url}";
    echo "<br/><br/>";
    echo "==<strong> Headers </strong>==<br/><br/>";
    
    foreach(apache_request_headers() as $header => $value){
        echo "[<strong>{$header}</strong>] {$value}<br/>";
    }
    
    echo "<br/>";
    echo "<hr/>";
    echo "<strong>Response Details</strong><br/><br/>";
    
    $responseCode = http_response_code();
    
    echo "Status Code: {$responseCode}<br/>";
    
    echo "</code>";
    echo "</div>";
}