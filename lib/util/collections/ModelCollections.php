<?php

namespace lib\util\collections;

use Closure;
use lib\util\Collections;
use lib\util\exceptions\CollectionTypeException;
use lib\util\exceptions\ModelException;
use lib\util\model\Model;

/**
 * 
 * a collections class powered by framework's ORM engine
 *
 */
class ModelCollections extends Collections {    
    
    /**
     * 
     * @param array $items
     */
    protected function __construct(array $items){
        parent::__construct($items, Model::class);
    }
    
    /**
     * picks only recently added items according to the past time span
     *  
     * @param string $column
     * @param string $spanOfTimeStr
     * 
     * @return \lib\util\ModelCollections
     */
    public function recent(string $column = null, string $spanOfTimeStr = null){
        $spanOfTimeStr = $spanOfTimeStr ?: "+8 days";
        $column = $column ?: "create_time";
        
        $spanOfTimeCallback = function($time) use ($spanOfTimeStr){
            $numDaysLater = strtotime($spanOfTimeStr, strtotime($time));
            $currentDate = strtotime(date(STANDARD_TIMESTAMP_FORMAT));
            
            return $numDaysLater > $currentDate;
        };
        
        return $this->spanningTime($column, $spanOfTimeCallback);
    }
    
    /**
     * 
     * sorts the items according to the oldest timestamp column data
     * 
     * @param string $column
     * @return ModelCollections
     */
    public function oldest(string $column = null){
        $column = $column ?: "create_time";
        
        $sorted = self::storeStack($this->items)->usort(function($m1, $m2) use ($column){
            return strtotime($m1->{$column}) - strtotime($m2->{$column});
        });
        
        return $sorted;
    }
    
    /**
     *
     * sorts the items according to the latest timestamp column data
     *
     * @param string $column
     * @return ModelCollections
     */
    public function latest(string $column = null){
        $column = $column ?: "create_time";
        
        $sorted = self::storeStack($this->items)->usort(function($m1, $m2) use ($column){
            return strtotime($m2->{$column}) - strtotime($m1->{$column});
        });
            
        return $sorted;
    }
    
    /**
     * 
     * @param string $column
     * @param Closure $spanOfTimeCallback
     * @throws CollectionTypeException
     * @return \lib\util\ModelCollections
     */
    public function spanningTime($column, $spanOfTimeCallback){
        $modeledData = [];
        
        $this->each(function(&$item) use ($spanOfTimeCallback, $column, &$modeledData){
            $columns = $item->getColumns();
            
            $prop = function($item){
                return $item->field;
            };
            
            $findColumn = $columns->binarySearch($column)
                ->toCompare($prop)
                ->get()
                ->value();
            
            if($findColumn !== null){
                if(!($spanOfTimeCallback instanceof Closure)){
                    throw new CollectionTypeException("'spanOfTimeCallback' must be an instance of Closure");
                }
                
                if(!in_array($findColumn->type, ["date", "datetime", "timestamp"])){
                    throw new CollectionTypeException("Not a time-constrained column: {{$findColumn->type}}");
                }
                
                $ranged = $spanOfTimeCallback($item->{$column});
                
                if($ranged){
                    $modeledData[] = $item;
                }
            }else{
                throw new CollectionTypeException("Column '{$column}' isn't defined on the model class");
            }
        });
        
        return self::storeStack($modeledData);
    }
    
    /**
     * 
     * @param array $mods
     * @throws ModelException
     * 
     * @return \lib\util\ModelCollections
     */
    public function update(array $mods){
        $this->each(function(&$item) use ($mods){
            foreach($mods as $prop => $value){                
                $item->{$prop} = $value;
            }
            
            $item->save();
        });    
        
        return $this;
    }
    
    /**
     * 
     * @return \lib\util\collections\ModelCollections
     */
    public function delete(){
        $this->each(function(&$item, $index){
            if($item->delete()){
                $this->remove($index);
            }
        });
        
        return $this;
    }
    
    /**
     * 
     * @param array $items
     * @return \lib\util\ModelCollections
     */
    public static function storeStack(array $items){
        $mcollect = new ModelCollections($items);
        $mcollect->strictlyCheckTypes();
        
        return $mcollect;
    }
}