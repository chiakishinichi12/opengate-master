<?php
namespace lib\util\builders;

use ArrayAccess;
use ArrayIterator;
use Countable;
use IteratorAggregate;
use lib\util\exceptions\SessionException;

/**
 * SessionValue is a versatile class that allows for easy manipulation of session data.
 *
 * It implements the ArrayAccess and Countable interfaces, enabling you to use
 * array-like operations on the session data when it's an array. It also provides
 * methods for interacting with session values when they are objects or other types.
 *
 * @author Jiya Sama
 */
class SessionValue implements ArrayAccess, Countable, IteratorAggregate {
    
    /**
     * 
     * @var mixed
     */
    protected $name;
    
    /**
     * 
     * @var mixed
     */
    protected $value;
    
    /**
     * 
     * Session identifier
     * @param mixed $name
     * 
     * Session value
     * @param mixed $value
     */
    public function __construct($name, $value){
        $this->name = $name;
        $this->value = $value;
    }
    
    /**
     * 
     * @param string $method
     * @throws SessionException
     */
    protected function throwIfInternalMethodDoesntExists($method){
        if(!method_exists($this, $method)){
            $this->throwNotFoundException("Method", $method);
        }
    }
    
    /**
     * 
     * @param string $what
     * @param string $lookingFor
     * @throws SessionException
     */
    protected function throwNotFoundException($what, $lookingFor){
        throw new SessionException("{$what} Not Found: {$lookingFor}");
    }
    
    /**
     * 
     * @throws SessionException
     */
    protected function throwIfNotAnArray(){
        if(!(is_array($this->value) || ($this->value instanceof Collections))){
            throw new SessionException("Value is not an array or an instance of Collections");
        }
    }
    
    /**
     * @inheritdoc
     *
     * will only be useful if the value is an array
     *
     * @param mixed $offset
     *
     * return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset){
        $this->throwIfNotAnArray();
        
        return $this->value[$offset];
    }
    
    /**
     * @inheritdoc
     *
     * @param mixed $offset
     * @param mixed $value
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value){
        $this->throwIfNotAnArray();
        
        $this->value[$offset] = $value;
    }
    
    /**
     * @inheritdoc
     *
     * will only be useful if the value is an array
     *
     * @param mixed $offset
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset){
        $this->throwIfNotAnArray();
        
        unset($this->value[$offset]);
    }
    
    /**
     * @inheritdoc
     *
     * will only be useful if the value is an array
     *
     * @param mixed $offset
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset){
        $this->throwIfNotAnArray();
        
        return isset($this->value[$offset]);
    }
    
    /**
     * @inheritdoc
     * 
     * @return \ArrayIterator
     */
    #[\ReturnTypeWillChange]
    public function getIterator(){
        $stack = [];
        
        if(is_array($this->value)){
            $stack = $this->value;
        }
        
        return new ArrayIterator($stack);
    }
    
    /**
     * @inheritdoc
     *
     * will only be useful if the value is an array
     *
     */
    #[\ReturnTypeWillChange]
    public function count(){
        $this->throwIfNotAnArray();
        
        return count($this->value);
    }
    
    /**
     *
     * @return string
     */
    public function type(){
        return class_name($this->value);
    }
    
    /**
     * 
     * @param mixed $value
     * @return boolean
     */
    public function create($value){
        if($this->active()){
            return false;
        }
        
        $this->value = $value;
        
        session($this->name, $value);
        
        return true;
    }
    
    /**
     *
     * @return mixed
     */
    public function value(){
        return $this->value;
    }
    
    /**
     *
     * unsets a session
     * 
     * @return bool
     */
    public function unset(){
        if(!$this->active()){
            return false;
        }
        
        session()->unset($this->name);
        
        return true;
    }
    
    /**
     * updates the session value
     *
     * @param mixed $value
     * 
     * @return bool
     */
    public function modify($value){
        if(!$this->active()){
            return false;
        }
        
        $this->value = $value;
        
        session($this->name, $this->value);
        
        return true;
    }
    
    /**
     * evaluates if a session is active
     * 
     * @return bool
     */
    public function active(){
        return session()->active($this->name);
    }
    
    /**
     * will be only useful when the value is an object
     *
     * @param mixed $name
     * @param mixed $value
     */
    public function __set($name, $value){
        if(is_object($this->value)){
            if(!property_exists($this->value, $name)){
                $this->throwNotFoundException("Property", sprintf("%s->%s",
                    class_name($this->value),
                    $name));
            }
            
            $this->value->$name = $value;
        }
    }
    
    /**
     * will be only useful when the value is an object
     *
     * @param string $name
     * @return mixed
     */
    public function __get($name){
        if(is_object($this->value)){
            if(!property_exists($this->value, $name)){
                $this->throwNotFoundException("Property", sprintf("%s->%s",
                    class_name($this->value),
                    $name));
            }
            
            return $this->value->$name;
        }
    }
    
    /**
     * will be only useful if the value is an object
     *
     * @param mixed $method
     * @param mixed $params
     * @throws Exception
     * @return unknown
     */
    public function __call($method, $params){
        if(is_object($this->value)){
            return $this->value->{$method}(...$params);
        }else{
            $this->throwIfInternalMethodDoesntExists($method);
        }
    }
    
    /**
     *
     * @param string $method
     * @param string $params
     * @return mixed
     */
    public static function __callStatic($method, $params){
        return (new static)->$method(...$params);
    }
    
    /**
     *
     * @return string
     */
    public function __toString(){
        if(!is_string($this->value)){
            return trim(print_r($this->value, true));
        }
        
        return $this->value;
    }
    
    /**
     * Destructor method for managing session data upon object destruction.
     *
     * This method checks if the session data associated with this object is marked as temporary.
     * If the session data is temporary, it is unset upon object destruction.
     * It helps ensure that temporary session data is automatically cleaned up when it is no longer needed.
     */
    public function __destruct(){
        if(session()->temporary($this->name)){
            $this->unset();
        }
    }
}