<?php

namespace lib\util;

use lib\inner\Accessor;
use lib\util\configurables\AppConfig;

/**
 * 
 * @author antonio
 *
 * @method static \lib\util\configurables\AppConfig prefix(string $prefix)
 * @method static \lib\util\configurables\AppConfig itemAt(string $key, mixed $defaultValue = null)
 * @method static mixed get(string $key, mixed $defaultValue = null)
 * @method static \lib\util\configurables\AppConfig modify($key, $value = null)
 */
class ConfigVar extends Accessor{
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\inner\Accessor::warpedInstance()
     */
    public function warpedInstance(){
        return AppConfig::class;
    }
}