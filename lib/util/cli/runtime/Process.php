<?php
namespace lib\util\cli\runtime;

use RuntimeException;
use lib\util\ElapsedTime;

/**
 * 
 * Lightweight command-line process handler
 * 
 * @author Antonius Aurelius
 *
 */
class Process {
    
    /**
     * 
     * @var string
     */
    protected $command;
    
    /**
     * 
     * @var int
     */
    protected $exitCode = null;
        
    /**
     * 
     * @var resource
     */
    protected $process;
    
    /**
     * 
     * @var boolean
     */
    protected $running = false;
    
    /**
     * 
     * @var string
     */
    protected $opbuilder = "";
    
    /**
     * 
     * @var string
     */
    protected $directory = null;
    
    /**
     * 
     * @var ElapsedTime
     */
    protected $elapsedTime;
    
    /**
     * 
     * @var number
     */
    protected $executionTime = 0;
    
    /**
     * 
     * @param string $command
     */
    public function __construct($command){
        $this->command = $command;
    }
    
    /**
     * 
     * @param string $directory
     * @return \lib\util\cli\Process
     */
    public function setDirectory(string $directory){
        $this->directory = $directory;
        
        return $this;
    }
    
    /**
     * 
     * @return number
     */
    public function getExitCode(){
        return $this->exitCode;
    }
    
    /**
     * 
     * @return boolean
     */
    public function isSuccessful(){
        return $this->exitCode === 0;
    }
    
    /**
     * 
     * @return boolean
     */
    public function isRunning(){
        if($this->running && feof($this->process)) {
            $this->close();
        }
        
        return $this->running;
    }
    
    /**
     * 
     * executes the process
     * 
     * @param string $mode
     * @return \lib\util\cli\Process
     */
    public function start(string $mode = "r"){
        $this->validate();
        
        // if the directory property is defined. a focused directory will be changed
        if(!is_blank($this->directory)){
            chdir($this->directory);
        }
        
        $this->process = popen("{$this->command} 2>&1", $mode);
        
        if(is_resource($this->process)){
            $this->elapsedTime = (new ElapsedTime())->start();
            $this->running = true;
        }else{
            throw new RuntimeException("Failed to start the process");
        }
        
        return $this;
    }
    
    /**
     * 
     * command processing validation
     * 
     */
    protected function validate(){
        if($this->running){
            throw new RuntimeException("Process is already running!");
        }
        
        if(is_blank($this->command)){
            throw new RuntimeException("command must not be empty");
        }
    }
    
    /**
     * 
     * Read output stream
     * 
     * @return string
     */
    public function getOutput() {
        $line = "";
        
        if($this->running && !feof($this->process)){
            $captured = true;
            $line = fgets($this->process, 4096); // Read in chunks
            $this->opbuilder .= $line;
        }
        
        return isset($captured) ? $line : $this->opbuilder;
    }
    
    /**
     * 
     * Wait for the process to complete
     * 
     * @return number
     */
    public function wait() {
        while ($this->isRunning()) {
            usleep(100000); // wait 0.1 seconds
            $this->getOutput(); // capture output if any
        }
        
        return $this->exitCode;
    }
    
    /**
     * 
     * @return number
     */
    public function getExecutionTime(){
        return $this->executionTime;
    }
    
    /**
     * 
     * Close the process and get the exit code
     * 
     */
    public function close(){
        $this->exitCode = pclose($this->process);
        $this->executionTime = $this->elapsedTime->getElapsed();
        $this->running = false;
    }
}