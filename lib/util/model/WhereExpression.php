<?php

namespace lib\util\model;

class WhereExpression {
    
    public $columns;
    
    public $expression;
    
    public function __construct(String $expression, Array $columns){
        $this->expression = $expression;
        $this->columns    = $columns;
    }
}