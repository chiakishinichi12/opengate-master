<?php

namespace lib\util;

use lib\util\file\FileUtil;

class ResponseCode {
    protected static $responseCodes = null;
    
    /**
     *
     * @param String $configKey
     * @return mixed|boolean
     */
    public static function get(String $responseCode){
        if(self::$responseCodes === null){
            self::$responseCodes = FileUtil::requireIfExists(__DIR__."/struct/response_codes.php");
        }
        
        if(is_array(self::$responseCodes)){
            return isset(self::$responseCodes[$responseCode])
            ? self::$responseCodes[$responseCode]
            : false;
        }
        
        return false;
    }
}