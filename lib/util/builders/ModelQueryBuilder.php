<?php

namespace lib\util\builders;

use Closure;
use lib\inner\App;
use lib\mocks\Mockery;
use lib\mocks\MockeryWarper;
use lib\traits\Sanitation;
use lib\util\Collections;
use lib\util\datagate\DB;
use lib\util\exceptions\ModelException;
use lib\util\model\Model;
use lib\util\model\PrimaryKey;
use lib\util\model\relational\RelationalMaterial;
use lib\util\model\relational\RelationalModelException;
use lib\util\model\relational\material\BelongsTo;
use lib\util\model\relational\material\HasMany;
use lib\util\model\relational\material\HasOne;
use lib\util\resolver\MethodInvocationResolver;

class ModelQueryBuilder {
    
    use Sanitation;
    
    /**
     * 
     * @var string;
     */
    protected $dbms;
    
    /**
     * 
     * @var PrimaryKey
     */
    protected $primary_key;
    
    /**
     * 
     * @var strin
     */
    protected $table_name;
    
    /**
     * 
     * @var Model
     */
    protected $model;
    
    /**
     * 
     * @var SelectBuilder
     */
    protected $query;
    
    /**
     * 
     * @var Collections
     */
    protected $merged;
    
    /**
     * 
     * @param Model $model
     */
    public function __construct($model){
        $this->model = $model;
        
        $this->defineImportantValues();
    }
    
    /**
     * 
     * defining important properties for model query builder class
     * 
     */
    protected function defineImportantValues(){
        $this->table_name = $this->model->getTableName();
        $this->primary_key = $this->model->getPrimaryKey();
        $this->dbms = $this->model->getDBMS();
        $this->query = $this->newSelectBuilder();
    }
    
    /**
     * 
     * @param array $fields
     * @param array $limits
     * @param array $orderBy
     * @return \lib\util\ModelCollections
     */
    public function all(array $fields = [], $limits = [], $orderBy = []){
        $builder = $this->query()
        ->columns($fields)
        ->where(1);
        
        $this->allHelper($builder, "limit", $limits);
        $this->allHelper($builder, "orderBy", $orderBy);
        
        return $builder->get();
    }
    
    /**
     * 
     * @param SelectBuilder $builder
     * @param string $clause
     * @param mixed $assigned
     */
    protected function allHelper(&$builder, string $clause, $assigned){
        if(is_int($assigned)){
            $assigned = "{$assigned}";
        }
        
        if(is_array($assigned) && !is_blank($assigned)){
            $builder->$clause(
                a($assigned, 0),
                a($assigned, 1)
            );
        }else if(is_string($assigned) && !is_blank($assigned)){
            $builder->$clause($assigned);
        }
    }
    
    /**
     *
     * find a record by primary key
     *
     * @param string $id
     * @param array $fields
     * @return array|Model
     */
    public function find($id, array $fields = []){        
        if(is_blank($this->primary_key)){
            throw new ModelException("Table `{$this->table_name}` has no PRIMARY KEY");
        }
        
        $data = $this->query()
            ->columns($fields)
            ->where($this->primary_key->getName(), "=", $id)
            ->get();
        
        return $data->get(0);
    }
    
    /**
     * 
     * fetches a record by primary key
     * otherwise, will throw an exception
     * 
     * @param mixed $id
     * @param array $fields
     * @throws ModelException
     * 
     * @return array|\lib\util\Model
     */
    public function findOrFail($id, Array $fields = []){
        $row = $this->find($id, $fields);
        
        if(is_null($row)){
            $primary = $this->primary_key ?: "id";
            
            $message = "Record wasn't successfully retrieved {{$primary}={$id}}";
            
            throw new ModelException($message, get_class($this->model), 
                ModelException::NONEXISTENT_RECORD);
        }
        
        return $row;
    }    
    
    /**
     * 
     * @param int $rowsPerLoop
     * @param callable $loadCallback
     * @param string $direction
     */
    public function chunkById(int $rowsPerLoop, $loadCallback, string $direction = "ASC"){
        if(is_blank($this->primary_key)){
            throw new ModelException("'chunkById' cannot be utilized due to the missing PRIMARY KEY", get_class($this->model));
        }
        
        $this->query()
            ->orderBy($this->primary_key->getName(), $direction) 
            ->chunk($rowsPerLoop, $loadCallback);
    }
    
    /**
     * 
     * @throws ModelException
     * 
     * @return \lib\util\builders\SelectBuilder
     */
    protected function newSelectBuilder(){
        $class = get_class($this->model);
        
        if(is_null($this->table_name)){
            throw new ModelException("{No table name indicated on model class}", $class);
        }
        
        $query = DB::connection($this->dbms)
            ->table($this->table_name)
            ->setModelClass($class);
                
        if($this->model->isSoftDeletesInherited()){
            $query->where("delete_time", "IS NULL");
        }
        
        return $query;
    }
    
    /**
     * 
     * @return \lib\util\builders\SelectBuilder
     */
    public function query(){
        return $this->query;
    }
    
    /**
     * 
     * @param string $column
     * @param array $possibilities
     * 
     * @return \lib\util\ModelCollections
     */
    public function whereIn(string $column, array $possibilities){
        return $this->query()
            ->where($column, "IN", $possibilities)
            ->get();
    }
    
    /**
     * 
     * @param array $colAndValuePair
     * 
     * @return boolean
     */
    public function create(array $colAndValuePair = []){
        if(count($colAndValuePair) === 0){
            return false;
        }
        
        $mclassname = get_class($this->model);
        
        return (new $mclassname($colAndValuePair))->save();
    }
    
    /**
     * 
     * @param array $whereArgs
     * @param array $toModify
     * 
     * @return boolean
     */
    public function updateOrCreate(array $whereArgs, array $toModify){
        try{
            $first = $this->firstOrFail($whereArgs);
            
            foreach($toModify as $col => $val){
                $first->$col = $val;
            }
            
            return $first->save();
        }catch(ModelException $e){
            if($e->getExceptionFlag() === ModelException::NONEXISTENT_RECORD){
                return $this->create(array_merge($whereArgs, $toModify));
            }else {
                throw $e;
            }
        }
        
        return false;
    }
    
    /**
     * 
     * @param array $whereArgs
     * @param array $fields
     * 
     * @return Model
     */
    public function first(array $whereArgs = null, $fields = []){        
        if(!is_null($fields)){
            $this->query->columns($fields);
        }
                
        if(!is_null($whereArgs)){
            foreach($whereArgs as $col => $value){
                $this->query->where($col, "=", $value);
            }           
        }
        
        return $this->query->first();
    }
    
    /**
     * 
     * @param string $relationship
     * @param Closure $forBuilder
     * @param string $boolOp
     * 
     * @return \lib\util\builders\ModelQueryBuilder
     */
    public function whereHas(string $relationship, Closure $forBuilder = null, string $boolOp = "AND"){
        // must be HasOne, HasMany
        $material = $this->model->warpedMethod($relationship)?->invoke($this->model);
        
        if(is_null($material)){
            throw new RelationalModelException("[".class_name($this->model)
                ."] Argument#2: Unknown model relationship method '{$relationship}'");
        }
        
        $inheritances = [HasOne::class, HasMany::class];
        
        if(!App::checkInheritance($material, $inheritances)){
            throw new RelationalModelException("[".class_name($this->model)
                ."] '{$relationship}' material must be an instance of any of these classes [".
                    implode(", ", $inheritances)."]. ".class_name($material)." found");
        }

        $this->whereExists($this->existWhereClause($material, $forBuilder, $boolOp), $boolOp);
                
        return $this;
    }
    
    /**
     * 
     * @param RelationalMaterial $material
     * @param Closure $forBuilder
     * @param string $boolOp
     * 
     * @return string
     */
    protected function existWhereClause(RelationalMaterial $material, Closure $forBuilder = null, string $boolOp = "AND"){
        $rclassnm = $material->foreignModel();
                            
        $fkeystr = "{$material->getTableName()}.{$material->foreignKey()}";
            
        $ckeystr = "{$this->model->getTableName()}.{$material->ownerKey()}";
            
        // formulating where clause for exists()
        return $rclassnm::columns([1])
        ->where($fkeystr, "=", col($ckeystr))
        ->ifWhat(!is_null($forBuilder), function(SelectBuilder $query) use ($rclassnm, $forBuilder, $boolOp){
            $relation = new $rclassnm();
            
            $forBuilder($relation);
            
            if(!is_blank($where = $relation->getStackedWhereClause())){
                $query->whereRaw($where, $boolOp);
            }
        })
        ->limit(null)
        ->queryString();
    }
    
    /**
     * 
     * @param string $relationship
     * @param Closure $forBuilder
     * @return \lib\util\builders\ModelQueryBuilder
     */
    public function orWhereHas(string $relationship, Closure $forBuilder = null){
        return $this->whereHas($relationship, $forBuilder, "OR");
    }
    
    /**
     *
     * @param string|array $relationship
     * @return \lib\util\model\Model|null
     */
    public function with(string|array $relationship){
        $relExtraLogic = [];
        
        $currentItems = $this->loadWithMergingQuery($relationship, $relExtraLogic)->get();
        
        foreach($relationship as $relname => $relobject){
            $relItems = $relobject->foreignModel()::where($relobject->foreignKey(), "IN",
                $currentItems->map(fn($i) => $i->{$relobject->foreignKey()}));
            
            if(isset($relExtraLogic [$relname])){
                if(is_callable($relExtraLogic[$relname])){
                    $relExtraLogic[$relname]($relItems);
                }
            }
            
            $this->mergeResult($relname, $relobject, $currentItems, $relItems->get());
        }
        
        $this->merged = $currentItems;
        
        return $this;
    }
    
    /**
     * 
     * @param string|array $relationship
     * @param array $relExtraLogic
     * 
     * @return \lib\util\builders\ModelQueryBuilder
     */
    protected function loadWithMergingQuery(string|array &$relationship, array &$relExtraLogic){
        if(is_array($relationship)){
            $temp = [];
            
            foreach($relationship as $key => $relobject){
                if(is_string($key)){
                    if(!is_callable($relobject)){
                        continue;
                    }
                    
                    $relExtraLogic[$key] = $relobject;    
                    $relobject = $key;
                }
                
                if(is_null($tref = $this->findRelationalObject($relobject))){
                    $this->throwRelationalModelException($relobject);
                }
                
                $temp[$relobject] = $tref;
                
                $this->orWhereExists($this->existWhereClause($tref));
            }
            
            $relationship = $temp;
        }else{
            if(is_null($relobject = $this->findRelationalObject($relationship))){
                $this->throwRelationalModelException($relationship);
            }
            
            $relationship = [$relationship => $relobject];
            
            $this->orWhereExists($this->existWhereClause($relobject));
        }
        
        return $this;
    }
    
    /**
     * 
     * @param string $relationship
     */
    protected function throwRelationalModelException(string $relationship){
        throw new RelationalModelException("[".class_name($this->model)."] ".
            "No relationship method '{$relationship}' defined");
    }
    
    /**
     * 
     * @param string $relationship
     * @return RelationalMaterial|NULL
     */
    protected function findRelationalObject(string $relationship){
        if(!is_null($relobject = $this->model->warpedMethod($relationship))){
            $relobject = $relobject->invoke($this->model);
            
            if(!is_null($relobject) && !App::checkInheritance($relobject, RelationalMaterial::class)){
                goto nothing;
            }
            
            if(App::checkInheritance($relobject, [BelongsTo::class, HasOne::class])){
                if(!$relobject->hasDefault()){
                    $relobject->withDefault();
                }
            }
            
            $relobject->retrieve();
            
            return $relobject;
        }
        
        nothing:
        return null;
    }
    
    /**
     * 
     * @param string $relationship
     * @param RelationalMaterial $relobject
     * @param Collections $currentItems
     * @param Collections $relItems
     */
    protected function mergeResult(string $relationship, RelationalMaterial $relobject, Collections &$currentItems, Collections $relItems){
        foreach($currentItems as &$item){
            $ownerKey = $item->{$relobject->ownerKey()};
            
            $relstack = collect();
            
            foreach($relItems as $relative){
                $foreignKey = $relative->{$relobject->foreignKey()};
                
                if($foreignKey === $ownerKey){
                    $relstack->add($relative);
                }
            }
            
            if(count($relstack) !== 0){
                if(App::checkInheritance($relobject, BelongsTo::class)){
                    $item->$relationship = $relstack->get(0);
                }else if(App::checkInheritance($relobject, HasMany::class, HasOne::class)){
                    $item->$relationship = $relstack;
                }
            }
        }
    }
    
    /**
     * 
     * this is a magic method but since the model query builder 
     * has other features to use. the collection stack utilized by this class
     * is somehow overriden.
     * 
     * @return \lib\util\Collections
     */
    public function get(){
        if(!is_null($this->merged)){
            return $this->merged;
        }
        
        return $this->query()->get();
    }
    
    /**
     * this is a magic method but since the model query builder 
     * has other features to use. the collection stack utilized by this class
     * is somehow overriden.
     * 
     * @param Closure $loopCallback
     */
    public function each(Closure $loopCallback){
        if(!is_null($this->merged)){
            $this->merged->each($loopCallback);
        }else{
            $this->query()->each($loopCallback);
        }
    }
    
    /**
     *
     * @param Model $relation
     * @param string $relationship
     *
     * @return Collections
     */
    public function whereBelongsTo(RelationalMaterial|Model $relation, string $relationship = null){
        if(!is_null($relationship)){
            $belongsTo = $this->model->warpedMethod($relationship)?->invoke($this->model);
            
            if(is_null($belongsTo)){
                throw new RelationalModelException("[".class_name($this->model)
                    ."] Argument#2: Unknown model relationship method '{$relationship}'");
            }
            
            if(!App::checkInheritance($belongsTo, BelongsTo::class)){
                throw new RelationalModelException("[".class_name($this->model)
                    ."] Argument#2: Relationship must be an instance of ".BelongsTo::class.". ".class_name($belongsTo)." found");
            }
            
            $belongsTo->retrieve();
            
            $clause = [
                $belongsTo->foreignKey(),
                $relation->{$belongsTo->ownerKey()}
            ];
        }else{
            $primaryKey = $relation->getPrimaryKey()->getName();
            
            $clause = [
                $primaryKey,
                $relation->$primaryKey
            ];
        }
        
        return $this->where(...$clause);
    }
    
    /**
     * 
     * @param array $whereArgs
     * @param array $fields
     * @throws ModelException
     * 
     * @return Model
     */
    public function firstOrFail(array $whereArgs, $fields = []){
        $model = $this->first($whereArgs, $fields);
        
        if(is_null($model)){
            throw new ModelException(
                "Nothing was retrieved by the first fetching", 
                get_class($this->model),
                ModelException::NONEXISTENT_RECORD);
        }
        
        return $model;
    }
    
    /**
     * 
     * @param array $whereArgs
     * @param array $fields
     * 
     * @return Model|boolean
     */
    public function firstOrCreate(array $whereArgs, array $attributesInCaseItsNew = []){
        $first = $this->firstOrNew($whereArgs, $attributesInCaseItsNew);
        
        if(!$first->isStored()){
            return $first->save();
        }
        
        return $first;
    }
    
    /**
     * 
     * @param array $whereArgs
     * @param array $attributesInCaseItsNew
     * 
     * @return Model|unknown
     */
    public function firstOrNew(array $whereArgs, array $attributesInCaseItsNew = []){
        try{
            $first = $this->firstOrFail($whereArgs);
            
            return $first;
        }catch(ModelException $e){
            $mclassname = get_class($this->model);
            
            $attributes = array_merge($whereArgs, $attributesInCaseItsNew);
            
            return new $mclassname($attributes);
        }
    }
    
    /**
     * 
     * Retrieve a mockery instance for the specified model.
     * 
     * @return Mockery
     */
    public function mockery(){
        return App::make(MockeryWarper::class)
            ->locateMockery(class_name($this->model));
    }
    
    /**
     *
     * Retrieve all records, including soft-deleted ones.
     *
     * @return SelectBuilder
     */
    public function withTrashed(){
        $class = get_class($this->model);
        
        if(!$this->model->isSoftDeletesInherited()){
            throw new ModelException("SoftDeletes (or delete_time column) isn't used or defined");
        }
        
        if(is_null($this->table_name)){
            throw new ModelException("{No table name indicated on model class}", $class);
        }
        
        $query = DB::connection($this->dbms)
        ->table($this->table_name)
        ->setModelClass($class);
        
        return $query;
    }
    
    /**
     *
     * Retrieve only soft-deleted records from the database.
     *
     * @return SelectBuilder
     */
    public function onlyTrashed(){
        if(!$this->model->isSoftDeletesInherited()){
            throw new ModelException("SoftDeletes (or delete_time column) isn't used or defined");
        }
        
        $class = get_class($this->model);
        
        if(is_null($this->table_name)){
            throw new ModelException("{No table name indicated on model class}", $class);
        }
        
        $query = DB::connection($this->dbms)
        ->table($this->table_name)
        ->setModelClass($class);
        
        if($this->model->isColumnExist("delete_time")){
            $query->where("delete_time", "IS NOT NULL");
        }
        
        return $query;
    }
    
    /**
     * 
     * @param string $method
     * @param array $params
     * 
     * @return mixed
     */
    public function __call(string $method, array $params){
        return App::make(MethodInvocationResolver::class)
        ->parent($this)
        ->instances([$this->query])
        ->toCall($method)
        ->params($params)
        ->allowMagicCall()
        ->resolve()
        ->invoke();
    }
}