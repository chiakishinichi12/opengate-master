<?php
namespace lib\util\migration\warper;

use lib\inner\ApplicationGate;
use lib\util\Collections;
use lib\util\GateFinder;
use lib\util\StringUtil;
use lib\util\cli\WarpArgv;
use lib\util\exceptions\MigrationException;
use lib\util\file\FileUtil;
use lib\util\migration\Migration;
use lib\util\exceptions\ClassNotFoundException;

class MigrationTableDown {
    
    /**
     * 
     * @var Collections
     */
    protected $args;
    
    /**
     *
     * @var ApplicationGate
     */
    protected $gate;
    
    /**
     *
     * @var Collections
     */
    protected $migrations;
    
    public function __construct(ApplicationGate $gate, WarpArgv $argv){
        $this->gate = $gate;
        $this->migrations = collect();
        $this->args = $argv->obtainArguments();
        
        $this->validateAndStore();
    }
    
    protected function validateAndStore(){
        $failedCallback = function(){
            die("No migration class found.\n");
        };
        
        $migpath = preferences("structure")->get("migrations");
        
        if(!FileUtil::exists($migpath)){
            $this->gate->call($failedCallback);
        }
        
        $inputtedClassname = $this->args->get(1);
        
        (new GateFinder(relative_path($migpath), true))->failedCallback($failedCallback)->load(function($class)
            use ($inputtedClassname){
            
            if(!is_subclass_of($class, Migration::class)){
                throw new MigrationException("Class {{$class}} is not an instance of ".Migration::class);
            }
            
            $migration = $this->gate->make($class);
            
            if(!method_exists($migration, "down")){
                throw new MigrationException("Method down() is not defined in {$class}");
            }
            
            if($inputtedClassname && !StringUtil::endsWith($class, $inputtedClassname)){
                return;
            }
            
            $this->migrations->add($migration);
        });
        
        if(!is_null($inputtedClassname) && !$this->migrations->count()){
            throw new ClassNotFoundException("Migration class '{$inputtedClassname}' doesn't exist");
        }
    }
    
    public function reverse(){
        $this->migrations->each(function($migration){
            $classbase = basename(str_replace("\\", "/", class_name($migration)));
            $migrated = $this->gate->call([$migration, "down"]);
            
            if(!is_bool($migrated)){
                return;
            }
            
            if($migrated){
                out()->println("Migration Reversed [{$classbase}]");
            }else{
                out()->println("Failed to reverse [{$classbase}]: table doesn't exists");
            }
        });
    }
}