<?php
namespace lib\util\resolver;

use BadMethodCallException;
use ReflectionMethod;
use lib\inner\App;
use lib\util\Collections;
use lib\util\EncapsulationHelper;
use lib\util\ReflectionUtil;
use lib\util\StringUtil;
use lib\util\annotation\Annotation;
use lib\util\annotation\AnnotationUtil;
use lib\util\exceptions\MethodNotFoundException;

/**
 * 
 * @author Jiya Sama
 *
 */
class MethodInvocationResolver {
    
    /**
     * 
     * @var EncapsulationHelper
     */
    protected $encapsulator;
    
    /**
     * 
     * @var string
     */
    protected $toCall;
    
    /**
     * 
     * @var array
     */
    protected $instances = [];
    
    /**
     * 
     * @var array
     */
    protected $exemptions = [];
    
    /**
     * 
     * @var array
     */
    protected $params = [];
    
    /**
     * 
     * @var object
     */
    protected $parent;
    
    /**
     * 
     * @var boolean
     */
    protected $notFound = true;
    
    /**
     * 
     * @var array
     */
    protected $withMagicCallClasses = [];
    
    /**
     * 
     * @var boolean
     */
    protected $allowMagic = false;
    
    /**
     * 
     * @var mixed
     */
    protected $invoked = null;
    
    /**
     * 
     * @var array
     */
    protected $aliases;
    
    public function __construct(){
        $this->encapsulator = App::make(EncapsulationHelper::class)->instance($this);
    }
    
    /**
     * 
     * @param string $value
     * @return MethodInvocationResolver|object
     */
    public function toCall(string $value){
        return $this->encapsulator->propertyDefinition($this->{__FUNCTION__}, $value);
    }
    
    /**
     * 
     * @param array $instances
     * @return MethodInvocationResolver|object
     */
    public function instances(array $stack){
        return $this->encapsulator->propertyDefinition($this->{__FUNCTION__}, $stack);
    }
    
    /**
     * 
     * @param array $stack
     * @return MethodInvocationResolver|object
     */
    public function exemptions(array $stack){
        return $this->encapsulator->propertyDefinition($this->{__FUNCTION__}, $stack);
    }
    
    /**
     * 
     * @param array $stack
     * @return MethodInvocationResolver|object
     */
    public function params(array $stack){
        return $this->encapsulator->propertyDefinition($this->{__FUNCTION__}, $stack);
    }
    
    /**
     * 
     * @param object $value
     * @return MethodInvocationResolver|object
     */
    public function parent(object $value){
        return $this->encapsulator->propertyDefinition($this->{__FUNCTION__}, $value);
    }
    
    /**
     * 
     * @param array $value
     * @return MethodInvocationResolver|object
     */
    public function aliases(array $value){
        return $this->encapsulator->propertyDefinition($this->{__FUNCTION__}, $value);
    }
    
    /**
     * 
     * @return ResolverExemption|NULL
     */
    protected function exemption(){
        $exemption = Collections::make($this->exemptions, ResolverExemption::class)
            ->binarySearch($this->toCall)
            ->toCompare(fn(ResolverExemption $re) => $re->method())
            ->get();
        
        return !is_null($exemption) ? $exemption->value() : null;
    }
    
    /**
     * 
     * resolves the special case method. this will be used especially 
     * if there's ambiguity with method name
     * 
     * @return mixed
     */
    protected function resolveExemption(){
        $owner = $this->exemption()?->owner();
        
        $exmpcls = collect($this->instances)->cmap(fn($object) => class_name($object));
        
        if(!$exmpcls->has($owner)){
            throw new MethodNotFoundException("Resolver Issue: The owner class {$owner} is not part of instance stack");
        }
        
        $this->notFound = false;
        
        $this->invoked = [$this->instances[$exmpcls->getKey($owner)], $this->toCall];
        
        return $this;
    }
    
    /**
     * 
     * @return boolean
     */
    protected function isMethodExemption(){
        $exemption = $this->exemption();
                
        return !is_null($exemption) && $exemption->condition();
    }
    
    /**
     * 
     * @param object $instance
     * @return boolean
     */
    protected function isExemptedMethodClass($instance){
        $owner = $this->exemption()?->owner();
        
        return !is_null($owner) && strcmp(class_name($instance), $owner) === 0;
    }
    
    /**
     * 
     * @param mixed $instance
     * @param array $params
     * @return mixed|NULL
     */
    protected function resolvingInvocation(mixed $instance){
        $this->notFound = false;
        
        return [$instance, $this->toCall];
    }
    
    /**
     * 
     * @return boolean
     */
    public function isNotFound(){
        return $this->notFound;
    }
    
    /**
     * 
     * @return \lib\util\resolver\MethodInvocationResolver
     */
    public function allowMagicCall(){
        $this->allowMagic = true;
        
        return $this;
    }
    
    /**
     * 
     * @param string|array $instance
     * @return \lib\util\resolver\MethodInvocationResolver
     */
    public function allowMagicCallIfInstanceOf(string|array $instance){
        if(is_array($instance)){
            $this->withMagicCallClasses = $instance;
        }else{
            $this->withMagicCallClasses = [$instance];
        }
        
        return $this;
    }
    
    /**
     * 
     * retrieving the actual method name if the resolving method holds an alias
     * 
     * @param mixed $instance
     */
    protected function retrieveActualMethodFromAlias(mixed $instance){
        $aliases = collect($this->aliases ?: []);
        
        if(!$aliases->has($this->toCall)){
            return;
        }
        
        if(method_exists($instance, $this->toCall)){
            throw new BadMethodCallException(
                "Method Resolver Concern: {".class_name($instance)."::{$this->toCall}} "
                    ."is a declared method and cannot be use as an alias.");
        }
        
        $actualMethodName = $aliases->getKey($this->toCall);
        
        if(is_string($actualMethodName)){
            $this->toCall = $actualMethodName;
        }
    }
    
    /**
     * 
     * evaluates if the invoked public method from parent class is invokable.
     * 
     * @param mixed $instance
     */
    protected function isResolvingMethodInvokable(mixed $instance){
        $publicMethods = ReflectionUtil::getDeclaredMethods($instance, ReflectionMethod::IS_PUBLIC)
        ->filter(function(ReflectionMethod $method){
            return !StringUtil::startsWith($method->getName(), "__");
        });
        
        $findInvokable = $publicMethods->binarySearch($this->toCall)
        ->toCompare(function(ReflectionMethod $method){
            return $method->getName();
        })->get()?->value();
        
        // considering this as true assuming the resolving method will be invoked
        // on the magic method __call
        if(is_null($findInvokable)){
           return true; 
        }
        
        // this is the check if the method holds @NotInvokable annotation
        if(AnnotationUtil::hasAnnotation($findInvokable, new Annotation("NotInvokable"))){
            return false;
        }
        
        return true;
    }
    
    /**
     * 
     * @return resolves the method to be resolved
     */
    public function resolve(){
        if($this->isMethodExemption()){
            return $this->resolveExemption();
        }
        
        foreach($this->instances as $instance){
            if(!is_object($instance)){
                continue;
            }   
                        
            $this->retrieveActualMethodFromAlias($instance);
            
            if(method_exists($instance, $this->toCall)){     
                if(!$this->isResolvingMethodInvokable($instance)){
                    throw new BadMethodCallException(
                        "Method Resolver Concern: {".class_name($instance)."::{$this->toCall}} "
                            ."method is not invokable.");
                }
                
                if($this->isExemptedMethodClass($instance)){
                    continue;
                }
                
                $this->invoked = $this->resolvingInvocation($instance);
                
                break;
            }
            
            if(!is_blank($this->withMagicCallClasses) && App::checkInheritance($instance, $this->withMagicCallClasses)){
                $this->allowMagic = true;
            }
            
            if($this->allowMagic && method_exists($instance, "__call")){
                $this->invoked = $this->resolvingInvocation($instance);
                break;
            }
        }
        
        return $this;
    }
    
    /**
     * 
     * @return mixed
     */
    public function invoke(){
        if(is_null($this->invoked)){
            $imploded = collect($this->instances)->implode(", ", fn($i) => class_name($i));
            
            throw new MethodNotFoundException("[".class_name($this->parent)
                ."] The method '{$this->toCall}' does not exist on any of the following instances: [$imploded]");
        }
        
        return call_user_func($this->invoked, ...$this->params);
    }
}

