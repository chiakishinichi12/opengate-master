<?php
namespace lib\mail;

interface Engine {
        
    /**
     * 
     * @param array $credentials
     */
    function initMailerEngine(array $credentials);
    
    /**
     * 
     * @param Dispatchable $dispatchable
     */
    function triggerSend(Dispatchable $dispatchable);
    
    /**
     * 
     * @param string $subject
     */
    function subject(string $subject);
    
    /**
     * 
     * @param array $recipients
     */
    function recipients(array $recipients);
    
    /**
     * 
     * @param array $attachments
     */
    function attachments(array $attachments);
}