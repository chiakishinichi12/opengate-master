<?php

use lib\util\nebula\interpretations\Directive;
use lib\util\nebula\interpretations\DirectiveThrowers;
use lib\util\nebula\interpretations\Keyword;
use lib\util\exceptions\MorphInterpretationException;
use lib\util\nebula\View;

return new class implements Directive, Keyword {
    
    use DirectiveThrowers;
    
    /**
     *
     * @var string
     */
    protected $name;
    
    /**
     *
     * @var string
     */
    protected $exp;
    
    public function __construct(){
        $base = basename(__FILE__);
        
        $this->name = substr($base, 0, strrpos($base, ".php"));
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\nebula\interpretations\Directive::obtains()
     */
    public function obtains($file, $interpreter){
        
        return $this;
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\nebula\interpretations\Directive::parenthesis()
     */
    public function parenthesis($exp){
        $this->exp = $exp;
        
        return $this;
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\nebula\interpretations\Directive::interpretation()
     */
    public function interpretation(){
        $this->requireParenthesis();
                
        $params = $this->parameters(2);
        
        $fallbackTemplate = preferences("predefined")->get("validator-issue");
        
        $validator = $params->get(0);
        $inputfield = $params->get(1);
        $view = $params->get(2);
                
        // default display
        $display = "\$invalid";
        
        if(is_null($view) && View::exists($fallbackTemplate)){
            $view = "\"{$fallbackTemplate}\"";
        }
        
        if(!is_null($view)){
            $display = "morph({$view}, [\"message\" => \$invalid])";
        }
        
        $phpcode =<<<SCRIPT
        <?php
            if(isset({$validator})){
                if({$validator} instanceof \lib\inner\arrangements\http\HttpValidator){
                    \$failures = {$validator}->failures();
                    
                    foreach(a(\$failures, {$inputfield}) ?: [] as \$invalid){
                        echo {$display};
                    }
                }
            }
        ?>
SCRIPT;
        
        return $phpcode;
    }
    
};