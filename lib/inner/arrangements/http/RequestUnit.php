<?php
namespace lib\inner\arrangements\http;

use lib\inner\App;
use lib\exceptions\OpenGateException;
use lib\util\StringUtil;
use GuzzleHttp\Psr7\Exception\MalformedUriException;

trait RequestUnit {
    
    /**
     * 
     * @var mixed
     */
    protected $unitTestInput;
    
    /**
     * 
     * @throws OpenGateException
     * 
     */
    protected function validateUnitTestContext(){
        if(!App::checkCLI()){
            throw new OpenGateException("[".__FUNCTION__."] the method must be used on CLI context");
        }
        
        if(!App::isRunningUnitTest()){
            throw new OpenGateException("[".__FUNCTION__."] the method is not allowed outside the unit test");
        }
    }
    
    /**
     * 
     * @param array $headers
     * 
     * @throws OpenGateException
     * 
     * @return \lib\inner\arrangements\http\Request
     */
    public function arrangeUnitTestHeaders(array $headers){
        $this->validateUnitTestContext();
        
        $this->headers->merge($headers);
        
        return $this;
    }
    
    /**
     *
     * @param array $unitTestInput
     *
     * @throws OpenGateException
     *
     * @return \lib\inner\arrangements\http\Request
     */
    public function arrangeUnitTestInput(mixed $unitTestInput){
        $this->validateUnitTestContext();
        
        if(!$this->is(["GET", "POST"])){
            $this->unitTestInput = $unitTestInput;
        }
        
        return $this;
    }
    
    /**
     * 
     * @param string $method
     * 
     * @throws OpenGateException
     * 
     * @return \lib\inner\arrangements\http\Request
     */
    public function arrangeUnitTestMethod(string $method){
        $this->validateUnitTestContext();
        
        $_SERVER["REQUEST_METHOD"] = strtoupper($method);
        
        return $this;
    }
    
    /**
     * 
     * @param string $uri
     * 
     * @throws OpenGateException
     * 
     * @return \lib\inner\arrangements\http\Request
     */
    public function arrangeRequestURI(string $uri){
        $this->validateUnitTestContext();
        
        if(!StringUtil::startsWith($uri, "/")){
            throw new MalformedUriException("uri must start with /");
        }
        
        $_SERVER["REQUEST_URI"] = $uri;
        
        $this->arrangeUnitTestGet();
        
        return $this;
    }
    
    /**
     * 
     * @param array $postData
     * 
     * @throws OpenGateException
     * 
     * @return \lib\inner\arrangements\http\Request
     */
    public function arrangeUnitTestPost(array $postData){
        $this->validateUnitTestContext();
        
        $_POST = $postData;
        
        return $this;
    }
    
    /**
     *
     * @param array $getData
     * 
     * @throws OpenGateException
     *
     * @return \lib\inner\arrangements\http\Request
     */
    protected function arrangeUnitTestGet(){
        $this->validateUnitTestContext();
        
        $url = $this->server("REQUEST_URI");
        
        // Prepend a dummy scheme if the URL doesn't contain one
        if(strpos($url, '://') === false){
            $url = 'http://' . ltrim($url, '/'); // Add a scheme and clean the leading '/'
        }
        
        $parsedUrl = collect(parse_url($url));
        
        $queryParams = [];
        
        $_SERVER["QUERY_STRING"] = $getData = $parsedUrl->get("query");
        
        if(!is_null($getData)){
            parse_str($getData, $queryParams);
            
            $_GET = $queryParams;
        }
    }
}

