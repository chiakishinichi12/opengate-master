<?php
namespace lib\util\validator;

use DateTime;
use InvalidArgumentException;
use ReflectionClass;
use ReflectionMethod;
use lib\inner\App;
use lib\inner\WebAppCore;
use lib\util\EncapsulationHelper;
use lib\util\ReflectionUtil;
use lib\util\annotation\Annotation;
use lib\util\annotation\AnnotationUtil;
use lib\util\exceptions\MethodNotFoundException;
use lib\util\nebula\ValueContainerException;
use lib\inner\json\JsonUtility;

class Validator {
    
    /**
     *
     * @var array
     */
    protected $params;
    
    /**
     *
     * @var Verifiable
     */
    protected $verifiable;
    
    /**
     *
     * @var array
     */
    protected $invalid = [];
    
    /**
     *
     * @var EncapsulationHelper
     */
    protected $encapsulation;
    
    /**
     *
     * @var WebAppCore
     */
    protected $core;
        
    /**
     *
     * @param array $params
     */
    public function __construct(){
        $this->core = App::make(WebAppCore::class);
        $this->encapsulation = App::make(EncapsulationHelper::class)->instance($this);
        
        if(method_exists($this, "configure")){
            App::call([$this, "configure"]);
        }
    }
    
    /**
     *
     * @Ignore
     *
     * @param array $params
     * @return mixed|object
     */
    public function params(array $params = null){
        return $this->encapsulation->propertyDefinition($this->{__FUNCTION__}, $params);
    }
    
    /**
     *
     * @Ignore
     *
     * @param Verifiable $verifiable
     *
     * @return \lib\inner\arrangements\http\HttpValidator
     */
    public function verifiable(Verifiable $verifiable = null){
        return $this->encapsulation->propertyDefinition($this->{__FUNCTION__}, $verifiable);
    }
    
    /**
     *
     * @Ignore
     *
     * @return \lib\inner\arrangements\http\HttpValidator
     */
    public function validate(){
        foreach($this->params() as $param => $validation){
            $value = $this->verifiable->verify($param);
            
            $this->evaluate($param, $value, $validation);
        }
        
        return $this;
    }
    
    /**
     *
     * @Ignore
     *
     * @return array
     */
    public function failures(){
        return $this->invalid;
    }
    
    /**
     *
     * @Ignore
     *
     * @return number
     */
    public function isIntercepted(){
        return count($this->failures());
    }
    
    /**
     * 
     * @return \lib\util\Collections
     */
    protected function declaredMethods(){
        return ReflectionUtil::getDeclaredMethods(
            new ReflectionClass($this),
            ReflectionMethod::IS_PROTECTED, true);
    }
    
    /**
     *
     * @Ignore
     *
     * @param string $func
     * @return boolean
     */
    protected function isIgnoringFunc($func){
        $findFunc = $this->declaredMethods()->binarySearch($func)
        ->toCompare(fn($method) => $method->getName())
        ->get();
        
        if(!is_blank($findFunc)){
            return AnnotationUtil::hasAnnotation($findFunc->value(), "Ignore");
        }
        
        return false;
    }
    
    /**
     * 
     * @param string $func
     * @return string
     */
    protected function getAnnotatedMessage(string $func){
        $findFunc = $this->declaredMethods()->binarySearch($func)
            ->toCompare(fn($method) => $method->getName())
            ->get();
        
        $properties = AnnotationUtil::loadAnnotation($findFunc->value(), 
            (new Annotation("Message"))->setProperty(["phrase", "string", true]));
        
        if(!is_null($properties)){
            return $properties->get("phrase");
        }
        
        ends:
        return "{$func} has invalidation";
    }
    
    /**
     *
     * @Ignore
     *
     * @param string $param
     * @param mixed $value
     * @param string $validation
     * @return \lib\util\Collections
     */
    protected function evaluate($param, $value, $validation){
        $tocheck = preg_split('/\|(?![^()]*\))/', $validation);
        
        foreach($tocheck as $slug){
            if(preg_match("/([a-zA-Z]+):(.*)?$/", $slug)){
                $this->validateParametric($slug, $param, $value);
            }else{
                $this->validateInput($slug, $param, $value);
            }
        }
    }
    
    /**
     *
     * @Ignore
     *
     * @param string $slug
     * @param string $param
     * @param mixed $value
     */
    protected function validateParametric($slug, $param, $value){
        $parameters = explode(":", $slug);
        
        $func = $parameters[0];
        $expected = $parameters[1];
        
        if(!method_exists($this, $func)){
            throw new MethodNotFoundException("Unknown parametric validation '{$func}()'");
        }
        
        if($this->isIgnoringFunc($func)){
            throw new InvalidArgumentException("'{$func}()' cannot be used as validator");
        }
        
        $this->$func($expected, $param, $value);
    }
    
    /**
     *
     * @Ignore
     *
     * @param string $func
     * @param string $param
     * @param mixed $value
     */
    protected function validateInput($func, $param, $value){
        if(!method_exists($this, $func)){
            throw new MethodNotFoundException("Unknown validation '{$func}'");
        }
        
        if($this->isIgnoringFunc($func)){
            throw new InvalidArgumentException("'{$func}()' cannot be used as validator");
        }
        
        $this->$func($param, $value);
    }
    
    /**
     *
     * @Ignore
     *
     * @param string $param
     * @param string $func
     */
    protected function appendFailure($param, $func){
        $this->invalid[$param][$func] = $this->getAnnotatedMessage($func);
    }
    
    /**
     *
     * @Ignore
     *
     * @param string $param
     * @param string $func
     * @param mixed ...$params
     */
    protected function appendFormattedFailure($param, $func, ...$params){
        $this->invalid[$param][$func] = sprintf($this->getAnnotatedMessage($func), ...$params);
    }
    
    /**
     *
     * @Message("Please fill out this field")
     * 
     * @param string $param
     * @param mixed $value
     */
    protected function required($param, $value){         
        if(is_blank($value)){
            $this->appendFailure($param, __FUNCTION__);
        }
    }
    
    /**
     *
     * @Message("The format is invalid")
     *
     * @param string $slug
     * @param string $param
     * @param mixed $value
     */
    protected function regex($pattern, $param, $value){
        // checks if a validator is a regex
        if(is_blank($pattern) || @preg_match("/{$pattern}/", "") === false){
            throw new InvalidArgumentException("Argument#1 is not a RegEx: /{$pattern}/");
        }
        
        if(!preg_match("/{$pattern}/", $value)){
            $this->appendFailure($param, __FUNCTION__);
        }
    }
    
    /**
     *
     * @Message("Minimum length of characters must be %d")
     *
     * @param int $expectedLength
     * @param string $param
     * @param string $value
     */
    protected function min($expectedLength, $param, $value){
        if(!is_numeric($expectedLength)){
            throw new InvalidArgumentException(__FUNCTION__.": Argument#1 holds a non-numeric value");
        }
        
        $strlen = strlen($value);
        
        if($expectedLength > $strlen){
            $this->appendFormattedFailure($param, __FUNCTION__, $expectedLength);
        }
    }
    
    /**
     *
     * @Message("Maximum length (%d) of characters exceeded")
     *
     * @param int $expectedLength
     * @param string $param
     * @param string $value
     */
    protected function max($expectedLength, $param, $value){
        if(!is_numeric($expectedLength)){
            throw new ValueContainerException(__FUNCTION__.": \$expectedLength holds a non-numeric value");
        }
        
        $strlen = strlen($value);
        
        if($expectedLength < $strlen){
            $this->appendFormattedFailure($param, __FUNCTION__, $expectedLength);
        }
    }
    
    /**
     *
     * @Message("Invalid URL")
     *
     * @param string $param
     * @param mixed $value
     */
    protected function url($param, $value){
        if(filter_var($value, FILTER_VALIDATE_URL) === false){
            $this->appendFailure($param, __FUNCTION__);
        }
    }
    
    /**
     *
     * @Message("Invalid IP Address")
     *
     * @param string $param
     * @param mixed $value
     */
    protected function ipAddress($param, $value){
        if(filter_var($value, FILTER_VALIDATE_IP) === false){
            $this->appendFailure($param, __FUNCTION__);
        }
    }
    
    /**
     *
     * @Message("Alphabet only")
     *
     * @param string $param
     * @param mixed $value
     */
    protected function alpha($param, $value){
        if(!ctype_alpha($value)){
            $this->appendFailure($param, __FUNCTION__);
        }
    }
    
    /**
     * 
     * @Message("This field must be alpha-numeric")
     *
     * @param string $param
     * @param mixed $value
     */
    protected function alphaNumeric($param, $value){
        if(!ctype_alnum($value)){
            $this->appendFailure($param, __FUNCTION__);
        }
    }
    
    /**
     *
     * @Message("Invalid email address")
     *
     * @param string $param
     * @param mixed $value
     */
    protected function email($param, $value){
        if(filter_var($value, FILTER_VALIDATE_EMAIL) === false){
            $this->appendFailure($param, __FUNCTION__);
        }
    }
    
    /**
     *
     * @Message("Characters must be numeric")
     *
     * @param string $param
     * @param mixed $value
     */
    protected function numeric($param, $value){
        if(!is_blank($value) && !is_numeric($value)){
            $this->appendFailure($param, __FUNCTION__);
        }
    }
    
    /**
     *
     * @Message("Invalid date format")
     *
     * @param string $param
     * @param mixed $value
     */
    protected function dateFormat($format, $param, $value){
        if(!DateTime::createFromFormat($format, $value)){
            $this->appendFailure($param, __FUNCTION__);
        }
    }
    
    /**
     * 
     * @Ignore
     * 
     * @return string
     */
    public function __toString(){
        return JsonUtility::toEncode($this->invalid)->encoded();
    }
}

