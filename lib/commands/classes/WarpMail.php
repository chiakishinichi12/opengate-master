<?php
namespace lib\commands\classes;

use lib\commands\CommonMethods;
use lib\instances\CommandInstance;
use lib\util\cli\WarpArgv;
use lib\inner\App;
use lib\mail\warper\DispatchableClassCreation;
use lib\mail\warper\DispatchableList;
use lib\mail\warper\MailSender;

/**
 *
 * @Commands(["mail", "mailer"])
 *
 */
class WarpMail extends CommandInstance {
    
    use CommonMethods;
    
    protected $name = "mailer";
    
    protected $desc = "OpenGate Mailer Feature";
    
    /**
     *
     * @Argument("creates mailer class. [option(s): { --subject=mail_subject }]")
     *
     */
    public function create(){
        App::make(DispatchableClassCreation::class)->write();
    }
    
    /**
     *
     * @Argument("show mailer class list.")
     *
     */
    public function list(){
        App::make(DispatchableList::class)->showList();
    }
    
    /**
     *
     * @Argument("sends email to the recipient using a Dispatchable class. [option(s): --recipient=email --dispatchable=classname]")
     *
     */
    public function send(){
        exit(App::make(MailSender::class)->dispatch());
    }
    
    /**
     * 
     * @param WarpArgv $argv
     */
    public function listen(WarpArgv $argv){
        $this->execution($argv);
    }
}

