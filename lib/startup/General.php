<?php
namespace lib\startup;

use lib\inner\ApplicationGate;
use lib\inner\Locale;
use lib\inner\reader\ReaderGate;
use lib\mocks\MockeryWarper;
use lib\util\html\HTMLUtil;
use lib\util\model\TableInfoContainer;
use lib\util\nebula\Translation;
use lib\util\nebula\ValuesConfig;
use lib\inner\arrangements\GateKeepDefiner;
use lib\util\datagate\Revealer;
use lib\inner\arrangements\ContextualComponent;

class General{
    
    /**
     * 
     * @param ApplicationGate $gate
     */
    public function bootstrap(ApplicationGate $gate){
        $gate->singleton(HTMLUtil::class);
        $gate->singleton(Translation::class);
        $gate->singleton(ValuesConfig::class);
        $gate->singleton(MockeryWarper::class);
        $gate->singleton(ReaderGate::class);
        $gate->singleton(Revealer::class);
        $gate->singleton(GateKeepDefiner::class);
        $gate->singleton(TableInfoContainer::class);
        $gate->singleton(ContextualComponent::class);
        
        $setup = preferences("setup");
                
        /*
         * 
         * setting application default locale 
         * 
         */
        Locale::set($setup->get("locale"));
        
        // Compliance: Check if the current PHP version is less than 8.0.0
        if(!version_compare(PHP_VERSION, '8.0.0', '>=')) {
            // If the version is older than 8, output an error message and terminate the script
            echo "[OPENGATE ERROR]: PHP version 8 or newer is required. "
                ."The current version in use is ".PHP_VERSION.".";
            
            // Exit with a status code of 1 to indicate an error
            exit(1);
        }
    }
}