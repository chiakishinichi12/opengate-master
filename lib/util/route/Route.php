<?php

namespace lib\util\route;

use lib\inner\Accessor;

/**
 * 
 * @author antonio
 *
 * @method static \lib\util\route\RouteBean get(string $uri, mixed $query)
 * @method static \lib\util\route\RouteBean post(string $uri, mixed $query)
 * @method static \lib\util\route\RouteBean put(string $uri, mixed $query)
 * @method static \lib\util\route\RouteBean patch(string $uri, mixed $query)
 * @method static \lib\util\route\RouteBean delete(string $uri, mixed $query)
 * @method static \lib\util\route\RouteBean responseInstance(string $uri, mixed $query)
 * @method static void subdomain(array $hosts, Closure $callback)
 * @method static void release()
 * @method static boolean isValidMethod(string $method)
 */
class Route extends Accessor{
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\inner\Accessor::warpedInstance()
     */
    public function warpedInstance(){
        return RouteGate::class;
    }
}
