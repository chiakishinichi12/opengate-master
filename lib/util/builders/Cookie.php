<?php
namespace lib\util\builders;

use lib\util\exceptions\CookieSettingException;

class Cookie {
    
    /**
     * 
     * @var string
     */
    protected $name = null;
    
    /**
     * 
     * @var mixed
     */
    protected $value = null;
    
    /**
     *
     * @var int
     */
    protected $expire;
    
    /**
     *
     * @var string
     */
    protected $path = "/";
    
    /**
     * 
     * @var string
     */
    protected $domain = "";
    
    /**
     * 
     * @var boolean
     */
    protected $secure = false;
    
    /**
     * 
     * @var boolean
     */
    protected $httpOnly = false;
    
    /**
     * 
     * @var string
     */
    protected $samesite = "";
    
    /**
     * 
     * @param string $name
     */
    public function __construct($name){
        $this->name = $name;
        
        // default value for expire property
        $this->expire = time() + (7 * 24 * 60 * 60);
    }
    
    /**
     * 
     * returns the name of a cookie
     * 
     * @return string
     */
    public function name(){
        return $this->name;
    }
    
    /**
     * required
     * 
     * @param mixed $value
     * @return mixed|\lib\util\builders\Cookie
     */
    public function value($value = null){
        if($value == null){
            return $this->value;
        }
        
        $this->value = $value;
        
        return $this;
    }
    
    /**
     * optional
     * 
     * @param int $expire
     * @return \lib\util\builders\Cookie
     */
    public function expires($expire){
        $this->expire = $expire;
        
        return $this;
    }
    
    /**
     * optional
     * 
     * @param string $path
     * @return \lib\util\builders\Cookie
     */
    public function path($path){
        $this->path = $path;
        
        return $this;
    }
    
    /**
     * optional
     *
     * @param string $domain
     * @return \lib\util\builders\Cookie
     */
    public function domain($domain){
        $this->domain = $domain;
        
        return $this;
    }
    
    /**
     * optional
     *
     * @param boolean $secure
     * @return \lib\util\builders\Cookie
     */
    public function secure($secure){
        $this->secure = $secure;
        
        return $this;
    }
    
    /**
     * optional
     *
     * @param boolean $httpOnly
     * @return \lib\util\builders\Cookie
     */
    public function httpOnly($httpOnly){
        $this->httpOnly = $httpOnly;
        
        return $this;
    }
    
    /**
     * optional
     * 
     * @param string $samesite
     * @return \lib\util\builders\Cookie
     */
    public function samesite($samesite){
        $this->samesite = $samesite;
        
        return $this;
    }
    
    /**
     * 
     * @return string[]|number[]|boolean[]|mixed[]
     */
    public function cookieParamsValueArray(){
        $this->validateRequiredValues();
        
        return [
            "expires" => $this->expire,
            "path" => $this->path,
            "domain" => $this->domain,
            "secure" => $this->secure,
            "httponly" => $this->httpOnly,
            "samesite" => $this->samesite
        ];
    }
    
    /**
     * 
     * @throws CookieSettingException
     */
    protected function validateRequiredValues(){
        $requiredValues = [
            "name" => $this->name,
            "value" => $this->value
        ];
        
        collect($requiredValues)->each(function($item, $index){
            if(is_null($item)){
                throw new CookieSettingException("'{$index}' must not be null");
            }
        });
    }
}