<?php
namespace lib\util\middleware\warper;

use lib\traits\WarperCommons;
use lib\util\Collections;
use lib\util\cli\LongOption;
use lib\util\cli\WarpArgv;
use lib\util\file\FileUtil;

class MiddlewareClassCreation{
    
    use WarperCommons;
    
    /**
     *
     * @var string
     */
    protected $warping = "Middleware";
    
    /**
     *
     * @var Collections
     */
    protected $args;
    
    /**
     *
     * @var LongOption
     */
    protected $primary;
    
    /**
     *
     * @var string
     */
    protected $middlewares;
    
    /**
     *
     * @var string
     */
    protected $prototype;
    
    /**
     *
     * @param WarpArgv $argv
     */
    public function __construct(WarpArgv $argv){
        $this->args = $argv->obtainArguments();
        
        $this->primary = $argv->findLongOption("--defined");
        
        $this->middlewares = preferences("structure")->get("middlewares");
        
        $this->prototype = base_path("lib/util/middleware/prototype");
    }
    
    /**
     *
     * @return \lib\util\Collections
     */
    protected function template(){
        $basetemplate = "MiddlewareDefault.tmp";
        
        if(!is_null($this->primary)){
            $basetemplate = "MiddlewareDefined.tmp";
        }
        
        return FileUtil::getContents("{$this->prototype}/{$basetemplate}", true);
    }
    
    public function write(){
        if($this->args->count() < 2){
            return;
        }
        
        $template = $this->template();
        
        $middleware_name = $this->args->get(1);
        
        $this->alterAndCreate($middleware_name, $this->middlewares);
        
        $toFind = collect([
            [
                "find" => "[Middleware_Class]",
                "replace" => $middleware_name
            ],
            [
                "find" => "[Middleware_Namespace]",
                "replace" => str_replace("/", "\\", relative_path($this->middlewares))
            ]
        ]);
        
        $this->writeWarpedScript($template,
            $toFind,
            $this->middlewares,
            $middleware_name);
    }
}

