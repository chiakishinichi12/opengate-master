<?php
/**
 *
 * @author Taylor Otwell
 *
 */

$uri = urldecode(parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH) ?? '');

/**
 *
 * emulation of apache's mod_rewrite functionality
 *
 */

if($uri !== "/" && @file_exists($resource = __DIR__."/public{$uri}")){
    $finfo = finfo_open(FILEINFO_MIME_TYPE);
    $mime_type = finfo_file($finfo, $resource);
    
    // for dependencies and flexible image resource (such as .svg)
    $dependencies = [
        "css" => "text/css",
        "js" => "text/javascript",
        "svg" => "image/svg+xml"
    ];
    
    $extension = substr($resource, strripos($resource, ".") + 1);
    
    if(in_array($extension, array_keys($dependencies))){
        $mime_type = $dependencies[$extension];
    }
    
    finfo_close($finfo);
    
    ob_clean();
    header("Content-Type: {$mime_type}");
    header("Content-Disposition: inline; filename=\"".basename($resource)."\"");
    
    readfile($resource);
}else{
    require_once __DIR__."/public/index.php";
}