<?php
namespace lib\util\migration\warper;

use lib\inner\ApplicationGate;
use lib\util\GateFinder;
use lib\util\exceptions\MigrationException;
use lib\util\file\FileUtil;
use lib\util\migration\Migration;
use lib\inner\App;

class MigrationList{
    
    /**
     * 
     * @var ApplicationGate
     */
    protected $gate;
    
    public function __construct(ApplicationGate $gate){
        $this->gate = $gate;    
    }
    
    public function showList(){
        out()->println("Established Migration Classes:\n");
        
        $failedCallback = function(){
            out()->println("<error>No migration class found.</error>");
            die();
        };
        
        $migpath = preferences("structure")->get("migrations");
        
        if(!FileUtil::exists($migpath)){
            $this->gate->call($failedCallback);
        }
        
        $migrations = collect();
        
        (new GateFinder(relative_path($migpath), true))->failedCallback($failedCallback)
            ->load(function($class) use ($migrations){
            if(!App::checkInheritance($class, Migration::class)){
                throw new MigrationException("Class {{$class}} is not an instance of ".Migration::class);
            }
            
            $migration = $this->gate->make($class);
                
            if($migration->isAnnotated()){
                $migrations->prepend($migration);
            }else{
                $migrations->append($migration);
            }
        });
            
        $migrations->each(function($migration){
            $class = get_class($migration);
            
            if($migration->isAnnotated()){
                out()->print("{$class} ");
                
                if($migration->isSynced()){
                    out()->println("<info>[Synced]</info>");
                }else{
                    out()->println("<warning>[Unsynced]</warning>");
                }
            }else{
                out()->println("<magenta>{$class}</magenta>");
            }
        });
    }
}