<?php

namespace lib\traits;

use lib\exceptions\OpenGateException;
use lib\inner\App;
use lib\util\StringUtil;
use lib\util\logger\EventLogger;
use lib\inner\json\JsonUtility;

trait Security {
    
    /**
     *
     * @param string $type
     */
    protected function onSysCheck($type){
        $this->instanceType($type);
        
        if(App::checkCLI() && !App::isRunningUnitTest()){
            return;
        }
        
        $security = collect(config("security"));
        
        if(!$security->count()){
            return;
        }
        
        if($maintenance = $security->get("maintenance_mode")){
            if(is_array($maintenance)){
                $maintenance = collect($maintenance);
                
                if($maintenance->get("active")){
                    $this->triggerMaintenanceMode($maintenance, $type);
                }
            }
        }
        
        if($fhttps = $security->get("force-https")){
            if(is_array($fhttps)){
                $fhttps = collect($fhttps);
                
                switch($type){
                    case "page":
                        $this->enforeHttpsRedirectionOnPages($fhttps);
                        break;
                    case "api":
                        $this->restrictNonHttpsURL($fhttps);
                        break;
                }
            }
        }
        
        if($restrict = $security->get("restrict-uri")){
            if(is_array($restrict)){
                $restrict = collect($restrict);
                $this->forbidSpecifiedURI($restrict);
            }
        }
        
        if($csrfToken = $security->get("csrf-token")){
            if(is_array($csrfToken)){
                $csrfToken = collect($csrfToken);
                $this->implementCSRFProtection($csrfToken, $type);
            }
        }
        
        $this->storePreviousUrl($type);
        
        if($type === "cli"){
            throw new OpenGateException("Using CLI instance on the web is discouraged.");
        }
    }
    
    /**
     * 
     * @param string $type
     */
    protected function storePreviousUrl(string $type){
        if(strcmp($type, "page") !== 0){
            return;
        }
        
        $request = request();
        
        $currentUrl = $request->getProtocolUsed()
            .$request->server("HTTP_HOST")
            .$request->server("REQUEST_URI");
        
        // setting previous url
        if(!session("og.previous.url")->active()){
            session("og.previous.url", $currentUrl);
        }else{            
            session("og.previous.url", session("og.current.url")->value());
        }
        
        // setting current url
        session("og.current.url", $currentUrl);
    }
    
    /**
     * 
     * @param array $csrfToken
     * @param string $type
     */
    protected function implementCSRFProtection($csrfToken, $type){         
        if(App::checkCLI() && !App::isRunningUnitTest()){
            return;
        }
        
        if($csrfToken->get("enabled") && $type === "page"){
            $this->defineCsrfTokenCookie($csrfToken);
            
            if($exemption = $csrfToken->get("exemption")){
                if(is_array($exemption)){
                    if($this->ignoreExemptedUri($exemption)){
                        return;
                    }
                }
            }
            
            $request = request();
            
            if(!$request->is("GET")){
                if($request->is("POST")){
                    $retrieve = strtolower($request->method());
                }else{
                    $retrieve = "input";
                }
                
                $failed = true;
                
                $message = $csrfToken->get("if-missing-say") ?: 
                    "Please provide CSRF Tokens to proceed!";
                
                $requestToken = $request->$retrieve("csrf-token");
                
                if(!is_blank($requestToken)){                    
                    $comparison = strcmp($requestToken, csrf_token());
                    
                    if($comparison === 0){
                        $failed = false;
                    }else{
                        $message = "Unable to process your request.";
                    }
                }
                                
                if($failed){
                    response(406)->errorTemplate($message);
                }
            }
        }
    }
    
    /**
     *
     * @param array $fhttps
     */
    protected function enforeHttpsRedirectionOnPages($fhttps){
        if(App::checkCLI()){
            return;
        }
        
        if($fhttps->get("pages")){
            if(!App::checkSSLPresenceOnURL()){
                $host = request()->server("HTTP_HOST");
                $rURI = request()->server("REQUEST_URI");
                
                response()->redirect("https://{$host}{$rURI}")->send();
            }
        }
    }
    
    /**
     *
     * @param array $fhttps
     */
    protected function restrictNonHttpsURL($fhttps){
        if(App::checkCLI()){
            return;
        }
        
        if($fhttps->get("api")){
            if(!App::checkSSLPresenceOnURL()){
                response(406)
                ->setHeaders(["Content-type" => "text/plain"])
                ->halts("SSL Missing");
            }
        }
    }
    
    /**
     *
     * @param array $exempted
     */
    protected function ignoreExemptedUri($exempted){
        if(App::checkCLI()){
            return;
        }
        
        $exempted = collect($exempted);
        
        $requestUri = request()->server("REQUEST_URI");
        
        foreach($exempted->get("uris") as $uri){
            if($this->findURIMatches($uri, $requestUri)){
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * 
     * checks if there something matched to the current URI
     * 
     * @param string $uri
     * @param string $subject
     * 
     * @return boolean
     */
    protected function findURIMatches($uri, $subject){
        if(StringUtil::endsWith($uri, "/*")){
            $lastIndex = strripos($uri, "/*");
            $uri = substr($uri, 0, $lastIndex);
            
            $comparison = StringUtil::startsWith($subject, $uri);
        }else{
            $comparison = strcmp($uri, $subject) === 0;
        }
        
        return $comparison;
    }
    
    /**
     *
     * @param array $security
     */
    protected function forbidSpecifiedURI($restrict){
        if(App::checkCLI() && !App::isRunningUnitTest()){
            return;
        }
        
        $action = $restrict->get("action") ?: "halt";
        $ifRedirect = $restrict->get("if-redirect");
        $requestUri = request()->server("REQUEST_URI");
        $remoteAddress = request()->server("REMOTE_ADDR");
        $ipAddresses = collect($restrict["exemption"]["ip_addresses"]);
        
        if(is_blank($ifRedirect)){
            $ifRedirect = base_url();
        }
        
        foreach($restrict->get("uris") as $uri){
            if($this->findURIMatches($uri, $requestUri)){        
                if($ipAddresses->has($remoteAddress)){
                    continue;
                }
                
                if($action === "redirect"){
                    response()->redirect($ifRedirect)->send();
                }else if($action === "halt"){
                    response(403)->errorTemplate("The request URI is prohibited from any access.");
                }else{
                    throw new OpenGateException("Unknown Rectrict-URI action: {$action}");   
                }
            }
        }
    }
    
    /**
     * 
     * @param array $mode
     * @param string $type
     */
    protected function triggerMaintenanceMode($mode, $type){        
        if(App::checkCLI() && !App::isRunningUnitTest()){
            return;
        }
        
        if($exemption = collect($mode->get("exemption"))){
            $ipAddresses = collect($exemption->get("ip_addresses"));
                        
            if($ipAddresses->has(request()->server("REMOTE_ADDR"))){
                return;
            }
            
            $responseType = $exemption->get("response_type");
            
            if(!is_blank($responseType) && strcmp($responseType, $type) === 0){
                return;
            }
        }
        
        response(503)->errorTemplate("Maintenance", $mode->get("page"));
    }
    
    /**
     * 
     * checks whats getting inside the http request
     * 
     */
    public function validateRequest(){
        if(App::checkCLI() && !App::isRunningUnitTest()){ 
            return;
        }
        
        $invalid = collect();
        $request = request();
        $responseConfig = collect($this->responseConfig);
        
        if($responseMethod = $responseConfig->get("method")){
            if(strcasecmp($responseMethod, "any") === 0){
                goto proceed;
            }
            
            if(strcasecmp($request->method(), $responseMethod) !== 0){
                $invalid->add("Method not allowed: {{$request->method()}}");
            }
        }
        
        proceed:
        if($responseConfig->get("readonly")){
            $notallowed = false;
            
            if($request->is("get")){
                goto ends;
            }
            
            switch(strtolower($request->method())){
                case "post":
                    if($request->post()->length()){
                        $notallowed = true;
                    }
                    
                    break;
                default:
                    $input = $request->input();
                    
                    if(App::checkJSONString($input)){                        
                        if(JsonUtility::parse($input)->length()){
                            $notallowed = true;
                        }
                    }else if(strlen(trim($input)) !== 0){
                        $notallowed = true;
                    }
                    
                    break;
            }
            
            ends:
            if($notallowed){
                $invalid->add("The resource is a readonly.");
            }
        }
        
        if($invalid->count()){
            $remarks = $invalid->toJSONString();
            
            App::make(EventLogger::class)->log($remarks, LOG_ERR);
            
            response(406)->halts($remarks);
        }
    }
}