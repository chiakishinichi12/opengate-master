<?php
namespace lib\inner;

use lib\util\controller\Controller;
use lib\exceptions\OpenGateException;

trait SessionVolatileBinder {
    
    /**
     * Binds volatile session data to a controller instance.
     * 
     * @param string $session
     * @param string $varname
     * 
     * @return boolean
     */
    protected function bindVolatileData(string $flash, string $varname = "flash"){
        if(!App::checkInheritance($this, [Controller::class])){
            throw new OpenGateException("Binding flash data is only allowed on ".Controller::class." instance");
        }
        
        return $this->bindIfDefined($varname, $this->accessVolatile($flash));
    }
    
    
    
    /**
     * 
     * retrieve flash data stored on session storage
     * 
     * @param string $session
     * @return mixed|null
     */
    protected function accessVolatile(string $session){
        if(($flash = session($session))->active()){
            return $flash->value();
        }
        
        return null;
    }
}

