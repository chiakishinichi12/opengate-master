<?php
namespace lib\util\vroller;

use lib\inner\App;
use lib\util\ValuesRoller;
use lib\util\exceptions\ClassNotFoundException;
use lib\util\exceptions\MethodNotFoundException;
use lib\util\exceptions\ValueRollingException;

class RollerGate {
    
    /**
     * 
     * @param string $vrcls
     * @return mixed
     */
    public static function valueOf(string $vrcls) : mixed{
        if(!class_exists($vrcls)){
            throw new ClassNotFoundException("Class '{$vrcls}' doesn't exist");
        }
        
        $vroller = App::make($vrcls);
        
        if(!App::checkInheritance($vroller, ValuesRoller::class)){
            throw new ValueRollingException("a class is not a ValueRoller instance {{$vrcls}}");
        }
        
        $queried = [$vroller, "getRolledValue"];
        
        if(!method_exists(...$queried)){
            throw new MethodNotFoundException("ValueRoller issue: getRolledValue() must be defined within the roller inherited class.");
        }
        
        return App::call($queried);
    }
}

