<!DOCTYPE html>
<html lang="{{ \lib\inner\Locale::get() }}">
	<head>
		<title>Maintenance Mode</title>
	</head>
	<body>
		<p>
			Sorry! The System is on Maintenance mode right now.<br/>
			We'll get back to you shortly.
		</p>
	</body>
</html>