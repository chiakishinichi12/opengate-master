<?php
namespace lib\util\exceptions;

use lib\exceptions\OpenGateException;

class UnknownPropertyException extends OpenGateException{
    
}