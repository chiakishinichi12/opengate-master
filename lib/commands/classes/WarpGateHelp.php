<?php

namespace lib\commands\classes;

use lib\inner\App;
use lib\instances\CommandInstance;
use lib\util\cli\CommandContainer;
use lib\util\cli\CommandBean;

/**
 * 
 * @Commands(["help", "h"])
 *
 */
class WarpGateHelp extends CommandInstance {
    
    /**
     * 
     * @var string
     */
    protected $name = "help";
    
    /**
     * 
     * @var string
     */
    protected $desc = "will show all available commands on warpgate";
    
    /**
     * 
     * evaluates if a command is exempted from live mode restriction
     * 
     * @param CommandBean $command
     * @return boolean
     */
    protected function exempted(CommandBean $command){
        if(!App::isRunningOnLiveMode()){
            return true;
        }
        
        $commands = $command->command();
        
        if(is_string($commands)){
            $commands = [$commands];
        }
        
        sort($commands);
        
        $allowed = config("live_mode_allowed_cmds");
        
        // reading exemptions
        foreach($allowed as $cmd){
            if(is_array($cmd)){
                sort($cmd);
                
                if($cmd === $commands){
                    return true;
                }
            }else if(is_string($cmd) && in_array($cmd, $commands, true)){
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * 
     * iterates all the allowed registered commands
     * 
     */
    public function listen(){
        $commands = App::make(CommandContainer::class)->getCommands();
        
        out()->println("\n====================================");
        out()->println(" <warning>OpenGate (v".OPENGATE_VERSION.")</warning>");
        out()->println("====================================\n");
        
        foreach($commands as $command){
            if(!$this->exempted($command)){
                continue;
            }
            
            $details = collect($command->details());
            
            $command = is_array($command->command()) 
                ? implode(", ", $command->command()) 
                : $command->command();
            
            $cmdname = $details->get("name") ?: "Unnamed";
            $cmddesc = $details->get("desc") ?: "No Description Available";
              
            out()->println("<info>{$cmdname}</info> [<magenta>{$command}</magenta>] {$cmddesc}\n");
        }
    }
}