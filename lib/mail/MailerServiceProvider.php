<?php
namespace lib\mail;

use lib\inner\ApplicationGate;
use lib\inner\arrangements\ServiceProvider;
use lib\mail\engines\PHPMailerEngine;
use lib\util\exceptions\MailEngineResolverException;
use lib\mail\engines\AWSSESEngine;

class MailerServiceProvider extends ServiceProvider{
    
    /**
     * 
     * @var boolean
     */
    protected $forAll = true;
    
    /**
     * 
     * @var array
     */
    protected $engines = [
        "phpmailer" => PHPMailerEngine::class,
        "aws-ses" => AWSSESEngine::class
    ];
    
    /**
     * 
     * @var ApplicationGate
     */
    protected $gate;
    
    /**
     * 
     * @param ApplicationGate $gate
     */
    public function boot(ApplicationGate $gate){
        $this->gate = $gate;
        $this->creatingInstance();
    }
    
    /**
     * 
     * defining mailer instance by the mailer engine
     * configured on the setup preference
     * 
     * @throws MailEngineResolverException
     */
    protected function creatingInstance(){
        $engine = preferences("setup")->get("mailer_engine");
        
        $credentials = preferences("mail")->get($engine);
        
        if(!isset($this->engines[$engine])){
            throw new MailEngineResolverException("Unknown Mailer Engine: {$engine}");
        }
        
        $this->gate->singleton(Engine::class, $this->engines[$engine]);
        
        $this->gate->make(Engine::class)
            ->initMailerEngine($credentials);
    }
}