<?php
namespace lib\util\collections;

use Closure;
use lib\util\Collections;

/**
 * 
 * @author Antonius Aurelius
 *
 */
class BinarySearching {
    
    /**
     * 
     * @var mixed
     */
    protected $search;
    
    /**
     * 
     * @var Collections
     */
    protected $list;
    
    /**
     * 
     * @var Collections
     */
    protected $experiment;
    
    /**
     * 
     * @var Closure
     */
    protected $sorts;
    
    /**
     * 
     * @var Closure
     */
    protected $toCompare;
    
    /**
     * 
     * @param Collections $list
     * @param mixed $search
     * 
     */
    public function __construct(Collections &$list, mixed $search){
        $this->list = $list;
        $this->experiment = clone $list;
        $this->search = $search;
    }
        
    /**
     * invocation of this method will set a modification to entire collection stack.
     * 
     * @param Closure $sorts
     * @return \lib\util\collections\BinarySearching
     */
    public function sorts(Closure $sorts){
        $this->sorts = $sorts;
        
        return $this;
    }
    
    /**
     * 
     * @param Closure $tocompare
     * @return \lib\util\collections\BinarySearching
     */
    public function toCompare(Closure $toCompare){
        $this->toCompare = $toCompare;
        
        return $this;
    }
    
    /**
     * 
     * @throws CollectionTypeException
     * @return \lib\util\collections\CollectionItem|NULL
     */
    public function get(){
        if(is_blank($this->sorts)){
            $temp1 = collect();
            
            $this->experiment->asort($this->toCompare);
            
            $this->experiment->each(function($item, $index) use ($temp1){
                $comparedItem = $this->toCompare
                ? ($this->toCompare)($item) : $item;
                
                $temp1->add([$index, $comparedItem]);
            });
                
            $temp2 = clone $this->experiment;
                
            $this->experiment->clear();
            $this->experiment->liftTypeRestriction();
            $this->experiment->addAll($temp1);
                
            unset($temp1);
                
            $this->toCompare = function($item){
                return $item[1];
            };
        }else{
            // if '$this->sorts' has a closure reference,
            // '$this->experiment' will hold the identity and values of the
            // main item stack. whatever happens to experiment will
            // be reflected to the main item stack right away.
            $this->experiment = $this->list;
            
            $this->experiment->usort($this->sorts);
        }
        
        $start = 0;
        
        $end = $this->experiment->count() - 1;
        
        while($start <= $end){
            $middle = floor(($start + $end) / 2);
            
            if(!is_blank($this->toCompare) 
                && !($this->toCompare instanceof Closure)){
                throw new CollectionTypeException("'toCompare' param must be a Closure type");
            }
            
            $midData = $this->toCompare
                ? ($this->toCompare)($this->experiment->get($middle))
                : $this->experiment->get($middle);
            
            if($midData === $this->search){
                $retrieved = $this->experiment->get($middle);
                
                $definiteKey = $middle;
                
                if(is_blank($this->sorts) && !is_blank($this->toCompare)){
                    $definiteKey = $retrieved[0];
                    $retrieved = $temp2->get($definiteKey);
                    
                    $this->experiment->clear();
                    
                    unset($temp2);
                }
                
                return new CollectionItem($retrieved, $definiteKey, $this->list);
            }else{
                if($midData < $this->search){
                    $start = $middle + 1;
                }else{
                    $end = $middle - 1;
                }
            }
        }
        
        return null;
    }

}