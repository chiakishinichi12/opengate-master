<?php
namespace lib\util;

use lib\inner\Accessor;
use lib\inner\client\RequestProcessor;

/**
 * 
 * @author antonio
 *
 * @method static \lib\inner\client\RequestProcessor url(string $url = null)
 * @method static \lib\inner\client\RequestProcessor data(array $data = null)
 * @method static \lib\inner\client\RequestProcessor logResponse()
 */
class HttpServiceClient extends Accessor{
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\inner\Accessor::warpedInstance()
     */
    public function warpedInstance(){
       return RequestProcessor::class; 
    }
}