<?php

namespace lib\instances;

use ReflectionClass;
use lib\inner\App;
use lib\inner\WebAppCore;
use lib\util\annotation\Annotation;
use lib\util\annotation\AnnotationUtil;

abstract class CommandInstance extends WebAppCore {
        
    /**
     * 
     * command name
     * 
     * @var string
     */
    protected $name;
    
    /**
     * 
     * command description
     * 
     * @var string
     */
    protected $desc;
    
    public function __construct(){
        parent::__construct();
    }
    
    /**
     * 
     * checks if the command is allowed during live mode execution
     * 
     */
    public function inspect(){
        if(!App::checkCLI()){
            return;
        }
        
        if(!$this->isExempted()){
            out()->println("<error>Command cannot be executed on live mode: ["
                .implode(", ", $this->keywords())."]</error>");
            exit(0);
        }
    }
        
    /**
     * 
     * @return array|null
     */
    protected function keywords(){
        $annotation = (new Annotation("Commands"))->setProperty(["keywords", "array", true]);
        
        return AnnotationUtil::loadAnnotation(
            new ReflectionClass($this),
            $annotation)->get("keywords");
    }
    
    /**
     * 
     * checks if the command is exempted from live mode restriction
     * 
     * @return boolean|null
     */
    protected function isExempted(){
        if(!App::isRunningOnLiveMode()){
            return true;
        }
        
        $commands = $this->keywords();
        
        sort($commands);
        
        $allowed = config("live_mode_allowed_cmds");
        
        // reading exemptions
        foreach($allowed as $cmd){
            if(is_array($cmd)){
                sort($cmd);
                
                if($cmd === $commands){
                    return true;
                }
            }else if(is_string($cmd) && in_array($cmd, $commands, true)){
                return true;
            }
        }
        
        return false;
    }
}