<?php
namespace lib\util\model;

use lib\traits\Sanitation;

class NotColumn{
    
    use Sanitation;
    
    /**
     * 
     * @var string
     */
    protected $exp;
    
    /**
     * 
     * @param string $exp
     */
    public function __construct(string $exp){
        $this->exp = $exp;
    }
    
    /**
     * 
     * @return mixed|String
     */
    public function sanitized(){
        return "\"{$this->escape($this->exp)}\"";
    }
    
    /**
     * 
     * @return \lib\util\model\string
     */
    public function __toString(){
        return $this->sanitized();
    }
}

