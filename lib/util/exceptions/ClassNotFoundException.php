<?php
namespace lib\util\exceptions;

use lib\exceptions\OpenGateException;

class ClassNotFoundException extends OpenGateException{
    
}