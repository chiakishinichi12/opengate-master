<?php

namespace lib\util\model;

use lib\traits\Sanitation;
use lib\util\Collections;
use lib\util\datagate\SQLBuilderHelper;
use lib\util\exceptions\ModelException;

class TableInfo {
    
    use Sanitation, SQLBuilderHelper;
    /**
     * 
     * @var string
     */
    protected $table_name;
    
    /**
     *
     * @var string
     */
    protected $table_type;
    
    /**
     * 
     * @var Collections
     */
    protected $columns;
    
    /**
     * 
     * @param string $table_name
     * @param string $dbms
     */
    public function __construct(string $table_name, string $dbms){ 
        $this->dbms = $dbms;
        
        $this->arrangeDefinitionAndColumns($this->fetchTableInfo($table_name));
    }
    
    /**
     * 
     * @param Collections $columns
     */
    protected function arrangeDefinitionAndColumns($info){
        $this->columns = Collections::make([], TableColumn::class);
        
        foreach($info->get("columns") as $column){
            $tc = new TableColumn($this);
            
            foreach($column as $colDetKey => $colDetValue){
                $colDetKey = strtolower($colDetKey);
                $tc->{$colDetKey} = $colDetValue;
            }
            
            $this->columns->add($tc);
        }
        
        if($definition = $info->get("definition")){
            if($definition->length() === 0){
                return;
            }
            
            foreach($definition->get(0) as $key => $val){
                switch(strtolower($key)){
                    case "table_name":
                        $this->table_name = $val;
                        break;
                    case "table_type":
                        $this->table_type = $val;
                        break;
                }
            }
        }
    }
    
    /**
     * 
     * @param string column
     * 
     * @return \lib\util\model\PrimaryKey|NULL
     */
    public function getPrimaryKey($column = null){        
        if(!is_null($column)){
            $find = $this->getColumns()
                ->binarySearch("PRI")
                ->toCompare(function($item){
                    return $item->key;
                })
                ->get();
            
            if(!is_null($find)){
                $find = $find->value();
            }else{
                throw new ModelException("Unknown Primary Key `{$this->table_name}`.`{$column}`", 
                    ModelException::NONEXISTENT_COLUMN);
            }
            
            return new PrimaryKey($find->field, $find->type);
        }
        
        foreach($this->getColumns() as $column){            
            if(!isset($column->key)){
                continue;
            }
            
            if(strcasecmp($column->key, "PRI") === 0){
                return new PrimaryKey($column->field, $column->type);
            }
        }
        
        return null;
    }
    
    /**
     * 
     * @return string
     */
    public function getTableName(){
        return $this->table_name;
    }
    
    /**
     * 
     * @return string
     */
    public function getTableType(){
        return $this->table_type;
    }
    
    /**
     * 
     * @return \lib\util\Collections
     */
    public function getColumns(){
        return $this->columns;
    }
}