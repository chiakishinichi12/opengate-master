<?php
namespace lib\util\model\relational\material;

use lib\inner\App;
use lib\util\exceptions\ClassNotFoundException;
use lib\util\exceptions\MethodNotFoundException;
use lib\util\model\Model;
use lib\util\model\relational\RelationalModelException;
use lib\util\resolver\MethodInvocationResolver;

trait MaterialCommon {
    
    /**
     *
     * @var Model
     */
    protected $model;
    
    /**
     *
     * @var Model
     */
    protected $relation;
    
    /**
     *
     * @var string
     */
    protected $ownerKey;
    
    /**
     *
     * @var string
     */
    protected $foreignKey;
    
    /**
     *
     * @var Model
     */
    protected $retrieved;
    
    /**
     *
     * evaluates instance of queried models
     *
     */
    protected function validate(){
        $models = [
            "model" => $this->ownerModel(),
            "relation" => $this->foreignModel()
        ];
        
        foreach($models as $key => $model){
            if(is_blank($model)){
                throw new RelationalModelException("{$key} model must be defined.");
            }
            
            if(is_string($model)){
                if(!class_exists($model)){
                    throw new ClassNotFoundException("Class doesn't exist {{$model}}");
                }
                $model = new $model;
            }
            
            $this->{$key} = $model;
            
            if(!App::checkInheritance($this->{$key}, Model::class)){
                throw new RelationalModelException("{$this->ownerModel()} is not instance of ".Model::class);
            }
        }
    }
    
    /**
     *
     * evaluates and initializes model relationships
     *
     */
    protected function materialize(){
        $this->validate();
        $this->initialize();
    }
    
    /**
     * 
     * @return boolean
     */
    protected function hasIndividualResultInstance(){
        return App::checkInheritance($this, [BelongsTo::class, HasOne::class]);
    }
    
    /**
     * 
     * @param string $propname
     * @param mixed $value
     */
    public function __set(string $propname, mixed $value){
        if(!$this->hasIndividualResultInstance()){
            return;
        }
        
        $this->retrieveIfNot();
        
        if(!is_null($this->retrieved)){
            $this->retrieved->$propname = $value;
        }
    }
    
    /**
     * 
     * @param string $propname
     * @return mixed|null
     */
    public function __get(string $propname){
        if(!$this->hasIndividualResultInstance()){
            return null;
        }
        
        $this->retrieveIfNot();
        
        if(!is_null($this->retrieved)){
            return $this->retrieved->$propname;
        }
    }
    
    /**
     * 
     * @param string $propname
     * @return boolean
     */
    public function __isset(string $propname){
        if(!$this->hasIndividualResultInstance()){
            return false;
        }
        
        $this->retrieveIfNot();
        
        if(method_exists($this->retrieved, "__isset")){
            return $this->retrieved->__isset($propname);
        }
        
        return false;
    }
    
    /**
     * 
     * retrieves the relational model instance
     * 
     */
    protected function retrieveIfNot(){
        if(is_null($this->retrieved)){
            $this->retrieve();
        }
    }
    
    /**
     *
     * @param string $method
     * @param array $params
     */
    public function __call(string $method, array $params){
        $this->retrieveIfNot();
        
        if(!is_null($this->retrieved)){   
            return App::make(MethodInvocationResolver::class)
            ->parent($this)
            ->instances([$this->retrieved])
            ->toCall($method)
            ->allowMagicCall()
            ->params($params)
            ->resolve()
            ->invoke();
        }
        
        throw new MethodNotFoundException("[".class_name($this)."] Unknown method '$method'");
    }
    
    /**
     * 
     * @return string
     */
    public function __toString(){
        $this->retrieveIfNot();
        
        if(!is_null($this->retrieved) && method_exists($this->retrieved, "__toString")){
            return $this->retrieved->__toString();
        }
        
        return "";
    }
}