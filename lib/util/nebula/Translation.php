<?php
namespace lib\util\nebula;

use lib\inner\client\Http;
use lib\inner\client\HttpClientException;
use lib\traits\ValueLocatorHelper;
use lib\util\StringUtil;
use lib\util\exceptions\LocalizationException;
use lib\util\file\File;
use lib\util\file\FileUtil;
use lib\inner\Locale;
use lib\util\builders\CookieFactory;
use lib\inner\arrangements\http\CookieGate;
use lib\util\PlainEncryptor;
use lib\util\Collections;
use lib\inner\json\JsonUtility;

/**
 *
 * Localization and translation management class.
 *
 * This class is responsible for managing translations and language localization within the OpenGate framework.
 * It provides methods for retrieving localized text and resources, as well as handling fallback languages.
 *
 * @author Jiya Sama
 *
 */
class Translation {
    
    use ValueLocatorHelper;
    
    /**
     *
     * @var string
     */
    protected $lang;
    
    /**
     *
     * @var string
     */
    protected $fallback;
    
    /**
     *
     * @var string
     */
    protected $base;
    
    /**
     * 
     * @var string
     */
    protected $ipLoc;
    
    /**
     * 
     * @var bool
     */
    protected $withCountryCode;
    
    public function __construct(){
        $setup = preferences("setup");
        
        $this->withCountryCode = $setup->get("include_country_code_in_dynamic_locale");
        $this->base = $setup->get("locale_base_path");
        $this->lang = $setup->get("locale");
        $this->fallback = $setup->get("fallback_locale");
    }
    
    /**
     *
     * @param string $lang
     */
    public function setLocale($lang){
        $this->lang = $lang;
    }
    
    /**
     *
     * locates the existing locale file
     *
     * @param string $lang
     * @param string $file
     * @return mixed|NULL
     */
    protected function retrieveFile(string $lang, string $file){
        $path = "{$this->base}/{$lang}/";
        
        // gets the fallback translation if the designated locale file doesn't exist
        if(!FileUtil::exists("{$path}/{$file}.php")){
            $path = "{$this->base}/{$this->fallback}";
        }
        
        return FileUtil::requireIfExists("{$path}/{$file}.php");
    }
    
    /**
     *
     * @param string $lang
     * @param array $explodedPath
     * @param bool $asArray
     *
     * @throws LocalizationException
     * @return mixed|NULL
     */
    protected function retrieveTranslation($lang, $explodedPath, bool $asArray = false){
        $translation = $this->retrieveFile($lang, $explodedPath[0]);
        
        if(is_null($translation)){
            throw new LocalizationException("'{$explodedPath[0]}': locale file doesn't exist");
        }
        
        if(!is_array($translation)){
            throw new LocalizationException("'{$lang}/{$explodedPath[0]}': file must return array");
        }
        
        unset($explodedPath[0]);
        
        $explodedPath = array_values($explodedPath);
        
        $located = $this->recursiveLocate($translation, $explodedPath, 0);
        
        if(is_array($located)){
            if($asArray){
                goto gets;
            }
            
            return null;
        }
        
        gets:
        return $located;
    }
    
    /**
     *
     * Retrieves the available languages from the specified base directory.
     *
     * This function scans the base directory for language files and returns
     * an array containing the basenames of those files, representing the available languages.
     *
     *
     * @return Collections
     */
    public function getAvailableLanguages(){
        $directory = new File($this->base);
        
        $languages = collect();
        
        // will return an empty stack if directory doesn't exist
        if(!$directory->exists()){
            goto finish;
        }
        
        // will return an empty stack if locale file is not a directory
        if(!$directory->isDirectory()){
            goto finish;
        }
        
        // obtains all available locales
        $languages->addAll($directory->listFiles()->map(function(File $file){
            return $file->getBasename();
        }));
        
        finish:
        return $languages;
    }
    
    /**
     *
     * checks if the locale configuration is available.
     * returns null if locale directory doesn't exist or if not a directory
     *
     * @param string $lang
     * 
     * @return boolean|null
     */
    public function isLocaleAvailable($lang){
        $locales = $this->getAvailableLanguages();
        
        if(is_blank($locales)){
            goto nothing;
        }
        
        if($locales === false){
            goto nothing;
        }
        
        foreach($locales as $language){
            if(strcasecmp($language, $lang) === 0){
                return true;
            }
        }
        
        nothing:
        return false;
    }
    
    /**
     *
     * returns current locale
     *
     * @throws LocalizationException
     */
    public function getLocale(){
        $this->throwIfLanguageNotAvailable();
        
        return $this->lang;
    }
    
    /**
     *
     * @param string $transPath
     * @param string $locale
     *
     * @throws LocalizationException
     *
     * @return array
     */
    public function localizedArray(string $transPath, $locale = null){
        $prevlang = $this->getLocale();
        
        if(is_null($locale)){
            $locale = $this->lang;
        }
        
        $this->setLocale($locale);
        
        $this->throwIfLanguageNotAvailable();
        
        $explodedPath = explode(".", $transPath);
        
        $langset = $this->retrieveTranslation($locale, $explodedPath, true);
        
        if(is_null($langset)){
            $langset = $this->retrieveTranslation($this->fallback, $explodedPath, true);
            
            if(is_null($langset)){
                throw new LocalizationException("Translation set for {$transPath} not found");
            }
        }
        
        if(!is_array($langset)){
            $type = class_name($langset);
            
            throw new LocalizationException("Translation set {{$transPath}} is not an array. {$type} found");
        }
        
        /**
         *
         * when the translation is already obtained,
         * the current locale will be reverted back to the configuration
         *
         */
        $this->setLocale($prevlang);
        
        return $langset;
    }
    
    /**
     *
     * @param string $transPath
     * @param array $params
     * @param string $local
     *
     * @return NULL|mixed
     */
    public function localizedTrans(string $transPath, array|string $params = null, string $locale = null){
        if(is_string($params)){
            $locale = $params;
            $params = [];
        }
        
        $prevlang = $this->getLocale();
        
        if(is_null($locale)){
            $locale = $this->lang;
        }
        
        $this->setLocale($locale);
        
        $translation = $this->get($transPath, $params);
        
        /**
         *
         * when the translation is already obtained,
         * the current locale will be reverted back to the configuration
         *
         */
        $this->setLocale($prevlang);
        
        return $translation;
    }
    
    /**
     *
     * @throws LocalizationException
     */
    protected function throwIfLanguageNotAvailable(){
        $path = "{$this->base}/{$this->lang}";
        
        if(!FileUtil::exists($path)){
            throw new LocalizationException("Configuration for the language code '{$this->lang}' is missing");
        }
    }
    
    /**
     *
     * returns the available translation of a text
     * according to the current locale language.
     *
     * @param string $transPath
     * @param array $params
     *
     * @throws LocalizationException
     *
     * @return NULL|mixed
     */
    public function get($transPath, $params = null){
        $this->throwIfLanguageNotAvailable();
        
        $explodedPath = StringUtil::explode(".", $transPath);
        
        if(count($explodedPath) === 0){
            throw new LocalizationException("param \$transpath must be in 'object.name' format");
        }
        
        $value = $this->retrieveTranslation($this->lang, $explodedPath);
        
        // resolving missing translation with a fallback localization
        if(is_null($value)){
            $value = $this->retrieveTranslation($this->fallback, $explodedPath);
            
            if(is_null($value)){
                throw new LocalizationException("Translations for {$transPath} not found");
            }
        }
        
        // type-strict
        if(!is_string($value)){
            $found = class_name($value);
            
            throw new LocalizationException("value of {$transPath} must be a string. {$found} found");
        }
        
        // formatted string parsing
        if(is_array($params)){
            collect($params)->each(function($v, $k) use (&$value){
                $tofind = ":{$k}";
                
                if(StringUtil::contains($value, $tofind)){
                    $value = str_replace($tofind, $v, $value);
                }
            });
        }
        
        return $value;
    }
    
    /**
     * 
     * @return string|null
     */
    public function getBrowserLanguage(){
        $accepted = StringUtil::explode(",", request()->server("HTTP_ACCEPT_LANGUAGE") ?: "");
        
        if(is_blank($accepted)){
            return null;
        }
        
        return $this->extractDynamicLocaleString($accepted[0]);
    }
    
    /**
     * 
     * @param string $subject
     * 
     * @return string
     */
    protected function extractDynamicLocaleString(string $subject){        
        $lastIndexOfDash = strripos($subject, "-");
        
        if($lastIndexOfDash === false){
            $lastIndexOfDash = null;
        }
        
        $languageCode = substr($subject, 0, $lastIndexOfDash);
        
        // if 'withCountryCode' is true, it will include region code after language code
        if($this->withCountryCode){
            // Regular expression to check if the language code contains a hyphen followed by two uppercase letters
            if(preg_match('/^[a-z]{2}-[A-Z]{2}$/', $subject)){
                $languageCode = $subject;
            }
        }
        
        return $languageCode;
    }
    
    /**
     * * Retrieve the ISO language code from the current locale.
     *
     * This method extracts the ISO language code from the locale string. 
     * The locale is expected to be in the format "language-country" (e.g., "en-US").
     * If the locale string does not contain a hyphen, it returns the full locale string.
     * 
     * @return string
     */
    public function iso(){
        $lastIndexOf = strripos(Locale::get(), "-");
        
        if($lastIndexOf === false){
            $lastIndexOf = null;
        }
        
        return substr(Locale::get(), 0, $lastIndexOf);
    }
    
    /**
     * 
     * gets the locale string from browser settings or client IP address
     * 
     * @return string|null
     */
    public function dynamic(){
        $setup = preferences("setup")->get("dynamic_locale");
        
        return Locale::$setup();
    }
    
    /**
     * 
     * @var string
     */
    protected $specialIp;
    
    /**
     * 
     * A special case of retrieving an IP address. if live-mode is disabled, 
     * will be utilizing an API to retrieve current client IP address.
     * 
     * @return mixed|string
     */
    protected function fetchIpAddress(){
        $ipAddress = request()->server("REMOTE_ADDR");
        
        try{
            if(!config("live_mode", false)){
                if(is_null($this->specialIp)){
                    $this->specialIp = Http::get("https://api.ipify.org")->body();
                }
                
                $ipAddress = $this->specialIp;
            }
        }catch(HttpClientException $ex){
            // communication with ipify.org cannot be made
        }
        
        return $ipAddress;
    }
    
    /**
     * 
     * @param string $locale
     */
    protected function ipLocaleCookie(string $locale = null){
        $seckey = "iploc-cookie-factory";
        
        $cookieName = "iplocinfo";
        
        $iplocArray = CookieGate::get($cookieName);
        
        $ipAddress = $this->fetchIpAddress();
         
        $salt = "iv";
        
        if(!is_null($locale)){
            $details = [
                "ipAddress" => $ipAddress,
                "locale" => $locale
            ];
            
            $localeCookie = CookieFactory::create($cookieName)
            ->value(PlainEncryptor::encryptData(JsonUtility::toEncode($details)->encoded(), $seckey, $salt))
            ->path("/");
            
            if(is_null($iplocArray)){
                CookieGate::set($localeCookie);
            }else{
                CookieGate::modify($localeCookie);
            }
            
            $iplocArray = $details;
        }else{
            if(is_null($iplocArray)){
                goto ends;
            }
            
            $iplocArray = JsonUtility::parse(PlainEncryptor::decryptData($iplocArray, $seckey, $salt));
            
            // invalidates the stored ip locale cookie
            if(strcmp($ipAddress, $iplocArray->ipAddress) !== 0){    
                $iplocArray = null;
            }
        }
        
        ends:
        return $iplocArray;
    }
    
    /**
     * Retrieves the primary language code based on the client's IP address.
     *
     * This method makes an HTTP request to the ipapi.co service to get information about the client's IP address.
     * It extracts the primary language code from the response. 
     * 
     * @return string|null
     */
    public function getIpAddressLanguage(){
        try{
            // retrieves locale string extracted from IP if it's defined
            $loccookie = $this->ipLocaleCookie();
            
            if(is_blank($this->ipLoc)){
                if(!is_null($loccookie)){
                    $this->ipLoc = $loccookie["locale"];
                    goto contains;
                }
                
                $response = Http::get("https://ipapi.co/{$this->fetchIpAddress()}/json")->json();
                
                if(is_blank($response)){
                    goto ends;
                }
                
                if(!isset($response->languages)){
                    goto ends;
                }
                
                $accepted = StringUtil::explode(",", $response->languages);
                
                $this->ipLoc = $this->extractDynamicLocaleString($accepted[0]);
                
                // defines new IP locale cookie
                $this->ipLocaleCookie($this->ipLoc);
            }
            
            contains:
            return $this->ipLoc;
        }catch(HttpClientException $ex){
            // something wrong happens with the url,
            // will end-up returning null instead.
        }
        
        ends:
        return null;
    }
}