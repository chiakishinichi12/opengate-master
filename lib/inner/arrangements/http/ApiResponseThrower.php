<?php
namespace lib\inner\arrangements\http;

use lib\util\exceptions\SessionException;
use lib\util\accords\SessionUtil;
use lib\inner\App;
use lib\inner\WebAppCore;

trait ApiResponseThrower{
    
    /**
     *
     * Throws an exception if the current response type is API.
     *
     * This method checks the instance type of the application core to determine
     * if the current response is an API response. If the instance type is "api",
     * it throws a `SessionException`, indicating that session updates are not
     * allowed in API responses. This enforces the stateless nature of API responses
     * and prevents misuse of session functionality in an API context.
     *
     */
    protected function throwIfOnApiResponse($func = ""){
        $strict = preferences("setup")->get("api_response_strict");
        
        $core = App::make(WebAppCore::class);
        
        if(strcmp($core->instanceType(), "api") === 0 && $strict){
            if($this instanceof SessionUtil){
                throw new SessionException("{$func}(): Session update cannot be performed with API response");
            }
            
            if($this instanceof Redirector){
                throw new RedirectionException("Redirection cannot be performed with API response");
            }
        }
    }
}

