<?php
namespace lib\inner;

use lib\util\exceptions\ClassNotFoundException;
use lib\util\exceptions\MethodNotFoundException;
use lib\exceptions\OpenGateException;

/**
 * 
 * @author Jiya Sama
 *
 */
class Accessor {
    
    /**
     * 
     * @var array
     */
    protected $alias = [];
    
    /**
     * 
     * return string
     * 
     * @throws OpenGateException
     */
    public function warpedInstance(){
        throw new OpenGateException("Warped Instance class name must be provided");
    }
    
    /**
     * 
     * retrieves the class name extending Accessor class.
     * 
     * @return string
     */
    protected function extendingClass(){
        return class_name($this);
    }
    
    /**
     * 
     * @param string $method
     * @param array $params
     */
    public static function __callStatic($method, $params){
        $accessor = (new static);
        
        $instname = $accessor->warpedInstance();
        
        if(!class_exists($instname)){
            throw new ClassNotFoundException("Class '{$instname}' not found.");
        }
        
        $warpedObject = gate()->make($instname);
        
        // this expression allows the accessor class to access methods
        // defined by the magic method __call
        $hasCall = method_exists($warpedObject, "__call");
        
        if(isset($accessor->alias[$method])){
            $method = $accessor->alias[$method];
        }
        
        if(!method_exists($warpedObject, $method) && !$hasCall){
            throw new MethodNotFoundException("Unwarped method of {$accessor->extendingClass()} : '{$method}'");
        }
        
        return $warpedObject->$method(...$params);
    }
}