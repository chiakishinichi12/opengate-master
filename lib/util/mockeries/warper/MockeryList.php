<?php
namespace lib\util\mockeries\warper;

use ReflectionClass;
use lib\inner\ApplicationGate;
use lib\mocks\Mockery;
use lib\mocks\MockeryException;
use lib\util\GateFinder;
use lib\util\file\FileUtil;
use lib\inner\App;

class MockeryList {
    
    /**
     *
     * @var ApplicationGate
     */
    protected $gate;
    
    public function __construct(ApplicationGate $gate){
        $this->gate = $gate;
    }
    
    public function showList(){
        echo "Established Mockery Classes:\n\n";
        
        $failedCallback = function(){
            out()->println("<error>No mockery class found.</error>");
            die();
        };
        
        $mockeriespath = preferences("structure")->get("mockeries");
        
        if(!FileUtil::exists($mockeriespath)){
            $this->gate->call($failedCallback);
        }
        
        (new GateFinder(relative_path($mockeriespath), true))->failedCallback($failedCallback)->load(function($class){
            if(!App::checkInheritance($class, Mockery::class)){
                throw new MockeryException("Class {{$class}} is not an instance of ".Mockery::class);
            }
            
            $model_property = (new ReflectionClass($class))->getProperty("model");
            
            if(!is_null($model_property)){
                $model_property->setAccessible(true);
                $model_property = "<magenta>[{$model_property->getValue(new $class())}]</magenta>";
            }else{
                $model_property = "";
            }
            
            out()->println("<primary>{$class}</primary> {$model_property}\n");
        });
    }
}

