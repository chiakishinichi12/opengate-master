<?php

namespace lib\util\controller;

use lib\inner\SessionVolatileBinder;
use lib\inner\VarBinder;
use lib\inner\ValidationBinder;

class Controller {
    
    use VarBinder, SessionVolatileBinder, ValidationBinder;
    
}