<?php
namespace lib\inner;

trait ValidationBinder {
    
    /**
     *
     * binds all the validation remarks and inputted data to the view
     * if there's a failure found from validator session
     *
     * @param string $valname - variable name for validator
     * @param string $objname - variable name for autoloaded data object
     * @param Closure $with - closures for inputted data amendments
     *
     * @return boolean
     */
    protected function bindValidationAndInput(string $valname = "validator", string $objname = "data", \Closure $with = null){
        if(is_array($validator = $this->accessVolatile("og.validator"))){
            $this->bind($valname, $validator["instance"]);
            
            // this is to amend some property value that needs to be amended
            if(!is_null($with)){
                $with($validator["inputted"]);
            }
            
            $this->bind($objname, $validator["inputted"]);
            
            return true;
        }
        
        return false;
    }
    
    /**
     *
     * @param string $promptname
     *
     * @return boolean|null
     */
    protected function bindValidatorMessage($varname = "message"){
        return $this->bindVolatileData("og.validator.message", $varname);
    }
}

