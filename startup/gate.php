<?php

$gate = new lib\inner\ApplicationGate();

/**
 * 
 * We can bind everything here using laravel's resolver engine
 * 
 */
$gate->singleton(lib\util\accords\Logger::class, 
    lib\util\logger\EventLogger::class);

$gate->singleton(lib\handlers\ExceptionHandler::class, 
    app\exceptions\AppExceptionHandler::class);

$gate->initAppReader();

return $gate;