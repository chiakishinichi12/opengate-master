<?php
namespace lib\mocks;

use lib\inner\ApplicationGate;
use lib\inner\arrangements\ServiceProvider;
use lib\util\GateFinder;

class MockeryServiceProvider extends ServiceProvider{
    
    /**
     * 
     * @var boolean
     */
    protected $forCLI = true;
    
    /**
     * 
     * @param ApplicationGate $gate
     */
    public function boot(ApplicationGate $gate){
        $mockwarper = $gate->make(MockeryWarper::class);
        
        /*
         * 
         * locates all the available mockery instances
         * 
         */
        (new GateFinder(relative_path(preferences("structure")->get("mockeries")), true))
        ->load(function($mockeryclass) use ($mockwarper){
            $mockwarper->define($mockeryclass);
        });
    }
}