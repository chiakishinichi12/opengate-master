<?php
namespace lib\inner;

use lib\util\nebula\ValuesConfig;

/**
 * 
 * @author antonio
 *
 * @method static mixed get(string $valuePath)
 */
class Values extends Accessor{
    /**
     * 
     * {@inheritDoc}
     * @see \lib\inner\Accessor::warpedInstance()
     */
    public function warpedInstance(){
        return ValuesConfig::class;
    }
}

