<?php

return [
    /**
     * 
     * default setting for local file upload
     * 
     */
    "local" => [
        /*
         * 
         * this is where the successfully uploaded files are stored
         * 
         */
        "storage" => base_path("stockade/uploads"),
        
        /**
         * 
         * when the uploaded files were intercepted due to their unlisted extension name, 
         * they will be moved to the intercepted file directory
         * 
         */
        "intercepted" => base_path("stockade/uploads/intercepted")
    ]
];