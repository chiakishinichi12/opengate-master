<?php

namespace lib\util\cli;

use lib\inner\Accessor;

/**
 * 
 * @author Jiya Sama
 *
 * @method static \lib\util\cli\CommandBean command(string|array $command, mixed $query)
 */
class Console extends Accessor {
   
    /**
     * 
     * {@inheritDoc}
     * @see \lib\inner\Accessor::warpedInstance()
     */
    public function warpedInstance(){
        return ConsoleGate::class;
    }
}