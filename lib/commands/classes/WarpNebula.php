<?php
namespace lib\commands\classes;

use lib\commands\CommonMethods;
use lib\inner\App;
use lib\instances\CommandInstance;
use lib\util\cli\WarpArgv;
use lib\util\nebula\warper\NebulaResourceCreator;
use lib\util\nebula\warper\TranslationResourceTool;

/**
 *
 * @Commands(["nebula"])
 *
 */
class WarpNebula extends CommandInstance {
    
    use CommonMethods;
    
    protected $name = "nebula";
    
    protected $desc = "OpenGate Nebula Resource Utilities";
    
    /**
     *
     * @Argument("creates nebula resource. [option(s): { --values, --translation, --view }]")
     *
     */
    public function create(){
        App::make(NebulaResourceCreator::class)->create();
    }
    
    /**
     *
     * @Argument("translation recource tool")
     *
     */
    public function translation(){
        App::make(TranslationResourceTool::class)->utilize();
    }
    
    /**
     * 
     * @param WarpArgv $argv
     */
    public function listen(WarpArgv $argv){
        $this->execution($argv);
    }
}

