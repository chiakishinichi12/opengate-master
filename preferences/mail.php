<?php

return [
    /**
     * 
     * PHPMailer credential configuration
     * 
     */
    "phpmailer" => [
        "smtp_debug" => config("phpmailer_smtp_debug", false),
        "host" => config("phpmailer_host", "smtp.gmail.com"), 
        "smtp_auth" => config("phpmailer_smtp_auth", true),
        "username" => config("phpmailer_username", ""), 
        "password" => config("phpmailer_password", ""),
        "port" => config("phpmailer_port", 465),
        "name" => config("phpmailer_name", null),
        "charset" => config("phpmailer_charset", "UTF-8")
    ],
    
    /**
     * 
     * AWS-SES credential configuration
     * 
     */
    "aws-ses" => [
        "version" => config("aws_ses_ver", "2010-12-01"),
        "region" => config("aws_ses_region", "ap-southeast-1"),
        "sender_mail" => config("aws_ses_sender", ""),
        "key" => config("aws_ses_key", ""),
        "secret" => config("aws_ses_secret", ""),
        "name" => config("aws_ses_alt", null),
        "charset" => config("aws_ses_charset", "UTF-8"),
        "error_trace" => config("aws_ses_error_trace", true)
    ],
    
    /**
     * 
     * a path where all successful and failed message will be stored
     * 
     */
    "mail_logs" => base_path("stockade/mails")
];