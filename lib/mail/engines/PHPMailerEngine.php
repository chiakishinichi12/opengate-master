<?php
namespace lib\mail\engines;

use PHPMailer\PHPMailer\PHPMailer;
use lib\mail\Dispatchable;
use lib\mail\Engine;
use lib\mail\Recipient;
use lib\util\exceptions\MailDispatchException;
use lib\util\exceptions\MailEngineResolverException;
use lib\util\nebula\ViewRenderer;
use lib\mail\Caching;
use lib\mail\Attachment;
use lib\util\nebula\View;

class PHPMailerEngine implements Engine {
       
    use Caching;
        
    /**
     * 
     * @var array
     */
    protected $credentials;
    
    /**
     * 
     * @var boolean
     */
    protected $loaded = false;
    
    /**
     * 
     * @var string
     */
    protected $subject = "";
    
    /**
     * 
     * @var array
     */
    protected $recipients = [];
    
    /**
     * 
     * @var array
     */
    protected $attachments = [];
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\mail\Engine::initMailerEngine()
     */
    public function initMailerEngine(array $credentials){        
        $this->credentials = $credentials;
        
        if(!class_exists(PHPMailer::class)){
            return;
        }
                
        $this->loaded = true;
    }
    
    /**
     * 
     * @return \PHPMailer\PHPMailer\PHPMailer
     */
    protected function mailerInstance(){
        $mailer = new PHPMailer();
                
        $mailer->SMTPDebug = $this->credentials["smtp_debug"];
        $mailer->isSMTP();
        $mailer->Host = $this->credentials["host"];
        $mailer->SMTPAuth = $this->credentials["smtp_auth"];
        $mailer->Username = $this->credentials["username"];
        $mailer->Password = $this->credentials["password"];
        $mailer->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
        $mailer->Port = $this->credentials["port"];
        
        if(!is_null($this->credentials["name"])){
            $mailer->setFrom($this->credentials["username"], $this->credentials["name"]);
        }
        
        return $mailer;
    }
    
    /**
     * 
     * sets the fallback subject when the subject specification 
     * is not present on the Dispatchable class
     * 
     * @param string $subject
     * @return \lib\mail\engines\PHPMailerEngine
     */
    public function subject(string $subject){
        $this->subject = $subject;
        
        return $this;
    }
    
    /**
     * 
     * @param string $recipient
     * @return \lib\mail\engines\PHPMailerEngine
     */
    public function recipients(array $recipients){
        $this->recipients = $recipients;
        
        return $this;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\mail\Engine::attachments()
     */
    public function attachments(array $attachments){
        $this->attachments = $attachments;
        
        return $this;
    }
    
    /**
     * 
     * @param Dispatchable $dispatchable
     * 
     * @throws MailEngineResolverException
     * @throws MailDispatchException
     */
    protected function intercept($dispatchable){
        if(!$this->loaded){
            throw new MailEngineResolverException(
                "Class ".PHPMailer::class." isn't loaded. do *composer require phpmailer/phpmailer*");
        }
        
        if(!($dispatchable instanceof Dispatchable)){
            throw new MailDispatchException("Mailable object is not an instance of Dispatcher");
        }
        
        if(!is_blank($dispatchable->subject())){
            $this->subject = $dispatchable->subject();
        }
        
        $notBlank = [
            "recipients" => $this->recipients,
            "subject" => $this->subject
        ];
        
        foreach($notBlank as $key => $value){
            if(is_blank($value)){
                throw new MailDispatchException("'{$key}' shouldn't be blank/empty");
            }
        }
        
        if(!is_null($dispatchable->attachments())){
            $this->attachments = $dispatchable->attachments();
        }
    }
    
    /**
     * 
     * @param Attachment $attachment
     * @return string[]|\lib\mail\Attachment[]
     */
    protected function attachToArray(Attachment $attachment){
        return [
            $attachment->path(),
            $attachment->as(),
            $attachment->encoding(),
            $attachment->mime()
        ];
    }
    
    /**
     * 
     * @param Dispatchable $dispatchable
     * @return \lib\util\nebula\ViewRenderer|string
     */
    protected function evaluateContent(Dispatchable $dispatchable){
        $content = $dispatchable->content();
        
        if($content instanceof ViewRenderer){
            $content = $dispatchable->content()->buffered();
        }else if(is_string($content)){
            if(View::exists($content)){
                $content = morph($content, $dispatchable->bindings())->buffered();
            }
        }else{
            throw new MailDispatchException("Content instance must be a string or ViewRenderer. ".gettext($content)." found");
        }
        
        return $content;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\mail\Engine::triggerSend()
     */
    public function triggerSend(Dispatchable $dispatchable){
        $this->intercept($dispatchable);
        
        $mailer = $this->mailerInstance();
        
        // iterates recipient stack elements
        collect($this->recipients)->each(function(Recipient $recipient) use ($mailer){
            $mailer->addAddress($recipient->email(), $recipient->name());
        });
                
        // iterates attachment stack elements
        collect($this->attachments)->each(function($attachment) use ($mailer){
            if($attachment instanceof Attachment){
                $mailer->addAttachment(...$this->attachToArray($attachment));
            }
        });
        
        $content = $this->evaluateContent($dispatchable);
        
        $mailer->isHTML($dispatchable->isHtml());
        
        $mailer->Subject = $this->subject;
        $mailer->Body = $content;
        $mailer->CharSet = $this->credentials["charset"];
         
        $sending = $mailer->send();
        
        $this->logMailCache($this->recipients, $this->subject, 
            $mailer->Body, class_basename(class_name($this)), 
            $sending);
        
        return $sending;
    }
}