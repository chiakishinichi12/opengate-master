<?php
namespace lib\util;

use lib\util\builders\Cookie;

/**
 * 
 * @author Jiya Sama
 *
 */
class CookieDataWarper {
    
    /**
     * 
     * sets a new cookie. returns false if already created
     * 
     * @param Cookie $cookie
     * 
     * @return boolean
     */
    public function set(Cookie $cookie){
        if(is_null($this->get($cookie->name()))){
            @setcookie($cookie->name(), 
                $cookie->value(),
                $cookie->cookieParamsValueArray());
            
            return true;
        }
        
        return false;
    }
    
    /**
     * 
     * gets the cookie value. returns null if undefined
     * 
     * @param string $cookieName
     * 
     * @return NULL|mixed
     */
    public function get(string $cookieName){
        if(!isset($_COOKIE[$cookieName])){
            return null;
        }
        
        return $_COOKIE[$cookieName];
    }
    
    /**
     * 
     * modify the value of a stored cookie
     * 
     * @param Cookie $cookie
     * 
     * @return boolean
     */
    public function modify(Cookie $cookie){
        if(is_null($this->get($cookie->name()))){
            return false;
        }
        
        @setcookie($cookie->name(), 
            $cookie->value(),
            $cookie->cookieParamsValueArray());
        
        return true;
    }
    
    /**
     * 
     * retrieves all stored cookies
     * 
     * @return Collections
     */
    public function all(){
        return collect($_COOKIE);
    }
    
    /**
     * 
     * deletes a retrieved stored cookie. returns false if not exist
     * 
     * @param string $name
     * 
     * @return boolean
     */
    public function delete(string $name){
        if(is_null($this->get($name))){
            return false;
        }
        
        @setcookie($name, "", time() - 3600, "/");
        
        unset($_COOKIE[$name]);
        
        return true;
    }
    
    /**
     * 
     * clears the entire cookie stack
     * 
     * @return boolean
     */
    public function clear(){
        $len = $this->all()->length();
        
        if($len === 0){
            return false;
        }
        
        $this->all()->each(function($value, $name){
            $this->delete($name);
        });
        
        $now = $this->all()->length();
            
        return $now === 0;
    }
}