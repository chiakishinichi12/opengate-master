<?php

namespace lib\util\exceptions;

use lib\exceptions\OpenGateException;

class CollectionTypeException extends OpenGateException{
    
    public function __construct(String $message){
        parent::__construct($message);
    }
}