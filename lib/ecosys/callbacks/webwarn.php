<?php

function webWarningCallback($errno, $errstr, $errfile, $errline){
    
    echo "<div style=\"margin:5px;border:1px solid black;padding:10px;\">";
    
    echo "<code>{$errno} | {$errstr}</code><br/>";
    echo "<code>{$errfile} @ <b>Line {$errline}</b></code>";
    
    echo "</div>";
}