<?php
namespace lib\commands\classes;

use lib\commands\CommonMethods;
use lib\inner\App;
use lib\instances\CommandInstance;
use lib\util\cli\WarpArgv;
use lib\util\vroller\warper\ValuesRollerClassCreation;
use lib\util\vroller\warper\ValuesRollerList;

/**
 *
 * @Commands(["vroller"])
 *
 */
class WarpVRoller extends CommandInstance {
    
    use CommonMethods;
    
    protected $name = "vroller";
    
    protected $desc = "ValuesRoller classes handler";
    
    /**
     * 
     * @Argument("creates ValuesRoller class.")
     * 
     */
    public function create(){
        App::make(ValuesRollerClassCreation::class)->write();
    }
    
    /**
     *
     * @Argument("show ValuesRoller class list.")
     *
     */
    public function list(){
        App::make(ValuesRollerList::class)->showList();
    }
    
    /**
     * 
     * @param WarpArgv $argv
     */
    public function listen(WarpArgv $argv){
        $this->execution($argv);
    }
}

