<?php
namespace lib\util\datagate;

use lib\inner\Accessor;

/**
 * 
 * @author antonio
 *
 * @method static boolean insert(ContentValues $values)
 * @method static boolean update(ContentValues $values, string $where = "")
 * @method static mixed query(QuerySpecification $specification)
 * @method static boolean delete(string $tableName, string $whereClause = "", array $whereArgs = [])
 */
class Prepare extends Accessor{
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\inner\Accessor::warpedInstance()
     */
    public function warpedInstance(){
        return PrepareStmt::class;
    }
}