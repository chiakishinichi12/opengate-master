<?php

use lib\util\nebula\interpretations\Directive;
use lib\util\nebula\interpretations\DirectiveThrowers;
use lib\util\nebula\interpretations\Keyword;
use lib\util\nebula\ViewMorphInterpreter;

return new class implements Directive, Keyword {
    
    use DirectiveThrowers;
    
    /**
     *
     * @var string
     */
    protected $name;
    
    /**
     *
     * @var string
     */
    protected $exp;
    
    /**
     *
     * @var ViewMorphInterpreter
     */
    protected $interpreter;
    
    public function __construct(){
        $base = basename(__FILE__);
        
        $this->name = substr($base, 0, strrpos($base, ".php"));
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\nebula\interpretations\Directive::obtains()
     */
    public function obtains($file, $interpreter){
        $this->interpreter = $interpreter;
        
        return $this;
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \lib\util\nebula\interpretations\Directive::parenthesis()
     */
    public function parenthesis($exp){
        $this->exp = $exp;
        
        return $this;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \lib\util\nebula\interpretations\Directive::interpretation()
     */
    public function interpretation(){        
        $code = "<?php {$this->name}: ?>";
        
        if($this->interpreter->switchStart()){
            if(!$this->interpreter->hasFirstCase()){
                $code = "{$this->name}: ?>";
                
                $this->interpreter->hasFirstCase(true);
            }
        }
        
        return $code;
    }
    
};