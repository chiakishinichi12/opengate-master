<?php

namespace app\instances\commands;

use lib\instances\CommandInstance;
use lib\util\Collections;

/**
 *
 * @Commands(["spill-trivia", "strivia"])
 *
 */
class SpillTrivia extends CommandInstance {
    
    protected $name = "spill-trivia";
    
    protected $desc = "echoes some interesting trivia";
    
    public function listen(){
        $trivia = Collections::make([
            "Did you know that during WWII, the United States detonated two atomic bombs over the Japanese cities of Hiroshima and Nagasaki",
            "The Great Wall of China is not visible from space with the naked eye, contrary to popular belief. It can only be seen with the aid of telescopic lenses.",
            "The world's oldest known musical instrument is a flute made from a vulture's wing bone, dating back over 35,000 years.",
            "The name \"Google\" originated from a misspelling of the word \"googol,\" which refers to the number represented by a 1 followed by 100 zeros.",
            "There are more possible iterations of a game of chess than there are atoms in the observable universe.",
            "The electric chair was invented by a dentist named Alfred P. Southwick in the late 19th century.",
            "The city of Istanbul, Turkey, is the only city in the world located on two continents—Europe and Asia.",
            "The shortest war in history occurred between Britain and Zanzibar on August 27, 1896, lasting only 38 minutes.",
            "The average person walks the equivalent of three times around the world in their lifetime.",
            "Honey never spoils. Archaeologists have found pots of honey in ancient Egyptian tombs that are over 3,000 years old and still perfectly edible.",
            "The total weight of all the ants on Earth is roughly equal to the weight of all the humans.",
            "The fingerprints of koala bears are so indistinguishable from humans that they have been confused at crime scenes.",
            "The world's oldest known recipe is a beer recipe from ancient Sumeria, dated around 1800 BC.",
            "The average person has about 70,000 thoughts per day.",
            "The word \"nerd\" was first coined by Dr. Seuss in his book \"If I Ran the Zoo\" in 1950."
        ])->random();
             
        out()->println("<warning>{$trivia}</warning>");
    }
}