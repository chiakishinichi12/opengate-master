<?php
namespace lib\util\nebula\warper;

use lib\traits\WarperCommons;
use lib\util\Collections;
use lib\util\StringUtil;
use lib\util\cli\LongOption;
use lib\util\cli\WarpArgv;
use lib\util\file\FileUtil;
use lib\inner\Locale;

/**
 * 
 * @author Jiya Sama
 *
 */
class NebulaResourceCreator {
    
    use WarperCommons;
    
    /**
     * 
     * @var string
     */
    protected $warping = "Nebula resource";
    
    /**
     * 
     * @var Collections
     */
    protected $args;
    
    /**
     *
     * @var LongOption
     */
    protected $translation;
    
    /**
     * 
     * @var LongOption
     */
    protected $values;
    
    /**
     * 
     * @var LongOption
     */
    protected $view;
    
    /**
     * 
     * @var string
     */
    protected $nebulas;
    
    /**
     * 
     * @var string
     */
    protected $chosenOption;
    
    /**
     * 
     * @var string
     */
    protected $prototype;
    
    /**
     * 
     * @var string
     */
    protected $template;
    
    /**
     * 
     * @var string
     */
    protected $resourceName;
    
    /**
     * 
     * @param WarpArgv $argv
     */
    public function __construct(WarpArgv $argv){
        $this->args = $argv->obtainArguments();
        
        $this->translation = $argv->findLongOption("--translation");
        
        $this->values = $argv->findLongOption("--values");
        
        $this->view = $argv->findLongOption("--view");
        
        $this->nebulas = base_path("nebula");
        
        $this->prototype = base_path("lib/util/nebula/prototype");
    }
    
    /**
     * 
     * Checks if only a single option among translation, view, or values is defined.
     * 
     * @param array $defstack
     * @return boolean
     */
    protected function isOnlySingleOptionDefined(array &$defstack = null){
        $options = [
            $this->translation,
            $this->view,
            $this->values
        ];
        
        $defined = 0;
        
        foreach($options as $option){
            if(!is_null($option)){
                $defined++;
                
                if(!is_null($defstack)){                    
                    $defstack[] = $option->name();
                }
            }
        }
        
        return $defined === 1;
    }
    
    /**
     * 
     * Sets up the chosen resource option based on user-defined arguments and configures
     * the target directory and template.
     * 
     * @param array $defined
     */
    protected function warpChosenResourceOption(array $defined){
        $defined = array_values($defined);
        
        switch($this->chosenOption = $defined[0]){
            case "--view":
                $this->nebulas = "{$this->nebulas}/views";
                
                $this->template = "view.tmp";
                break;
            case "--translation":
                $this->nebulas = "{$this->nebulas}/lang";
                $this->template = "lang.tmp";
                break;
            case "--values":
                $this->nebulas = "{$this->nebulas}/values";
                $this->template = "values.tmp";
                break;
        }
        
        $this->validateResourceNaming();
    }
    
    /**
     * 
     * Validates the resource name to ensure it does not contain restricted characters.
     * Also adds an appropriate extension for the view resource.
     */
    protected function validateResourceNaming(){
        $this->resourceName = $this->args->get(1);
        
        if(is_null($this->resourceName)){
            return;
        }
        
        $restrictedChars = ["."];
        
        if(strcmp($this->chosenOption, "--view") !== 0){
            $restrictedChars = array_merge($restrictedChars, ["/", "\\"]);   
        }
        
        if(StringUtil::contains($this->resourceName, $restrictedChars)){
            out()->haltln("[ERROR]: <error>{{$this->chosenOption}} Invalid nebula resource name [{$this->resourceName}]</error>");
        }else if(strcmp($this->chosenOption, "--view") === 0){
            $this->resourceName = "{$this->resourceName}.morph";
        }
    }
    
    /**
     * 
     * Evaluates the provided arguments, checking that only one resource option is selected
     * and that it is a valid selection.
     */
    protected function evaluate(){
        if(is_null($this->view) && is_null($this->values) && is_null($this->translation)){
            out()->haltln("[ERROR]: <error>No nebula resource option was defined."
                ."{ --translation, --values, --view }</error>");
        }
        
        $defstack = [];
        
        if(!$this->isOnlySingleOptionDefined($defstack)){
            out()->haltln("[ERROR]: <error>Only one nebula resource option must be defined. ["
                .implode(", ", $defstack)."] specified</error>");
        }
        
        $this->warpChosenResourceOption($defstack);
    }
    
    /**
     * 
     * Creates the necessary resource files in the specified directories.
     * Iterates through languages for translation resources.
     */
    protected function materialize(){
        $writing = function($nebuladir){
            $this->writeWarpedScript($this->template,
                collect(),
                $nebuladir,
                $this->resourceName);
        };
        
        if(in_array($this->chosenOption, ["--view", "--values"])){
            $writing($this->nebulas);
        }else{
            foreach(Locale::languages() as $language){
                $writing("{$this->nebulas}/{$language}");
            }
        }
        
        out()->println("<warning>DONE WARPING {{$this->chosenOption} resource(s)}.</warning>");
    }
    
    /**
     * 
     * executing nebula command.
     * Evaluates arguments, prepares templates, and creates the resource.
     */
    public function create(){
        if($this->args->count() < 2){
            return;
        }
        
        $this->evaluate();
        
        if(strcmp($this->chosenOption, "--view") === 0){
            $this->alterAndCreate($this->resourceName, $this->nebulas);
        }
        
        $this->template = FileUtil::getContents("{$this->prototype}/{$this->template}", true);
        
        $this->materialize();
    }
}

