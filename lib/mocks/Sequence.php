<?php
namespace lib\mocks;

class Sequence{
    
    /**
     * 
     * @var int
     */
    public $index;
    
    /**
     * 
     * @var int
     */
    public $length;
}