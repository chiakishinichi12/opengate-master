<?php
namespace lib\util\route;

use lib\inner\App;
use lib\inner\arrangements\http\Request;
use lib\util\Collections;
use lib\util\StringUtil;
use lib\util\exceptions\RoutingException;

class RouteArgs {
    
    /**
     * 
     * @var Request
     */
    protected $request;
    
    /**
     * 
     * @var boolean
     */
    protected $found = false;
    
    /**
     * 
     * @var Collections
     */
    protected $uriParamValues;
    
    /**
     * 
     * @var RouteBean
     */
    protected $locatedRouteBean;
    
    /**
     *
     * @var Collections
     */
    protected $constants;
    
    /**
     * 
     * @var Collections
     */
    protected $parametrics;
    
    /**
     * 
     * @var string
     */
    protected $parametricStringPattern = "/\{([a-zA-Z0-9]+)\}/";
    
    /**
     * 
     * @param Request $request
     */
    public function __construct(Request $request){
        $this->request = $request;    
        $this->constants = collect();
        $this->parametrics = collect();
        $this->uriParamValues = collect();
        
        $this->collectUris();
        $this->alignURIValues();
    }
    
    /**
     * 
     * determines if the uri string has been located
     * 
     * @return boolean
     */
    public function hasFound(){
        return !is_null($this->locatedRouteBean);
    }
    
    /**
     * 
     * @return boolean
     */
    public function isParametric(){
        return $this->uriParamValues->count() > 0;
    }
    
    /**
     * 
     * @return \lib\util\Collections
     */
    public function getValues(){
        return $this->uriParamValues;
    }
    
    /**
     * 
     * @return RouteBean
     */
    public function getLocatedRouteBean(){
        return $this->locatedRouteBean;
    }
    
    /**
     * 
     * @return string|\lib\inner\arrangements\http\unknown
     */
    public function getCurrentURIString(){
        $uristr = $this->request->server("REQUEST_URI");
        
        return !is_null($uristr) ? $this->omittingParams($uristr) : "";
    }
    
    /**
     *
     * @return string
     */
    protected function omittingParams(string $urlOrUri){
        return preg_replace('/\?.*/', '', $urlOrUri);
    }
    
    /**
     * 
     * @param string $uri
     * @return Collections
     */
    protected function getUriParts(string $uri){
        return collect(explode("/", $uri));
    }
    
    /**
     * gets the length of parametric values within the URI string
     *
     * @param string $uri
     * @return number
     */
    protected function parametricCount($uri){
        $param_count = 0;
        
        $exploded = collect(explode("/", $uri));
        
        $exploded->each(function($phrase) use (&$param_count){
            if($this->isParametricPhrase($phrase)){
                $param_count++;
            }
        });
            
        return $param_count;
    }
  
    /**
     * 
     * @param string $slug
     * @return number
     */
    protected function isParametricPhrase(string $slug, array &$matches = []){
        return preg_match($this->parametricStringPattern, $slug, $matches);
    }
    
    /**
     * 
     * placing URIs to their respective stack
     * 
     */
    protected function collectUris(){
        App::make(RouteContainer::class)->getRoutes()->each(function(RouteBean $route, $index){
            if(is_array($route->uri())){
                collect($route->uri())->each(function(string $uri, int $index) use($route){
                    $this->addRouteUri($uri, $route);
                });
            }else{
                $this->addRouteUri($route->uri(), $route);
            }
        });
    }
    
    /**
     * 
     * @param string $uri
     */
    protected function addRouteUri(string $uri, RouteBean $route){
        // routes must start with /
        if(!StringUtil::startsWith($uri, "/")){
            throw new RoutingException("Route string must start with /. [{$uri}]");
        }
        
        $element = [
            "to_route" => $uri,
            "object" => $route
        ];
        
        if($this->parametricCount($uri)){       
            // intercepting duplicated routes
            if($this->isDuplicatedParametric($uri)){
                throw new RoutingException("Duplicated Route: <strong>{$route->query()}</strong> [{$uri}]");
            }
            
            $this->parametrics->add($element);
        }else{
            // intercepting duplicated routes
            if($this->isDuplicatedConstant($uri)){
                throw new RoutingException("Duplicated Route: <strong>{$route->query()}</strong> [{$uri}]");
            }
            
            $this->constants->add($element);
        }
    }
    
    /**
     * 
     * @param string $uri
     * 
     * @return boolean
     */
    protected function isDuplicatedConstant(string $uri){
        return $this->constants->has($uri, function(array $item){
            return $item["to_route"];
        });
    }
    
    /**
     * 
     * @param string $uri
     * @return mixed|NULL
     */
    protected function parametricRegex(string $uri){
        if($this->isParametricPhrase($uri)){
            $regexed = str_replace("/", "\/", $uri);
            
            return preg_replace("/\{(.+)\}/", "(.+)", $regexed);
        }
        
        return null;
    }
    
    /**
     * 
     * @param string $uri
     */
    protected function isDuplicatedParametric(string $uri){
        foreach($this->parametrics as $item){
            $recent = $this->parametricRegex($uri);
            $stored = $this->parametricRegex($item["to_route"]);
            
            if(is_null($stored) || is_null($recent)){
                continue;   
            }
            
            if(strcmp($recent, $stored) === 0){
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * 
     * @return \lib\util\route\RouteArgs
     */
    public function alignURIValues(){        
        $relativeUri = $this->getCurrentURIString();
        
        $relativeDir = substr(
            str_replace(DIRECTORY_SEPARATOR, "/", base_path()),
            strlen(request()->server("DOCUMENT_ROOT"))
        );
        
        /*
         * maintains the relative URI based on the directory where the index.php belongs
         *
         */
        if(StringUtil::startsWith($relativeUri, $relativeDir)){
            $this->relativeUri = substr($relativeUri, strlen($relativeDir));    
        }
        
        // locates the matches for currently requested URI
        $locatedRouteBean = $this->findingMatchedUri($relativeUri);
        
        // locatedRouteBean property will go back to being null when
        // there is no located route bean
        if(!is_null($locatedRouteBean)){
            $this->locatedRouteBean = $locatedRouteBean;
        }else{
            $this->locatedRouteBean = null;
        }
        
        return $this;
    }
    
    /**
     * 
     * @param string $relativeRoute
     * @param string $registeredRoute
     * 
     * @return boolean
     */
    protected function findSimilarities(string $relativeRoute, string $registeredRoute){
        $registeredRouteParts = collect(explode("/", $registeredRoute));
        $relativeRouteParts = collect(explode("/", $relativeRoute));
        
        if($relativeRouteParts->length() === $registeredRouteParts->length()){
            $regexed = $this->parametricRegex($registeredRouteParts->implode("/")); 
            
            if(preg_match("/{$regexed}/", $relativeRouteParts->implode("/"))){
                $registeredRouteParts->each(function($expression, $index) use ($relativeRouteParts){
                    $matches = [];
                    
                    if($this->isParametricPhrase($expression, $matches)){
                        $this->uriParamValues->put($matches[1], urldecode($relativeRouteParts->get($index)));
                    }
                });
                
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * 
     * @param string $relativeRoute
     * 
     * @return RouteBean
     */
    protected function findingMatchedUri(string $relativeRoute){
        $locatedRouteBean = null;
        
        $mapper = function($element){
            return $element["to_route"];
        };
        
        // 1st finding the exact
        if($this->constants->has($relativeRoute, $mapper)){
            $index = $this->constants->binarySearch($relativeRoute)
                        ->toCompare($mapper)
                        ->get()
                        ->key();
                        
            $locatedRouteBean = $this->constants->get($index)["object"];
        }else{    
            $iteration = function(string $registeredRoute, int $index) use ($relativeRoute, &$locatedRouteBean){
                if($this->findSimilarities($relativeRoute, $registeredRoute)){
                    $locatedRouteBean = $this->parametrics->get($index)["object"];
                }
            };
            
            // 2nd: locate the nearest parametric uri to the currently requested uri
            collect($this->parametrics->map($mapper))->each($iteration);
        }
        
        return $locatedRouteBean;
    }
}