<?php
namespace lib\util\model\relational;

use lib\inner\App;
use lib\util\model\relational\material\BelongsTo;
use lib\util\model\relational\material\HasMany;
use lib\util\model\relational\material\HasOne;

trait RelationalModel {
        
    /**
     * 
     * @param string $mclass
     * @param string $foreignKey
     * @param string $ownerKey
     *
     * @return HasOne
     */
    protected function hasOne(string $mclass, string $foreignKey = null, string $ownerKey = null){        
        $hasOne = App::make(HasOne::class)
        ->ownerModel($this)
        ->foreignModel($mclass);
        
        if(!is_blank($foreignKey)){
            $hasOne->foreignKey($foreignKey);
        }
        
        if(!is_blank($ownerKey)){
            $hasOne->ownerKey($ownerKey);
        }
        
        return $hasOne->withBuilder();
    }
    
    /**
     * 
     * @param string $mclass
     * @param string $foreignKey
     * @param string $ownerKey
     * 
     * @return HasMany
     */
    protected function hasMany(string $mclass, string $foreignKey = null, string $ownerKey = null){
        $hasMany = App::make(HasMany::class)
        ->ownerModel($this)
        ->foreignModel($mclass);
        
        if(!is_blank($foreignKey)){
            $hasMany->foreignKey($foreignKey);
        }
        
        if(!is_blank($ownerKey)){
            $hasMany->ownerKey($ownerKey);
        }
        
        return $hasMany;
    }
    
    /**
     * 
     * @param string $mclass
     * @param string $foreignKey
     * @param string $ownerKey
     * @param string $relationship
     * 
     * @return BelongsTo
     */
    protected function belongsTo(string $mclass, string $foreignKey = null, string $ownerKey = null){
        $belongsTo = App::make(BelongsTo::class)
        ->ownerModel($this)
        ->foreignModel($mclass);
        
        if(!is_blank($foreignKey)){
            $belongsTo->foreignKey($foreignKey);
        }
        
        if(!is_blank($ownerKey)){
            $belongsTo->ownerKey($ownerKey);
        }
        
        return $belongsTo;
    }
}

