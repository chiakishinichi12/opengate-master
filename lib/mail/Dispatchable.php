<?php
namespace lib\mail;

use lib\util\exceptions\MailDispatchException;
use lib\inner\VarBinder;

class Dispatchable {
    
    use VarBinder;
    
    /**
     * 
     * @var boolean
     */
    protected $html = true;
    
    /**
     * 
     * @var string
     */
    protected $subject = "";
    
    /**
     * 
     * @return array
     */
    public function attachments() : array {
        return [];
    }
    
    /**
     * 
     * @return boolean
     */
    public function isHtml(){
        return $this->html;
    }
    
    /**
     * 
     * @return string
     */
    public function subject(){
        return $this->subject;
    }
    
    /**
     * 
     * @throws MailDispatchException
     */
    public function content(){
        throw new MailDispatchException("Mail Content wasn't provided");
    }
}