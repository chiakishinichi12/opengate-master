<?php
namespace lib\util\seeder\warper;

use lib\util\Collections;
use lib\util\cli\WarpArgv;
use lib\util\file\FileUtil;
use lib\util\file\File;
use lib\traits\WarperCommons;

class SeederClassCreation {
    
    use WarperCommons;
    
    /**
     * 
     * @var string
     */
    protected $warping = "Seeder";
    
    /**
     *
     * @var Collections
     */
    protected $args;
    
    /**
     *
     * @var LongOption
     */
    protected $inputClass;
    
    public function __construct(WarpArgv $argv){
        $this->args = $argv->obtainArguments();
        
        $this->inputClass = $argv->findLongOption("--class");
    }
    
    public function write(){
        $template = FileUtil::getContents(base_path("lib/util/seeder/prototype/Seeder.tmp"), true);
        
        $seeder_name = $this->inputClass?->value();
        
        if(is_blank($seeder_name)){
            out()->haltln("<error background=\"true\">Seeder name must not be empty.</error>");
        }
        
        if(!FileUtil::exists($seedpath = preferences("structure")->get("seeders"))){
            if((new File($seedpath))->makeDir()){
                out()->println("<info>Designated Seeder Directory Created! {{$seedpath}}</info>");
            }
        }
        
        $toFind = collect([
            [
                "find" => "[Seeder_Name]",
                "replace" => $seeder_name
            ],
            [
                "find" => "[Date_Created]",
                "replace" => date(STANDARD_TIMESTAMP_FORMAT)
            ],
            [
                "find" => "[Seeder_Namespace]",
                "replace" => str_replace("/", "\\", relative_path($seedpath))
            ]
        ]);
        
        $this->writeWarpedScript($template,
            $toFind,
            $seedpath,
            $seeder_name);
    }
}