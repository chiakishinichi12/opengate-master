<?php
namespace lib\util\migration\warper;

use ReflectionClass;
use lib\util\StringUtil;
use lib\util\annotation\Annotation;
use lib\util\annotation\AnnotationProperty;
use lib\util\annotation\AnnotationUtil;
use lib\util\migration\Migration;

trait MigrationClassValidation {
    
    /**
     *
     * @var LongOption
     */
    protected $inputClass;
    
    /**
     * 
     * Filtering Migration Purposes
     * 
     * @param Migration $migration
     * @param string $class
     * @param string $inputtedClassname
     */
    public function filtering($migration, $class, $inputtedClassname){
        $annotation = (new Annotation("ForTable"))
        ->setProperty(new AnnotationProperty("name", "string", true));
        
        $properties = collect();
        
        $isAnnotated = AnnotationUtil::hasAnnotation(
            new ReflectionClass($class), $annotation, $properties);
        
        if(!is_blank($inputtedClassname)){
            if(strcasecmp($inputtedClassname, "table-creator") === 0){
                if($isAnnotated){
                    $this->migrations->add($migration);
                }
                
                return;
            }if(strcasecmp($inputtedClassname, "table-modifier") === 0){
                if(!$isAnnotated){
                    $this->migrations->add($migration);
                }
                
                return;
            }else if(!StringUtil::endsWith($class, $inputtedClassname)){
                return;
            }
        }
        
        if(AnnotationUtil::hasAnnotation(new ReflectionClass($class), $annotation, $properties)){
            $this->migrations->prepend($migration);
        }else{
            $this->migrations->add($migration);
        }
    }
}