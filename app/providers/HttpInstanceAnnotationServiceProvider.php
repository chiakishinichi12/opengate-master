<?php
namespace app\providers;

use ReflectionClass;
use lib\inner\ApplicationGate;
use lib\inner\arrangements\ServiceProvider;
use lib\instances\APIInstance;
use lib\instances\CommandInstance;
use lib\instances\PageInstance;
use lib\util\GateFinder;
use lib\util\annotation\Annotation;
use lib\util\annotation\AnnotationUtil;
use lib\util\route\Route;
use lib\inner\App;

class HttpInstanceAnnotationServiceProvider extends ServiceProvider{
    
    /**
     * 
     * @param ApplicationGate $gate
     */
    public function boot(ApplicationGate $gate){
        (new GateFinder("app/instances", true))->load(function($class) use ($gate){
            if(App::checkInheritance($class, CommandInstance::class)){
                return;
            }
            
            $annotation = (new Annotation("Route"))
                ->setProperty(["uri", ["string", "array"], true]);
            
            $properties = collect();
            
            if(AnnotationUtil::hasAnnotation(new ReflectionClass($class), $annotation, $properties)){
                if($gate->checkInheritance($class, [APIInstance::class, PageInstance::class])){
                    Route::responseInstance($properties->get("uri"), $class);
                }
            }
        });
    }
}