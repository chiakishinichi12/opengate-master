<?php
namespace lib\util\datagate;

use lib\traits\SQLFeatures;
use lib\util\QueryUtil;
use lib\util\exceptions\SQLException;

class WarpRawSQL {
    
    use SQLFeatures, SQLResultInfoDump;
    
    /**
     *
     * @var string
     */
    protected $dbms = "mysql";
    
    /**
     * 
     * @var QueryUtil
     */
    protected $queryUtil;
    
    /**
     * 
     * @param string $dbms
     */
    public function __construct($dbms){
        $this->dbms = $dbms;
        
        $this->initConnection();
    }
    
    /**
     * 
     * initiates the connection
     * 
     */
    protected function initConnection(){
        try{
            $this->queryUtil = $this->getQueryUtil($this->dbms);
        }catch(SQLException $ex){
            $this->error = $ex->getMessage();
        }
    }
    
    /**
     * 
     * changes the database connectivity
     * 
     * @param string $dbms
     */
    public function changeConnection(string $dbms){
        $this->dbms = $dbms;
        
        $this->initConnection();
    }
    
    /**
     * 
     * @return boolean
     */
    public function isConnected(){
        return !is_null($this->queryUtil);
    }
    
    /**
     * 
     * @param string $sql
     * @param array $args
     * 
     * @return boolean|mixed
     */
    public function rawQuery(string $sql, array $args = []){
        if(is_null($this->queryUtil)){
           return false; 
        }
        
        return $this->queryUtil->executeQuery($sql, $args);
    }
    
    /**
     * 
     * @param string $sql
     * @param array $args
     * 
     * @return boolean|mixed
     */
    public function rawModify(string $sql, array $args = []){
        if(is_null($this->queryUtil)){
            return false;
        }
        
        return $this->queryUtil->executeUpdate($sql, $args);
    }
}